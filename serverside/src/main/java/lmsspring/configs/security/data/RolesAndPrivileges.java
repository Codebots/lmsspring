/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.configs.security.data;

import lmsspring.configs.security.helpers.AnonymousHelper;
import lmsspring.entities.RoleEntity;
import lmsspring.repositories.RoleRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static lmsspring.configs.security.helpers.RoleAndPrivilegeHelper.createOrUpdatePrivilege;

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * Setup the Roles and Privileges for the application
 */
@Component
@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RolesAndPrivileges implements ApplicationRunner {

	private final RoleRepository roleRepository;

	@Autowired
	public RolesAndPrivileges(
			// % protected region % [Add any additional constructor parameters here] off begin
			// % protected region % [Add any additional constructor parameters here] end
			RoleRepository roleRepository
	) {
		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end

		this.roleRepository = roleRepository;

		// % protected region % [Add any additional constructor logic after the main body here] off begin
		// % protected region % [Add any additional constructor logic after the main body here] end
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		// % protected region % [Add any additional pre-setup logic here] off begin
		// % protected region % [Add any additional pre-setup logic here] end

		AnonymousHelper.runAnonymously(this::setup);

		// % protected region % [Add any additional post-setup logic here] off begin
		// % protected region % [Add any additional post-setup logic here] end
	}

	/**
	 * Create default roles and privileges to be associated with users. Note that this method does not take anonymous
	 * roles into account. Instead it is deferred to {@link lmsspring.configs.security.filters.AuthenticationFilter}.
	 */
	private void setup() {
		final String coreUserRoleName = "CORE_USER";
		Optional<RoleEntity> coreUserRoleOpt = roleRepository.findByName(coreUserRoleName);
		var coreUserRoleEntity = coreUserRoleOpt.orElseGet(() -> {
			var role = new RoleEntity();
			role.setName(coreUserRoleName);
			return roleRepository.save(role);
		});

		createOrUpdatePrivilege(coreUserRoleEntity, "CoreUserEntity",
		"ROLE_CORE_USER_CORE_USER_ENTITY_CORE_USER", false, true, false, false);
		createOrUpdatePrivilege(coreUserRoleEntity, "TagEntity",
		"ROLE_CORE_USER_TAG_ENTITY_TAG", false, true, false, false);
		createOrUpdatePrivilege(coreUserRoleEntity, "LessonEntity",
		"ROLE_CORE_USER_LESSON_ENTITY_LESSON", false, true, false, false);
		createOrUpdatePrivilege(coreUserRoleEntity, "LessonFormVersionEntity",
		"ROLE_CORE_USER_LESSON_ENTITY_LESSON_FORM_VERSION", false, true, false, false);
		createOrUpdatePrivilege(coreUserRoleEntity, "LessonFormTileEntity",
		"ROLE_CORE_USER_LESSON_ENTITY_LESSON_FORM_TILE", false, true, false, false);
		createOrUpdatePrivilege(coreUserRoleEntity, "CourseLessonEntity",
		"ROLE_CORE_USER_COURSE_LESSON_ENTITY_COURSE_LESSON", false, true, false, false);
		createOrUpdatePrivilege(coreUserRoleEntity, "CourseCategoryEntity",
		"ROLE_CORE_USER_COURSE_CATEGORY_ENTITY_COURSE_CATEGORY", false, true, false, false);
		createOrUpdatePrivilege(coreUserRoleEntity, "CourseEntity",
		"ROLE_CORE_USER_COURSE_ENTITY_COURSE", false, true, false, false);
		createOrUpdatePrivilege(coreUserRoleEntity, "BookEntity",
		"ROLE_CORE_USER_BOOK_ENTITY_BOOK", false, true, false, false);
		createOrUpdatePrivilege(coreUserRoleEntity, "ArticleEntity",
		"ROLE_CORE_USER_ARTICLE_ENTITY_ARTICLE", false, true, false, false);
		createOrUpdatePrivilege(coreUserRoleEntity, "LessonFormSubmissionEntity",
		"ROLE_CORE_USER_LESSON_FORM_SUBMISSION_LESSON_FORM_SUBMISSION", false, true, false, false);
		createOrUpdatePrivilege(coreUserRoleEntity, "WorkflowEntity",
		"ROLE_CORE_USER_WORKFLOW_BEHAVIOUR_WORKFLOW", false, true, false, false);
		createOrUpdatePrivilege(coreUserRoleEntity, "WorkflowStateEntity",
		"ROLE_CORE_USER_WORKFLOW_BEHAVIOUR_WORKFLOW_STATE", false, true, false, false);
		createOrUpdatePrivilege(coreUserRoleEntity, "WorkflowTransitionEntity",
		"ROLE_CORE_USER_WORKFLOW_BEHAVIOUR_WORKFLOW_TRANSITION", false, true, false, false);
		createOrUpdatePrivilege(coreUserRoleEntity, "WorkflowVersionEntity",
		"ROLE_CORE_USER_WORKFLOW_BEHAVIOUR_WORKFLOW_VERSION", false, true, false, false);
		roleRepository.save(coreUserRoleEntity);

		final String administratorRoleName = "ADMINISTRATOR";
		Optional<RoleEntity> administratorRoleOpt = roleRepository.findByName(administratorRoleName);
		var administratorRoleEntity = administratorRoleOpt.orElseGet(() -> {
			var role = new RoleEntity();
			role.setName(administratorRoleName);
			return roleRepository.save(role);
		});

		createOrUpdatePrivilege(administratorRoleEntity, "CoreUserEntity",
		"ROLE_ADMINISTRATOR_CORE_USER_ENTITY_CORE_USER", true, true, true, true);
		createOrUpdatePrivilege(administratorRoleEntity, "TagEntity",
		"ROLE_ADMINISTRATOR_TAG_ENTITY_TAG", true, true, true, true);
		createOrUpdatePrivilege(administratorRoleEntity, "LessonEntity",
		"ROLE_ADMINISTRATOR_LESSON_ENTITY_LESSON", true, true, true, true);
		createOrUpdatePrivilege(administratorRoleEntity, "LessonFormVersionEntity",
		"ROLE_ADMINISTRATOR_LESSON_ENTITY_LESSON_FORM_VERSION", true, true, true, true);
		createOrUpdatePrivilege(administratorRoleEntity, "LessonFormTileEntity",
		"ROLE_ADMINISTRATOR_LESSON_ENTITY_LESSON_FORM_TILE", true, true, true, true);
		createOrUpdatePrivilege(administratorRoleEntity, "CourseLessonEntity",
		"ROLE_ADMINISTRATOR_COURSE_LESSON_ENTITY_COURSE_LESSON", true, true, true, true);
		createOrUpdatePrivilege(administratorRoleEntity, "CourseCategoryEntity",
		"ROLE_ADMINISTRATOR_COURSE_CATEGORY_ENTITY_COURSE_CATEGORY", true, true, true, true);
		createOrUpdatePrivilege(administratorRoleEntity, "CourseEntity",
		"ROLE_ADMINISTRATOR_COURSE_ENTITY_COURSE", true, true, true, true);
		createOrUpdatePrivilege(administratorRoleEntity, "BookEntity",
		"ROLE_ADMINISTRATOR_BOOK_ENTITY_BOOK", true, true, true, true);
		createOrUpdatePrivilege(administratorRoleEntity, "ArticleEntity",
		"ROLE_ADMINISTRATOR_ARTICLE_ENTITY_ARTICLE", true, true, true, true);
		createOrUpdatePrivilege(administratorRoleEntity, "AdministratorEntity",
		"ROLE_ADMINISTRATOR_ADMINISTRATOR_ENTITY_ADMINISTRATOR", true, true, true, true);
		createOrUpdatePrivilege(administratorRoleEntity, "ApplicationLocaleEntity",
		"ROLE_ADMINISTRATOR_APPLICATION_LOCALE_ENTITY_APPLICATION_LOCALE", true, true, true, true);
		createOrUpdatePrivilege(administratorRoleEntity, "LessonFormSubmissionEntity",
		"ROLE_ADMINISTRATOR_LESSON_FORM_SUBMISSION_LESSON_FORM_SUBMISSION", true, true, true, true);
		createOrUpdatePrivilege(administratorRoleEntity, "RoleEntity",
		"ROLE_ADMINISTRATOR_USER_BEHAVIOUR_ROLE", false, true, false, false);
		createOrUpdatePrivilege(administratorRoleEntity, "PrivilegeEntity",
		"ROLE_ADMINISTRATOR_USER_BEHAVIOUR_PRIVILEGE", false, true, false, false);
		createOrUpdatePrivilege(administratorRoleEntity, "WorkflowEntity",
		"ROLE_ADMINISTRATOR_WORKFLOW_BEHAVIOUR_WORKFLOW", true, true, true, true);
		createOrUpdatePrivilege(administratorRoleEntity, "WorkflowStateEntity",
		"ROLE_ADMINISTRATOR_WORKFLOW_BEHAVIOUR_WORKFLOW_STATE", true, true, true, true);
		createOrUpdatePrivilege(administratorRoleEntity, "WorkflowTransitionEntity",
		"ROLE_ADMINISTRATOR_WORKFLOW_BEHAVIOUR_WORKFLOW_TRANSITION", true, true, true, true);
		createOrUpdatePrivilege(administratorRoleEntity, "WorkflowVersionEntity",
		"ROLE_ADMINISTRATOR_WORKFLOW_BEHAVIOUR_WORKFLOW_VERSION", true, true, true, true);
		roleRepository.save(administratorRoleEntity);

		// % protected region % [Add additional custom logic for the role setup] off begin
		// % protected region % [Add additional custom logic for the role setup] end
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
