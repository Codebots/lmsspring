/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package lmsspring.entities;

import lmsspring.dtos.LessonFormTileEntityDto;
import lmsspring.entities.listeners.LessonFormTileEntityListener;
import lmsspring.serializers.LessonFormTileSerializer;
import lmsspring.lib.validators.ValidatorPatterns;
import lmsspring.services.utils.converters.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.opencsv.bean.CsvIgnore;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;
import org.springframework.lang.Nullable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Optional;
import java.util.UUID;

// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Entity
// % protected region % [Override the auditing annotation or add additional annotations here] off begin
@Audited
// % protected region % [Override the auditing annotation or add additional annotations here] end
@ExcludeSuperclassListeners
@EntityListeners({LessonFormTileEntityListener.class, AuditingEntityListener.class})
@JsonSerialize(using = LessonFormTileSerializer.class)
@Table(
	uniqueConstraints = {
		@UniqueConstraint(columnNames = {"form_tile_name"}, name = "form_tile_name"),
	}
)
public class LessonFormTileEntity extends AbstractEntity {

	/**
	 * Takes a LessonFormTileEntityDto and converts it into a LessonFormTileEntity.
	 * Primarily used when endpoints are invoked, as they take DTOs as input, which need to be converted to the entity type
	 *
	 * @param lessonFormTileEntityDto
	 */
	public LessonFormTileEntity(LessonFormTileEntityDto lessonFormTileEntityDto) {
		// Need to account for potential that any field is empty.  This will be run before any server validation and
		// endpoints will most likely not be subject to clientside validation

		if (lessonFormTileEntityDto.getId() != null) {
			this.setId(lessonFormTileEntityDto.getId());
		}

		if (lessonFormTileEntityDto.getFormTileName() != null) {
			this.setFormTileName(lessonFormTileEntityDto.getFormTileName());
		}

		if (lessonFormTileEntityDto.getForm() != null) {
			this.setForm(lessonFormTileEntityDto.getForm());
		}

		// % protected region % [Add any additional DTO constructor logic here] off begin
		// % protected region % [Add any additional DTO constructor logic here] end
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Form Tile Name here] off begin
	@CsvBindByName(column = "FORM_TILE_NAME", required = false)
	@Nullable
	@Column(name = "form_tile_name")
	@ApiModelProperty(notes = "The Form Tile Name of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Form Tile Name here] end
	private String formTileName;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Update the annotation for Form here] off begin
	@ApiModelProperty(notes = "The Form entities that are related to this entity.")
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@NotNull(message = "Reference Form is required")
	@CsvIgnore
	// % protected region % [Update the annotation for Form here] end
	private LessonEntity form;

	// % protected region % [Update the annotation for formId here] off begin
	@Transient
	@CsvCustomBindByName(column = "FORM_ID", converter = UUIDConverter.class)
	// % protected region % [Update the annotation for formId here] end
	private UUID formId;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Similar to {@link this#setForm(LessonEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonEntity to be set to this entity
	 */
	public void setForm(@NotNull LessonEntity entity) {
		setForm(entity, true);
	}

	/**
	 * Set or update the form in this entity with single LessonEntity.
	 *
	 * @param entity the given LessonEntity to be set or updated to form
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setForm(@NotNull LessonEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setForm here] off begin
		// % protected region % [Add any additional logic here before the main logic for setForm here] end

		if (sameAsFormer(this.form, entity)) {
			return;
		}

		if (this.form != null) {
			this.form.removeFormTiles(this, false);
		}
		this.form = entity;
		if (reverseAdd) {
			this.form.addFormTiles(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setLesson here] off begin
		// % protected region % [Add any additional logic here after the main logic for setLesson here] end
	}

	/**
	 * Similar to {@link this#unsetForm(boolean)} but default to true.
	 */
	public void unsetForm() {
		this.unsetForm(true);
	}

	/**
	 * Remove Form in this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetForm(boolean reverse) {
		if (reverse && this.form != null) {
			this.form.removeFormTiles(this, false);
		}
		this.form = null;
	}

	/**
	 * The CSV annotations used to generate and import CSV files require headers on the abstract entity attributes
	 * (id, created, modified, created by, modified by).  These should not be present in imports, as this information is
	 * added by the server when saving entities.  Therefore a string containing the example csv headers is required
	 *
	 * @return the headers recommended in the CSV import file in CSV format
	 */
	public static String getExampleCsvHeader() {

		// % protected region % [Modify the headers in the CSV file here] off begin
		return "FORM_TILE_NAME,FORM_ID,ID";
		// % protected region % [Modify the headers in the CSV file here] end
	}

	public void addRelationEntitiesToIdSet() {
		// % protected region % [Add any additional logic for entity relations here] off begin
		Optional<LessonEntity> formRelation = Optional.ofNullable(this.form);
		formRelation.ifPresent(entity -> this.formId = entity.getId());

		// % protected region % [Add any additional logic for entity relations here] end
	}
	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
