/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Action} from '@ngrx/store';
import {ApplicationLocaleModel} from './application_locale.model';
import {ApplicationLocaleModelAudit} from './application_locale.model.state';
import {PassableStateConfig} from '../../lib/services/http/interfaces';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * List of Application Locale model actions to be dispatched by NgRx.
 */
export enum ApplicationLocaleModelActionTypes {
	CREATE_APPLICATION_LOCALE = '[ENTITY] Create ApplicationLocaleModel',
	CREATE_APPLICATION_LOCALE_OK = '[ENTITY] Create ApplicationLocaleModel successfully',
	CREATE_APPLICATION_LOCALE_FAIL = '[ENTITY] Create ApplicationLocaleModel failed',

	CREATE_ALL_APPLICATION_LOCALE = '[ENTITY] Create All ApplicationLocaleModel',
	CREATE_ALL_APPLICATION_LOCALE_OK = '[ENTITY] Create All ApplicationLocaleModel successfully',
	CREATE_ALL_APPLICATION_LOCALE_FAIL = '[ENTITY] Create All ApplicationLocaleModel failed',

	DELETE_APPLICATION_LOCALE = '[ENTITY] Delete ApplicationLocaleModel',
	DELETE_APPLICATION_LOCALE_OK = '[ENTITY] Delete ApplicationLocaleModel successfully',
	DELETE_APPLICATION_LOCALE_FAIL = '[ENTITY] Delete ApplicationLocaleModel failed',


	DELETE_APPLICATION_LOCALE_EXCLUDING_IDS = '[ENTITY] Delete ApplicationLocaleModels Excluding Ids',
	DELETE_APPLICATION_LOCALE_EXCLUDING_IDS_OK = '[ENTITY] Delete ApplicationLocaleModels Excluding Ids successfully',
	DELETE_APPLICATION_LOCALE_EXCLUDING_IDS_FAIL = '[ENTITY] Delete ApplicationLocaleModels Excluding Ids failed',

	DELETE_ALL_APPLICATION_LOCALE = '[ENTITY] Delete all ApplicationLocaleModels',
	DELETE_ALL_APPLICATION_LOCALE_OK = '[ENTITY] Delete all ApplicationLocaleModels successfully',
	DELETE_ALL_APPLICATION_LOCALE_FAIL = '[ENTITY] Delete all ApplicationLocaleModels failed',

	UPDATE_APPLICATION_LOCALE = '[ENTITY] Update ApplicationLocaleModel',
	UPDATE_APPLICATION_LOCALE_OK = '[ENTITY] Update ApplicationLocaleModel successfully',
	UPDATE_APPLICATION_LOCALE_FAIL = '[ENTITY] Update ApplicationLocaleModel failed',

	UPDATE_ALL_APPLICATION_LOCALE = '[ENTITY] Update all ApplicationLocaleModel',
	UPDATE_ALL_APPLICATION_LOCALE_OK = '[ENTITY] Update all ApplicationLocaleModel successfully',
	UPDATE_ALL_APPLICATION_LOCALE_FAIL = '[ENTITY] Update all ApplicationLocaleModel failed',

	FETCH_APPLICATION_LOCALE= '[ENTITY] Fetch ApplicationLocaleModel',
	FETCH_APPLICATION_LOCALE_OK = '[ENTITY] Fetch ApplicationLocaleModel successfully',
	FETCH_APPLICATION_LOCALE_FAIL = '[ENTITY] Fetch ApplicationLocaleModel failed',

	FETCH_APPLICATION_LOCALE_AUDIT= '[ENTITY] Fetch ApplicationLocaleModel audit',
	FETCH_APPLICATION_LOCALE_AUDIT_OK = '[ENTITY] Fetch ApplicationLocaleModel audit successfully',
	FETCH_APPLICATION_LOCALE_AUDIT_FAIL = '[ENTITY] Fetch ApplicationLocaleModel audit failed',

	FETCH_APPLICATION_LOCALE_AUDITS_BY_ENTITY_ID= '[ENTITY] Fetch ApplicationLocaleModel audits by entity id',
	FETCH_APPLICATION_LOCALE_AUDITS_BY_ENTITY_ID_OK = '[ENTITY] Fetch ApplicationLocaleModel audits by entity id successfully',
	FETCH_APPLICATION_LOCALE_AUDITS_BY_ENTITY_ID_FAIL = '[ENTITY] Fetch ApplicationLocaleModel audits by entity id failed',

	FETCH_ALL_APPLICATION_LOCALE = '[ENTITY] Fetch all ApplicationLocaleModel',
	FETCH_ALL_APPLICATION_LOCALE_OK = '[ENTITY] Fetch all ApplicationLocaleModel successfully',
	FETCH_ALL_APPLICATION_LOCALE_FAIL = '[ENTITY] Fetch all ApplicationLocaleModel failed',

	FETCH_APPLICATION_LOCALE_WITH_QUERY = '[ENTITY] Fetch ApplicationLocaleModel with query',
	FETCH_APPLICATION_LOCALE_WITH_QUERY_OK = '[ENTITY] Fetch ApplicationLocaleModel with query successfully',
	FETCH_APPLICATION_LOCALE_WITH_QUERY_FAIL = '[ENTITY] Fetch ApplicationLocaleModel with query failed',

	FETCH_LAST_APPLICATION_LOCALE_WITH_QUERY = '[ENTITY] Fetch last ApplicationLocaleModel with query',
	FETCH_LAST_APPLICATION_LOCALE_WITH_QUERY_OK = '[ENTITY] Fetch last ApplicationLocaleModel with query successfully',
	FETCH_LAST_APPLICATION_LOCALE_WITH_QUERY_FAIL = '[ENTITY] Fetch last ApplicationLocaleModel with query failed',

	EXPORT_APPLICATION_LOCALE = '[ENTITY] Export ApplicationLocaleModel',
	EXPORT_APPLICATION_LOCALE_OK = '[ENTITY] Export ApplicationLocaleModel successfully',
	EXPORT_APPLICATION_LOCALE_FAIL = '[ENTITY] Export ApplicationLocaleModel failed',

	EXPORT_ALL_APPLICATION_LOCALE = '[ENTITY] Export All ApplicationLocaleModels',
	EXPORT_ALL_APPLICATION_LOCALE_OK = '[ENTITY] Export All ApplicationLocaleModels successfully',
	EXPORT_ALL_APPLICATION_LOCALE_FAIL = '[ENTITY] Export All ApplicationLocaleModels failed',

	EXPORT_APPLICATION_LOCALE_EXCLUDING_IDS = '[ENTITY] Export ApplicationLocaleModels excluding Ids',
	EXPORT_APPLICATION_LOCALE_EXCLUDING_IDS_OK = '[ENTITY] Export ApplicationLocaleModel excluding Ids successfully',
	EXPORT_APPLICATION_LOCALE_EXCLUDING_IDS_FAIL = '[ENTITY] Export ApplicationLocaleModel excluding Ids failed',

	COUNT_APPLICATION_LOCALES = '[ENTITY] Fetch number of ApplicationLocaleModel records',
	COUNT_APPLICATION_LOCALES_OK = '[ENTITY] Fetch number of ApplicationLocaleModel records successfully ',
	COUNT_APPLICATION_LOCALES_FAIL = '[ENTITY] Fetch number of ApplicationLocaleModel records failed',

	IMPORT_APPLICATION_LOCALES = '[ENTITY] Import ApplicationLocaleModels',
	IMPORT_APPLICATION_LOCALES_OK = '[ENTITY] Import ApplicationLocaleModels successfully',
	IMPORT_APPLICATION_LOCALES_FAIL = '[ENTITY] Import ApplicationLocaleModels fail',


	INITIALISE_APPLICATION_LOCALE_COLLECTION_STATE = '[ENTITY] Initialize the CollectionState of ApplicationLocaleModel',
	// % protected region % [Add any additional model actions here] off begin
	// % protected region % [Add any additional model actions here] end
}

export abstract class BaseApplicationLocaleAction implements Action {
	readonly className: string = 'ApplicationLocaleModel';

	abstract readonly type: string;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	protected constructor(
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
		public readonly afterwardActions: Action[] = []
	) {
		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}
}

export class ApplicationLocaleAction extends BaseApplicationLocaleAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for ApplicationLocaleAction here] off begin
	// % protected region % [Add any additional class fields for ApplicationLocaleAction here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<ApplicationLocaleModel>,
		// % protected region % [Add any additional constructor parameters for ApplicationLocaleAction here] off begin
		// % protected region % [Add any additional constructor parameters for ApplicationLocaleAction here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for ApplicationLocaleAction here] off begin
			// % protected region % [Add any additional constructor arguments for ApplicationLocaleAction here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for ApplicationLocaleAction here] off begin
		// % protected region % [Add any additional constructor logic for ApplicationLocaleAction here] end
	}

	// % protected region % [Add any additional class methods for ApplicationLocaleAction here] off begin
	// % protected region % [Add any additional class methods for ApplicationLocaleAction here] end
}

export class ApplicationLocaleActionOK extends BaseApplicationLocaleAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for ApplicationLocaleActionOK here] off begin
	// % protected region % [Add any additional class fields for ApplicationLocaleActionOK here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<ApplicationLocaleModel>,
		// % protected region % [Add any additional constructor parameters for ApplicationLocaleActionOK here] off begin
		// % protected region % [Add any additional constructor parameters for ApplicationLocaleActionOK here] end
		afterwardActions: Action[] = [],
		public audits?: ApplicationLocaleModelAudit[],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for ApplicationLocaleActionOK here] off begin
			// % protected region % [Add any additional constructor arguments for ApplicationLocaleActionOK here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for ApplicationLocaleActionOK here] off begin
		// % protected region % [Add any additional constructor logic for ApplicationLocaleActionOK here] end
	}

	// % protected region % [Add any additional class methods for ApplicationLocaleActionOK here] off begin
	// % protected region % [Add any additional class methods for ApplicationLocaleActionOK here] end
}

export class ApplicationLocaleActionFail extends BaseApplicationLocaleAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for ApplicationLocaleActionFail here] off begin
	// % protected region % [Add any additional class fields for ApplicationLocaleActionFail here] end

	public constructor(
		public typeInput: string,
		public readonly error?: any,
		public stateConfig?: PassableStateConfig<ApplicationLocaleModel>,
		// % protected region % [Add any additional constructor parameters for ApplicationLocaleActionFail here] off begin
		// % protected region % [Add any additional constructor parameters for ApplicationLocaleActionFail here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for ApplicationLocaleActionFail here] off begin
			// % protected region % [Add any additional constructor arguments for ApplicationLocaleActionFail here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for ApplicationLocaleActionFail here] off begin
		// % protected region % [Add any additional constructor logic for ApplicationLocaleActionFail here] end
	}

	// % protected region % [Add any additional class methods for ApplicationLocaleActionFail here] off begin
	// % protected region % [Add any additional class methods for ApplicationLocaleActionFail here] end
}

export function isApplicationLocaleModelAction(e: any): e is BaseApplicationLocaleAction {
	return Object.values(ApplicationLocaleModelActionTypes).includes(e);
}

// % protected region % [Add any additional actions here] off begin
// % protected region % [Add any additional actions here] end
