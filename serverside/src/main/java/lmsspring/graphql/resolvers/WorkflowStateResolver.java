/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package lmsspring.graphql.resolvers;

import lmsspring.entities.*;
import com.coxautodev.graphql.tools.GraphQLResolver;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import java.util.*;

// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Component
public class WorkflowStateResolver implements GraphQLResolver<WorkflowStateEntity> {

	// % protected region % [Import any additional class fields here] off begin
	// % protected region % [Import any additional class fields here] end

	@PreAuthorize("hasPermission('WorkflowTransitionEntity', 'read')")
	public Set<WorkflowTransitionEntity> outgoingTransitions(WorkflowStateEntity workflowState) {
		return workflowState.getOutgoingTransitions();
	}

	@PreAuthorize("hasPermission('WorkflowTransitionEntity', 'read')")
	public Set<WorkflowTransitionEntity> incomingTransitions(WorkflowStateEntity workflowState) {
		return workflowState.getIncomingTransitions();
	}

	@PreAuthorize("hasPermission('WorkflowVersionEntity', 'read')")
	public WorkflowVersionEntity workflowVersion(WorkflowStateEntity workflowState) {
		return workflowState.getWorkflowVersion();
	}

	@PreAuthorize("hasPermission('ArticleEntity', 'read')")
	public Set<ArticleEntity> article(WorkflowStateEntity workflowState) {
		return workflowState.getArticle();
	}


	// % protected region % [Import any additional class methods here] off begin
	// % protected region % [Import any additional class methods here] end
}
