/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package lmsspring.entities;

import lmsspring.dtos.LessonFormVersionEntityDto;
import lmsspring.entities.listeners.LessonFormVersionEntityListener;
import lmsspring.serializers.LessonFormVersionSerializer;
import lmsspring.lib.validators.ValidatorPatterns;
import lmsspring.services.utils.converters.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.opencsv.bean.CsvIgnore;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;
import org.springframework.lang.Nullable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.hibernate.envers.Audited;

import java.util.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Optional;
import java.util.UUID;

// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Entity
// % protected region % [Override the auditing annotation or add additional annotations here] off begin
@Audited
// % protected region % [Override the auditing annotation or add additional annotations here] end
@ExcludeSuperclassListeners
@EntityListeners({LessonFormVersionEntityListener.class, AuditingEntityListener.class})
@JsonSerialize(using = LessonFormVersionSerializer.class)
@Table(
	uniqueConstraints = {
	}
)
public class LessonFormVersionEntity extends AbstractEntity {

	/**
	 * Takes a LessonFormVersionEntityDto and converts it into a LessonFormVersionEntity.
	 * Primarily used when endpoints are invoked, as they take DTOs as input, which need to be converted to the entity type
	 *
	 * @param lessonFormVersionEntityDto
	 */
	public LessonFormVersionEntity(LessonFormVersionEntityDto lessonFormVersionEntityDto) {
		// Need to account for potential that any field is empty.  This will be run before any server validation and
		// endpoints will most likely not be subject to clientside validation

		if (lessonFormVersionEntityDto.getId() != null) {
			this.setId(lessonFormVersionEntityDto.getId());
		}

		if (lessonFormVersionEntityDto.getVersion() != null) {
			this.setVersion(lessonFormVersionEntityDto.getVersion());
		}

		if (lessonFormVersionEntityDto.getFormData() != null) {
			this.setFormData(lessonFormVersionEntityDto.getFormData());
		}

		if (lessonFormVersionEntityDto.getSubmission() != null) {
			this.setSubmission(lessonFormVersionEntityDto.getSubmission());
		}

		if (lessonFormVersionEntityDto.getForm() != null) {
			this.setForm(lessonFormVersionEntityDto.getForm());
		}

		if (lessonFormVersionEntityDto.getPublishedForm() != null) {
			this.setPublishedForm(lessonFormVersionEntityDto.getPublishedForm());
		}

		// % protected region % [Add any additional DTO constructor logic here] off begin
		// % protected region % [Add any additional DTO constructor logic here] end
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Version here] off begin
	@CsvBindByName(column = "VERSION", required = true)
	@NotNull(message = "Version must not be empty")
	@Column(name = "version")
	@ApiModelProperty(notes = "The Version of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Version here] end
	private Integer version;

	// % protected region % [Modify attribute annotation for Form Data here] off begin
	@CsvBindByName(column = "FORM_DATA", required = true)
	@NotNull(message = "Form Data must not be empty")
	@Lob
	@Column(name = "form_data")
	@ApiModelProperty(notes = "The Form Data of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Form Data here] end
	private String formData;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Update the annotation for Submission here] off begin
	@ApiModelProperty(notes = "The Lesson Form Submission entities that are related to this entity.")
	@OneToMany(mappedBy = "submittedForm", cascade = {CascadeType.MERGE}, fetch = FetchType.LAZY)
	@CsvIgnore
	// % protected region % [Update the annotation for Submission here] end
	private Set<LessonFormSubmissionEntity> submission = new HashSet<>();

	// % protected region % [Update the annotation for submissionIds here] off begin
	@Transient
	@CsvCustomBindByName(column = "SUBMISSION_IDS", converter = UUIDHashSetConverter.class)
	// % protected region % [Update the annotation for submissionIds here] end
	private Set<UUID> submissionIds = new HashSet<>();

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Update the annotation for Form here] off begin
	@ApiModelProperty(notes = "The Form entities that are related to this entity.")
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@CsvIgnore
	// % protected region % [Update the annotation for Form here] end
	private LessonEntity form;

	// % protected region % [Update the annotation for formId here] off begin
	@Transient
	@CsvCustomBindByName(column = "FORM_ID", converter = UUIDConverter.class)
	// % protected region % [Update the annotation for formId here] end
	private UUID formId;

	// % protected region % [Update the annotation for Published form here] off begin
	@ApiModelProperty(notes = "The Published form entities that are related to this entity.")
	@OneToOne(cascade = {}, fetch = FetchType.LAZY)
	@CsvIgnore
	// % protected region % [Update the annotation for Published form here] end
	private LessonEntity publishedForm;

	// % protected region % [Update the annotation for publishedFormId here] off begin
	@Transient
	@CsvCustomBindByName(column = "PUBLISHED_FORM_ID", converter = UUIDConverter.class)
	// % protected region % [Update the annotation for publishedFormId here] end
	private UUID publishedFormId;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
	 * Similar to {@link this#addSubmission(LessonFormSubmissionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonFormSubmissionEntity to be added to this entity
	 */
	public void addSubmission(@NotNull LessonFormSubmissionEntity entity) {
		addSubmission(entity, true);
	}

	/**
	 * Add a new LessonFormSubmissionEntity to submission in this entity.
	 *
	 * @param entity the given LessonFormSubmissionEntity  to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addSubmission(@NonNull LessonFormSubmissionEntity entity, boolean reverseAdd) {
		if (!this.submission.contains(entity)) {
			submission.add(entity);
			if (reverseAdd) {
				entity.setSubmittedForm(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addSubmission(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of LessonFormSubmissionEntity to be added to this entity
	 */
	public void addSubmission(@NotNull Collection<LessonFormSubmissionEntity> entities) {
		addSubmission(entities, true);
	}

	/**
	 * Add a new collection of LessonFormSubmissionEntity to Submission in this entity.
	 *
	 * @param entities the given collection of LessonFormSubmissionEntity to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addSubmission(@NonNull Collection<LessonFormSubmissionEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> addSubmission(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeSubmission(LessonFormSubmissionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonFormSubmissionEntity to be set to this entity
	 */
	public void removeSubmission(@NotNull LessonFormSubmissionEntity entity) {
		this.removeSubmission(entity, true);
	}

	/**
	 * Remove the given LessonFormSubmissionEntity from this entity.
	 *
	 * @param entity the given LessonFormSubmissionEntity to be removed from this entity
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeSubmission(@NotNull LessonFormSubmissionEntity entity, boolean reverse) {
		if (reverse) {
			entity.unsetSubmittedForm(false);
		}
		this.submission.remove(entity);
	}

	/**
	 * Similar to {@link this#removeSubmission(Collection, boolean)} but
	 * default to true for reverse remove.
	 *
	 * @param entities the given collection of LessonFormSubmissionEntity to be removed to this entity
	 */
	public void removeSubmission(@NotNull Collection<LessonFormSubmissionEntity> entities) {
		this.removeSubmission(entities, true);
	}

	/**
	 * Remove the given collection of LessonFormSubmissionEntity from  to this entity.
	 *
	 * @param entities the given collection of LessonFormSubmissionEntity to be removed to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void removeSubmission(@NonNull Collection<LessonFormSubmissionEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.removeSubmission(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#setSubmission(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of LessonFormSubmissionEntity to be set to this entity
	 */
	public void setSubmission(@NotNull Collection<LessonFormSubmissionEntity> entities) {
		setSubmission(entities, true);
	}

	/**
	 * Replace the current entities in Submission with the given ones.
	 *
	 * @param entities the given collection of LessonFormSubmissionEntity to replace the old ones
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setSubmission(@NotNull Collection<LessonFormSubmissionEntity> entities, boolean reverseAdd) {

		this.unsetSubmission();
		this.submission = new HashSet<>(entities);
		if (reverseAdd) {
			this.submission.forEach(submissionEntity -> submissionEntity.setSubmittedForm(this, false));
		}
	}

	/**
	 * Similar to {@link this#unsetSubmission(boolean)} but
	 * default to true for reverse unset
	 */
	public void unsetSubmission() {
		this.unsetSubmission(true);
	}

	/**
	 * Remove all the entities in Submission from this entity.
	 * @param doReverse whether this entity should be removed from the given entities
	 */
	public void unsetSubmission(boolean doReverse) {
		if (doReverse) {
			this.submission.forEach(submissionEntity -> submissionEntity.unsetSubmittedForm(false));
		}
		this.submission.clear();
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Similar to {@link this#setForm(LessonEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonEntity to be set to this entity
	 */
	public void setForm(@NotNull LessonEntity entity) {
		setForm(entity, true);
	}

	/**
	 * Set or update the form in this entity with single LessonEntity.
	 *
	 * @param entity the given LessonEntity to be set or updated to form
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setForm(@NotNull LessonEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setForm here] off begin
		// % protected region % [Add any additional logic here before the main logic for setForm here] end

		if (sameAsFormer(this.form, entity)) {
			return;
		}

		if (this.form != null) {
			this.form.removeVersions(this, false);
		}
		this.form = entity;
		if (reverseAdd) {
			this.form.addVersions(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setLesson here] off begin
		// % protected region % [Add any additional logic here after the main logic for setLesson here] end
	}

	/**
	 * Similar to {@link this#unsetForm(boolean)} but default to true.
	 */
	public void unsetForm() {
		this.unsetForm(true);
	}

	/**
	 * Remove Form in this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetForm(boolean reverse) {
		if (reverse && this.form != null) {
			this.form.removeVersions(this, false);
		}
		this.form = null;
	}
	/**
	 * Similar to {@link this#setPublishedForm(LessonEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonEntity to be set to this entity
	 */
	public void setPublishedForm(LessonEntity entity) {
		setPublishedForm(entity, true);
	}

	/**
	 * Set or update the publishedForm in this entity with single LessonEntity.
	 *
	 * @param entity the given LessonEntity to be set or updated to publishedForm
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setPublishedForm(LessonEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setPublishedForm here] off begin
		// % protected region % [Add any additional logic here before the main logic for setPublishedForm here] end

		if (sameAsFormer(this.publishedForm, entity)) {
			return;
		}

		if (this.publishedForm != null) {
			this.publishedForm.unsetPublishedVersion(false);
		}

		this.publishedForm = entity;
		if (reverseAdd) {
			this.publishedForm.setPublishedVersion(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setLesson here] off begin
		// % protected region % [Add any additional logic here after the main logic for setLesson here] end
	}

	/**
	 * Similar to {@link this#unsetPublishedForm(boolean)} but default to true.
	 */
	public void unsetPublishedForm() {
		this.unsetPublishedForm(true);
	}

	/**
	 * Remove the LessonEntity of publishedForm from this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetPublishedForm(boolean reverse) {
		if (reverse && this.publishedForm != null) {
			this.publishedForm.unsetPublishedVersion();
		}
		this.publishedForm = null;
	}

	/**
	 * The CSV annotations used to generate and import CSV files require headers on the abstract entity attributes
	 * (id, created, modified, created by, modified by).  These should not be present in imports, as this information is
	 * added by the server when saving entities.  Therefore a string containing the example csv headers is required
	 *
	 * @return the headers recommended in the CSV import file in CSV format
	 */
	public static String getExampleCsvHeader() {

		// % protected region % [Modify the headers in the CSV file here] off begin
		return "VERSION,FORM_DATA,FORM_ID,PUBLISHED_FORM_ID,SUBMISSION_IDS,ID";
		// % protected region % [Modify the headers in the CSV file here] end
	}

	public void addRelationEntitiesToIdSet() {
		// % protected region % [Add any additional logic for entity relations here] off begin
		Optional<LessonEntity> formRelation = Optional.ofNullable(this.form);
		formRelation.ifPresent(entity -> this.formId = entity.getId());

		Optional<LessonEntity> publishedFormRelation = Optional.ofNullable(this.publishedForm);
		publishedFormRelation.ifPresent(entity -> this.publishedFormId = entity.getId());

		for (LessonFormSubmissionEntity submission: this.submission) {
			this.submissionIds.add(submission.getId());
		}

		// % protected region % [Add any additional logic for entity relations here] end
	}
	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
