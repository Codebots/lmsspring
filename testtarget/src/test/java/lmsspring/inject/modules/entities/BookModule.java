/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.inject.modules.entities;

import lmsspring.entities.*;
import lmsspring.entities.enums.*;
import lmsspring.inject.factories.*;
import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.andreinc.mockneat.MockNeat;
import net.andreinc.mockneat.types.enums.StringType;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.UUID;

// % protected region % [Apply any additional imports here] off begin
// % protected region % [Apply any additional imports here] end

/**
 * Guice module for Book used to register providers for dependency injection.
 */
@Slf4j
@AllArgsConstructor
public class BookModule extends AbstractModule {
	// % protected region % [Apply any additional class fields here] off begin
	// % protected region % [Apply any additional class fields here] end

	@Override
	protected void configure() {
		log.trace("Configuring BookModule");

		super.configure();

		// % protected region % [Apply any additional configure steps here] off begin
		// % protected region % [Apply any additional configure steps here] end

		log.trace("Configured BookModule");
	}

	/**
	 * Return a factory for mass data generation.
	 */
	@Provides
	public BookEntityFactory bookEntityFactory(
			// % protected region % [Apply any additional injected arguments for bookEntityFactory here] off begin
			// % protected region % [Apply any additional injected arguments for bookEntityFactory here] end
			Injector injector
	) {
		log.trace("Creating BookEntityFactory");

		// % protected region % [Apply any additional logic for bookEntityFactory before the main body here] off begin
		// % protected region % [Apply any additional logic for bookEntityFactory before the main body here] end

		BookEntityFactory entityFactory = new BookEntityFactory(
				// % protected region % [Apply any additional constructor arguments for BookEntityFactory here] off begin
				// % protected region % [Apply any additional constructor arguments for BookEntityFactory here] end
				injector
		);

		// % protected region % [Apply any additional logic for bookEntityFactory after the main body here] off begin
		// % protected region % [Apply any additional logic for bookEntityFactory after the main body here] end

		log.trace("Created BookEntityFactory");

		return entityFactory;
	}

	/**
	 * Return an empty Book entity with no references set.
	 */
	@Provides
	@Named("bookEntityWithNoRef")
	public BookEntity bookEntityWithNoRef(
			// % protected region % [Apply any additional constructor parameters for bookEntityWithNoRef here] off begin
			// % protected region % [Apply any additional constructor parameters for bookEntityWithNoRef here] end
			MockNeat mock
	) {
		log.trace("Creating entity of type BookEntity with no reference");

		// % protected region % [Apply any additional logic for bookWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for bookWithNoRef before the main body here] end

		BookEntity newEntity = new BookEntity();
		newEntity.setId(UUID.randomUUID());
		newEntity.setCreated(OffsetDateTime.now());
		newEntity.setModified(OffsetDateTime.now());
		// % protected region % [Add customisation for Name here] off begin
		String randomStringforName = mock
				.strings()
				.get();
		newEntity.setName(randomStringforName);
		// % protected region % [Add customisation for Name here] end
		// % protected region % [Add customisation for Summary here] off begin
		String randomStringforSummary = mock
				.strings()
				.get();
		newEntity.setSummary(randomStringforSummary);
		// % protected region % [Add customisation for Summary here] end

		// % protected region % [Apply any additional logic for bookWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for bookWithNoRef after the main body here] end

		log.trace("Created entity of type BookEntity with no reference");

		return newEntity;
	}

	/**
	 * Return a collection of Book entities with no reference at all.
	 */
	@Provides
	@Named("bookEntitiesWithNoRef")
	public Collection<BookEntity> bookEntitiesWithNoRef(
		// % protected region % [Apply any additional constructor parameters for bookEntitiesWithNoRef here] off begin
		// % protected region % [Apply any additional constructor parameters for bookEntitiesWithNoRef here] end
		BookEntityFactory bookEntityFactory
	) {
		log.trace("Creating entities of type BookEntity with no reference");

		// % protected region % [Apply any additional logic for bookEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for bookEntitiesWithNoRef before the main body here] end

		Collection<BookEntity> newEntities = bookEntityFactory.createMultipleWithNoRef(10);

		// % protected region % [Apply any additional logic for bookEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for bookEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type BookEntity with no reference");

		return newEntities;
	}

	/**
	 * Return a Book entity with all references set.
	 */
	@Provides
	@Named("bookEntityWithRefs")
	public BookEntity bookEntityWithRefs(
			// % protected region % [Apply any additional constructor parameters for bookEntityWithRefs here] off begin
			// % protected region % [Apply any additional constructor parameters for bookEntityWithRefs here] end
			Injector injector
	) {
		log.trace("Creating entity of type BookEntity with references");

		// % protected region % [Apply any additional logic for bookEntityWithRefs before the main body here] off begin
		// % protected region % [Apply any additional logic for bookEntityWithRefs before the main body here] end

		BookEntity bookEntity = injector.getInstance(Key.get(BookEntity.class, Names.named("bookEntityWithNoRef")));

		// % protected region % [Apply any additional logic for bookEntityWithRefs after the main body here] off begin
		// % protected region % [Apply any additional logic for bookEntityWithRefs after the main body here] end

		log.trace("Created entity of type BookEntity with references");

		return bookEntity;
	}

	/**
	 * Return a collection of Book entities with all references set.
	 */
	@Provides
	@Named("bookEntitiesWithRefs")
	public Collection<BookEntity> bookEntitiesWithRefs(
		// % protected region % [Apply any additional constructor parameters for bookEntitiesWithRefs here] off begin
		// % protected region % [Apply any additional constructor parameters for bookEntitiesWithRefs here] end
		BookEntityFactory bookEntityFactory
	) {
		log.trace("Creating entities of type BookEntity with references");

		// % protected region % [Apply any additional logic for bookEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for bookEntitiesWithNoRef before the main body here] end

		Collection<BookEntity> newEntities = bookEntityFactory.createMultipleWithRefs(10);

		// % protected region % [Apply any additional logic for bookEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for bookEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type BookEntity with references");

		return newEntities;
	}

	// % protected region % [Apply any additional class methods here] off begin
	// % protected region % [Apply any additional class methods here] end
}
