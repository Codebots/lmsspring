/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package lmsspring.entities;

import lmsspring.dtos.LessonEntityDto;
import lmsspring.entities.enums.*;
import lmsspring.services.utils.converters.enums.*;
import lmsspring.entities.listeners.LessonEntityListener;
import lmsspring.serializers.LessonSerializer;
import lmsspring.lib.validators.ValidatorPatterns;
import lmsspring.services.utils.converters.*;
import lmsspring.lib.file.models.FileEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.opencsv.bean.CsvIgnore;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;
import org.springframework.lang.Nullable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.hibernate.envers.Audited;

import java.util.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Optional;
import java.util.UUID;

// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Entity
// % protected region % [Override the auditing annotation or add additional annotations here] off begin
@Audited
// % protected region % [Override the auditing annotation or add additional annotations here] end
@ExcludeSuperclassListeners
@EntityListeners({LessonEntityListener.class, AuditingEntityListener.class})
@JsonSerialize(using = LessonSerializer.class)
@Table(
	uniqueConstraints = {
		@UniqueConstraint(columnNames = {"summary"}, name = "summary"),
	}
)
public class LessonEntity extends AbstractEntity {

	/**
	 * Takes a LessonEntityDto and converts it into a LessonEntity.
	 * Primarily used when endpoints are invoked, as they take DTOs as input, which need to be converted to the entity type
	 *
	 * @param lessonEntityDto
	 */
	public LessonEntity(LessonEntityDto lessonEntityDto) {
		// Need to account for potential that any field is empty.  This will be run before any server validation and
		// endpoints will most likely not be subject to clientside validation

		if (lessonEntityDto.getId() != null) {
			this.setId(lessonEntityDto.getId());
		}

		if (lessonEntityDto.getSummary() != null) {
			this.setSummary(lessonEntityDto.getSummary());
		}

		if (lessonEntityDto.getDescription() != null) {
			this.setDescription(lessonEntityDto.getDescription());
		}

		if (lessonEntityDto.getDuration() != null) {
			this.setDuration(lessonEntityDto.getDuration());
		}

		if (lessonEntityDto.getDifficulty() != null) {
			this.setDifficulty(lessonEntityDto.getDifficulty());
		}

		if (lessonEntityDto.getName() != null) {
			this.setName(lessonEntityDto.getName());
		}

		if (lessonEntityDto.getCourseLessons() != null) {
			this.setCourseLessons(lessonEntityDto.getCourseLessons());
		}

		if (lessonEntityDto.getVersions() != null) {
			this.setVersions(lessonEntityDto.getVersions());
		}

		if (lessonEntityDto.getFormTiles() != null) {
			this.setFormTiles(lessonEntityDto.getFormTiles());
		}

		if (lessonEntityDto.getPublishedVersion() != null) {
			this.setPublishedVersion(lessonEntityDto.getPublishedVersion());
		}

		// % protected region % [Add any additional DTO constructor logic here] off begin
		// % protected region % [Add any additional DTO constructor logic here] end
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Summary here] off begin
	@CsvBindByName(column = "SUMMARY", required = true)
	@NotNull(message = "Summary must not be empty")
	@Column(name = "summary")
	@ApiModelProperty(notes = "The Summary of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Summary here] end
	private String summary;

	// % protected region % [Modify attribute annotation for Description here] off begin
	@CsvBindByName(column = "DESCRIPTION", required = false)
	@Nullable
	@Lob
	@Column(name = "description")
	@ApiModelProperty(notes = "The Description of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Description here] end
	private String description;

	// % protected region % [Modify attribute annotation for Duration here] off begin
	@CsvBindByName(column = "DURATION", required = false)
	@Nullable
	@Column(name = "duration")
	@ApiModelProperty(notes = "The Duration of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Duration here] end
	private Integer duration;

	// % protected region % [Modify attribute annotation for Difficulty here] off begin
	@CsvCustomBindByName(column = "DIFFICULTY", required = false, converter = DifficultyEnumConverter.class)
	@Nullable
	@Column(name = "difficulty")
	@ApiModelProperty(notes = "The Difficulty of this entity.")
	@ToString.Include
	@Enumerated
	// % protected region % [Modify attribute annotation for Difficulty here] end
	private DifficultyEnum difficulty;

	// % protected region % [Modify attribute annotation for Name here] off begin
	@CsvBindByName(column = "NAME", required = true)
	@NotNull(message = "Name must not be empty")
	@Column(name = "name")
	@ApiModelProperty(notes = "The Name of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Name here] end
	private String name;

	@CsvIgnore
	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY)
	private Set<FileEntity> coverImage = new HashSet<>();

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Update the annotation for Course Lessons here] off begin
	@ApiModelProperty(notes = "The Course Lesson entities that are related to this entity.")
	@OneToMany(mappedBy = "lesson", cascade = {CascadeType.MERGE}, fetch = FetchType.LAZY)
	@CsvIgnore
	// % protected region % [Update the annotation for Course Lessons here] end
	private Set<CourseLessonEntity> courseLessons = new HashSet<>();

	// % protected region % [Update the annotation for courseLessonsIds here] off begin
	@Transient
	@CsvCustomBindByName(column = "COURSE_LESSONS_IDS", converter = UUIDHashSetConverter.class)
	// % protected region % [Update the annotation for courseLessonsIds here] end
	private Set<UUID> courseLessonsIds = new HashSet<>();

	// % protected region % [Update the annotation for Versions here] off begin
	@ApiModelProperty(notes = "The Lesson Form Version entities that are related to this entity.")
	@OneToMany(mappedBy = "form", cascade = {CascadeType.MERGE}, fetch = FetchType.LAZY)
	@CsvIgnore
	// % protected region % [Update the annotation for Versions here] end
	private Set<LessonFormVersionEntity> versions = new HashSet<>();

	// % protected region % [Update the annotation for versionsIds here] off begin
	@Transient
	@CsvCustomBindByName(column = "VERSIONS_IDS", converter = UUIDHashSetConverter.class)
	// % protected region % [Update the annotation for versionsIds here] end
	private Set<UUID> versionsIds = new HashSet<>();

	// % protected region % [Update the annotation for Form tiles here] off begin
	@ApiModelProperty(notes = "The Lesson Form Tile entities that are related to this entity.")
	@OneToMany(mappedBy = "form", cascade = {CascadeType.MERGE}, fetch = FetchType.LAZY)
	@CsvIgnore
	// % protected region % [Update the annotation for Form tiles here] end
	private Set<LessonFormTileEntity> formTiles = new HashSet<>();

	// % protected region % [Update the annotation for formTilesIds here] off begin
	@Transient
	@CsvCustomBindByName(column = "FORM_TILES_IDS", converter = UUIDHashSetConverter.class)
	// % protected region % [Update the annotation for formTilesIds here] end
	private Set<UUID> formTilesIds = new HashSet<>();

	// % protected region % [Update the annotation for Published version here] off begin
	@ApiModelProperty(notes = "The Lesson Form Version entities that are related to this entity.")
	@OneToOne(mappedBy = "publishedForm", cascade = {CascadeType.MERGE}, fetch = FetchType.LAZY)
	@CsvIgnore
	// % protected region % [Update the annotation for Published version here] end
	private LessonFormVersionEntity publishedVersion;

	// % protected region % [Update the annotation for publishedVersionId here] off begin
	@Transient
	@CsvCustomBindByName(column = "PUBLISHED_VERSION_ID", converter = UUIDConverter.class)
	// % protected region % [Update the annotation for publishedVersionId here] end
	private UUID publishedVersionId;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
	 * Similar to {@link this#addCourseLessons(CourseLessonEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given CourseLessonEntity to be added to this entity
	 */
	public void addCourseLessons(@NotNull CourseLessonEntity entity) {
		addCourseLessons(entity, true);
	}

	/**
	 * Add a new CourseLessonEntity to courseLessons in this entity.
	 *
	 * @param entity the given CourseLessonEntity  to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addCourseLessons(@NonNull CourseLessonEntity entity, boolean reverseAdd) {
		if (!this.courseLessons.contains(entity)) {
			courseLessons.add(entity);
			if (reverseAdd) {
				entity.setLesson(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addCourseLessons(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of CourseLessonEntity to be added to this entity
	 */
	public void addCourseLessons(@NotNull Collection<CourseLessonEntity> entities) {
		addCourseLessons(entities, true);
	}

	/**
	 * Add a new collection of CourseLessonEntity to Course Lessons in this entity.
	 *
	 * @param entities the given collection of CourseLessonEntity to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addCourseLessons(@NonNull Collection<CourseLessonEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> addCourseLessons(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeCourseLessons(CourseLessonEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given CourseLessonEntity to be set to this entity
	 */
	public void removeCourseLessons(@NotNull CourseLessonEntity entity) {
		this.removeCourseLessons(entity, true);
	}

	/**
	 * Remove the given CourseLessonEntity from this entity.
	 *
	 * @param entity the given CourseLessonEntity to be removed from this entity
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeCourseLessons(@NotNull CourseLessonEntity entity, boolean reverse) {
		if (reverse) {
			entity.unsetLesson(false);
		}
		this.courseLessons.remove(entity);
	}

	/**
	 * Similar to {@link this#removeCourseLessons(Collection, boolean)} but
	 * default to true for reverse remove.
	 *
	 * @param entities the given collection of CourseLessonEntity to be removed to this entity
	 */
	public void removeCourseLessons(@NotNull Collection<CourseLessonEntity> entities) {
		this.removeCourseLessons(entities, true);
	}

	/**
	 * Remove the given collection of CourseLessonEntity from  to this entity.
	 *
	 * @param entities the given collection of CourseLessonEntity to be removed to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void removeCourseLessons(@NonNull Collection<CourseLessonEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.removeCourseLessons(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#setCourseLessons(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of CourseLessonEntity to be set to this entity
	 */
	public void setCourseLessons(@NotNull Collection<CourseLessonEntity> entities) {
		setCourseLessons(entities, true);
	}

	/**
	 * Replace the current entities in Course Lessons with the given ones.
	 *
	 * @param entities the given collection of CourseLessonEntity to replace the old ones
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setCourseLessons(@NotNull Collection<CourseLessonEntity> entities, boolean reverseAdd) {

		this.unsetCourseLessons();
		this.courseLessons = new HashSet<>(entities);
		if (reverseAdd) {
			this.courseLessons.forEach(courseLessonsEntity -> courseLessonsEntity.setLesson(this, false));
		}
	}

	/**
	 * Similar to {@link this#unsetCourseLessons(boolean)} but
	 * default to true for reverse unset
	 */
	public void unsetCourseLessons() {
		this.unsetCourseLessons(true);
	}

	/**
	 * Remove all the entities in Course Lessons from this entity.
	 * @param doReverse whether this entity should be removed from the given entities
	 */
	public void unsetCourseLessons(boolean doReverse) {
		if (doReverse) {
			this.courseLessons.forEach(courseLessonsEntity -> courseLessonsEntity.unsetLesson(false));
		}
		this.courseLessons.clear();
	}

/**
	 * Similar to {@link this#addVersions(LessonFormVersionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonFormVersionEntity to be added to this entity
	 */
	public void addVersions(@NotNull LessonFormVersionEntity entity) {
		addVersions(entity, true);
	}

	/**
	 * Add a new LessonFormVersionEntity to versions in this entity.
	 *
	 * @param entity the given LessonFormVersionEntity  to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addVersions(@NonNull LessonFormVersionEntity entity, boolean reverseAdd) {
		if (!this.versions.contains(entity)) {
			versions.add(entity);
			if (reverseAdd) {
				entity.setForm(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addVersions(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of LessonFormVersionEntity to be added to this entity
	 */
	public void addVersions(@NotNull Collection<LessonFormVersionEntity> entities) {
		addVersions(entities, true);
	}

	/**
	 * Add a new collection of LessonFormVersionEntity to Versions in this entity.
	 *
	 * @param entities the given collection of LessonFormVersionEntity to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addVersions(@NonNull Collection<LessonFormVersionEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> addVersions(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeVersions(LessonFormVersionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonFormVersionEntity to be set to this entity
	 */
	public void removeVersions(@NotNull LessonFormVersionEntity entity) {
		this.removeVersions(entity, true);
	}

	/**
	 * Remove the given LessonFormVersionEntity from this entity.
	 *
	 * @param entity the given LessonFormVersionEntity to be removed from this entity
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeVersions(@NotNull LessonFormVersionEntity entity, boolean reverse) {
		if (reverse) {
			entity.unsetForm(false);
		}
		this.versions.remove(entity);
	}

	/**
	 * Similar to {@link this#removeVersions(Collection, boolean)} but
	 * default to true for reverse remove.
	 *
	 * @param entities the given collection of LessonFormVersionEntity to be removed to this entity
	 */
	public void removeVersions(@NotNull Collection<LessonFormVersionEntity> entities) {
		this.removeVersions(entities, true);
	}

	/**
	 * Remove the given collection of LessonFormVersionEntity from  to this entity.
	 *
	 * @param entities the given collection of LessonFormVersionEntity to be removed to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void removeVersions(@NonNull Collection<LessonFormVersionEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.removeVersions(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#setVersions(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of LessonFormVersionEntity to be set to this entity
	 */
	public void setVersions(@NotNull Collection<LessonFormVersionEntity> entities) {
		setVersions(entities, true);
	}

	/**
	 * Replace the current entities in Versions with the given ones.
	 *
	 * @param entities the given collection of LessonFormVersionEntity to replace the old ones
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setVersions(@NotNull Collection<LessonFormVersionEntity> entities, boolean reverseAdd) {

		this.unsetVersions();
		this.versions = new HashSet<>(entities);
		if (reverseAdd) {
			this.versions.forEach(versionsEntity -> versionsEntity.setForm(this, false));
		}
	}

	/**
	 * Similar to {@link this#unsetVersions(boolean)} but
	 * default to true for reverse unset
	 */
	public void unsetVersions() {
		this.unsetVersions(true);
	}

	/**
	 * Remove all the entities in Versions from this entity.
	 * @param doReverse whether this entity should be removed from the given entities
	 */
	public void unsetVersions(boolean doReverse) {
		if (doReverse) {
			this.versions.forEach(versionsEntity -> versionsEntity.unsetForm(false));
		}
		this.versions.clear();
	}

/**
	 * Similar to {@link this#addFormTiles(LessonFormTileEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonFormTileEntity to be added to this entity
	 */
	public void addFormTiles(@NotNull LessonFormTileEntity entity) {
		addFormTiles(entity, true);
	}

	/**
	 * Add a new LessonFormTileEntity to formTiles in this entity.
	 *
	 * @param entity the given LessonFormTileEntity  to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addFormTiles(@NonNull LessonFormTileEntity entity, boolean reverseAdd) {
		if (!this.formTiles.contains(entity)) {
			formTiles.add(entity);
			if (reverseAdd) {
				entity.setForm(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addFormTiles(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of LessonFormTileEntity to be added to this entity
	 */
	public void addFormTiles(@NotNull Collection<LessonFormTileEntity> entities) {
		addFormTiles(entities, true);
	}

	/**
	 * Add a new collection of LessonFormTileEntity to Form tiles in this entity.
	 *
	 * @param entities the given collection of LessonFormTileEntity to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addFormTiles(@NonNull Collection<LessonFormTileEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> addFormTiles(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeFormTiles(LessonFormTileEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonFormTileEntity to be set to this entity
	 */
	public void removeFormTiles(@NotNull LessonFormTileEntity entity) {
		this.removeFormTiles(entity, true);
	}

	/**
	 * Remove the given LessonFormTileEntity from this entity.
	 *
	 * @param entity the given LessonFormTileEntity to be removed from this entity
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeFormTiles(@NotNull LessonFormTileEntity entity, boolean reverse) {
		if (reverse) {
			entity.unsetForm(false);
		}
		this.formTiles.remove(entity);
	}

	/**
	 * Similar to {@link this#removeFormTiles(Collection, boolean)} but
	 * default to true for reverse remove.
	 *
	 * @param entities the given collection of LessonFormTileEntity to be removed to this entity
	 */
	public void removeFormTiles(@NotNull Collection<LessonFormTileEntity> entities) {
		this.removeFormTiles(entities, true);
	}

	/**
	 * Remove the given collection of LessonFormTileEntity from  to this entity.
	 *
	 * @param entities the given collection of LessonFormTileEntity to be removed to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void removeFormTiles(@NonNull Collection<LessonFormTileEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.removeFormTiles(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#setFormTiles(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of LessonFormTileEntity to be set to this entity
	 */
	public void setFormTiles(@NotNull Collection<LessonFormTileEntity> entities) {
		setFormTiles(entities, true);
	}

	/**
	 * Replace the current entities in Form tiles with the given ones.
	 *
	 * @param entities the given collection of LessonFormTileEntity to replace the old ones
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setFormTiles(@NotNull Collection<LessonFormTileEntity> entities, boolean reverseAdd) {

		this.unsetFormTiles();
		this.formTiles = new HashSet<>(entities);
		if (reverseAdd) {
			this.formTiles.forEach(formTilesEntity -> formTilesEntity.setForm(this, false));
		}
	}

	/**
	 * Similar to {@link this#unsetFormTiles(boolean)} but
	 * default to true for reverse unset
	 */
	public void unsetFormTiles() {
		this.unsetFormTiles(true);
	}

	/**
	 * Remove all the entities in Form tiles from this entity.
	 * @param doReverse whether this entity should be removed from the given entities
	 */
	public void unsetFormTiles(boolean doReverse) {
		if (doReverse) {
			this.formTiles.forEach(formTilesEntity -> formTilesEntity.unsetForm(false));
		}
		this.formTiles.clear();
	}

	/**
	 * Similar to {@link this#setPublishedVersion(LessonFormVersionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonEntity to be set to this entity
	 */
	public void setPublishedVersion(@NotNull LessonFormVersionEntity entity) {
		setPublishedVersion(entity, true);
	}

	/**
	 * Set or update publishedVersion with the given LessonFormVersionEntity.
	 *
	 * @param entity the LessonEntity to be set or updated
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setPublishedVersion(@NotNull LessonFormVersionEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setPublishedVersion here] off begin
		// % protected region % [Add any additional logic here before the main logic for setPublishedVersion here] end

		if (sameAsFormer(this.publishedVersion, entity)) {
			return;
		}

		if (this.publishedVersion != null) {
			this.publishedVersion.unsetPublishedForm();
		}

		this.publishedVersion = entity;

		if (reverseAdd) {
			this.publishedVersion.setPublishedForm(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setPublishedVersion here] off begin
		// % protected region % [Add any additional logic here after the main logic for setPublishedVersion here] end
	}

	/**
	 * Similar to {@link this#unsetPublishedVersion(boolean)} but default to true.
	 */
	public void unsetPublishedVersion() {
		this.unsetPublishedVersion(true);
	}

	/**
	 * Remove the LessonFormVersionEntity in Published version from this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetPublishedVersion(boolean reverse) {
		if (reverse && this.publishedVersion != null) {
			this.publishedVersion.unsetPublishedForm(false);
		}
		this.publishedVersion = null;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	public void addCoverImage(FileEntity newFile) {
		coverImage.add(newFile);
	}

	public void addAllCoverImage(Collection<FileEntity> newFiles) {
		coverImage.addAll(newFiles);
	}

	public void removeCoverImage(FileEntity newFile) {
		coverImage.remove(newFile);
	}

	public boolean containsCoverImage(FileEntity newFile) {
		return coverImage.contains(newFile);
	}

	public void clearAllCoverImage() {
		coverImage.clear();
	}

	/**
	 * The CSV annotations used to generate and import CSV files require headers on the abstract entity attributes
	 * (id, created, modified, created by, modified by).  These should not be present in imports, as this information is
	 * added by the server when saving entities.  Therefore a string containing the example csv headers is required
	 *
	 * @return the headers recommended in the CSV import file in CSV format
	 */
	public static String getExampleCsvHeader() {

		// % protected region % [Modify the headers in the CSV file here] off begin
		return "SUMMARY,DESCRIPTION,DURATION,DIFFICULTY,NAME,COURSE_LESSONS_IDS,VERSIONS_IDS,FORM_TILES_IDS,PUBLISHED_VERSION_ID,ID";
		// % protected region % [Modify the headers in the CSV file here] end
	}

	public void addRelationEntitiesToIdSet() {
		// % protected region % [Add any additional logic for entity relations here] off begin
		for (CourseLessonEntity courseLessons: this.courseLessons) {
			this.courseLessonsIds.add(courseLessons.getId());
		}

		for (LessonFormVersionEntity versions: this.versions) {
			this.versionsIds.add(versions.getId());
		}

		for (LessonFormTileEntity formTiles: this.formTiles) {
			this.formTilesIds.add(formTiles.getId());
		}

		Optional<LessonFormVersionEntity> publishedVersionRelation = Optional.ofNullable(this.publishedVersion);
		publishedVersionRelation.ifPresent(entity -> this.publishedVersionId = entity.getId());

		// % protected region % [Add any additional logic for entity relations here] end
	}
	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
