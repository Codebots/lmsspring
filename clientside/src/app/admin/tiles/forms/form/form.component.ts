/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Component, HostBinding, Type} from '@angular/core';
import {Store} from '@ngrx/store';
import {AbstractComponent} from '../../../../lib/components/abstract.component';
import {ButtonStyle, ButtonSize, IconPosition} from '../../../../lib/components/button/button.component';
import {AbstractModel} from '../../../../lib/models/abstract.model';
import {ModelState} from '../../../../models/model.state';
import {LessonModel} from '../../../../models/lesson/lesson.model';
import {getLessonModels} from '../../../../models/lesson/lesson.model.selector';
import * as lessonModelActions from '../../../../models/lesson/lesson.model.action';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * List of all tile forms within the applications. Used to allow clientside binding between a form entity and a form
 * tile.
 */
export const formTiles = {
	lesson: [
	],
	// % protected region % [Add any extra form tiles here] off begin
	// % protected region % [Add any extra form tiles here] end
};

@Component({
	selector: 'section[cb-form]',
	templateUrl: './form.component.html',
	// % protected region % [Add any additional component configurations here] off begin
	// % protected region % [Add any additional component configurations here] end
})
export class FormComponent extends AbstractComponent {
	readonly ButtonStyle = ButtonStyle;
	readonly ButtonSize = ButtonSize;
	readonly IconPosition = IconPosition;

	@HostBinding('class')
	get formCSSClass() {
		return [
			'forms-behaviour',
			'forms-behaviour__landing'
		].join(' ');
	}

	/**
	 * All the configured form entities.
	 */
	forms = [
		{
			name: 'Lesson',
			createUrl: '/admin/entities/lesson/create',
			buildQueryParamEntityType: 'lesson',
			instances: [],
		},
	];

	// % protected region % [Add any additional class properties here] off begin
	// % protected region % [Add any additional class properties here] end

	constructor(
		private readonly store: Store<{ model: ModelState }>,
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
	) {
		super(
			// % protected region % [Add any additional constructor arguments here] off begin
			// % protected region % [Add any additional constructor arguments here] end
		);

		// TODO Consider to put this step into selector
		store.select(getLessonModels).subscribe(models =>
			this.forms[0].instances = models.map(model => new LessonModel(model)));

		store.dispatch(new lessonModelActions.LessonAction(
			lessonModelActions.LessonModelActionTypes.FETCH_ALL_LESSON,
			null
		));
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
