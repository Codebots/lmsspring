/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.inject.modules.entities;

import lmsspring.entities.*;
import lmsspring.entities.enums.*;
import lmsspring.inject.factories.*;
import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.andreinc.mockneat.MockNeat;
import net.andreinc.mockneat.types.enums.StringType;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.UUID;

// % protected region % [Apply any additional imports here] off begin
// % protected region % [Apply any additional imports here] end

/**
 * Guice module for Course Category used to register providers for dependency injection.
 */
@Slf4j
@AllArgsConstructor
public class CourseCategoryModule extends AbstractModule {
	// % protected region % [Apply any additional class fields here] off begin
	// % protected region % [Apply any additional class fields here] end

	@Override
	protected void configure() {
		log.trace("Configuring CourseCategoryModule");

		super.configure();

		// % protected region % [Apply any additional configure steps here] off begin
		// % protected region % [Apply any additional configure steps here] end

		log.trace("Configured CourseCategoryModule");
	}

	/**
	 * Return a factory for mass data generation.
	 */
	@Provides
	public CourseCategoryEntityFactory courseCategoryEntityFactory(
			// % protected region % [Apply any additional injected arguments for courseCategoryEntityFactory here] off begin
			// % protected region % [Apply any additional injected arguments for courseCategoryEntityFactory here] end
			Injector injector
	) {
		log.trace("Creating CourseCategoryEntityFactory");

		// % protected region % [Apply any additional logic for courseCategoryEntityFactory before the main body here] off begin
		// % protected region % [Apply any additional logic for courseCategoryEntityFactory before the main body here] end

		CourseCategoryEntityFactory entityFactory = new CourseCategoryEntityFactory(
				// % protected region % [Apply any additional constructor arguments for CourseCategoryEntityFactory here] off begin
				// % protected region % [Apply any additional constructor arguments for CourseCategoryEntityFactory here] end
				injector
		);

		// % protected region % [Apply any additional logic for courseCategoryEntityFactory after the main body here] off begin
		// % protected region % [Apply any additional logic for courseCategoryEntityFactory after the main body here] end

		log.trace("Created CourseCategoryEntityFactory");

		return entityFactory;
	}

	/**
	 * Return an empty Course Category entity with no references set.
	 */
	@Provides
	@Named("courseCategoryEntityWithNoRef")
	public CourseCategoryEntity courseCategoryEntityWithNoRef(
			// % protected region % [Apply any additional constructor parameters for courseCategoryEntityWithNoRef here] off begin
			// % protected region % [Apply any additional constructor parameters for courseCategoryEntityWithNoRef here] end
			MockNeat mock
	) {
		log.trace("Creating entity of type CourseCategoryEntity with no reference");

		// % protected region % [Apply any additional logic for courseCategoryWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for courseCategoryWithNoRef before the main body here] end

		CourseCategoryEntity newEntity = new CourseCategoryEntity();
		newEntity.setId(UUID.randomUUID());
		newEntity.setCreated(OffsetDateTime.now());
		newEntity.setModified(OffsetDateTime.now());
		// % protected region % [Add customisation for Name here] off begin
		String randomStringforName = mock
				.strings()
				.get();
		newEntity.setName(randomStringforName);
		// % protected region % [Add customisation for Name here] end
		// % protected region % [Add customisation for Colour here] off begin
		String randomStringforColour = mock
				.strings()
				.get();
		newEntity.setColour(randomStringforColour);
		// % protected region % [Add customisation for Colour here] end
		// % protected region % [Add customisation for Summary here] off begin
		String randomStringforSummary = mock
				.strings()
				.get();
		newEntity.setSummary(randomStringforSummary);
		// % protected region % [Add customisation for Summary here] end

		// % protected region % [Apply any additional logic for courseCategoryWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for courseCategoryWithNoRef after the main body here] end

		log.trace("Created entity of type CourseCategoryEntity with no reference");

		return newEntity;
	}

	/**
	 * Return a collection of Course Category entities with no reference at all.
	 */
	@Provides
	@Named("courseCategoryEntitiesWithNoRef")
	public Collection<CourseCategoryEntity> courseCategoryEntitiesWithNoRef(
		// % protected region % [Apply any additional constructor parameters for courseCategoryEntitiesWithNoRef here] off begin
		// % protected region % [Apply any additional constructor parameters for courseCategoryEntitiesWithNoRef here] end
		CourseCategoryEntityFactory courseCategoryEntityFactory
	) {
		log.trace("Creating entities of type CourseCategoryEntity with no reference");

		// % protected region % [Apply any additional logic for courseCategoryEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for courseCategoryEntitiesWithNoRef before the main body here] end

		Collection<CourseCategoryEntity> newEntities = courseCategoryEntityFactory.createMultipleWithNoRef(10);

		// % protected region % [Apply any additional logic for courseCategoryEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for courseCategoryEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type CourseCategoryEntity with no reference");

		return newEntities;
	}

	/**
	 * Return a Course Category entity with all references set.
	 */
	@Provides
	@Named("courseCategoryEntityWithRefs")
	public CourseCategoryEntity courseCategoryEntityWithRefs(
			// % protected region % [Apply any additional constructor parameters for courseCategoryEntityWithRefs here] off begin
			// % protected region % [Apply any additional constructor parameters for courseCategoryEntityWithRefs here] end
			Injector injector
	) {
		log.trace("Creating entity of type CourseCategoryEntity with references");

		// % protected region % [Apply any additional logic for courseCategoryEntityWithRefs before the main body here] off begin
		// % protected region % [Apply any additional logic for courseCategoryEntityWithRefs before the main body here] end

		CourseCategoryEntity courseCategoryEntity = injector.getInstance(Key.get(CourseCategoryEntity.class, Names.named("courseCategoryEntityWithNoRef")));

		// % protected region % [Apply any additional logic for courseCategoryEntityWithRefs after the main body here] off begin
		// % protected region % [Apply any additional logic for courseCategoryEntityWithRefs after the main body here] end

		log.trace("Created entity of type CourseCategoryEntity with references");

		return courseCategoryEntity;
	}

	/**
	 * Return a collection of Course Category entities with all references set.
	 */
	@Provides
	@Named("courseCategoryEntitiesWithRefs")
	public Collection<CourseCategoryEntity> courseCategoryEntitiesWithRefs(
		// % protected region % [Apply any additional constructor parameters for courseCategoryEntitiesWithRefs here] off begin
		// % protected region % [Apply any additional constructor parameters for courseCategoryEntitiesWithRefs here] end
		CourseCategoryEntityFactory courseCategoryEntityFactory
	) {
		log.trace("Creating entities of type CourseCategoryEntity with references");

		// % protected region % [Apply any additional logic for courseCategoryEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for courseCategoryEntitiesWithNoRef before the main body here] end

		Collection<CourseCategoryEntity> newEntities = courseCategoryEntityFactory.createMultipleWithRefs(10);

		// % protected region % [Apply any additional logic for courseCategoryEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for courseCategoryEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type CourseCategoryEntity with references");

		return newEntities;
	}

	// % protected region % [Apply any additional class methods here] off begin
	// % protected region % [Apply any additional class methods here] end
}
