/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { ComponentFixture, waitForAsync, TestBed, tick, fakeAsync } from '@angular/core/testing';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { reducers, clearState } from 'src/app/models/model.reducer';
import { initialRouterState, initialModelState } from 'src/app/models/model.state';
import { RouterTestingModule } from '@angular/router/testing';

import { FormSlideBuilderComponent } from './form-slide-builder.component';
import { FormBehaviourModule } from 'src/app/lib/behaviours/form/form-behaviour.module';
import { FormsTileModule } from '../forms-tile.module';
import { DebugElement } from '@angular/core';

enum KeyCode {
	Tab = 9,
	Enter = 13,
	Esc = 27,
	Space = 32,
	ArrowUp = 38,
	ArrowDown = 40,
	Backspace = 8
}

describe('Testing Form Tile Component', () => {
	let fixture: ComponentFixture<FormSlideBuilderComponent>;
	let formComponent: FormSlideBuilderComponent;

	/**
	 * Adds a question to the form.  This involves creating a slide, and then adding a question to that slide
	 * Function also allows for opening the question sidebar
	 * @param openSidebar Whether the question sidebar should be opened after the question is created.  True to open the sidebar
	 */
	function createQuestion(openSidebar: boolean) {
		// Add the slide
		const newSlideBtn = fixture.debugElement
		.queryAll(By.css('button'))
		.filter(el => el.nativeElement.textContent.trim() === 'Add a new slide');

		newSlideBtn[0].nativeElement.click();
		fixture.detectChanges();

		// Create a new Question in the slide
		const newQuestionBtn = fixture.debugElement
			.queryAll(By.css('button'))
			.filter(el => el.nativeElement.textContent.trim() === 'New Question');

		newQuestionBtn[0].nativeElement.click();
		fixture.detectChanges();

		// Open the sidebar if the flag is true
		if (openSidebar) {
			openQuestionSidebar();
		}
	}

	/**
	 * Opens the question sidebar.  This function assumes that a question has already been created, as the
	 * button which is clicked in this function will only exist if a question is present.
	 *
	 * If no questions have been created then this function will not do anything
	 */
	function openQuestionSidebar() {
		// Click the options button for the question to open the question sidebar
		const questionOptions = fixture.debugElement
		.queryAll(By.css('button'))
		.filter(el => el.nativeElement.textContent.trim() === 'Options');

		// Wrap the button click in a conditional to ensure that an error is not thrown if the function is called incorrectly
		if (questionOptions.length > 0) {
			questionOptions[0].nativeElement.click();
		}

		fixture.detectChanges();
	}

	/**
	 * Opens the dropdown for the question context menu, and then clicks the button specified by the function input
	 *
	 * @param buttonText The button in the dropdown which will be clicked
	 */
	function clickQuestionDropdown(buttonText: string) {
		// Click the dropdown for the question, which will allow us to click the duplicate button
		const questionDropdown = fixture.debugElement.query(By.css('div.btn-group--static-elements > div.context-menu'));
		questionDropdown.nativeElement.click();
		fixture.detectChanges();

		const btn = questionDropdown.queryAll(By.css('button'))
			.filter(el => el.nativeElement.textContent.trim() === buttonText);
		btn[0].nativeElement.click();
		fixture.detectChanges();
	}

	function selectDropdownElement(element: DebugElement, endIndex: number, startingIndex: number) {

		// ng-select dropdown can be opened by clicking the spacebar
		triggerKeyDownEvent(element, KeyCode.Space);
		fixture.detectChanges();
		tick();

		// Already at the intended index so we can return without doing anything else
		if (endIndex === startingIndex) {
			return;
		}

		const placesToMove: number = endIndex - startingIndex;

		// Want to be pressing the down key to move if places to move is positive.  Will be pressing up if places to move is negative
		if (placesToMove > 0) {
			// Keep pressing the down key to move through the options until we hit the index of the option we want
			for (let i = 0; i < placesToMove; i++) {
				triggerKeyDownEvent(element, KeyCode.ArrowDown);
			}
		} else {
			for (let i = 0; i > placesToMove; i--) {
				triggerKeyDownEvent(element, KeyCode.ArrowUp);
			}
		}

		// Click enter to select the option
		triggerKeyDownEvent(element, KeyCode.Enter);
		fixture.detectChanges();
		tick();
	}


	/**
	 * Triggers an event in the provided element which presses the specified key
	 *
	 * @param element The element which the key press event should be triggered for
	 * @param key The key which is to be pressed.  Takes an instance of the KeyCode enum declared above in this file
	 */
	function triggerKeyDownEvent(element: DebugElement, key: KeyCode): void {
		element.triggerEventHandler('keydown', {
			which: key,
			preventDefault: () => { },
		});
	}

	/**
	 * Creates a slide in the form and then clicks the relevant dropdown button if it is passed as a parameter.
	 * If no button input is specified then create the slide and return
	 *
	 * @param dropdownButton The text of the button in the slide dropdown to click.  If empty return without clicking dropdown
	 */
	function createSlideAndClickDropdown(dropdownButton?: string) {
		const newSlideBtn = fixture.debugElement
		.queryAll(By.css('button'))
		.filter(el => el.nativeElement.textContent.trim() === 'Add a new slide');

		newSlideBtn[0].nativeElement.click();
		fixture.detectChanges();

		if (dropdownButton) {
			// Find the Button which toggles the dropdown for slides and click it
			const slideDropdownToggle = fixture.debugElement.query(By.css('section.accordion > div.btn.context-menu'));
			slideDropdownToggle.nativeElement.click();
			fixture.detectChanges();

			// Find the button for duplicating slides and click it
			const editSlideBtn = slideDropdownToggle
					.queryAll(By.css('button'))
					.filter(el => el.nativeElement.textContent.trim() === dropdownButton);
			editSlideBtn[0].nativeElement.click();
		}

		fixture.detectChanges();
	}

	beforeEach(waitForAsync (() => {

		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				RouterTestingModule,
				FormBehaviourModule,
				FormsTileModule,
				StoreModule.forRoot(reducers, {
					initialState: {
						router: initialRouterState,
						models: initialModelState,
					},
					metaReducers: [
						clearState,
					],
				}),
			],
		}).compileComponents().then(() => {
			fixture = TestBed.createComponent(FormSlideBuilderComponent);

			formComponent = fixture.debugElement.componentInstance;
			formComponent.formMode = 'build';
			fixture.detectChanges();
		});
	}));

	afterEach(() => {
		// Need to do this since for some reason the last component queried from the fixture will be rendered on the
		// browser
		if (fixture.nativeElement instanceof HTMLElement) {
			(fixture.nativeElement as HTMLElement).remove();
		}
	});

	it ('Renders Form Slide Builder Component', () => {
		expect(formComponent).toBeTruthy();
	});

	//  TESTS RELATING TO FORM SLIDES

	it ('Does not have any slides when the form is empty', () => {
		const slideAccordians = fixture.debugElement.queryAll(By.css('section.accordion'));
		const slidesInBuilder = fixture.debugElement.queryAll(By.css('section.form-builder > section.form__slide-container'));

		expect(slideAccordians.length).toBe(0);
		expect(slidesInBuilder.length).toBe(0);
	});

	it ('Adds new slide to the form', () => {
		const newSlideBtn = fixture.debugElement
			.queryAll(By.css('button'))
			.filter(el => el.nativeElement.textContent.trim() === 'Add a new slide');

		newSlideBtn[0].nativeElement.click();
		fixture.detectChanges();

		const slideAccordians = fixture.debugElement.queryAll(By.css('section.accordion'));
		const slidesInBuilder = fixture.debugElement.queryAll(By.css('section.form-builder > section.form__slide-container'));

		expect(slideAccordians.length).toBe(1);
		expect(slidesInBuilder.length).toBe(1);

		const sidebarSlideName = fixture.debugElement.queryAll(By.css('section.accordion > button'));
		const formBuilderSlideName = fixture.debugElement.queryAll(By.css('section.form-builder > h3'));

		expect(sidebarSlideName[0].nativeElement.textContent).toBe('New Slide');
		expect(formBuilderSlideName[0].nativeElement.textContent).toBe('New Slide');
	});

	it ('Edits a Slides Name', async () => {
		createSlideAndClickDropdown('Edit');

		// find the input for the Slide name based on it's ID's, and then simulate key presses to change the name of the slide
		const newName = 'Changed Slide Name';
		const slideNameInputField = fixture.debugElement.query(By.css('input#question-field'));
		slideNameInputField.nativeElement.value = newName;
		slideNameInputField.nativeElement.dispatchEvent(new Event('input'));
		fixture.detectChanges();

		// Save the new Slide Name
		const slideSaveBtn = fixture.debugElement.query(By.css('section.slide-builder__list > cb-button-group > button.icon-save'));
		slideSaveBtn.nativeElement.click();
		fixture.detectChanges();

		// Now that the slide name has been updated, check that is has been updated in the display
		const sidebarSlideName = fixture.debugElement.queryAll(By.css('section.accordion > button'));
		const formBuilderSlideName = fixture.debugElement.queryAll(By.css('section.form-builder > h3'));

		expect(sidebarSlideName[0].nativeElement.textContent).toBe(newName);
		expect(formBuilderSlideName[0].nativeElement.textContent).toBe(newName);
	});

	it ('duplicates a slide', () => {
		createSlideAndClickDropdown('Duplicate');

		const sidebarSlideNames = fixture.debugElement.queryAll(By.css('section.accordion > button'));
		const formBuilderSlideNames = fixture.debugElement.queryAll(By.css('section.form-builder > h3'));

		expect(sidebarSlideNames.length).toBe(2);
		expect(formBuilderSlideNames.length).toBe(2);
		expect(sidebarSlideNames[0].nativeElement.textContent).toBe(sidebarSlideNames[1].nativeElement.textContent);
		expect(formBuilderSlideNames[0].nativeElement.textContent).toBe(formBuilderSlideNames[1].nativeElement.textContent);
	});

	it ('Deletes a slide', () => {
		// Running this function with the 'Delete' variable will create the slide, and then delete it
		// Once this function is finished we should see that there is no slides in the form
		createSlideAndClickDropdown('Delete');

		const sidebarSlideNames = fixture.debugElement.queryAll(By.css('section.accordion > button'));
		const formBuilderSlideNames = fixture.debugElement.queryAll(By.css('section.form-builder > h3'));

		expect(sidebarSlideNames.length).toBe(0);
		expect(formBuilderSlideNames.length).toBe(0);
	});

	// TESTS RELATING TO QUESTIONS

	it ('Adds a question to a slide', () => {
		const newSlideBtn = fixture.debugElement
			.queryAll(By.css('button'))
			.filter(el => el.nativeElement.textContent.trim() === 'Add a new slide');

		newSlideBtn[0].nativeElement.click();
		fixture.detectChanges();

		const newQuestionBtn = fixture.debugElement
			.queryAll(By.css('button'))
			.filter(el => el.nativeElement.textContent.trim() === 'New Question');

		newQuestionBtn[0].nativeElement.click();
		fixture.detectChanges();

		const questions = fixture.debugElement.queryAll(By.css('div.form__question-container'));
		expect(questions.length).toBe(1);

		const questionContent = fixture.debugElement.query(By.css('p.question__content'));
		expect(questionContent.nativeElement.textContent).toBe('Click on options to change question details');
	});

	it ('edits a questions name', async () => {
		fixture.detectChanges();
		createQuestion(true);
		const sidebar = fixture.debugElement.query(By.css('section.forms-properties'));
		const questionNameInput = sidebar.query(By.css('input[placeholder="Change Question"]'));

		// Change the name of the question, and check that it is updated on the form builder
		const newName = 'Changed Question Name';
		questionNameInput.nativeElement.value = newName;
		questionNameInput.nativeElement.dispatchEvent(new Event('input'));
		fixture.detectChanges();

		const questionContent = fixture.debugElement.query(By.css('p.question__content'));
		expect(questionContent.nativeElement.textContent).toBe(newName);
	});

	it ('duplicates a question', () => {
		// Do not need to open the question sidebar to test question duplication
		fixture.detectChanges();
		createQuestion(false);

		clickQuestionDropdown('Duplicate');

		// Check that an additional question has been added and that the question is the same as the duplicated question
		const questions = fixture.debugElement.queryAll(By.css('div.form__question-container'));
		expect(questions.length).toBe(2);

		const questionContent = fixture.debugElement.queryAll(By.css('p.question__content'));
		expect(questionContent[0].nativeElement.textContent).toBe(questionContent[1].nativeElement.textContent);
	});

	it ('Deletes a Question from a slide', () => {
		// Do not need to open the question sidebar to test question deletion
		fixture.detectChanges();
		createQuestion(false);

		clickQuestionDropdown('Delete');

		// Question was added and then deleted.  We can now check that there is no questions in the form
		const questions = fixture.debugElement.queryAll(By.css('div.form__question-container'));
		expect(questions.length).toBe(0);
	});

	// TESTS REGARDING QUESTION TYPES
	// Changing the question type was done using indexes of the type instead of the names
	// due to issues with clicking an ng-select (dropdown) element

	// Method of selecting the items from the dropdown is based on the method used in the tests for the component
	// in the ng-select component repository, so it is assumed to be the best way to do it

	// The question types and their relevant indexes can be seen in the relevant tests.
	// These will need to be updated if the order ever changes

	// Everything for the textfield question type (index 1) has also already been tested, so no tests need to be written for it

	it ('Edits Statement Question Type', fakeAsync (() => {
		fixture.detectChanges();
		createQuestion(true);
		const questionTypeDropdown = fixture.debugElement.query(By.css('ng-select#question-type-field'));
		selectDropdownElement(questionTypeDropdown, 0, 1);

		const sidebar = fixture.debugElement.query(By.css('section.forms-properties'));
		const questionNameInput = sidebar.query(By.css('input[placeholder="Change Question"]'));
		const statementContentInput = sidebar.query(By.css('textarea[placeholder="Enter you content to be displayed in statement"]'));

		// Change the name of the question, and check that it is updated on the form builder
		const newName = 'Statement Question Type';
		const statementContentText = 'Content for Statement Question Type';

		questionNameInput.nativeElement.value = newName;
		questionNameInput.nativeElement.dispatchEvent(new Event('input'));
		fixture.detectChanges();

		statementContentInput.nativeElement.value = statementContentText;
		statementContentInput.nativeElement.dispatchEvent(new Event('input'));
		fixture.detectChanges();

		const questionName = fixture.debugElement.query(By.css('p.question__content'));
		expect(questionName.nativeElement.textContent).toBe(newName);

		const statementContent = fixture.debugElement.query(By.css('p.form-statement__content'));
		expect(statementContent.nativeElement.textContent).toBe(statementContentText);

	}));

	it ('Edits Checkbox Question Type', fakeAsync (() => {
		fixture.detectChanges();
		createQuestion(true);
		const questionTypeDropdown = fixture.debugElement.query(By.css('ng-select#question-type-field'));
		selectDropdownElement(questionTypeDropdown, 2, 1);


		const firstOptionTitle = 'First Option';
		const secondOptionTitle = 'Second Option';

		// Add two checkbox options to the question
		const addOption = fixture.debugElement.query(By.css('button.forms__add-option'));
		addOption.nativeElement.click();
		fixture.detectChanges();

		addOption.nativeElement.click();
		fixture.detectChanges();

		// Set the names of the checkbox options in the question
		const checkboxFirstOptionTitleInput = fixture.debugElement.query(By.css('#question-checkbox-option-name-0-field'));
		const checkboxSecondOptionTitleInput = fixture.debugElement.query(By.css('#question-checkbox-option-name-1-field'));

		checkboxFirstOptionTitleInput.nativeElement.value = firstOptionTitle;
		checkboxFirstOptionTitleInput.nativeElement.dispatchEvent(new Event('input'));
		fixture.detectChanges();

		checkboxSecondOptionTitleInput.nativeElement.value = secondOptionTitle;
		checkboxSecondOptionTitleInput.nativeElement.dispatchEvent(new Event('input'));
		fixture.detectChanges();

		// Get the checkboxes which have been added to the form.
		// Check that the correct number have been added and that their titles are correct
		const formCheckboxes = fixture.debugElement.queryAll(By.css('cb-checkbox > label'));

		expect(formCheckboxes.length).toBe(2);
		expect(formCheckboxes[0].nativeElement.textContent).toBe(firstOptionTitle);
		expect(formCheckboxes[1].nativeElement.textContent).toBe(secondOptionTitle);
	}));

	it ('Edits Radio Buttons Question Type', fakeAsync (() => {
		fixture.detectChanges();
		createQuestion(true);
		const questionTypeDropdown = fixture.debugElement.query(By.css('ng-select#question-type-field'));
		selectDropdownElement(questionTypeDropdown, 3, 1);

		const firstOptionTitle = 'First Option';
		const secondOptionTitle = 'Second Option';

		// Add two checkbox options to the question
		const addOption = fixture.debugElement.query(By.css('button.forms__add-option'));
		addOption.nativeElement.click();
		fixture.detectChanges();

		addOption.nativeElement.click();
		fixture.detectChanges();

		// Set the names of the checkbox options in the question
		const radioButtontitleInputs = fixture.debugElement.queryAll(By.css('input[placeholder="Radio button title"]'));

		radioButtontitleInputs[0].nativeElement.value = firstOptionTitle;
		radioButtontitleInputs[0].nativeElement.dispatchEvent(new Event('input'));
		fixture.detectChanges();

		radioButtontitleInputs[1].nativeElement.value = secondOptionTitle;
		radioButtontitleInputs[1].nativeElement.dispatchEvent(new Event('input'));
		fixture.detectChanges();

		// Get the checkboxes which have been added to the form.
		// Check that the correct number have been added and that their titles are correct
		const formCheckboxes = fixture.debugElement.queryAll(By.css('cb-radio-button > label'));

		expect(formCheckboxes.length).toBe(2);
		expect(formCheckboxes[0].nativeElement.textContent).toBe(firstOptionTitle);
		expect(formCheckboxes[1].nativeElement.textContent).toBe(secondOptionTitle);
	}));

	it ('Edits Date / Date Time Question Type to use Date Time Option', fakeAsync (() => {
		fixture.detectChanges();
		createQuestion(true);
		const questionTypeDropdown = fixture.debugElement.query(By.css('ng-select#question-type-field'));
		selectDropdownElement(questionTypeDropdown, 4, 1);

		// Options in dropdown from top to bottom are DateTime, Date, Time
		// Change dropdown to all options and ensure the type of the input is correct
		const datePickerType = fixture.debugElement.queryAll(By.css('ng-select'))[1];
		selectDropdownElement(datePickerType, 0, 0);
		fixture.detectChanges();

		const datePicker = fixture.debugElement.query(By.css('owl-date-time'));
		expect(datePicker.nativeElement.getAttribute('ng-reflect-picker-type')).toBe('both');
	}));

	it ('Edits Date / Date Time Question Type to use Date Option', fakeAsync (() => {
		fixture.detectChanges();
		createQuestion(true);
		const questionTypeDropdown = fixture.debugElement.query(By.css('ng-select#question-type-field'));
		selectDropdownElement(questionTypeDropdown, 4, 1);

		// Options in dropdown from top to bottom are DateTime, Date, Time
		// Change dropdown to all options and ensure the type of the input is correct
		const datePickerType = fixture.debugElement.queryAll(By.css('ng-select'))[1];
		selectDropdownElement(datePickerType, 1, 0);
		fixture.detectChanges();

		const datePicker = fixture.debugElement.query(By.css('owl-date-time'));
		expect(datePicker.nativeElement.getAttribute('ng-reflect-picker-type')).toBe('calendar');
	}));

	it ('Edits Date / Date Time Question Type to use Time Option', fakeAsync (() => {
		fixture.detectChanges();
		createQuestion(true);
		const questionTypeDropdown = fixture.debugElement.query(By.css('ng-select#question-type-field'));
		selectDropdownElement(questionTypeDropdown, 4, 1);

		// Options in dropdown from top to bottom are DateTime, Date, Time
		// Change dropdown to all options and ensure the type of the input is correct
		const datePickerType = fixture.debugElement.queryAll(By.css('ng-select'))[1];
		selectDropdownElement(datePickerType, 2, 0);
		fixture.detectChanges();

		const datePicker = fixture.debugElement.query(By.css('owl-date-time'));
		expect(datePicker.nativeElement.getAttribute('ng-reflect-picker-type')).toBe('timer');
	}));
});
