/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {
	Component,
	EventEmitter,
	HostBinding,
	Input,
	OnChanges,
	Output,
	SimpleChanges,
} from '@angular/core';
import {ButtonAccentColour, ButtonStyle, IconPosition} from '../../../../../../../lib/components/button/button.component';
import {WorkflowStateModel} from '../../../../../../../models/workflowState/workflow_state.model';
import {AbstractComponent} from '../../../../../../../lib/components/abstract.component';
import {FormGroup} from '@angular/forms';
import {createReactiveFormFromModel} from '../../../../../../../lib/models/model-utils';
import {WorkflowTransitionModel} from '../../../../../../../models/workflowTransition/workflow_transition.model';


// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

@Component({
	selector: 'div[cb-workflow-transition-edit]',
	templateUrl: './transition-edit.component.html',
	styleUrls: [
		'./transition-edit.component.scss',
		// % protected region % [Add any additional styles here] off begin
		// % protected region % [Add any additional styles here] end
	]
})
export class TransitionEditComponent extends AbstractComponent implements OnChanges{
	buttonStyle = ButtonStyle;
	buttonAccentColour = ButtonAccentColour;
	iconPos = IconPosition;

	@HostBinding('class')
	className = 'workflow-properties__option';

	@Input()
	workflowTransition: WorkflowTransitionModel;

	@Input()
	workflowStates: WorkflowStateModel[];

	transitionFormGroup: FormGroup;

	@Output()
	deleteClicked: EventEmitter<WorkflowTransitionModel> = new EventEmitter();

	// % protected region % [Add any additional fields here] off begin
	// % protected region % [Add any additional fields here] end

	constructor(
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
	) {

		super();
		this.transitionFormGroup = createReactiveFormFromModel(WorkflowTransitionModel.getProps(), WorkflowTransitionModel.getRelations());

		// % protected region % [Add any constructor logic here] off begin
		// % protected region % [Add any constructor logic here] end
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (changes.hasOwnProperty('workflowTransition')) {
			this.transitionFormGroup.patchValue(changes.workflowTransition.currentValue);
		}
	}

	onDeleteButtonClicked() {
		this.deleteClicked.emit(this.workflowTransition);
	}

	/**
	 * Validate form and trigger update to show error message
	 */
	validateFormGroup(): boolean {
		if (this.transitionFormGroup.invalid) {
			Object.entries(this.transitionFormGroup.controls).forEach(([key, control]) => {
				control.updateValueAndValidity();
			});
		}

		// % protected region % [Add any additional logic for validateFormAndUpdateValue here] off begin
		// % protected region % [Add any additional logic for validateFormAndUpdateValue here] end

		return this.transitionFormGroup.valid;
	}

	/**
	 * Get changes from the form
	 */
	getUpdateValue() {
		const updatedValue = {};
		Object.entries(this.transitionFormGroup.controls).forEach(([key, control]) => {
			if (control.dirty) {
				updatedValue[key] = control.value;
			}
		});

		// % protected region % [Add any additional logic for getUpdateValue here] off begin
		// % protected region % [Add any additional logic for getUpdateValue here] end

		return updatedValue;
	}

	// % protected region % [Add any additional methods here] off begin
	// % protected region % [Add any additional methods here] end
}
