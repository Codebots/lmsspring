/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.entities;

import lombok.*;
import javax.validation.constraints.NotNull;

import java.util.*;

// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
public class LessonFormVersionEntity extends AbstractEntity {

	public LessonFormVersionEntity() {
		initialiseReferences();
	}

	private void initialiseReferences() {


		var FormOneMany = new EntityReference();
			FormOneMany.entityName = "Lesson";
			FormOneMany.oppositeName = "Form";
			FormOneMany.name = "Versions";
			FormOneMany.optional = true;
			FormOneMany.type = "One";
			FormOneMany.oppositeType = "Many";

		References.add(FormOneMany);

		var SubmissionOneMany = new EntityReference();
			SubmissionOneMany.entityName = "LessonFormSubmission";
			SubmissionOneMany.oppositeName = "SubmittedForm";
			SubmissionOneMany.name = "Submission";
			SubmissionOneMany.optional = true;
			SubmissionOneMany.type = "One";
			SubmissionOneMany.oppositeType = "Many";

		References.add(SubmissionOneMany);

		var PublishedFormOneOne = new EntityReference();
			PublishedFormOneOne.entityName = "Lesson";
			PublishedFormOneOne.oppositeName = "PublishedForm";
			PublishedFormOneOne.name = "PublishedVersion";
			PublishedFormOneOne.optional = true;
			PublishedFormOneOne.type = "One";
			PublishedFormOneOne.oppositeType = "One";

		References.add(PublishedFormOneOne);
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Version here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Version here] end
	private Integer version;

	// % protected region % [Modify attribute annotation for Form Data here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Form Data here] end
	private String formData;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private LessonEntity publishedForm;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private Set<LessonFormSubmissionEntity> submission = new HashSet<>();

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private LessonEntity form;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
	 * Similar to {@link this#addSubmission(LessonFormSubmissionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonFormSubmissionEntity to be added to this entity
	 */
	public void addSubmission(@NotNull LessonFormSubmissionEntity entity) {
		addSubmission(entity, true);
	}

	/**
	 * Add a new LessonFormSubmissionEntity to submission in this entity.
	 *
	 * @param entity the given LessonFormSubmissionEntity  to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addSubmission(@NonNull LessonFormSubmissionEntity entity, boolean reverseAdd) {
		if (!this.submission.contains(entity)) {
			submission.add(entity);
			if (reverseAdd) {
				entity.setSubmittedForm(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addSubmission(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of LessonFormSubmissionEntity to be added to this entity
	 */
	public void addSubmission(@NotNull Collection<LessonFormSubmissionEntity> entities) {
		addSubmission(entities, true);
	}

	/**
	 * Add a new collection of LessonFormSubmissionEntity to Submission in this entity.
	 *
	 * @param entities the given collection of LessonFormSubmissionEntity to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addSubmission(@NonNull Collection<LessonFormSubmissionEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> addSubmission(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeSubmission(LessonFormSubmissionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonFormSubmissionEntity to be set to this entity
	 */
	public void removeSubmission(@NotNull LessonFormSubmissionEntity entity) {
		this.removeSubmission(entity, true);
	}

	/**
	 * Remove the given LessonFormSubmissionEntity from this entity.
	 *
	 * @param entity the given LessonFormSubmissionEntity to be removed from this entity
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeSubmission(@NotNull LessonFormSubmissionEntity entity, boolean reverse) {
		if (reverse) {
			entity.unsetSubmittedForm(false);
		}
		this.submission.remove(entity);
	}

	/**
	 * Similar to {@link this#removeSubmission(Collection, boolean)} but
	 * default to true for reverse remove.
	 *
	 * @param entities the given collection of LessonFormSubmissionEntity to be removed to this entity
	 */
	public void removeSubmission(@NotNull Collection<LessonFormSubmissionEntity> entities) {
		this.removeSubmission(entities, true);
	}

	/**
	 * Remove the given collection of LessonFormSubmissionEntity from  to this entity.
	 *
	 * @param entities the given collection of LessonFormSubmissionEntity to be removed to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void removeSubmission(@NonNull Collection<LessonFormSubmissionEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.removeSubmission(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#setSubmission(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of LessonFormSubmissionEntity to be set to this entity
	 */
	public void setSubmission(@NotNull Collection<LessonFormSubmissionEntity> entities) {
		setSubmission(entities, true);
	}

	/**
	 * Replace the current entities in Submission with the given ones.
	 *
	 * @param entities the given collection of LessonFormSubmissionEntity to replace the old ones
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setSubmission(@NotNull Collection<LessonFormSubmissionEntity> entities, boolean reverseAdd) {

		this.unsetSubmission();
		this.submission = new HashSet<>(entities);
		if (reverseAdd) {
			this.submission.forEach(submissionEntity -> submissionEntity.setSubmittedForm(this, false));
		}
	}

	/**
	 * Similar to {@link this#unsetSubmission(boolean)} but
	 * default to true for reverse unset
	 */
	public void unsetSubmission() {
		this.unsetSubmission(true);
	}

	/**
	 * Remove all the entities in Submission from this entity.
	 * @param doReverse whether this entity should be removed from the given entities
	 */
	public void unsetSubmission(boolean doReverse) {
		if (doReverse) {
			this.submission.forEach(submissionEntity -> submissionEntity.unsetSubmittedForm(false));
		}
		this.submission.clear();
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Similar to {@link this#setForm(LessonEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonEntity to be set to this entity
	 */
	public void setForm(@NotNull LessonEntity entity) {
		setForm(entity, true);
	}

	/**
	 * Set or update the form in this entity with single LessonEntity.
	 *
	 * @param entity the given LessonEntity to be set or updated to form
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setForm(@NotNull LessonEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setForm here] off begin
		// % protected region % [Add any additional logic here before the main logic for setForm here] end

		if (sameAsFormer(this.form, entity)) {
			return;
		}

		if (this.form != null) {
			this.form.removeVersions(this, false);
		}
		this.form = entity;
		if (reverseAdd) {
			this.form.addVersions(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setLesson here] off begin
		// % protected region % [Add any additional logic here after the main logic for setLesson here] end
	}

	/**
	 * Similar to {@link this#unsetForm(boolean)} but default to true.
	 */
	public void unsetForm() {
		this.unsetForm(true);
	}

	/**
	 * Remove Form in this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetForm(boolean reverse) {
		if (reverse && this.form != null) {
			this.form.removeVersions(this, false);
		}
		this.form = null;
	}
	/**
	 * Similar to {@link this#setPublishedForm(LessonEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonEntity to be set to this entity
	 */
	public void setPublishedForm(LessonEntity entity) {
		setPublishedForm(entity, true);
	}

	/**
	 * Set or update the publishedForm in this entity with single LessonEntity.
	 *
	 * @param entity the given LessonEntity to be set or updated to publishedForm
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setPublishedForm(LessonEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setPublishedForm here] off begin
		// % protected region % [Add any additional logic here before the main logic for setPublishedForm here] end

		if (sameAsFormer(this.publishedForm, entity)) {
			return;
		}

		if (this.publishedForm != null) {
			this.publishedForm.unsetPublishedVersion(false);
		}

		this.publishedForm = entity;
		if (reverseAdd) {
			this.publishedForm.setPublishedVersion(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setLesson here] off begin
		// % protected region % [Add any additional logic here after the main logic for setLesson here] end
	}

	/**
	 * Similar to {@link this#unsetPublishedForm(boolean)} but default to true.
	 */
	public void unsetPublishedForm() {
		this.unsetPublishedForm(true);
	}

	/**
	 * Remove the LessonEntity of publishedForm from this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetPublishedForm(boolean reverse) {
		if (reverse && this.publishedForm != null) {
			this.publishedForm.unsetPublishedVersion();
		}
		this.publishedForm = null;
	}

	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
