/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Action} from '@ngrx/store';
import {CourseLessonModel} from './course_lesson.model';
import {CourseLessonModelAudit} from './course_lesson.model.state';
import {PassableStateConfig} from '../../lib/services/http/interfaces';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * List of Course Lesson model actions to be dispatched by NgRx.
 */
export enum CourseLessonModelActionTypes {
	CREATE_COURSE_LESSON = '[ENTITY] Create CourseLessonModel',
	CREATE_COURSE_LESSON_OK = '[ENTITY] Create CourseLessonModel successfully',
	CREATE_COURSE_LESSON_FAIL = '[ENTITY] Create CourseLessonModel failed',

	CREATE_ALL_COURSE_LESSON = '[ENTITY] Create All CourseLessonModel',
	CREATE_ALL_COURSE_LESSON_OK = '[ENTITY] Create All CourseLessonModel successfully',
	CREATE_ALL_COURSE_LESSON_FAIL = '[ENTITY] Create All CourseLessonModel failed',

	DELETE_COURSE_LESSON = '[ENTITY] Delete CourseLessonModel',
	DELETE_COURSE_LESSON_OK = '[ENTITY] Delete CourseLessonModel successfully',
	DELETE_COURSE_LESSON_FAIL = '[ENTITY] Delete CourseLessonModel failed',


	DELETE_COURSE_LESSON_EXCLUDING_IDS = '[ENTITY] Delete CourseLessonModels Excluding Ids',
	DELETE_COURSE_LESSON_EXCLUDING_IDS_OK = '[ENTITY] Delete CourseLessonModels Excluding Ids successfully',
	DELETE_COURSE_LESSON_EXCLUDING_IDS_FAIL = '[ENTITY] Delete CourseLessonModels Excluding Ids failed',

	DELETE_ALL_COURSE_LESSON = '[ENTITY] Delete all CourseLessonModels',
	DELETE_ALL_COURSE_LESSON_OK = '[ENTITY] Delete all CourseLessonModels successfully',
	DELETE_ALL_COURSE_LESSON_FAIL = '[ENTITY] Delete all CourseLessonModels failed',

	UPDATE_COURSE_LESSON = '[ENTITY] Update CourseLessonModel',
	UPDATE_COURSE_LESSON_OK = '[ENTITY] Update CourseLessonModel successfully',
	UPDATE_COURSE_LESSON_FAIL = '[ENTITY] Update CourseLessonModel failed',

	UPDATE_ALL_COURSE_LESSON = '[ENTITY] Update all CourseLessonModel',
	UPDATE_ALL_COURSE_LESSON_OK = '[ENTITY] Update all CourseLessonModel successfully',
	UPDATE_ALL_COURSE_LESSON_FAIL = '[ENTITY] Update all CourseLessonModel failed',

	FETCH_COURSE_LESSON= '[ENTITY] Fetch CourseLessonModel',
	FETCH_COURSE_LESSON_OK = '[ENTITY] Fetch CourseLessonModel successfully',
	FETCH_COURSE_LESSON_FAIL = '[ENTITY] Fetch CourseLessonModel failed',

	FETCH_COURSE_LESSON_AUDIT= '[ENTITY] Fetch CourseLessonModel audit',
	FETCH_COURSE_LESSON_AUDIT_OK = '[ENTITY] Fetch CourseLessonModel audit successfully',
	FETCH_COURSE_LESSON_AUDIT_FAIL = '[ENTITY] Fetch CourseLessonModel audit failed',

	FETCH_COURSE_LESSON_AUDITS_BY_ENTITY_ID= '[ENTITY] Fetch CourseLessonModel audits by entity id',
	FETCH_COURSE_LESSON_AUDITS_BY_ENTITY_ID_OK = '[ENTITY] Fetch CourseLessonModel audits by entity id successfully',
	FETCH_COURSE_LESSON_AUDITS_BY_ENTITY_ID_FAIL = '[ENTITY] Fetch CourseLessonModel audits by entity id failed',

	FETCH_ALL_COURSE_LESSON = '[ENTITY] Fetch all CourseLessonModel',
	FETCH_ALL_COURSE_LESSON_OK = '[ENTITY] Fetch all CourseLessonModel successfully',
	FETCH_ALL_COURSE_LESSON_FAIL = '[ENTITY] Fetch all CourseLessonModel failed',

	FETCH_COURSE_LESSON_WITH_QUERY = '[ENTITY] Fetch CourseLessonModel with query',
	FETCH_COURSE_LESSON_WITH_QUERY_OK = '[ENTITY] Fetch CourseLessonModel with query successfully',
	FETCH_COURSE_LESSON_WITH_QUERY_FAIL = '[ENTITY] Fetch CourseLessonModel with query failed',

	FETCH_LAST_COURSE_LESSON_WITH_QUERY = '[ENTITY] Fetch last CourseLessonModel with query',
	FETCH_LAST_COURSE_LESSON_WITH_QUERY_OK = '[ENTITY] Fetch last CourseLessonModel with query successfully',
	FETCH_LAST_COURSE_LESSON_WITH_QUERY_FAIL = '[ENTITY] Fetch last CourseLessonModel with query failed',

	EXPORT_COURSE_LESSON = '[ENTITY] Export CourseLessonModel',
	EXPORT_COURSE_LESSON_OK = '[ENTITY] Export CourseLessonModel successfully',
	EXPORT_COURSE_LESSON_FAIL = '[ENTITY] Export CourseLessonModel failed',

	EXPORT_ALL_COURSE_LESSON = '[ENTITY] Export All CourseLessonModels',
	EXPORT_ALL_COURSE_LESSON_OK = '[ENTITY] Export All CourseLessonModels successfully',
	EXPORT_ALL_COURSE_LESSON_FAIL = '[ENTITY] Export All CourseLessonModels failed',

	EXPORT_COURSE_LESSON_EXCLUDING_IDS = '[ENTITY] Export CourseLessonModels excluding Ids',
	EXPORT_COURSE_LESSON_EXCLUDING_IDS_OK = '[ENTITY] Export CourseLessonModel excluding Ids successfully',
	EXPORT_COURSE_LESSON_EXCLUDING_IDS_FAIL = '[ENTITY] Export CourseLessonModel excluding Ids failed',

	COUNT_COURSE_LESSONS = '[ENTITY] Fetch number of CourseLessonModel records',
	COUNT_COURSE_LESSONS_OK = '[ENTITY] Fetch number of CourseLessonModel records successfully ',
	COUNT_COURSE_LESSONS_FAIL = '[ENTITY] Fetch number of CourseLessonModel records failed',

	IMPORT_COURSE_LESSONS = '[ENTITY] Import CourseLessonModels',
	IMPORT_COURSE_LESSONS_OK = '[ENTITY] Import CourseLessonModels successfully',
	IMPORT_COURSE_LESSONS_FAIL = '[ENTITY] Import CourseLessonModels fail',


	INITIALISE_COURSE_LESSON_COLLECTION_STATE = '[ENTITY] Initialize the CollectionState of CourseLessonModel',
	// % protected region % [Add any additional model actions here] off begin
	// % protected region % [Add any additional model actions here] end
}

export abstract class BaseCourseLessonAction implements Action {
	readonly className: string = 'CourseLessonModel';

	abstract readonly type: string;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	protected constructor(
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
		public readonly afterwardActions: Action[] = []
	) {
		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}
}

export class CourseLessonAction extends BaseCourseLessonAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for CourseLessonAction here] off begin
	// % protected region % [Add any additional class fields for CourseLessonAction here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<CourseLessonModel>,
		// % protected region % [Add any additional constructor parameters for CourseLessonAction here] off begin
		// % protected region % [Add any additional constructor parameters for CourseLessonAction here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for CourseLessonAction here] off begin
			// % protected region % [Add any additional constructor arguments for CourseLessonAction here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for CourseLessonAction here] off begin
		// % protected region % [Add any additional constructor logic for CourseLessonAction here] end
	}

	// % protected region % [Add any additional class methods for CourseLessonAction here] off begin
	// % protected region % [Add any additional class methods for CourseLessonAction here] end
}

export class CourseLessonActionOK extends BaseCourseLessonAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for CourseLessonActionOK here] off begin
	// % protected region % [Add any additional class fields for CourseLessonActionOK here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<CourseLessonModel>,
		// % protected region % [Add any additional constructor parameters for CourseLessonActionOK here] off begin
		// % protected region % [Add any additional constructor parameters for CourseLessonActionOK here] end
		afterwardActions: Action[] = [],
		public audits?: CourseLessonModelAudit[],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for CourseLessonActionOK here] off begin
			// % protected region % [Add any additional constructor arguments for CourseLessonActionOK here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for CourseLessonActionOK here] off begin
		// % protected region % [Add any additional constructor logic for CourseLessonActionOK here] end
	}

	// % protected region % [Add any additional class methods for CourseLessonActionOK here] off begin
	// % protected region % [Add any additional class methods for CourseLessonActionOK here] end
}

export class CourseLessonActionFail extends BaseCourseLessonAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for CourseLessonActionFail here] off begin
	// % protected region % [Add any additional class fields for CourseLessonActionFail here] end

	public constructor(
		public typeInput: string,
		public readonly error?: any,
		public stateConfig?: PassableStateConfig<CourseLessonModel>,
		// % protected region % [Add any additional constructor parameters for CourseLessonActionFail here] off begin
		// % protected region % [Add any additional constructor parameters for CourseLessonActionFail here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for CourseLessonActionFail here] off begin
			// % protected region % [Add any additional constructor arguments for CourseLessonActionFail here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for CourseLessonActionFail here] off begin
		// % protected region % [Add any additional constructor logic for CourseLessonActionFail here] end
	}

	// % protected region % [Add any additional class methods for CourseLessonActionFail here] off begin
	// % protected region % [Add any additional class methods for CourseLessonActionFail here] end
}

export function isCourseLessonModelAction(e: any): e is BaseCourseLessonAction {
	return Object.values(CourseLessonModelActionTypes).includes(e);
}

// % protected region % [Add any additional actions here] off begin
// % protected region % [Add any additional actions here] end
