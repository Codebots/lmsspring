/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.controllers;

import com.opencsv.CSVWriter;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.security.core.Authentication;
import org.springframework.web.multipart.MultipartFile;
import lmsspring.configs.security.authorities.CustomGrantedAuthority;
import lmsspring.entities.LessonFormTileEntity;
import lmsspring.dtos.LessonFormTileEntityDto;
import lmsspring.dtos.LessonFormTileEntityDto;
import lmsspring.entities.LessonFormTileEntityAudit;
import lmsspring.repositories.LessonFormTileRepository;
import lmsspring.services.LessonFormTileService;
import lmsspring.configs.security.services.AuthenticationService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import lmsspring.services.utils.CsvUtils;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import javax.validation.ConstraintViolation;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * Controller used to handle all REST operations regarding Lesson Form Tile
 */
@Api(description = "Set of endpoints for Creating, Retrieving, Updating and Deleting of LessonFormTiles.")
@RestController
@Slf4j
@RequestMapping("/api/lessonformtile")
public class LessonFormTileController {

	private final LessonFormTileService lessonFormTileService;
	private final CsvUtils<LessonFormTileEntity, LessonFormTileRepository, LessonFormTileEntityAudit,  LessonFormTileService> csvUtils;
	private final AuthenticationService authService;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	@Autowired
	public LessonFormTileController(
			// % protected region % [Add any additional constructor parameters here] off begin
			// % protected region % [Add any additional constructor parameters here] end
			AuthenticationService authService,
			LessonFormTileService lessonFormTileService
	) {
		this.authService = authService;
		this.lessonFormTileService = lessonFormTileService;
		this.csvUtils = new CsvUtils<>(lessonFormTileService, LessonFormTileEntity.class);

		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}

	/**
	 * Return all the Lesson Form Tiles.
	 *
	 * @return all the Lesson Form Tiles
	 */
	@ApiOperation(
			value = "Returns a single page of LessonFormTiles",
			authorizations = {@Authorization(value = "bearerToken")}
	)
	@PreAuthorize("hasPermission('LessonFormTileEntity', 'read')")
	@GetMapping(produces = "application/json")
	public ResponseEntity<List<LessonFormTileEntity>> getAllWithPage(
			@ApiParam("The page to return.")
			@RequestParam(value = "page", defaultValue = "1", required = false) int page,
			@ApiParam("The size of the page to return.")
			@RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize
	) {
		List<LessonFormTileEntity> lessonFormTiles = lessonFormTileService.findAllWithPage((page > 0) ? page - 1 : page, pageSize);

		// % protected region % [Add any custom logic before returning the entities here] off begin
		// % protected region % [Add any custom logic before returning the entities here] end

		return new ResponseEntity<>(lessonFormTiles, HttpStatus.OK);
	}

	/**
	 * Return the Lesson Form Tile that has the same id as the given id.
	 *
	 * @param id      The id of the LessonFormTileEntity to be returned
	 * @return the Lesson Form Tile that has the same id as the given id
	 */
	// % protected region % [Customise the security configuration here for the getWithId endpoint] off begin
	// % protected region % [Customise the security configuration here for the getWithId endpoint] end
	@ApiOperation(
			value = "Return a single LessonFormTile as defined by the id provided.",
			authorizations = {@Authorization(value = "bearerToken")}
	)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "LessonFormTile entity not found"),
	})
	@PreAuthorize("hasPermission('LessonFormTileEntity', 'read')")
	@GetMapping(value = "/{id}", produces = "application/json")
	public ResponseEntity<LessonFormTileEntity> getById(
			@ApiParam("The UUID of the LessonFormTile to return.")
			@PathVariable("id") UUID id
	) {
		Optional<LessonFormTileEntity> lessonFormTileEntity = lessonFormTileService.findById(id);

		// % protected region % [Add any final logic before returning the entity here] off begin
		// % protected region % [Add any final logic before returning the entity here] end

		if (lessonFormTileEntity.isPresent()) {
			return new ResponseEntity<>(lessonFormTileEntity.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * Create/Update the given Lesson Form Tile. If the entity has an id it will be updated, if not it will be created.
	 * The appropriate status codes will be set.
	 *
	 * @param lessonFormTileEntityDto The Dto of the entity to save or update
	 */
	@ApiOperation(
			value = "Create a LessonFormTile if id not exists. Return 409 otherwise",
			authorizations = {@Authorization(value = "bearerToken")}
	)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully created the new LessonFormTile."),
			@ApiResponse(code = 409, message = "Failed to create the new LessonFormTile, duplicate record found")
	})
	@PreAuthorize("hasPermission('LessonFormTileEntity', 'create')")
	@PostMapping
	public ResponseEntity<LessonFormTileEntity> saveLessonFormTile(
			@ApiParam("The LessonFormTileEntity to create or update.")
			@RequestBody LessonFormTileEntityDto lessonFormTileEntityDto
	) {
		// % protected region % [Add any logic before save here] off begin
		// % protected region % [Add any logic before save here] end

		LessonFormTileEntity lessonFormTileEntity = lessonFormTileService.create(new LessonFormTileEntity(lessonFormTileEntityDto));

		return new ResponseEntity<>(lessonFormTileEntity, HttpStatus.OK);
	}

	/**
	 * Update the given Lesson Form Tile. If the entity has an id which is associated with an existing entity it will be updated, if not then the request will fail
	 * The appropriate status codes will be set.
	 *
	 * @param lessonFormTileEntityDto The Dto of the entity to save or update
	 */
	@ApiOperation(
			value = "Update a LessonFormTile entity if id exists and is associated with an existing entity.  Return BAD_REQUEST otherwise",
			authorizations = {@Authorization(value = "bearerToken")}
	)
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Successfully updated the LessonFormTile."),
		@ApiResponse(code = 400, message = "Failed to update the LessonFormTile, entity id either does not exist" +
				"or is not associated with an existing entity")
	})
	@PreAuthorize("hasPermission('LessonFormTileEntity', 'update')")
	@PutMapping
	public ResponseEntity<LessonFormTileEntity> updateLessonFormTile(
			@ApiParam("The LessonFormTileEntity to create or update.")
			@RequestBody LessonFormTileEntityDto lessonFormTileEntityDto
	) {
		// % protected region % [Add any logic before save here] off begin
		// % protected region % [Add any logic before save here] end
		Optional<LessonFormTileEntity> checkLessonFormTileExists = lessonFormTileService.findById(lessonFormTileEntityDto.getId());
		LessonFormTileEntity lessonFormTileEntity = new LessonFormTileEntity(lessonFormTileEntityDto);

		if (checkLessonFormTileExists.isPresent()) {
			lessonFormTileEntity = lessonFormTileService.update(lessonFormTileEntity);

			return new ResponseEntity<>(lessonFormTileEntity, HttpStatus.OK);
		}

		return new ResponseEntity<>(lessonFormTileEntity, HttpStatus.BAD_REQUEST);
	}

	/**
	 * Delete the Lesson Form Tile that has the same id as the given id.
	 *
	 * @param id The id of the LessonFormTileEntity to be deleted
	 * @return void HTTP status code will be set on success
	 */
	// % protected region % [Customise the security config here for the delete endpoint] off begin
	// % protected region % [Customise the security config here for the delete endpoint] end
	@ApiOperation(
			value = "Delete a single LessonFormTile as defined by the id provided.",
			authorizations = {@Authorization(value = "bearerToken")}
	)
	@PreAuthorize("hasPermission('LessonFormTileEntity', 'delete')")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> deleteById(
			@ApiParam("The UUID of the LessonFormTile to delete.")
			@PathVariable(value="id") UUID id
	) {
		// % protected region % [Add any additional logic before deleting the given entity] off begin
		// % protected region % [Add any additional logic before deleting the given entity] end

		lessonFormTileService.deleteById(id);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * Download a CSV file containing all of the entities from the ID List.  Endpoint needs to be opened in a new tab
	 * on the clientside bcause this will allow the file download to start.

	 * @param ids The ids of the entities to export
	 * @param response
	 */
	@ApiOperation(
			value = "Download a csv file including all Lesson Form Tile entities included in the ids provided",
			authorizations = {@Authorization(value = "bearerToken")}
	)
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Successfully exported the selected entities"),
		@ApiResponse(code = 400, message = "Failed to export the entities due to no ids being provided"),
		@ApiResponse(code = 500, message = "Error occurred while creating the CSV file to export or while downloading the file")
	})
	@PreAuthorize("hasPermission('LessonFormTileEntity', 'read')")
	@GetMapping(value = "/export", produces = "text/csv")
	public void exportLessonFormTile(
			@ApiParam("The ids of the LessonFormTileEntities to export")
			@RequestParam @NotNull List<UUID> ids, HttpServletResponse response
	) {
		if (ids == null || ids.size() == 0) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		} else {
			List<LessonFormTileEntity> entitiesToExport = lessonFormTileService.findAllByIds(ids);
			// % protected region % [Modify the default file name for the Export endpoint here] off begin
			String datetime = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());
			String fileName = "LessonFormTile_Export_" + datetime + ".csv";
			// % protected region % [Modify the default file name for the Export endpoint here] end

		for (LessonFormTileEntity entity: entitiesToExport) {
			entity.addRelationEntitiesToIdSet();
		}

			// % protected region % [Add any additional logic before executing the Export endpoint here] off begin
			// % protected region % [Add any additional logic before executing the Export endpoint here] end

			try {
				csvUtils.exportCsvFile(entitiesToExport, fileName, response);
			} catch (IOException | CsvRequiredFieldEmptyException | CsvDataTypeMismatchException e) {
				// % protected region % [Modify the error response for the Export endpoint here] off begin
				log.error(Arrays.toString(e.getStackTrace()));
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				// % protected region % [Modify the error response for the Export endpoint here] end
			}
		}
	}

	/**
	 * Download a CSV file containing all of the entities with ids NOT contained in the id list.  Endpoint needs to be opened in a new tab
	 * on the clientside bcause this will allow the file download to start.

	 * @param ids The ids of the entities to exclude from the export
	 * @param response
	 */
	@ApiOperation(
			value = "Download a csv file with all of the entities not included in the id's list",
			authorizations = {@Authorization(value = "bearerToken")}
	)
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Successfully exported the excluded entities"),
		@ApiResponse(code = 500, message = "Error occurred while creating the CSV file to export or while downloading the file")
	})
	@PreAuthorize("hasPermission('LessonFormTileEntity', 'read')")
	@GetMapping(value = "/export-excluding", produces = "text/csv")
	public void exportLessonFormTileExcludingIds(
			@ApiParam("The ids of the LessonFormTileEntities to exclude from the export")
			@RequestParam @NotNull List<UUID> ids, HttpServletResponse response
	) {
		List<LessonFormTileEntity> entitiesToExport;
		// % protected region % [Modify the default file name for the Export Excluding endpoint here] off begin
		String datetime = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());
		String fileName = "LessonFormTile_Export_" + datetime + ".csv";
		// % protected region % [Modify the default file name for the Export Excluding endpoint here] end

		if (ids == null || ids.size() == 0) {
			entitiesToExport = lessonFormTileService.findAllExcludingIds(new ArrayList<>());
		} else {
			entitiesToExport = lessonFormTileService.findAllExcludingIds(ids);
		}

		for (LessonFormTileEntity entity: entitiesToExport) {
			entity.addRelationEntitiesToIdSet();
		}

		// % protected region % [Add any additional logic before executing the Export Excluding endpoint here] off begin
		// % protected region % [Add any additional logic before executing the Export Excluding endpoint here] end

		try {
			if (entitiesToExport.isEmpty()) {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, "You must export at least one entity");
			} else {
				csvUtils.exportCsvFile(entitiesToExport, fileName, response);
			}
		} catch (IOException | CsvRequiredFieldEmptyException | CsvDataTypeMismatchException e) {
			// % protected region % [Modify the error response for the Export Excluding endpoint here] off begin
			log.error(Arrays.toString(e.getStackTrace()));
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			// % protected region % [Modify the error response for the Export Excluding endpoint here] end
		}
	}

	/**
	 * Download a CSV file containing all of the headers of the LessonFormTile entity.
	 * This file can then be used to in the import endpoint
	 *
	 * @param response
	 */
	@ApiOperation(
			value = "Download an empty csv file with the example headers for a CSV Import",
			authorizations = {@Authorization(value = "bearerToken")}
	)
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Successfully downloaded the example CSV"),
		@ApiResponse(code = 500, message = "Error occurred while creating the CSV file to export or while downloading the file")
	})
	@PreAuthorize("hasPermission('LessonFormTileEntity', 'create')")
	@GetMapping(value = "/example-import", produces = "text/csv")
	public void csvImportExample(
		HttpServletResponse response
	) {
		// % protected region % [Modify the default file name for the Example Import endpoint here] off begin
		String datetime = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());
		String fileName = "LessonFormTile_Import_Example_" + datetime + ".csv";
		// % protected region % [Modify the default file name for the Example Import endpoint here] end
		try {
			InputStream is = new ByteArrayInputStream(LessonFormTileEntity.getExampleCsvHeader().getBytes());

			IOUtils.copy(is, response.getOutputStream());
			response.setContentType("text/csv");
			response.setHeader("Content-disposition", "attachment; filename=" + fileName);

			response.flushBuffer();
			is.close();
		} catch (IOException e) {
			// % protected region % [Modify the error response for the Example Import endpoint here] off begin
			log.error(Arrays.toString(e.getStackTrace()));
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			// % protected region % [Modify the error response for the Example Import endpoint here] end
		}
	}

	/**
	 * Upload a CSV file containing attributes for Lesson Form Tile entities, and import them into the database.  This method
	 * contains a lot of error checking to ensure that any entity uploaded will not cause an exception when it is saved
	 * to the database
	 *
	 * @param file The CSV file which contains the entities
	 * @return A Response entity with a 200 Status code if successful, and 400 if there are issues with the CSV
	 * @throws IOException
	 */
	@ApiOperation(
			value = "Upload a csv file containing entities to add to the database",
			authorizations = {@Authorization(value = "bearerToken")}
	)
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Successfully imported the entities provided in the CSV."),
		@ApiResponse(code = 400, message = "Failed to import the entities due to parsing or validation errors in thr CSV file"),
		@ApiResponse(code = 403, message = "Failed to import the entities due to insufficient permissions")
	})
	@PreAuthorize("hasPermission('LessonFormTileEntity', 'create')  || hasPermission('LessonFormTileEntity', 'update')")
	@PostMapping(value = "/import-csv")
	public ResponseEntity<String> csvUpload(HttpServletRequest request,
			@ApiParam("The CSV file with LessonFormTileEntity entities to import")
			@RequestParam("file") MultipartFile file) throws IOException {
		Authentication authentication = this.authService.getAuthentication(request);

		Collection<CustomGrantedAuthority> authorities = new ArrayList<>();
		//Cast each GrantedAuthority individually as casting the collection as a whole introduces an UncheckedCast warning
		authentication.getAuthorities().forEach(authority -> {
			authorities.add((CustomGrantedAuthority) authority);
		});

		// If a authority for the target entity does not exist, we assume that the user is not authenticated to perform the action
		// Therefore we create a new authority with disallowed permissions and use that for the authorization checks
		CustomGrantedAuthority entityAuthority = authorities.stream().filter(x -> x.getTargetEntity().matches("LessonFormTileEntity"))
				.findFirst().orElse(new CustomGrantedAuthority("", "", false, false, false, false));
		boolean allowCreate = entityAuthority.isAllowCreate();
		boolean allowUpdate = entityAuthority.isAllowUpdate();

		// All error checking returns a response, so we can create a response here
		// As long as this response remains null the import is still valid
		ResponseEntity<String> response = csvUtils.uploadValid(file);

		if (response != null) {
			return response;
		}

		//Create two readers.  First is used for the CSV reader, second is used to verify that the headers are valid
		BufferedReader reader = new BufferedReader(new InputStreamReader(file.getInputStream()));
		BufferedReader readerForHeaderCheck = new BufferedReader(new InputStreamReader(file.getInputStream()));

		String[] columnHeaders = readerForHeaderCheck.readLine().split(",");
		List<String> allowedHeaders = Arrays.asList(LessonFormTileEntity.getExampleCsvHeader().split(","));
		response = csvUtils.headersValid(allowedHeaders, columnHeaders);
		readerForHeaderCheck.close();

		if (response != null) {
			reader.close();
			return response;
		}

		// The CSV to Bean Builder will parse the csv file one line at a time, and create an entity of the given type
		// This entity can then be added to the database
		CsvToBean<LessonFormTileEntity> csvToBean = new CsvToBeanBuilder<LessonFormTileEntity>(reader)
				.withType(LessonFormTileEntity.class)
				.withIgnoreLeadingWhiteSpace(true)
				.withThrowExceptions(true)
				.build();

		List<LessonFormTileEntity> entities = new ArrayList<>();
		List<String> importErrors = new ArrayList<>();
		int currentLine = 2; //Header is line 1, so start at 2

		// If the first entry in the CSV has a parsing error, it will throw an exception
		// Therefore need to wrap in try catch
		try {
			Iterator<LessonFormTileEntity> iterator = csvToBean.iterator();

			while (iterator.hasNext()) {
				//Putting additional try catch in loop allows for multiple entries to be iterated through
				// even if a parsing error is encountered
				// All parsing errors are stored and returned to the client
				try {
					// % protected region % [Modify the parsing of CSV files here] off begin
					var next = iterator.next();
					// This is the earliest we know whether an entity is being created or updated.  Therefore we should
					if (next.getId() != null && !allowUpdate) {
						return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Line " + currentLine
								+ ": You attempted to update a Lesson Form Tile entity and you do not have permission");
					}

					if (next.getId() == null && !allowCreate) {
						return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Line " + currentLine
								+ ": You attempted to create a Lesson Form Tile entity and you do not have permission");
					}

					entities.add(next);
					// % protected region % [Modify the parsing of CSV files here] end
				} catch (RuntimeException e) {
					// % protected region % [Modify the import parsing error messages within the loop here] off begin
					importErrors.add("Line " + currentLine + ": " + e.getCause().getMessage());
					// % protected region % [Modify the import parsing error messages within the loop here] end
				}
				currentLine++;
			}
		} catch (RuntimeException e) {
			// % protected region % [Modify the import parsing error messages here] off begin
			importErrors.add("Line " + currentLine + ": " + e.getCause().getMessage());
			// % protected region % [Modify the import parsing error messages here] end
		}

		/*
		 * Need to check this before we check that that there are no entities
		 * If an entity throws an error, it will not be added to the list
		 * If all have errors, then checking that entities exist first would
		 * cause it to return an error saying that there were no entities present, which is not the case
		 */
		if (importErrors.size() > 0) {
			reader.close();
			// % protected region % [Modify the error response for parsing errors here] off begin
			String errorMessage = String.join("\n", importErrors);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessage);
			// % protected region % [Modify the error response for parsing errors here] end
		}

		// Don't want to progress any further if there is no entities
		// Can't do this any earlier as we don't know how many entities there are until we have finished parsing the file
		if (entities.isEmpty()) {
			// % protected region % [Modify error message for a CSV with no entities here] off begin
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("CSV file must contain at least one entity");
			// % protected region % [Modify error message for a CSV with no entities here] end
		}

		/*
		 * Want to validate all entities before attempting to create any
		 * Entities are validated as part of creation, but doing this keeps it transational
		 * and ensures that either all entities are added or none are
		 */
		response = csvUtils.validateEntities(entities);

		if (response != null) {
			reader.close();
			return response;
		}

		// % protected region % [Add any additional logic before completing the CSV Import here] off begin
		// % protected region % [Add any additional logic before completing the CSV Import here] end

		// Now that all known errors have been checked and cleared, we can create the entities
		List<String> entityIds = csvUtils.addAllEntities(entities);


		// % protected region % [Add any additional logic after completing the CSV Import here] off begin
		// % protected region % [Add any additional logic after completing the CSV Import here] end

		reader.close();
		// % protected region % [Modify the response for a successful import here] off begin
		String idReturn = String.join(",", entityIds);
		return ResponseEntity.status(HttpStatus.OK).body(idReturn);
		// % protected region % [Modify the response for a successful import here] end
	}

	// % protected region % [Add any additional endpoints here] off begin
	// % protected region % [Add any additional endpoints here] end
}
