/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Component, HostBinding, Input} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Observable, of} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {filter, switchMap, take, tap} from 'rxjs/operators';
import {WorkflowVersionModel} from 'src/app/models/workflowVersion/workflow_version.model';
import {createReactiveFormFromModel} from 'src/app/lib/models/model-utils';
import {ButtonGroupAlignment} from 'src/app/lib/components/buttonGroup/button.group.component';
import {ButtonStyle, ButtonSize, ButtonAccentColour} from 'src/app/lib/components/button/button.component';
import { ActionsSubject, Store} from '@ngrx/store';
import {RouterState} from 'src/app/models/model.state';
import * as routerAction from 'src/app/lib/routing/routing.action';
import * as workflowVersionAction from 'src/app/models/workflowVersion/workflow_version.model.action';
import * as uuid from 'uuid';
import {getWorkflowVersionModelWithId} from 'src/app/models/workflowVersion/workflow_version.model.selector';
import {WorkflowVersionModelActionTypes} from 'src/app/models/workflowVersion/workflow_version.model.action';
import {getAllEntitiesWithWorkflowBehaviour, WorkflowEntityOption} from 'src/app/lib/utils/workflow-utils';
import {Expand} from 'src/app/lib/services/http/interfaces';
import {WorkflowVersionModelState} from 'src/app/models/workflowVersion/workflow_version.model.state';
import {AbstractComponent} from 'src/app/lib/components/abstract.component';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

@Component({
	selector: 'section[cb-workflow-edit-details]',
	templateUrl: './workflow-details.component.html',
	styleUrls: [
		'./workflow-details.component.scss',
		// % protected region % [Add any additional styles here] off begin
		// % protected region % [Add any additional styles here] end
	],
	// % protected region % [Add any additional component annotation here] off begin
	// % protected region % [Add any additional component annotation here] end
})
export class WorkflowDetailsComponent extends AbstractComponent {
	ButtonStyle = ButtonStyle;
	buttonSize = ButtonSize;
	buttonColor = ButtonAccentColour;

	@HostBinding('class')
	className = 'workflow__edit';

	draftVersion$: Observable<WorkflowVersionModel>;

	draftVersion: WorkflowVersionModel;

	workflowDetailsForm: FormGroup;

	versionId: string;

	modelChanges: {};

	get defaultExpands(): Expand[] {
		let expands: Expand[] =  Object.entries(WorkflowVersionModel.getRelations()).map(
			([key, entry]): Expand => {
				return {
					name: key,
					fields: ['id']
				};
			}
		);
		return expands;
	}

	entitiesWithBehaviourWorkflow: WorkflowEntityOption[] = getAllEntitiesWithWorkflowBehaviour();

	/**
	 * Whether disable cancel button and save draft button
	 */
	buttonDisabled = false;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	constructor(
		private readonly store: Store<{ model: WorkflowVersionModelState }>,
		private readonly routerStore: Store<{ router: RouterState }>,
		private readonly route: ActivatedRoute,
		private readonly actionSubjects: ActionsSubject,
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
	) {
		// % protected region % [Add any additional logic before main logic of constructor here] off begin
		// % protected region % [Add any additional logic before main logic of constructor here] end

		super();

		this.workflowDetailsForm = createReactiveFormFromModel(WorkflowVersionModel.getProps(), {});

		this.workflowDetailsForm.addControl('workflowAssociations', new FormControl([]));

		this.draftVersion$ = this.route.parent.paramMap.pipe(
			switchMap(
				params => {
					this.versionId = params.get('versionId');
					if (this.versionId) {
						this.store.dispatch(new workflowVersionAction.WorkflowVersionAction(
							workflowVersionAction.WorkflowVersionModelActionTypes.FETCH_WORKFLOW_VERSION,
							{
								targetModelId: this.versionId,
								queryParams: {
									expands: this.defaultExpands
								}
							}
						));
						return this.store.select(getWorkflowVersionModelWithId, this.versionId);
					} else {
						return of(new WorkflowVersionModel());
					}
				}
			),
			filter(workflowVersion => !!workflowVersion),
			tap(workflowVersion => {
				this.workflowDetailsForm.patchValue(workflowVersion);
				const selectedEntities = [];
				// Update the selected array
				this.entitiesWithBehaviourWorkflow.forEach(
					entity => {
						// If entity is already selected, push to dropdown options
						if (workflowVersion[entity.fieldName]) {
							selectedEntities.push(entity);
						}
					}
				);
				// Dispatch value for selected ones
				this.workflowDetailsForm.patchValue({
					workflowAssociations: selectedEntities
				});
			})
		);

		this.draftVersion$.subscribe(version => this.draftVersion = version);

		// % protected region % [Add any additional logic after main logic of constructor here] off begin
		// % protected region % [Add any additional logic after main logic of constructor here] end
	}

	/**
	 * Save a draft version of workflow
	 */
	onSaveDraftClicked() {
		// % protected region % [Add any additional logic before main process of onSaveDraftClicked here] off begin
		// % protected region % [Add any additional logic before main process of onSaveDraftClicked here] end

		// if workflow is not create, create a new workflow first
		if (this.validateForm()) {
			this.modelChanges = this.workflowDetailsForm.value;
			this.updateAssociationModels();
			if (!this.draftVersion.id) {
				this.createWorkflow();
			} else {
				this.saveDraftVersion();
			}
		}

		// % protected region % [Add any additional logic after main process of onSaveDraftClicked here] off begin
		// % protected region % [Add any additional logic after main process of onSaveDraftClicked here] end
	}

	/**
	 * Update association model relations
	 */
	updateAssociationModels() {
		// % protected region % [Add any additional logic before main process of updateAssociationModels here] off begin
		// % protected region % [Add any additional logic before main process of updateAssociationModels here] end

		const selectedOptions: WorkflowEntityOption[] = this.workflowDetailsForm.get('workflowAssociations').value;
		this.entitiesWithBehaviourWorkflow.forEach(
			entity => {
				this.modelChanges[entity.fieldName] = false;
			}
		);
		selectedOptions.forEach(selectedOption => {
			this.modelChanges[selectedOption.fieldName] = true;
		});

		// % protected region % [Add any additional logic after main process of updateAssociationModels here] off begin
		// % protected region % [Add any additional logic after main process of updateAssociationModels here] end
	}

	/**
	 * Create a new version of workflow
	 */
	createWorkflow() {
		// % protected region % [Add any additional logic before main logic of createWorkflow here] off begin
		// % protected region % [Add any additional logic before main logic of createWorkflow here] end

		const createVersionActionId = uuid.v4();

		// TODO Move all this part of logic to service for all entities.
		// Subscribe to successful changes
		Object.assign(this.draftVersion, this.modelChanges);
		// Set the id of draft version

		this.store.dispatch(new workflowVersionAction.WorkflowVersionAction(
			workflowVersionAction.WorkflowVersionModelActionTypes.CREATE_WORKFLOW_VERSION,
			{
				targetModel: this.draftVersion,
				actionId: createVersionActionId,
			}
		));

		this.actionSubjects.pipe(
			filter((action: workflowVersionAction.WorkflowVersionActionOK ) => {
				return action.type === WorkflowVersionModelActionTypes.CREATE_WORKFLOW_VERSION_OK
					&& action.stateConfig.actionId === createVersionActionId;
			}),
			take(1)
		).subscribe(
			data => {
				this.routerStore.dispatch(new routerAction.NavigateRoutingAction(
					['../edit', data.stateConfig.targetModel.id, 'states'], {
						relativeTo: this.route.parent
					}
				));
			},
		);

		this.buttonDisabled = true;
		// Create a new version of workflow
	}

	/**
	 * Save Draft Version of Workflow
	 */
	saveDraftVersion() {
		// % protected region % [Add any additional logic before main process of saveDraftVersion here] off begin
		// % protected region % [Add any additional logic before main process of saveDraftVersion here] end

		// Update workflow version
		this.store.dispatch(new workflowVersionAction.WorkflowVersionAction(
			workflowVersionAction.WorkflowVersionModelActionTypes.UPDATE_WORKFLOW_VERSION,
			{
				targetModel: this.draftVersion,
				updates: this.modelChanges
			},
			[
				new routerAction.NavigateRoutingAction(['states'], {
					relativeTo: this.route.parent
				})
			]
		));

		// % protected region % [Add any additional logic after main process of saveDraftVersion here] off begin
		// % protected region % [Add any additional logic after main process of saveDraftVersion here] end
	}

	/**
	 * Validate the changed forms
	 * @return true if the form is valid
	 */
	validateForm(): boolean {
		Object.values(this.workflowDetailsForm.controls)
			.filter(control => !control.disabled)
			.forEach(formControl => formControl.updateValueAndValidity({emitEvent: true}));

		return this.workflowDetailsForm.valid;
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
