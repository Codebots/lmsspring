/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package lmsspring.validators;

import java.util.Set;
import java.util.stream.Stream;
import java.util.UUID;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import net.andreinc.mockneat.MockNeat;
import net.andreinc.mockneat.types.enums.StringType;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import lmsspring.entities.*;

import static org.junit.jupiter.api.Assertions.*;

@Tag("validation")
public class AttributeValidatorTest {

	private Validator validator;
	private MockNeat mockNeat;

	private Class getEntityClass(String entityName) throws Exception {
		switch (entityName) {
			case "Administrator":
				return AdministratorEntity.class;
			case "ApplicationLocale":
				return ApplicationLocaleEntity.class;
			case "Article":
				return ArticleEntity.class;
			case "Book":
				return BookEntity.class;
			case "CoreUser":
				return CoreUserEntity.class;
			case "Course":
				return CourseEntity.class;
			case "CourseCategory":
				return CourseCategoryEntity.class;
			case "CourseLesson":
				return CourseLessonEntity.class;
			case "Lesson":
				return LessonEntity.class;
			case "Tag":
				return TagEntity.class;
			case "Role":
				return RoleEntity.class;
			case "Privilege":
				return PrivilegeEntity.class;
			case "Workflow":
				return WorkflowEntity.class;
			case "WorkflowState":
				return WorkflowStateEntity.class;
			case "WorkflowTransition":
				return WorkflowTransitionEntity.class;
			case "WorkflowVersion":
				return WorkflowVersionEntity.class;
			case "LessonFormSubmission":
				return LessonFormSubmissionEntity.class;
			case "LessonFormVersion":
				return LessonFormVersionEntity.class;
			case "LessonFormTile":
				return LessonFormTileEntity.class;
			default:
				throw new Exception(entityName + "must be a valid entity");
		}
	}

	private String getAlphanumericString(int length) {
		return mockNeat.strings().size(length).type(StringType.valueOf("ALPHA_NUMERIC")).get();
	}

	@BeforeEach
	void setup() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
		//Multiple options are available for instantiating MockNeat.  MockNeat.threadLocal() is recommended in the docs
		mockNeat = MockNeat.threadLocal();
	}

	private static Stream<Arguments> requiredValidators() {
		return Stream.of(
				Arguments.of("ApplicationLocale", "locale"),
				Arguments.of("ApplicationLocale", "key"),
				Arguments.of("ApplicationLocale", "value"),
				Arguments.of("Article", "title"),
				Arguments.of("Article", "summary"),
				Arguments.of("Book", "name"),
				Arguments.of("CoreUser", "firstName"),
				Arguments.of("CoreUser", "lastName"),
				Arguments.of("Course", "name"),
				Arguments.of("CourseCategory", "name"),
				Arguments.of("Lesson", "summary"),
				Arguments.of("Lesson", "name"),
				Arguments.of("Tag", "name"),
				Arguments.of("Role", "name"),
				Arguments.of("Privilege", "name"),
				Arguments.of("Privilege", "targetEntity"),
				Arguments.of("Privilege", "allowCreate"),
				Arguments.of("Privilege", "allowRead"),
				Arguments.of("Privilege", "allowUpdate"),
				Arguments.of("Privilege", "allowDelete"),
				Arguments.of("Workflow", "name"),
				Arguments.of("WorkflowState", "stepName"),
				Arguments.of("WorkflowTransition", "transitionName"),
				Arguments.of("WorkflowVersion", "workflowName"),
				Arguments.of("LessonFormVersion", "version"),
				Arguments.of("LessonFormVersion", "formData")
		);
	}

	@ParameterizedTest(name = "Invalid Inputs to {1} attribute in {0} entity with required validator should trigger validation error")
	@MethodSource("requiredValidators")
	void testRequired(String entityName, String attributeName) throws Exception {
		assertFalse(validator.validateValue(this.getEntityClass(entityName), attributeName, null).isEmpty());
	}

}
