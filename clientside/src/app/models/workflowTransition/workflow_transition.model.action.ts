/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Action} from '@ngrx/store';
import {WorkflowTransitionModel} from './workflow_transition.model';
import {WorkflowTransitionModelAudit} from './workflow_transition.model.state';
import {PassableStateConfig} from '../../lib/services/http/interfaces';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * List of Workflow Transition model actions to be dispatched by NgRx.
 */
export enum WorkflowTransitionModelActionTypes {
	CREATE_WORKFLOW_TRANSITION = '[ENTITY] Create WorkflowTransitionModel',
	CREATE_WORKFLOW_TRANSITION_OK = '[ENTITY] Create WorkflowTransitionModel successfully',
	CREATE_WORKFLOW_TRANSITION_FAIL = '[ENTITY] Create WorkflowTransitionModel failed',

	CREATE_ALL_WORKFLOW_TRANSITION = '[ENTITY] Create All WorkflowTransitionModel',
	CREATE_ALL_WORKFLOW_TRANSITION_OK = '[ENTITY] Create All WorkflowTransitionModel successfully',
	CREATE_ALL_WORKFLOW_TRANSITION_FAIL = '[ENTITY] Create All WorkflowTransitionModel failed',

	DELETE_WORKFLOW_TRANSITION = '[ENTITY] Delete WorkflowTransitionModel',
	DELETE_WORKFLOW_TRANSITION_OK = '[ENTITY] Delete WorkflowTransitionModel successfully',
	DELETE_WORKFLOW_TRANSITION_FAIL = '[ENTITY] Delete WorkflowTransitionModel failed',


	DELETE_WORKFLOW_TRANSITION_EXCLUDING_IDS = '[ENTITY] Delete WorkflowTransitionModels Excluding Ids',
	DELETE_WORKFLOW_TRANSITION_EXCLUDING_IDS_OK = '[ENTITY] Delete WorkflowTransitionModels Excluding Ids successfully',
	DELETE_WORKFLOW_TRANSITION_EXCLUDING_IDS_FAIL = '[ENTITY] Delete WorkflowTransitionModels Excluding Ids failed',

	DELETE_ALL_WORKFLOW_TRANSITION = '[ENTITY] Delete all WorkflowTransitionModels',
	DELETE_ALL_WORKFLOW_TRANSITION_OK = '[ENTITY] Delete all WorkflowTransitionModels successfully',
	DELETE_ALL_WORKFLOW_TRANSITION_FAIL = '[ENTITY] Delete all WorkflowTransitionModels failed',

	UPDATE_WORKFLOW_TRANSITION = '[ENTITY] Update WorkflowTransitionModel',
	UPDATE_WORKFLOW_TRANSITION_OK = '[ENTITY] Update WorkflowTransitionModel successfully',
	UPDATE_WORKFLOW_TRANSITION_FAIL = '[ENTITY] Update WorkflowTransitionModel failed',

	UPDATE_ALL_WORKFLOW_TRANSITION = '[ENTITY] Update all WorkflowTransitionModel',
	UPDATE_ALL_WORKFLOW_TRANSITION_OK = '[ENTITY] Update all WorkflowTransitionModel successfully',
	UPDATE_ALL_WORKFLOW_TRANSITION_FAIL = '[ENTITY] Update all WorkflowTransitionModel failed',

	FETCH_WORKFLOW_TRANSITION= '[ENTITY] Fetch WorkflowTransitionModel',
	FETCH_WORKFLOW_TRANSITION_OK = '[ENTITY] Fetch WorkflowTransitionModel successfully',
	FETCH_WORKFLOW_TRANSITION_FAIL = '[ENTITY] Fetch WorkflowTransitionModel failed',

	FETCH_WORKFLOW_TRANSITION_AUDIT= '[ENTITY] Fetch WorkflowTransitionModel audit',
	FETCH_WORKFLOW_TRANSITION_AUDIT_OK = '[ENTITY] Fetch WorkflowTransitionModel audit successfully',
	FETCH_WORKFLOW_TRANSITION_AUDIT_FAIL = '[ENTITY] Fetch WorkflowTransitionModel audit failed',

	FETCH_WORKFLOW_TRANSITION_AUDITS_BY_ENTITY_ID= '[ENTITY] Fetch WorkflowTransitionModel audits by entity id',
	FETCH_WORKFLOW_TRANSITION_AUDITS_BY_ENTITY_ID_OK = '[ENTITY] Fetch WorkflowTransitionModel audits by entity id successfully',
	FETCH_WORKFLOW_TRANSITION_AUDITS_BY_ENTITY_ID_FAIL = '[ENTITY] Fetch WorkflowTransitionModel audits by entity id failed',

	FETCH_ALL_WORKFLOW_TRANSITION = '[ENTITY] Fetch all WorkflowTransitionModel',
	FETCH_ALL_WORKFLOW_TRANSITION_OK = '[ENTITY] Fetch all WorkflowTransitionModel successfully',
	FETCH_ALL_WORKFLOW_TRANSITION_FAIL = '[ENTITY] Fetch all WorkflowTransitionModel failed',

	FETCH_WORKFLOW_TRANSITION_WITH_QUERY = '[ENTITY] Fetch WorkflowTransitionModel with query',
	FETCH_WORKFLOW_TRANSITION_WITH_QUERY_OK = '[ENTITY] Fetch WorkflowTransitionModel with query successfully',
	FETCH_WORKFLOW_TRANSITION_WITH_QUERY_FAIL = '[ENTITY] Fetch WorkflowTransitionModel with query failed',

	FETCH_LAST_WORKFLOW_TRANSITION_WITH_QUERY = '[ENTITY] Fetch last WorkflowTransitionModel with query',
	FETCH_LAST_WORKFLOW_TRANSITION_WITH_QUERY_OK = '[ENTITY] Fetch last WorkflowTransitionModel with query successfully',
	FETCH_LAST_WORKFLOW_TRANSITION_WITH_QUERY_FAIL = '[ENTITY] Fetch last WorkflowTransitionModel with query failed',

	EXPORT_WORKFLOW_TRANSITION = '[ENTITY] Export WorkflowTransitionModel',
	EXPORT_WORKFLOW_TRANSITION_OK = '[ENTITY] Export WorkflowTransitionModel successfully',
	EXPORT_WORKFLOW_TRANSITION_FAIL = '[ENTITY] Export WorkflowTransitionModel failed',

	EXPORT_ALL_WORKFLOW_TRANSITION = '[ENTITY] Export All WorkflowTransitionModels',
	EXPORT_ALL_WORKFLOW_TRANSITION_OK = '[ENTITY] Export All WorkflowTransitionModels successfully',
	EXPORT_ALL_WORKFLOW_TRANSITION_FAIL = '[ENTITY] Export All WorkflowTransitionModels failed',

	EXPORT_WORKFLOW_TRANSITION_EXCLUDING_IDS = '[ENTITY] Export WorkflowTransitionModels excluding Ids',
	EXPORT_WORKFLOW_TRANSITION_EXCLUDING_IDS_OK = '[ENTITY] Export WorkflowTransitionModel excluding Ids successfully',
	EXPORT_WORKFLOW_TRANSITION_EXCLUDING_IDS_FAIL = '[ENTITY] Export WorkflowTransitionModel excluding Ids failed',

	COUNT_WORKFLOW_TRANSITIONS = '[ENTITY] Fetch number of WorkflowTransitionModel records',
	COUNT_WORKFLOW_TRANSITIONS_OK = '[ENTITY] Fetch number of WorkflowTransitionModel records successfully ',
	COUNT_WORKFLOW_TRANSITIONS_FAIL = '[ENTITY] Fetch number of WorkflowTransitionModel records failed',

	IMPORT_WORKFLOW_TRANSITIONS = '[ENTITY] Import WorkflowTransitionModels',
	IMPORT_WORKFLOW_TRANSITIONS_OK = '[ENTITY] Import WorkflowTransitionModels successfully',
	IMPORT_WORKFLOW_TRANSITIONS_FAIL = '[ENTITY] Import WorkflowTransitionModels fail',


	INITIALISE_WORKFLOW_TRANSITION_COLLECTION_STATE = '[ENTITY] Initialize the CollectionState of WorkflowTransitionModel',
	// % protected region % [Add any additional model actions here] off begin
	// % protected region % [Add any additional model actions here] end
}

export abstract class BaseWorkflowTransitionAction implements Action {
	readonly className: string = 'WorkflowTransitionModel';

	abstract readonly type: string;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	protected constructor(
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
		public readonly afterwardActions: Action[] = []
	) {
		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}
}

export class WorkflowTransitionAction extends BaseWorkflowTransitionAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for WorkflowTransitionAction here] off begin
	// % protected region % [Add any additional class fields for WorkflowTransitionAction here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<WorkflowTransitionModel>,
		// % protected region % [Add any additional constructor parameters for WorkflowTransitionAction here] off begin
		// % protected region % [Add any additional constructor parameters for WorkflowTransitionAction here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for WorkflowTransitionAction here] off begin
			// % protected region % [Add any additional constructor arguments for WorkflowTransitionAction here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for WorkflowTransitionAction here] off begin
		// % protected region % [Add any additional constructor logic for WorkflowTransitionAction here] end
	}

	// % protected region % [Add any additional class methods for WorkflowTransitionAction here] off begin
	// % protected region % [Add any additional class methods for WorkflowTransitionAction here] end
}

export class WorkflowTransitionActionOK extends BaseWorkflowTransitionAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for WorkflowTransitionActionOK here] off begin
	// % protected region % [Add any additional class fields for WorkflowTransitionActionOK here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<WorkflowTransitionModel>,
		// % protected region % [Add any additional constructor parameters for WorkflowTransitionActionOK here] off begin
		// % protected region % [Add any additional constructor parameters for WorkflowTransitionActionOK here] end
		afterwardActions: Action[] = [],
		public audits?: WorkflowTransitionModelAudit[],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for WorkflowTransitionActionOK here] off begin
			// % protected region % [Add any additional constructor arguments for WorkflowTransitionActionOK here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for WorkflowTransitionActionOK here] off begin
		// % protected region % [Add any additional constructor logic for WorkflowTransitionActionOK here] end
	}

	// % protected region % [Add any additional class methods for WorkflowTransitionActionOK here] off begin
	// % protected region % [Add any additional class methods for WorkflowTransitionActionOK here] end
}

export class WorkflowTransitionActionFail extends BaseWorkflowTransitionAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for WorkflowTransitionActionFail here] off begin
	// % protected region % [Add any additional class fields for WorkflowTransitionActionFail here] end

	public constructor(
		public typeInput: string,
		public readonly error?: any,
		public stateConfig?: PassableStateConfig<WorkflowTransitionModel>,
		// % protected region % [Add any additional constructor parameters for WorkflowTransitionActionFail here] off begin
		// % protected region % [Add any additional constructor parameters for WorkflowTransitionActionFail here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for WorkflowTransitionActionFail here] off begin
			// % protected region % [Add any additional constructor arguments for WorkflowTransitionActionFail here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for WorkflowTransitionActionFail here] off begin
		// % protected region % [Add any additional constructor logic for WorkflowTransitionActionFail here] end
	}

	// % protected region % [Add any additional class methods for WorkflowTransitionActionFail here] off begin
	// % protected region % [Add any additional class methods for WorkflowTransitionActionFail here] end
}

export function isWorkflowTransitionModelAction(e: any): e is BaseWorkflowTransitionAction {
	return Object.values(WorkflowTransitionModelActionTypes).includes(e);
}

// % protected region % [Add any additional actions here] off begin
// % protected region % [Add any additional actions here] end
