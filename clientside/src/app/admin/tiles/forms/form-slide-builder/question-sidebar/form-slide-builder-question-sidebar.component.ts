/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {
	Component, ComponentFactoryResolver,
	EventEmitter,
	Input, OnChanges,
	Output, SimpleChanges,
	Type,
	ViewChild,
	ViewContainerRef
} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Subject} from 'rxjs';
import {AbstractComponent} from 'src/app/lib/components/abstract.component';
import {
	ButtonStyle,
	ButtonSize,
	IconPosition,
	ButtonAccentColour
} from 'src/app/lib/components/button/button.component';
import {TextfieldType} from 'src/app/lib/components/textfield/textfield.component';
import {FormQuestionData} from 'src/app/lib/behaviours/form/form-interfaces';
import * as _ from 'lodash';
import {allQuestionTypes, getQuestionConfig} from 'src/app/lib/behaviours/form/form-utils';
import {FormCustomQuestionConfigComponent} from 'src/app/lib/behaviours/form/form-questions/form-custom-question/config/form-custom-question-config.component';
import {ButtonGroupAlignment} from 'src/app/lib/components/buttonGroup/button.group.component';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * Sidebar in Form slide builder to edit different types of question
 */
@Component({
	selector: '*[cb-form-slide-builder-question-sidebar]',
	templateUrl: './form-slide-builder-question-sidebar.component.html',
	// % protected region % [Add any additional configurations here] off begin
	// % protected region % [Add any additional configurations here] end
})
export class FormSlideBuilderQuestionSidebarComponent extends AbstractComponent implements OnChanges {
	readonly TextfieldType = TextfieldType;
	readonly IconPosition = IconPosition;
	readonly ButtonSize = ButtonSize;
	readonly ButtonStyle = ButtonStyle;
	readonly ButtonAccentColour = ButtonAccentColour;
	readonly ButtonGroupAlignment = ButtonGroupAlignment;

	@Output()
	close: EventEmitter<null> = new EventEmitter();

	@Input()
	question: FormQuestionData;

	/**
	 * Subject to trigger when question type is changed
	 * Trigger event to re-render the question in the slide
	 */
	@Input()
	questionTypeSubject: Subject<string>;

	/**
	 * All available question types
	 */
	allQuestionTypes: any[] = allQuestionTypes;

	/**
	 * Question type of current question
	 */
	questionType: FormControl;

	/**
	 * The previous question data before save button clicked
	 */
	previousQuestionData: FormQuestionData;

	@ViewChild('questionConfig', {static: true, read: ViewContainerRef})
	questionConfigContainer: ViewContainerRef;

	// % protected region % [Add any additional class properties here] off begin
	// % protected region % [Add any additional class properties here] end

	constructor(
		private readonly componentFactoryResolver: ComponentFactoryResolver,
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
	) {
		super();
		this.questionType = new FormControl('');
		this.questionType.valueChanges.subscribe(value => {
			if (value !== this.question.type) {
				this.question.type = value;
				this.question.options = {};
				this.loadQuestionConfig(this.question);
				this.questionTypeSubject.next(this.question.type);
			}
		});

		// % protected region % [Add any additional logic for constructor here] off begin
		// % protected region % [Add any additional logic for constructor here] end
	}

	/**
	 * @inheritDoc
	 */
	ngOnChanges(changes: SimpleChanges): void {
		if (changes.hasOwnProperty('question')) {
			this.previousQuestionData = _.cloneDeep(this.question);
			this.questionType.patchValue(this.question.type, {emitEvent: false});
			this.loadQuestionConfig(this.question);
		}

		// % protected region % [Add any additional logic for ngOnChanges here] off begin
		// % protected region % [Add any additional logic for ngOnChanges here] end
	}

	/**
	 * Load question config to display in the sidebar
	 * TODO extract this part of function separately
	 */
	private loadQuestionConfig(questionData: FormQuestionData) {

		// % protected region % [Add any additional logic before main body of loadQuestionConfig here] off begin
		// % protected region % [Add any additional logic before main body of loadQuestionConfig here] end

		// Create new form question config component dynamically and then load that into the slot marked by FormSlideDirective
		const configComponentType: Type<any> = getQuestionConfig(questionData.type);
		const componentFactory = this.componentFactoryResolver.resolveComponentFactory(configComponentType);
		const viewContainerRef = this.questionConfigContainer;
		viewContainerRef.clear();
		const componentRef = viewContainerRef.createComponent(componentFactory);
		const component: FormCustomQuestionConfigComponent = componentRef.instance as FormCustomQuestionConfigComponent;

		component.data = this.question;
		component.parseData();

		componentRef.changeDetectorRef.detectChanges();

		// % protected region % [Add any additional logic after main body of loadQuestionConfig here] off begin
		// % protected region % [Add any additional logic after main body of loadQuestionConfig here] end
	}

	/**
	 * Event triggered when save button clicked
	 */
	onSaveButtonClicked() {
		// % protected region % [Add any additional logic before main body of onSaveButtonClicked here] off begin
		// % protected region % [Add any additional logic before main body of onSaveButtonClicked here] end

		this.close.emit(null);

		// % protected region % [Add any additional logic after main body of onSaveButtonClicked here] off begin
		// % protected region % [Add any additional logic after main body of onSaveButtonClicked here] end
	}

	/**
	 * Event triggered when cancel button clicked
	 */
	onCloseButtonClicked() {
		// % protected region % [Add any additional logic before main body of onCloseButtonClicked here] off begin
		// % protected region % [Add any additional logic before main body of onCloseButtonClicked here] end

		Object.assign(this.question, this.previousQuestionData);
		this.questionTypeSubject.next(this.question.type);
		this.close.emit(null);

		// % protected region % [Add any additional logic after main body of onCloseButtonClicked here] off begin
		// % protected region % [Add any additional logic after main body of onCloseButtonClicked here] end
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
