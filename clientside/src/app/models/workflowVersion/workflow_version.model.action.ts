/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Action} from '@ngrx/store';
import {WorkflowVersionModel} from './workflow_version.model';
import {WorkflowVersionModelAudit} from './workflow_version.model.state';
import {PassableStateConfig} from '../../lib/services/http/interfaces';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * List of Workflow Version model actions to be dispatched by NgRx.
 */
export enum WorkflowVersionModelActionTypes {
	CREATE_WORKFLOW_VERSION = '[ENTITY] Create WorkflowVersionModel',
	CREATE_WORKFLOW_VERSION_OK = '[ENTITY] Create WorkflowVersionModel successfully',
	CREATE_WORKFLOW_VERSION_FAIL = '[ENTITY] Create WorkflowVersionModel failed',

	CREATE_ALL_WORKFLOW_VERSION = '[ENTITY] Create All WorkflowVersionModel',
	CREATE_ALL_WORKFLOW_VERSION_OK = '[ENTITY] Create All WorkflowVersionModel successfully',
	CREATE_ALL_WORKFLOW_VERSION_FAIL = '[ENTITY] Create All WorkflowVersionModel failed',

	DELETE_WORKFLOW_VERSION = '[ENTITY] Delete WorkflowVersionModel',
	DELETE_WORKFLOW_VERSION_OK = '[ENTITY] Delete WorkflowVersionModel successfully',
	DELETE_WORKFLOW_VERSION_FAIL = '[ENTITY] Delete WorkflowVersionModel failed',


	DELETE_WORKFLOW_VERSION_EXCLUDING_IDS = '[ENTITY] Delete WorkflowVersionModels Excluding Ids',
	DELETE_WORKFLOW_VERSION_EXCLUDING_IDS_OK = '[ENTITY] Delete WorkflowVersionModels Excluding Ids successfully',
	DELETE_WORKFLOW_VERSION_EXCLUDING_IDS_FAIL = '[ENTITY] Delete WorkflowVersionModels Excluding Ids failed',

	DELETE_ALL_WORKFLOW_VERSION = '[ENTITY] Delete all WorkflowVersionModels',
	DELETE_ALL_WORKFLOW_VERSION_OK = '[ENTITY] Delete all WorkflowVersionModels successfully',
	DELETE_ALL_WORKFLOW_VERSION_FAIL = '[ENTITY] Delete all WorkflowVersionModels failed',

	UPDATE_WORKFLOW_VERSION = '[ENTITY] Update WorkflowVersionModel',
	UPDATE_WORKFLOW_VERSION_OK = '[ENTITY] Update WorkflowVersionModel successfully',
	UPDATE_WORKFLOW_VERSION_FAIL = '[ENTITY] Update WorkflowVersionModel failed',

	UPDATE_ALL_WORKFLOW_VERSION = '[ENTITY] Update all WorkflowVersionModel',
	UPDATE_ALL_WORKFLOW_VERSION_OK = '[ENTITY] Update all WorkflowVersionModel successfully',
	UPDATE_ALL_WORKFLOW_VERSION_FAIL = '[ENTITY] Update all WorkflowVersionModel failed',

	FETCH_WORKFLOW_VERSION= '[ENTITY] Fetch WorkflowVersionModel',
	FETCH_WORKFLOW_VERSION_OK = '[ENTITY] Fetch WorkflowVersionModel successfully',
	FETCH_WORKFLOW_VERSION_FAIL = '[ENTITY] Fetch WorkflowVersionModel failed',

	FETCH_WORKFLOW_VERSION_AUDIT= '[ENTITY] Fetch WorkflowVersionModel audit',
	FETCH_WORKFLOW_VERSION_AUDIT_OK = '[ENTITY] Fetch WorkflowVersionModel audit successfully',
	FETCH_WORKFLOW_VERSION_AUDIT_FAIL = '[ENTITY] Fetch WorkflowVersionModel audit failed',

	FETCH_WORKFLOW_VERSION_AUDITS_BY_ENTITY_ID= '[ENTITY] Fetch WorkflowVersionModel audits by entity id',
	FETCH_WORKFLOW_VERSION_AUDITS_BY_ENTITY_ID_OK = '[ENTITY] Fetch WorkflowVersionModel audits by entity id successfully',
	FETCH_WORKFLOW_VERSION_AUDITS_BY_ENTITY_ID_FAIL = '[ENTITY] Fetch WorkflowVersionModel audits by entity id failed',

	FETCH_ALL_WORKFLOW_VERSION = '[ENTITY] Fetch all WorkflowVersionModel',
	FETCH_ALL_WORKFLOW_VERSION_OK = '[ENTITY] Fetch all WorkflowVersionModel successfully',
	FETCH_ALL_WORKFLOW_VERSION_FAIL = '[ENTITY] Fetch all WorkflowVersionModel failed',

	FETCH_WORKFLOW_VERSION_WITH_QUERY = '[ENTITY] Fetch WorkflowVersionModel with query',
	FETCH_WORKFLOW_VERSION_WITH_QUERY_OK = '[ENTITY] Fetch WorkflowVersionModel with query successfully',
	FETCH_WORKFLOW_VERSION_WITH_QUERY_FAIL = '[ENTITY] Fetch WorkflowVersionModel with query failed',

	FETCH_LAST_WORKFLOW_VERSION_WITH_QUERY = '[ENTITY] Fetch last WorkflowVersionModel with query',
	FETCH_LAST_WORKFLOW_VERSION_WITH_QUERY_OK = '[ENTITY] Fetch last WorkflowVersionModel with query successfully',
	FETCH_LAST_WORKFLOW_VERSION_WITH_QUERY_FAIL = '[ENTITY] Fetch last WorkflowVersionModel with query failed',

	EXPORT_WORKFLOW_VERSION = '[ENTITY] Export WorkflowVersionModel',
	EXPORT_WORKFLOW_VERSION_OK = '[ENTITY] Export WorkflowVersionModel successfully',
	EXPORT_WORKFLOW_VERSION_FAIL = '[ENTITY] Export WorkflowVersionModel failed',

	EXPORT_ALL_WORKFLOW_VERSION = '[ENTITY] Export All WorkflowVersionModels',
	EXPORT_ALL_WORKFLOW_VERSION_OK = '[ENTITY] Export All WorkflowVersionModels successfully',
	EXPORT_ALL_WORKFLOW_VERSION_FAIL = '[ENTITY] Export All WorkflowVersionModels failed',

	EXPORT_WORKFLOW_VERSION_EXCLUDING_IDS = '[ENTITY] Export WorkflowVersionModels excluding Ids',
	EXPORT_WORKFLOW_VERSION_EXCLUDING_IDS_OK = '[ENTITY] Export WorkflowVersionModel excluding Ids successfully',
	EXPORT_WORKFLOW_VERSION_EXCLUDING_IDS_FAIL = '[ENTITY] Export WorkflowVersionModel excluding Ids failed',

	COUNT_WORKFLOW_VERSIONS = '[ENTITY] Fetch number of WorkflowVersionModel records',
	COUNT_WORKFLOW_VERSIONS_OK = '[ENTITY] Fetch number of WorkflowVersionModel records successfully ',
	COUNT_WORKFLOW_VERSIONS_FAIL = '[ENTITY] Fetch number of WorkflowVersionModel records failed',

	IMPORT_WORKFLOW_VERSIONS = '[ENTITY] Import WorkflowVersionModels',
	IMPORT_WORKFLOW_VERSIONS_OK = '[ENTITY] Import WorkflowVersionModels successfully',
	IMPORT_WORKFLOW_VERSIONS_FAIL = '[ENTITY] Import WorkflowVersionModels fail',


	INITIALISE_WORKFLOW_VERSION_COLLECTION_STATE = '[ENTITY] Initialize the CollectionState of WorkflowVersionModel',
	// % protected region % [Add any additional model actions here] off begin
	// % protected region % [Add any additional model actions here] end
}

export abstract class BaseWorkflowVersionAction implements Action {
	readonly className: string = 'WorkflowVersionModel';

	abstract readonly type: string;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	protected constructor(
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
		public readonly afterwardActions: Action[] = []
	) {
		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}
}

export class WorkflowVersionAction extends BaseWorkflowVersionAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for WorkflowVersionAction here] off begin
	// % protected region % [Add any additional class fields for WorkflowVersionAction here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<WorkflowVersionModel>,
		// % protected region % [Add any additional constructor parameters for WorkflowVersionAction here] off begin
		// % protected region % [Add any additional constructor parameters for WorkflowVersionAction here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for WorkflowVersionAction here] off begin
			// % protected region % [Add any additional constructor arguments for WorkflowVersionAction here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for WorkflowVersionAction here] off begin
		// % protected region % [Add any additional constructor logic for WorkflowVersionAction here] end
	}

	// % protected region % [Add any additional class methods for WorkflowVersionAction here] off begin
	// % protected region % [Add any additional class methods for WorkflowVersionAction here] end
}

export class WorkflowVersionActionOK extends BaseWorkflowVersionAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for WorkflowVersionActionOK here] off begin
	// % protected region % [Add any additional class fields for WorkflowVersionActionOK here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<WorkflowVersionModel>,
		// % protected region % [Add any additional constructor parameters for WorkflowVersionActionOK here] off begin
		// % protected region % [Add any additional constructor parameters for WorkflowVersionActionOK here] end
		afterwardActions: Action[] = [],
		public audits?: WorkflowVersionModelAudit[],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for WorkflowVersionActionOK here] off begin
			// % protected region % [Add any additional constructor arguments for WorkflowVersionActionOK here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for WorkflowVersionActionOK here] off begin
		// % protected region % [Add any additional constructor logic for WorkflowVersionActionOK here] end
	}

	// % protected region % [Add any additional class methods for WorkflowVersionActionOK here] off begin
	// % protected region % [Add any additional class methods for WorkflowVersionActionOK here] end
}

export class WorkflowVersionActionFail extends BaseWorkflowVersionAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for WorkflowVersionActionFail here] off begin
	// % protected region % [Add any additional class fields for WorkflowVersionActionFail here] end

	public constructor(
		public typeInput: string,
		public readonly error?: any,
		public stateConfig?: PassableStateConfig<WorkflowVersionModel>,
		// % protected region % [Add any additional constructor parameters for WorkflowVersionActionFail here] off begin
		// % protected region % [Add any additional constructor parameters for WorkflowVersionActionFail here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for WorkflowVersionActionFail here] off begin
			// % protected region % [Add any additional constructor arguments for WorkflowVersionActionFail here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for WorkflowVersionActionFail here] off begin
		// % protected region % [Add any additional constructor logic for WorkflowVersionActionFail here] end
	}

	// % protected region % [Add any additional class methods for WorkflowVersionActionFail here] off begin
	// % protected region % [Add any additional class methods for WorkflowVersionActionFail here] end
}

export function isWorkflowVersionModelAction(e: any): e is BaseWorkflowVersionAction {
	return Object.values(WorkflowVersionModelActionTypes).includes(e);
}

// % protected region % [Add any additional actions here] off begin
// % protected region % [Add any additional actions here] end
