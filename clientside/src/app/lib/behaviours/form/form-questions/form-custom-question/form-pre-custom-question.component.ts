/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Component, Input} from '@angular/core';
import {FormQuestionData} from '../../form-interfaces';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * Fragment component to be displayed before the main content of the question component. Mainly used to centralise all
 * pre-question logic into one place.
 */
@Component({
	selector: '*[cb-form-pre-question]',
	template: `
	<p class='question__content'>{{ data.questionContent ? data.questionContent : 'Click on options to change question details' }}</p>
	<p *ngIf="data.questionSubtext" class='question__sub-text'>{{ data.questionSubtext }}</p>
	`
})
export class FormPreCustomQuestionComponent {
	@Input()
	data: FormQuestionData;

	// % protected region % [Add any additional class properties here] off begin
	// % protected region % [Add any additional class properties here] end

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
