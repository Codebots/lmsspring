/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Component, EventEmitter, Input, Output, HostBinding} from '@angular/core';
import {AbstractComponent} from '../../../../components/abstract.component';
import {FormMode} from '../../form-interfaces';
import {FormQuestionData} from '../../form-interfaces';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * Base class for all question components, also acts as a custom question component that can be instantiated and used
 * standalone. AngularBot by default provides multiple common question components that can be found under `components`
 * directory. Unlike most other regular Angular component, question components are meant to be loaded dynamically, hence
 * inputs are usually passed in as data rather than typical Angular's @Input decorator.
 */
@Component({
	selector: 'div[cb-form-textfield-question]',
	templateUrl: './form-custom-question.component.html',
	// % protected region % [Add any additional component configurations here] off begin
	// % protected region % [Add any additional component configurations here] end
})
export class FormCustomQuestionComponent extends AbstractComponent {
	/**
	 * Default host class.
	 */
	@HostBinding('class')
	get formQuestionContainerCSSClass() {
		return [
			'form__question-container',
			// % protected region % [Add any additional host classes here] off begin
			// % protected region % [Add any additional host classes here] end
		].join(' ');
	}

	questionData: FormQuestionData;

	/**
	 * Payload given when loaded with relevant data.
	 */
	@Input()
	set data(data: FormQuestionData) {
		this.questionData = data;
		this.parseData();
	}

	get data(): FormQuestionData {
		return this.questionData;
	}

	@Input()
	formMode: FormMode;

	/**
	 * Event emitter for question edit requests.
	 */
	@Output('edit')
	editEmitter = new EventEmitter<FormQuestionData>();

	/**
	 * Event emitter for question duplicate requests.
	 */
	@Output('duplicate')
	duplicateEmitter = new EventEmitter<FormQuestionData>();

	/**
	 * Event emitter for question delete requests.
	 */
	@Output('delete')
	deleteEmitter = new EventEmitter<FormQuestionData>();

	/**
	 * Event emitter for moving question up / down
	 */
	@Output('changeOrder')
	changeOrderEmitter = new EventEmitter<{change: number, question: FormQuestionData}>();

	// % protected region % [Add any additional class properties here] off begin
	// % protected region % [Add any additional class properties here] end

	/**
	 * Triggered when the user wants to edit the current question.
	 */
	onEditQuestion() {
		// % protected region % [Add any additional logic for onEditQuestion before the main body here] off begin
		// % protected region % [Add any additional logic for onEditQuestion before the main body here] end

		this.editEmitter.emit(this.data);

		// % protected region % [Add any additional logic for onEditQuestion after the main body here] off begin
		// % protected region % [Add any additional logic for onEditQuestion after the main body here] end
	}

	/**
	 * Triggered when the user wants to duplicate the current question.
	 */
	onDuplicateQuestion() {
		// % protected region % [Add any additional logic for onDuplicateQuestion before the main body here] off begin
		// % protected region % [Add any additional logic for onDuplicateQuestion before the main body here] end

		this.duplicateEmitter.emit(this.data);

		// % protected region % [Add any additional logic for onDuplicateQuestion after the main body here] off begin
		// % protected region % [Add any additional logic for onDuplicateQuestion after the main body here] end
	}

	/**
	 * Triggered when the user wants to delete the current question
	 */
	onDeleteQuestion() {
		// % protected region % [Add any additional logic for onDuplicateQuestion before the main body here] off begin
		// % protected region % [Add any additional logic for onDuplicateQuestion before the main body here] end

		this.deleteEmitter.emit(this.data);

		// % protected region % [Add any additional logic for onDuplicateQuestion after the main body here] off begin
		// % protected region % [Add any additional logic for onDuplicateQuestion after the main body here] end
	}

	/**
	 * Triggered when the user wants to move up / move down the current question
	 */
	onOrderChange(change) {
		this.changeOrderEmitter.emit({
			question: this.data,
			change: change
		});
	}

	/**
	 * Parse and process Question Data to display
	 */
	parseData() {
		// % protected region % [Add any additional logic for parseData here] off begin
		// % protected region % [Add any additional logic for parseData here] end
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
