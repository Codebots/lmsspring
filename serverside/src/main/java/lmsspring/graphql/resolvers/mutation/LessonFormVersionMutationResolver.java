/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package lmsspring.graphql.resolvers.mutation;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import lmsspring.entities.LessonFormVersionEntity;
import lmsspring.services.LessonFormVersionService;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Slf4j
@Component
public class LessonFormVersionMutationResolver implements GraphQLMutationResolver {

	private final LessonFormVersionService lessonFormVersionService;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	@Autowired
	public LessonFormVersionMutationResolver(
			// % protected region % [Add any additional constructor parameters here] off begin
			// % protected region % [Add any additional constructor parameters here] end
			LessonFormVersionService lessonFormVersionService
	) {
		// % protected region % [Add any additional constructor logic before the main body here] off begin
		// % protected region % [Add any additional constructor logic before the main body here] end

		this.lessonFormVersionService = lessonFormVersionService;

		// % protected region % [Add any additional constructor logic after the main body here] off begin
		// % protected region % [Add any additional constructor logic after the main body here] end
	}

	/**
	 * Persist the given entity into the database.
	 *
	 * @param rawEntity the entity before persistence
	 * @return the entity after persistence
	 */
	@PreAuthorize("hasPermission('LessonFormVersionEntity', 'create')")
	public LessonFormVersionEntity createLessonFormVersion(@NonNull LessonFormVersionEntity rawEntity) {
		// % protected region % [Add any additional logic for create before creating the new entity here] off begin
		// % protected region % [Add any additional logic for create before creating the new entity here] end

		LessonFormVersionEntity newEntity = lessonFormVersionService.create(rawEntity);

		// % protected region % [Add any additional logic for create before returning the newly created entity here] off begin
		// % protected region % [Add any additional logic for create before returning the newly created entity here] end

		return newEntity;
	}

	/**
	 * Similar to {@link LessonFormVersionMutationResolver#createLessonFormVersion(LessonFormVersionEntity)}
	 * but with multiple entities at once.
	 */
	@PreAuthorize("hasPermission('LessonFormVersionEntity', 'create')")
	public List<LessonFormVersionEntity> createAllLessonFormVersion(@NonNull List<LessonFormVersionEntity> rawEntities) {
		List<LessonFormVersionEntity> newEntities = Lists.newArrayList(lessonFormVersionService.createAll(rawEntities));

		// % protected region % [Add any additional logic for update before returning the created entities here] off begin
		// % protected region % [Add any additional logic for update before returning the created entities here] end

		return newEntities;
	}

	/**
	 * Persist the given entity into the database.
	 *
	 * @param rawEntity the entity before persistence
	 * @return the entity after persistence
	 */
	@PreAuthorize("hasPermission('LessonFormVersionEntity', 'update')")
	public LessonFormVersionEntity updateLessonFormVersion(@NonNull LessonFormVersionEntity rawEntity) {
		LessonFormVersionEntity entityFromDb = lessonFormVersionService.findById(rawEntity.getId()).orElseThrow();
		rawEntity.setCreatedBy(entityFromDb.getCreatedBy());
		LessonFormVersionEntity newEntity = lessonFormVersionService.update(rawEntity);

		// % protected region % [Add any additional logic for create before returning the newly updated entity here] off begin
		// % protected region % [Add any additional logic for create before returning the newly updated entity here] end

		return newEntity;
	}

	/**
	 * Similar to {@link LessonFormVersionMutationResolver#updateLessonFormVersion(LessonFormVersionEntity)}
	 * but with multiple entities at once.
	 */
	@PreAuthorize("hasPermission('LessonFormVersionEntity', 'update')")
	public List<LessonFormVersionEntity> updateAllLessonFormVersion(@NonNull List<LessonFormVersionEntity> rawEntities) {
		List<LessonFormVersionEntity> newEntities = Lists.newArrayList(lessonFormVersionService.updateAll(rawEntities));

		// % protected region % [Add any additional logic for update before returning the updated entities here] off begin
		// % protected region % [Add any additional logic for update before returning the updated entities here] end

		return newEntities;
	}



	// % protected region % [Import any additional class methods here] off begin
	// % protected region % [Import any additional class methods here] end
}
