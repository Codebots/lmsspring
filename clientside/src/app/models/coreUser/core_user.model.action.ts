/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Action} from '@ngrx/store';
import {CoreUserModel} from './core_user.model';
import {CoreUserModelAudit} from './core_user.model.state';
import {PassableStateConfig} from '../../lib/services/http/interfaces';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * List of Core User model actions to be dispatched by NgRx.
 */
export enum CoreUserModelActionTypes {
	CREATE_CORE_USER = '[ENTITY] Create CoreUserModel',
	CREATE_CORE_USER_OK = '[ENTITY] Create CoreUserModel successfully',
	CREATE_CORE_USER_FAIL = '[ENTITY] Create CoreUserModel failed',

	CREATE_ALL_CORE_USER = '[ENTITY] Create All CoreUserModel',
	CREATE_ALL_CORE_USER_OK = '[ENTITY] Create All CoreUserModel successfully',
	CREATE_ALL_CORE_USER_FAIL = '[ENTITY] Create All CoreUserModel failed',

	DELETE_CORE_USER = '[ENTITY] Delete CoreUserModel',
	DELETE_CORE_USER_OK = '[ENTITY] Delete CoreUserModel successfully',
	DELETE_CORE_USER_FAIL = '[ENTITY] Delete CoreUserModel failed',


	DELETE_CORE_USER_EXCLUDING_IDS = '[ENTITY] Delete CoreUserModels Excluding Ids',
	DELETE_CORE_USER_EXCLUDING_IDS_OK = '[ENTITY] Delete CoreUserModels Excluding Ids successfully',
	DELETE_CORE_USER_EXCLUDING_IDS_FAIL = '[ENTITY] Delete CoreUserModels Excluding Ids failed',

	DELETE_ALL_CORE_USER = '[ENTITY] Delete all CoreUserModels',
	DELETE_ALL_CORE_USER_OK = '[ENTITY] Delete all CoreUserModels successfully',
	DELETE_ALL_CORE_USER_FAIL = '[ENTITY] Delete all CoreUserModels failed',

	UPDATE_CORE_USER = '[ENTITY] Update CoreUserModel',
	UPDATE_CORE_USER_OK = '[ENTITY] Update CoreUserModel successfully',
	UPDATE_CORE_USER_FAIL = '[ENTITY] Update CoreUserModel failed',

	UPDATE_ALL_CORE_USER = '[ENTITY] Update all CoreUserModel',
	UPDATE_ALL_CORE_USER_OK = '[ENTITY] Update all CoreUserModel successfully',
	UPDATE_ALL_CORE_USER_FAIL = '[ENTITY] Update all CoreUserModel failed',

	FETCH_CORE_USER= '[ENTITY] Fetch CoreUserModel',
	FETCH_CORE_USER_OK = '[ENTITY] Fetch CoreUserModel successfully',
	FETCH_CORE_USER_FAIL = '[ENTITY] Fetch CoreUserModel failed',

	FETCH_CORE_USER_AUDIT= '[ENTITY] Fetch CoreUserModel audit',
	FETCH_CORE_USER_AUDIT_OK = '[ENTITY] Fetch CoreUserModel audit successfully',
	FETCH_CORE_USER_AUDIT_FAIL = '[ENTITY] Fetch CoreUserModel audit failed',

	FETCH_CORE_USER_AUDITS_BY_ENTITY_ID= '[ENTITY] Fetch CoreUserModel audits by entity id',
	FETCH_CORE_USER_AUDITS_BY_ENTITY_ID_OK = '[ENTITY] Fetch CoreUserModel audits by entity id successfully',
	FETCH_CORE_USER_AUDITS_BY_ENTITY_ID_FAIL = '[ENTITY] Fetch CoreUserModel audits by entity id failed',

	FETCH_ALL_CORE_USER = '[ENTITY] Fetch all CoreUserModel',
	FETCH_ALL_CORE_USER_OK = '[ENTITY] Fetch all CoreUserModel successfully',
	FETCH_ALL_CORE_USER_FAIL = '[ENTITY] Fetch all CoreUserModel failed',

	FETCH_CORE_USER_WITH_QUERY = '[ENTITY] Fetch CoreUserModel with query',
	FETCH_CORE_USER_WITH_QUERY_OK = '[ENTITY] Fetch CoreUserModel with query successfully',
	FETCH_CORE_USER_WITH_QUERY_FAIL = '[ENTITY] Fetch CoreUserModel with query failed',

	FETCH_LAST_CORE_USER_WITH_QUERY = '[ENTITY] Fetch last CoreUserModel with query',
	FETCH_LAST_CORE_USER_WITH_QUERY_OK = '[ENTITY] Fetch last CoreUserModel with query successfully',
	FETCH_LAST_CORE_USER_WITH_QUERY_FAIL = '[ENTITY] Fetch last CoreUserModel with query failed',

	EXPORT_CORE_USER = '[ENTITY] Export CoreUserModel',
	EXPORT_CORE_USER_OK = '[ENTITY] Export CoreUserModel successfully',
	EXPORT_CORE_USER_FAIL = '[ENTITY] Export CoreUserModel failed',

	EXPORT_ALL_CORE_USER = '[ENTITY] Export All CoreUserModels',
	EXPORT_ALL_CORE_USER_OK = '[ENTITY] Export All CoreUserModels successfully',
	EXPORT_ALL_CORE_USER_FAIL = '[ENTITY] Export All CoreUserModels failed',

	EXPORT_CORE_USER_EXCLUDING_IDS = '[ENTITY] Export CoreUserModels excluding Ids',
	EXPORT_CORE_USER_EXCLUDING_IDS_OK = '[ENTITY] Export CoreUserModel excluding Ids successfully',
	EXPORT_CORE_USER_EXCLUDING_IDS_FAIL = '[ENTITY] Export CoreUserModel excluding Ids failed',

	COUNT_CORE_USERS = '[ENTITY] Fetch number of CoreUserModel records',
	COUNT_CORE_USERS_OK = '[ENTITY] Fetch number of CoreUserModel records successfully ',
	COUNT_CORE_USERS_FAIL = '[ENTITY] Fetch number of CoreUserModel records failed',

	IMPORT_CORE_USERS = '[ENTITY] Import CoreUserModels',
	IMPORT_CORE_USERS_OK = '[ENTITY] Import CoreUserModels successfully',
	IMPORT_CORE_USERS_FAIL = '[ENTITY] Import CoreUserModels fail',


	INITIALISE_CORE_USER_COLLECTION_STATE = '[ENTITY] Initialize the CollectionState of CoreUserModel',
	// % protected region % [Add any additional model actions here] off begin
	// % protected region % [Add any additional model actions here] end
}

export abstract class BaseCoreUserAction implements Action {
	readonly className: string = 'CoreUserModel';

	abstract readonly type: string;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	protected constructor(
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
		public readonly afterwardActions: Action[] = []
	) {
		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}
}

export class CoreUserAction extends BaseCoreUserAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for CoreUserAction here] off begin
	// % protected region % [Add any additional class fields for CoreUserAction here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<CoreUserModel>,
		// % protected region % [Add any additional constructor parameters for CoreUserAction here] off begin
		// % protected region % [Add any additional constructor parameters for CoreUserAction here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for CoreUserAction here] off begin
			// % protected region % [Add any additional constructor arguments for CoreUserAction here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for CoreUserAction here] off begin
		// % protected region % [Add any additional constructor logic for CoreUserAction here] end
	}

	// % protected region % [Add any additional class methods for CoreUserAction here] off begin
	// % protected region % [Add any additional class methods for CoreUserAction here] end
}

export class CoreUserActionOK extends BaseCoreUserAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for CoreUserActionOK here] off begin
	// % protected region % [Add any additional class fields for CoreUserActionOK here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<CoreUserModel>,
		// % protected region % [Add any additional constructor parameters for CoreUserActionOK here] off begin
		// % protected region % [Add any additional constructor parameters for CoreUserActionOK here] end
		afterwardActions: Action[] = [],
		public audits?: CoreUserModelAudit[],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for CoreUserActionOK here] off begin
			// % protected region % [Add any additional constructor arguments for CoreUserActionOK here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for CoreUserActionOK here] off begin
		// % protected region % [Add any additional constructor logic for CoreUserActionOK here] end
	}

	// % protected region % [Add any additional class methods for CoreUserActionOK here] off begin
	// % protected region % [Add any additional class methods for CoreUserActionOK here] end
}

export class CoreUserActionFail extends BaseCoreUserAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for CoreUserActionFail here] off begin
	// % protected region % [Add any additional class fields for CoreUserActionFail here] end

	public constructor(
		public typeInput: string,
		public readonly error?: any,
		public stateConfig?: PassableStateConfig<CoreUserModel>,
		// % protected region % [Add any additional constructor parameters for CoreUserActionFail here] off begin
		// % protected region % [Add any additional constructor parameters for CoreUserActionFail here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for CoreUserActionFail here] off begin
			// % protected region % [Add any additional constructor arguments for CoreUserActionFail here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for CoreUserActionFail here] off begin
		// % protected region % [Add any additional constructor logic for CoreUserActionFail here] end
	}

	// % protected region % [Add any additional class methods for CoreUserActionFail here] off begin
	// % protected region % [Add any additional class methods for CoreUserActionFail here] end
}

export function isCoreUserModelAction(e: any): e is BaseCoreUserAction {
	return Object.values(CoreUserModelActionTypes).includes(e);
}

// % protected region % [Add any additional actions here] off begin
// % protected region % [Add any additional actions here] end
