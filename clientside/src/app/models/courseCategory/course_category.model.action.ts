/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Action} from '@ngrx/store';
import {CourseCategoryModel} from './course_category.model';
import {CourseCategoryModelAudit} from './course_category.model.state';
import {PassableStateConfig} from '../../lib/services/http/interfaces';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * List of Course Category model actions to be dispatched by NgRx.
 */
export enum CourseCategoryModelActionTypes {
	CREATE_COURSE_CATEGORY = '[ENTITY] Create CourseCategoryModel',
	CREATE_COURSE_CATEGORY_OK = '[ENTITY] Create CourseCategoryModel successfully',
	CREATE_COURSE_CATEGORY_FAIL = '[ENTITY] Create CourseCategoryModel failed',

	CREATE_ALL_COURSE_CATEGORY = '[ENTITY] Create All CourseCategoryModel',
	CREATE_ALL_COURSE_CATEGORY_OK = '[ENTITY] Create All CourseCategoryModel successfully',
	CREATE_ALL_COURSE_CATEGORY_FAIL = '[ENTITY] Create All CourseCategoryModel failed',

	DELETE_COURSE_CATEGORY = '[ENTITY] Delete CourseCategoryModel',
	DELETE_COURSE_CATEGORY_OK = '[ENTITY] Delete CourseCategoryModel successfully',
	DELETE_COURSE_CATEGORY_FAIL = '[ENTITY] Delete CourseCategoryModel failed',


	DELETE_COURSE_CATEGORY_EXCLUDING_IDS = '[ENTITY] Delete CourseCategoryModels Excluding Ids',
	DELETE_COURSE_CATEGORY_EXCLUDING_IDS_OK = '[ENTITY] Delete CourseCategoryModels Excluding Ids successfully',
	DELETE_COURSE_CATEGORY_EXCLUDING_IDS_FAIL = '[ENTITY] Delete CourseCategoryModels Excluding Ids failed',

	DELETE_ALL_COURSE_CATEGORY = '[ENTITY] Delete all CourseCategoryModels',
	DELETE_ALL_COURSE_CATEGORY_OK = '[ENTITY] Delete all CourseCategoryModels successfully',
	DELETE_ALL_COURSE_CATEGORY_FAIL = '[ENTITY] Delete all CourseCategoryModels failed',

	UPDATE_COURSE_CATEGORY = '[ENTITY] Update CourseCategoryModel',
	UPDATE_COURSE_CATEGORY_OK = '[ENTITY] Update CourseCategoryModel successfully',
	UPDATE_COURSE_CATEGORY_FAIL = '[ENTITY] Update CourseCategoryModel failed',

	UPDATE_ALL_COURSE_CATEGORY = '[ENTITY] Update all CourseCategoryModel',
	UPDATE_ALL_COURSE_CATEGORY_OK = '[ENTITY] Update all CourseCategoryModel successfully',
	UPDATE_ALL_COURSE_CATEGORY_FAIL = '[ENTITY] Update all CourseCategoryModel failed',

	FETCH_COURSE_CATEGORY= '[ENTITY] Fetch CourseCategoryModel',
	FETCH_COURSE_CATEGORY_OK = '[ENTITY] Fetch CourseCategoryModel successfully',
	FETCH_COURSE_CATEGORY_FAIL = '[ENTITY] Fetch CourseCategoryModel failed',

	FETCH_COURSE_CATEGORY_AUDIT= '[ENTITY] Fetch CourseCategoryModel audit',
	FETCH_COURSE_CATEGORY_AUDIT_OK = '[ENTITY] Fetch CourseCategoryModel audit successfully',
	FETCH_COURSE_CATEGORY_AUDIT_FAIL = '[ENTITY] Fetch CourseCategoryModel audit failed',

	FETCH_COURSE_CATEGORY_AUDITS_BY_ENTITY_ID= '[ENTITY] Fetch CourseCategoryModel audits by entity id',
	FETCH_COURSE_CATEGORY_AUDITS_BY_ENTITY_ID_OK = '[ENTITY] Fetch CourseCategoryModel audits by entity id successfully',
	FETCH_COURSE_CATEGORY_AUDITS_BY_ENTITY_ID_FAIL = '[ENTITY] Fetch CourseCategoryModel audits by entity id failed',

	FETCH_ALL_COURSE_CATEGORY = '[ENTITY] Fetch all CourseCategoryModel',
	FETCH_ALL_COURSE_CATEGORY_OK = '[ENTITY] Fetch all CourseCategoryModel successfully',
	FETCH_ALL_COURSE_CATEGORY_FAIL = '[ENTITY] Fetch all CourseCategoryModel failed',

	FETCH_COURSE_CATEGORY_WITH_QUERY = '[ENTITY] Fetch CourseCategoryModel with query',
	FETCH_COURSE_CATEGORY_WITH_QUERY_OK = '[ENTITY] Fetch CourseCategoryModel with query successfully',
	FETCH_COURSE_CATEGORY_WITH_QUERY_FAIL = '[ENTITY] Fetch CourseCategoryModel with query failed',

	FETCH_LAST_COURSE_CATEGORY_WITH_QUERY = '[ENTITY] Fetch last CourseCategoryModel with query',
	FETCH_LAST_COURSE_CATEGORY_WITH_QUERY_OK = '[ENTITY] Fetch last CourseCategoryModel with query successfully',
	FETCH_LAST_COURSE_CATEGORY_WITH_QUERY_FAIL = '[ENTITY] Fetch last CourseCategoryModel with query failed',

	EXPORT_COURSE_CATEGORY = '[ENTITY] Export CourseCategoryModel',
	EXPORT_COURSE_CATEGORY_OK = '[ENTITY] Export CourseCategoryModel successfully',
	EXPORT_COURSE_CATEGORY_FAIL = '[ENTITY] Export CourseCategoryModel failed',

	EXPORT_ALL_COURSE_CATEGORY = '[ENTITY] Export All CourseCategoryModels',
	EXPORT_ALL_COURSE_CATEGORY_OK = '[ENTITY] Export All CourseCategoryModels successfully',
	EXPORT_ALL_COURSE_CATEGORY_FAIL = '[ENTITY] Export All CourseCategoryModels failed',

	EXPORT_COURSE_CATEGORY_EXCLUDING_IDS = '[ENTITY] Export CourseCategoryModels excluding Ids',
	EXPORT_COURSE_CATEGORY_EXCLUDING_IDS_OK = '[ENTITY] Export CourseCategoryModel excluding Ids successfully',
	EXPORT_COURSE_CATEGORY_EXCLUDING_IDS_FAIL = '[ENTITY] Export CourseCategoryModel excluding Ids failed',

	COUNT_COURSE_CATEGORYS = '[ENTITY] Fetch number of CourseCategoryModel records',
	COUNT_COURSE_CATEGORYS_OK = '[ENTITY] Fetch number of CourseCategoryModel records successfully ',
	COUNT_COURSE_CATEGORYS_FAIL = '[ENTITY] Fetch number of CourseCategoryModel records failed',

	IMPORT_COURSE_CATEGORYS = '[ENTITY] Import CourseCategoryModels',
	IMPORT_COURSE_CATEGORYS_OK = '[ENTITY] Import CourseCategoryModels successfully',
	IMPORT_COURSE_CATEGORYS_FAIL = '[ENTITY] Import CourseCategoryModels fail',


	INITIALISE_COURSE_CATEGORY_COLLECTION_STATE = '[ENTITY] Initialize the CollectionState of CourseCategoryModel',
	// % protected region % [Add any additional model actions here] off begin
	// % protected region % [Add any additional model actions here] end
}

export abstract class BaseCourseCategoryAction implements Action {
	readonly className: string = 'CourseCategoryModel';

	abstract readonly type: string;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	protected constructor(
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
		public readonly afterwardActions: Action[] = []
	) {
		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}
}

export class CourseCategoryAction extends BaseCourseCategoryAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for CourseCategoryAction here] off begin
	// % protected region % [Add any additional class fields for CourseCategoryAction here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<CourseCategoryModel>,
		// % protected region % [Add any additional constructor parameters for CourseCategoryAction here] off begin
		// % protected region % [Add any additional constructor parameters for CourseCategoryAction here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for CourseCategoryAction here] off begin
			// % protected region % [Add any additional constructor arguments for CourseCategoryAction here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for CourseCategoryAction here] off begin
		// % protected region % [Add any additional constructor logic for CourseCategoryAction here] end
	}

	// % protected region % [Add any additional class methods for CourseCategoryAction here] off begin
	// % protected region % [Add any additional class methods for CourseCategoryAction here] end
}

export class CourseCategoryActionOK extends BaseCourseCategoryAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for CourseCategoryActionOK here] off begin
	// % protected region % [Add any additional class fields for CourseCategoryActionOK here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<CourseCategoryModel>,
		// % protected region % [Add any additional constructor parameters for CourseCategoryActionOK here] off begin
		// % protected region % [Add any additional constructor parameters for CourseCategoryActionOK here] end
		afterwardActions: Action[] = [],
		public audits?: CourseCategoryModelAudit[],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for CourseCategoryActionOK here] off begin
			// % protected region % [Add any additional constructor arguments for CourseCategoryActionOK here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for CourseCategoryActionOK here] off begin
		// % protected region % [Add any additional constructor logic for CourseCategoryActionOK here] end
	}

	// % protected region % [Add any additional class methods for CourseCategoryActionOK here] off begin
	// % protected region % [Add any additional class methods for CourseCategoryActionOK here] end
}

export class CourseCategoryActionFail extends BaseCourseCategoryAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for CourseCategoryActionFail here] off begin
	// % protected region % [Add any additional class fields for CourseCategoryActionFail here] end

	public constructor(
		public typeInput: string,
		public readonly error?: any,
		public stateConfig?: PassableStateConfig<CourseCategoryModel>,
		// % protected region % [Add any additional constructor parameters for CourseCategoryActionFail here] off begin
		// % protected region % [Add any additional constructor parameters for CourseCategoryActionFail here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for CourseCategoryActionFail here] off begin
			// % protected region % [Add any additional constructor arguments for CourseCategoryActionFail here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for CourseCategoryActionFail here] off begin
		// % protected region % [Add any additional constructor logic for CourseCategoryActionFail here] end
	}

	// % protected region % [Add any additional class methods for CourseCategoryActionFail here] off begin
	// % protected region % [Add any additional class methods for CourseCategoryActionFail here] end
}

export function isCourseCategoryModelAction(e: any): e is BaseCourseCategoryAction {
	return Object.values(CourseCategoryModelActionTypes).includes(e);
}

// % protected region % [Add any additional actions here] off begin
// % protected region % [Add any additional actions here] end
