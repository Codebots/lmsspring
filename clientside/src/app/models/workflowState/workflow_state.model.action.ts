/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Action} from '@ngrx/store';
import {WorkflowStateModel} from './workflow_state.model';
import {WorkflowStateModelAudit} from './workflow_state.model.state';
import {PassableStateConfig} from '../../lib/services/http/interfaces';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * List of Workflow State model actions to be dispatched by NgRx.
 */
export enum WorkflowStateModelActionTypes {
	CREATE_WORKFLOW_STATE = '[ENTITY] Create WorkflowStateModel',
	CREATE_WORKFLOW_STATE_OK = '[ENTITY] Create WorkflowStateModel successfully',
	CREATE_WORKFLOW_STATE_FAIL = '[ENTITY] Create WorkflowStateModel failed',

	CREATE_ALL_WORKFLOW_STATE = '[ENTITY] Create All WorkflowStateModel',
	CREATE_ALL_WORKFLOW_STATE_OK = '[ENTITY] Create All WorkflowStateModel successfully',
	CREATE_ALL_WORKFLOW_STATE_FAIL = '[ENTITY] Create All WorkflowStateModel failed',

	DELETE_WORKFLOW_STATE = '[ENTITY] Delete WorkflowStateModel',
	DELETE_WORKFLOW_STATE_OK = '[ENTITY] Delete WorkflowStateModel successfully',
	DELETE_WORKFLOW_STATE_FAIL = '[ENTITY] Delete WorkflowStateModel failed',


	DELETE_WORKFLOW_STATE_EXCLUDING_IDS = '[ENTITY] Delete WorkflowStateModels Excluding Ids',
	DELETE_WORKFLOW_STATE_EXCLUDING_IDS_OK = '[ENTITY] Delete WorkflowStateModels Excluding Ids successfully',
	DELETE_WORKFLOW_STATE_EXCLUDING_IDS_FAIL = '[ENTITY] Delete WorkflowStateModels Excluding Ids failed',

	DELETE_ALL_WORKFLOW_STATE = '[ENTITY] Delete all WorkflowStateModels',
	DELETE_ALL_WORKFLOW_STATE_OK = '[ENTITY] Delete all WorkflowStateModels successfully',
	DELETE_ALL_WORKFLOW_STATE_FAIL = '[ENTITY] Delete all WorkflowStateModels failed',

	UPDATE_WORKFLOW_STATE = '[ENTITY] Update WorkflowStateModel',
	UPDATE_WORKFLOW_STATE_OK = '[ENTITY] Update WorkflowStateModel successfully',
	UPDATE_WORKFLOW_STATE_FAIL = '[ENTITY] Update WorkflowStateModel failed',

	UPDATE_ALL_WORKFLOW_STATE = '[ENTITY] Update all WorkflowStateModel',
	UPDATE_ALL_WORKFLOW_STATE_OK = '[ENTITY] Update all WorkflowStateModel successfully',
	UPDATE_ALL_WORKFLOW_STATE_FAIL = '[ENTITY] Update all WorkflowStateModel failed',

	FETCH_WORKFLOW_STATE= '[ENTITY] Fetch WorkflowStateModel',
	FETCH_WORKFLOW_STATE_OK = '[ENTITY] Fetch WorkflowStateModel successfully',
	FETCH_WORKFLOW_STATE_FAIL = '[ENTITY] Fetch WorkflowStateModel failed',

	FETCH_WORKFLOW_STATE_AUDIT= '[ENTITY] Fetch WorkflowStateModel audit',
	FETCH_WORKFLOW_STATE_AUDIT_OK = '[ENTITY] Fetch WorkflowStateModel audit successfully',
	FETCH_WORKFLOW_STATE_AUDIT_FAIL = '[ENTITY] Fetch WorkflowStateModel audit failed',

	FETCH_WORKFLOW_STATE_AUDITS_BY_ENTITY_ID= '[ENTITY] Fetch WorkflowStateModel audits by entity id',
	FETCH_WORKFLOW_STATE_AUDITS_BY_ENTITY_ID_OK = '[ENTITY] Fetch WorkflowStateModel audits by entity id successfully',
	FETCH_WORKFLOW_STATE_AUDITS_BY_ENTITY_ID_FAIL = '[ENTITY] Fetch WorkflowStateModel audits by entity id failed',

	FETCH_ALL_WORKFLOW_STATE = '[ENTITY] Fetch all WorkflowStateModel',
	FETCH_ALL_WORKFLOW_STATE_OK = '[ENTITY] Fetch all WorkflowStateModel successfully',
	FETCH_ALL_WORKFLOW_STATE_FAIL = '[ENTITY] Fetch all WorkflowStateModel failed',

	FETCH_WORKFLOW_STATE_WITH_QUERY = '[ENTITY] Fetch WorkflowStateModel with query',
	FETCH_WORKFLOW_STATE_WITH_QUERY_OK = '[ENTITY] Fetch WorkflowStateModel with query successfully',
	FETCH_WORKFLOW_STATE_WITH_QUERY_FAIL = '[ENTITY] Fetch WorkflowStateModel with query failed',

	FETCH_LAST_WORKFLOW_STATE_WITH_QUERY = '[ENTITY] Fetch last WorkflowStateModel with query',
	FETCH_LAST_WORKFLOW_STATE_WITH_QUERY_OK = '[ENTITY] Fetch last WorkflowStateModel with query successfully',
	FETCH_LAST_WORKFLOW_STATE_WITH_QUERY_FAIL = '[ENTITY] Fetch last WorkflowStateModel with query failed',

	EXPORT_WORKFLOW_STATE = '[ENTITY] Export WorkflowStateModel',
	EXPORT_WORKFLOW_STATE_OK = '[ENTITY] Export WorkflowStateModel successfully',
	EXPORT_WORKFLOW_STATE_FAIL = '[ENTITY] Export WorkflowStateModel failed',

	EXPORT_ALL_WORKFLOW_STATE = '[ENTITY] Export All WorkflowStateModels',
	EXPORT_ALL_WORKFLOW_STATE_OK = '[ENTITY] Export All WorkflowStateModels successfully',
	EXPORT_ALL_WORKFLOW_STATE_FAIL = '[ENTITY] Export All WorkflowStateModels failed',

	EXPORT_WORKFLOW_STATE_EXCLUDING_IDS = '[ENTITY] Export WorkflowStateModels excluding Ids',
	EXPORT_WORKFLOW_STATE_EXCLUDING_IDS_OK = '[ENTITY] Export WorkflowStateModel excluding Ids successfully',
	EXPORT_WORKFLOW_STATE_EXCLUDING_IDS_FAIL = '[ENTITY] Export WorkflowStateModel excluding Ids failed',

	COUNT_WORKFLOW_STATES = '[ENTITY] Fetch number of WorkflowStateModel records',
	COUNT_WORKFLOW_STATES_OK = '[ENTITY] Fetch number of WorkflowStateModel records successfully ',
	COUNT_WORKFLOW_STATES_FAIL = '[ENTITY] Fetch number of WorkflowStateModel records failed',

	IMPORT_WORKFLOW_STATES = '[ENTITY] Import WorkflowStateModels',
	IMPORT_WORKFLOW_STATES_OK = '[ENTITY] Import WorkflowStateModels successfully',
	IMPORT_WORKFLOW_STATES_FAIL = '[ENTITY] Import WorkflowStateModels fail',


	INITIALISE_WORKFLOW_STATE_COLLECTION_STATE = '[ENTITY] Initialize the CollectionState of WorkflowStateModel',
	// % protected region % [Add any additional model actions here] off begin
	// % protected region % [Add any additional model actions here] end
}

export abstract class BaseWorkflowStateAction implements Action {
	readonly className: string = 'WorkflowStateModel';

	abstract readonly type: string;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	protected constructor(
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
		public readonly afterwardActions: Action[] = []
	) {
		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}
}

export class WorkflowStateAction extends BaseWorkflowStateAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for WorkflowStateAction here] off begin
	// % protected region % [Add any additional class fields for WorkflowStateAction here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<WorkflowStateModel>,
		// % protected region % [Add any additional constructor parameters for WorkflowStateAction here] off begin
		// % protected region % [Add any additional constructor parameters for WorkflowStateAction here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for WorkflowStateAction here] off begin
			// % protected region % [Add any additional constructor arguments for WorkflowStateAction here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for WorkflowStateAction here] off begin
		// % protected region % [Add any additional constructor logic for WorkflowStateAction here] end
	}

	// % protected region % [Add any additional class methods for WorkflowStateAction here] off begin
	// % protected region % [Add any additional class methods for WorkflowStateAction here] end
}

export class WorkflowStateActionOK extends BaseWorkflowStateAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for WorkflowStateActionOK here] off begin
	// % protected region % [Add any additional class fields for WorkflowStateActionOK here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<WorkflowStateModel>,
		// % protected region % [Add any additional constructor parameters for WorkflowStateActionOK here] off begin
		// % protected region % [Add any additional constructor parameters for WorkflowStateActionOK here] end
		afterwardActions: Action[] = [],
		public audits?: WorkflowStateModelAudit[],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for WorkflowStateActionOK here] off begin
			// % protected region % [Add any additional constructor arguments for WorkflowStateActionOK here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for WorkflowStateActionOK here] off begin
		// % protected region % [Add any additional constructor logic for WorkflowStateActionOK here] end
	}

	// % protected region % [Add any additional class methods for WorkflowStateActionOK here] off begin
	// % protected region % [Add any additional class methods for WorkflowStateActionOK here] end
}

export class WorkflowStateActionFail extends BaseWorkflowStateAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for WorkflowStateActionFail here] off begin
	// % protected region % [Add any additional class fields for WorkflowStateActionFail here] end

	public constructor(
		public typeInput: string,
		public readonly error?: any,
		public stateConfig?: PassableStateConfig<WorkflowStateModel>,
		// % protected region % [Add any additional constructor parameters for WorkflowStateActionFail here] off begin
		// % protected region % [Add any additional constructor parameters for WorkflowStateActionFail here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for WorkflowStateActionFail here] off begin
			// % protected region % [Add any additional constructor arguments for WorkflowStateActionFail here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for WorkflowStateActionFail here] off begin
		// % protected region % [Add any additional constructor logic for WorkflowStateActionFail here] end
	}

	// % protected region % [Add any additional class methods for WorkflowStateActionFail here] off begin
	// % protected region % [Add any additional class methods for WorkflowStateActionFail here] end
}

export function isWorkflowStateModelAction(e: any): e is BaseWorkflowStateAction {
	return Object.values(WorkflowStateModelActionTypes).includes(e);
}

// % protected region % [Add any additional actions here] off begin
// % protected region % [Add any additional actions here] end
