/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {FormGroup, Validators} from '@angular/forms';
import {Group, AbstractModel, ModelProperty, ModelPropertyType, ModelRelation, ModelRelationType} from '../../lib/models/abstract.model';
import {WorkflowStateModel} from '../workflowState/workflow_state.model';
import * as _ from 'lodash';
import {QueryOperation, Where} from '../../lib/services/http/interfaces';
import {ElementType} from '../../lib/components/abstract.input.component';
import { CustomValidators } from 'src/app/lib/utils/validators/custom-validators';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * POJO model class used to store information related to the entity.
 */
export class WorkflowTransitionModel extends AbstractModel {
	/**
	 * The fields which are set as searchable in the entity model
	 * The fields could be used in search in the server side
	 * The fields would be by default used as search in the crud tile.
	 * You could also use this in other tiles for searching.
	 */
	static searchFields: string[] = [
		// % protected region % [Add any additional searchable field names here] off begin
		// % protected region % [Add any additional searchable field names here] end
	];

	/**
	 * Attributes to be shown in value to display
	 */
	static displayAttributes: string[] = [
		// % protected region % [Change displayAttributes here if needed] off begin
		'transitionName',
		// % protected region % [Change displayAttributes here if needed] end
	];

	static modelPropGroups: { [s: string]: Group } = {
		// % protected region % [Add groups for the entity here] off begin
		// % protected region % [Add groups for the entity here] end
	};

	readonly className = 'WorkflowTransitionModel';

	/**
	 * Default value to be displayed in dropdown etc
	 */
	get valueToDisplay(): string {
		// % protected region % [Change displayName here if needed] off begin
		return WorkflowTransitionModel.displayAttributes.map((attr) => this[attr]).join(' ');
		// % protected region % [Change displayName here if needed] end
	}

	/**
	 * The name of transition.
	 */
	transitionName: string;

	sourceStateId: string;

	targetStateId: string;

	modelPropGroups: { [s: string]: Group } = WorkflowTransitionModel.modelPropGroups;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	static getProps(): ModelProperty[] {
		return super.getProps().concat([
			{
				name: 'transitionName',
				// % protected region % [Set displayName for Transition Name here] off begin
				displayName: 'Transition Name',
				// % protected region % [Set displayName for Transition Name here] end
				type: ModelPropertyType.STRING,
				// % protected region % [Set display element type for Transition Name here] off begin
				elementType: ElementType.INPUT,
				// % protected region % [Set display element type for Transition Name here] end
				// % protected region % [Set isSensitive for Transition Name here] off begin
				isSensitive: false,
				// % protected region % [Set isSensitive for Transition Name here] end
				// % protected region % [Set readonly for Transition Name here] off begin
				readOnly: false,
				// % protected region % [Set readonly for Transition Name here] end
				validators: [
					Validators.required,
					// % protected region % [Add other validators for Transition Name here] off begin
					// % protected region % [Add other validators for Transition Name here] end
				],
				// % protected region % [Add any additional model attribute properties for Transition Name here] off begin
				// % protected region % [Add any additional model attribute properties for Transition Name here] end
			},
			// % protected region % [Add any additional class field names here] off begin
			// % protected region % [Add any additional class field names here] end
		]);
	}

	/**
	 * The relations of the entity
	 */
	static getRelations(): { [name: string]: ModelRelation } {
		return {
			...super.getRelations(),
			sourceState: {
				type: ModelRelationType.ONE,
				name: 'sourceStateId',
				// % protected region % [Customise your label for Source State here] off begin
				label: 'Source State',
				// % protected region % [Customise your label for Source State here] end
				// % protected region % [Customise your display name for Source State here] off begin
				// TODO change implementation to use OrderBy or create new metamodel property DisplayBy
				displayName: 'displayIndex',
				// % protected region % [Customise your display name for Source State here] end
				validators: [
					Validators.required
					// % protected region % [Add other validators for Source State here] off begin
					// % protected region % [Add other validators for Source State here] end
				],
				// % protected region % [Add any additional field for relation Source State here] off begin
				// % protected region % [Add any additional field for relation Source State here] end
			},
			targetState: {
				type: ModelRelationType.ONE,
				name: 'targetStateId',
				// % protected region % [Customise your label for Target State here] off begin
				label: 'Target State',
				// % protected region % [Customise your label for Target State here] end
				// % protected region % [Customise your display name for Target State here] off begin
				// TODO change implementation to use OrderBy or create new metamodel property DisplayBy
				displayName: 'displayIndex',
				// % protected region % [Customise your display name for Target State here] end
				validators: [
					Validators.required
					// % protected region % [Add other validators for Target State here] off begin
					// % protected region % [Add other validators for Target State here] end
				],
				// % protected region % [Add any additional field for relation Target State here] off begin
				// % protected region % [Add any additional field for relation Target State here] end
			},
		};
	}

	/**
	 * Convert the form group to the query conditions
	 */
	static convertFilterToCondition(formGroup: FormGroup): Where[][] {
		let conditions: Where[][] = [];

		// % protected region % [Overide the default convertFilterToCondition here] off begin
		Object.keys(formGroup.value).forEach((key) => {
			switch (key) {
				case 'created':
					const created = formGroup.value[key];
					// is the range of date
					if (created instanceof Array) {
						conditions.push([
							{
								path: key,
								operation: QueryOperation.GREATER_THAN_OR_EQUAL,
								value: created[0]
							}
						]);
						conditions.push([
							{
								path: key,
								operation: QueryOperation.LESS_THAN_OR_EQUAL,
								value: created[1]
							}
						]);
					}
			}
		});
		// % protected region % [Overide the default convertFilterToCondition here] end


		return conditions;
	}

	/**
	 * Convert a nested JSON object into an array of flatten objects.
	 */
	static deepParse(data: string | { [K in keyof WorkflowTransitionModel]?: WorkflowTransitionModel[K] }, currentModel?): AbstractModel[] {
		if (currentModel == null) {
			currentModel = new WorkflowTransitionModel(data);
		}

		let returned: AbstractModel[] = [currentModel];
		const json = typeof data === 'string' ? JSON.parse(data) : data;

		// Incoming one to many
		if (json.sourceState) {
			currentModel.sourceStateId = json.sourceState.id;
			returned = _.union(returned, WorkflowStateModel.deepParse(json.sourceState));
		}

		// Incoming one to many
		if (json.targetState) {
			currentModel.targetStateId = json.targetState.id;
			returned = _.union(returned, WorkflowStateModel.deepParse(json.targetState));
		}


		// % protected region % [Customise your deep parse before return here] off begin
		// % protected region % [Customise your deep parse before return here] end

		return returned;
	}

	/**
	 * @example
	 *
	 * `let workflowTransitionModel = new WorkflowTransitionModel(data);`
	 *
	 * @param data The input data to be initialised as the WorkflowTransitionModel,
	 *    it is expected as a JSON string or as a nullable WorkflowTransitionModel.
	 */
	constructor(data?: string | Partial<WorkflowTransitionModel>) {
		super(data);

		if (data) {
			const json = typeof data === 'string'
				? JSON.parse(data) as Partial<WorkflowTransitionModel>
				: data;

			this.transitionName = json.transitionName;
			this.sourceStateId = json.sourceStateId;
			this.targetStateId = json.targetStateId;
			// % protected region % [Add any additional logic here after set the data] off begin
			// % protected region % [Add any additional logic here after set the data] end
		}

		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}

	toJSON() {
		return {
			...super.toJSON(),
			transitionName: this.transitionName,
			sourceStateId: this.sourceStateId,
			targetStateId: this.targetStateId,
			// % protected region % [Add any additional logic here to json] off begin
			// % protected region % [Add any additional logic here to json] end
		};
	}

	getPropDisplayNames(): { [s: string]: ModelProperty } {
		const returned = {};
		WorkflowTransitionModel.getProps().map(prop => returned[prop.name] = prop);
		return returned;
	}

	applyUpdates(updates: any): WorkflowTransitionModel {
		let newModelJson = this.toJSON();

		if (updates.transitionName) {
			newModelJson.transitionName = updates.transitionName;
		}

		if (updates.sourceStateId) {
			newModelJson.sourceStateId = updates.sourceStateId;
		}

		if (updates.targetStateId) {
			newModelJson.targetStateId = updates.targetStateId;
		}

		return new WorkflowTransitionModel(newModelJson);
	}

	/**
	 * @inheritDoc
	 */
	difference(other: AbstractModel): any {
		if (!(other instanceof WorkflowTransitionModel)) {
			return {};
		}

		const diff = {};

		for (const key of _.keys(this)) {
			const thisValue = this[key];
			const otherValue = other[key];

			// Handle dates differently
			if (thisValue instanceof Date) {
				let thisDate = (thisValue) ? thisValue.getTime() : null;
				let otherDate = (otherValue) ? otherValue.getTime() : null;

				if (thisDate !== otherDate) {
					diff[key] = thisValue;
				}
			} else if (thisValue !== otherValue) {
				diff[key] = thisValue;
			}
		}

		return _.omit(diff, [
			'created',
			'modified',
			'sourceStateIds',
			'targetStateIds',
			// % protected region % [Add any other fields to omit here] off begin
			// % protected region % [Add any other fields to omit here] end
		]);
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
