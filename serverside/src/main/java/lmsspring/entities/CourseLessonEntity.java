/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package lmsspring.entities;

import lmsspring.dtos.CourseLessonEntityDto;
import lmsspring.entities.listeners.CourseLessonEntityListener;
import lmsspring.serializers.CourseLessonSerializer;
import lmsspring.lib.validators.ValidatorPatterns;
import lmsspring.services.utils.converters.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.opencsv.bean.CsvIgnore;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;
import org.springframework.lang.Nullable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Optional;
import java.util.UUID;

// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Entity
// % protected region % [Override the auditing annotation or add additional annotations here] off begin
@Audited
// % protected region % [Override the auditing annotation or add additional annotations here] end
@ExcludeSuperclassListeners
@EntityListeners({CourseLessonEntityListener.class, AuditingEntityListener.class})
@JsonSerialize(using = CourseLessonSerializer.class)
@Table(
	uniqueConstraints = {
	}
)
public class CourseLessonEntity extends AbstractEntity {

	/**
	 * Takes a CourseLessonEntityDto and converts it into a CourseLessonEntity.
	 * Primarily used when endpoints are invoked, as they take DTOs as input, which need to be converted to the entity type
	 *
	 * @param courseLessonEntityDto
	 */
	public CourseLessonEntity(CourseLessonEntityDto courseLessonEntityDto) {
		// Need to account for potential that any field is empty.  This will be run before any server validation and
		// endpoints will most likely not be subject to clientside validation

		if (courseLessonEntityDto.getId() != null) {
			this.setId(courseLessonEntityDto.getId());
		}

		if (courseLessonEntityDto.getOrder() != null) {
			this.setOrder(courseLessonEntityDto.getOrder());
		}

		if (courseLessonEntityDto.getCourse() != null) {
			this.setCourse(courseLessonEntityDto.getCourse());
		}

		if (courseLessonEntityDto.getLesson() != null) {
			this.setLesson(courseLessonEntityDto.getLesson());
		}

		// % protected region % [Add any additional DTO constructor logic here] off begin
		// % protected region % [Add any additional DTO constructor logic here] end
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Order here] off begin
	@CsvBindByName(column = "ORDER", required = false)
	@Nullable
	@Column(name = "order")
	@ApiModelProperty(notes = "The Order of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Order here] end
	private Integer order;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Update the annotation for Course here] off begin
	@ApiModelProperty(notes = "The Course entities that are related to this entity.")
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@CsvIgnore
	// % protected region % [Update the annotation for Course here] end
	private CourseEntity course;

	// % protected region % [Update the annotation for courseId here] off begin
	@Transient
	@CsvCustomBindByName(column = "COURSE_ID", converter = UUIDConverter.class)
	// % protected region % [Update the annotation for courseId here] end
	private UUID courseId;

	// % protected region % [Update the annotation for Lesson here] off begin
	@ApiModelProperty(notes = "The Lesson entities that are related to this entity.")
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@CsvIgnore
	// % protected region % [Update the annotation for Lesson here] end
	private LessonEntity lesson;

	// % protected region % [Update the annotation for lessonId here] off begin
	@Transient
	@CsvCustomBindByName(column = "LESSON_ID", converter = UUIDConverter.class)
	// % protected region % [Update the annotation for lessonId here] end
	private UUID lessonId;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Similar to {@link this#setCourse(CourseEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given CourseEntity to be set to this entity
	 */
	public void setCourse(@NotNull CourseEntity entity) {
		setCourse(entity, true);
	}

	/**
	 * Set or update the course in this entity with single CourseEntity.
	 *
	 * @param entity the given CourseEntity to be set or updated to course
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setCourse(@NotNull CourseEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setCourse here] off begin
		// % protected region % [Add any additional logic here before the main logic for setCourse here] end

		if (sameAsFormer(this.course, entity)) {
			return;
		}

		if (this.course != null) {
			this.course.removeCourseLessons(this, false);
		}
		this.course = entity;
		if (reverseAdd) {
			this.course.addCourseLessons(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setCourse here] off begin
		// % protected region % [Add any additional logic here after the main logic for setCourse here] end
	}

	/**
	 * Similar to {@link this#unsetCourse(boolean)} but default to true.
	 */
	public void unsetCourse() {
		this.unsetCourse(true);
	}

	/**
	 * Remove Course in this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetCourse(boolean reverse) {
		if (reverse && this.course != null) {
			this.course.removeCourseLessons(this, false);
		}
		this.course = null;
	}
	/**
	 * Similar to {@link this#setLesson(LessonEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonEntity to be set to this entity
	 */
	public void setLesson(@NotNull LessonEntity entity) {
		setLesson(entity, true);
	}

	/**
	 * Set or update the lesson in this entity with single LessonEntity.
	 *
	 * @param entity the given LessonEntity to be set or updated to lesson
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setLesson(@NotNull LessonEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setLesson here] off begin
		// % protected region % [Add any additional logic here before the main logic for setLesson here] end

		if (sameAsFormer(this.lesson, entity)) {
			return;
		}

		if (this.lesson != null) {
			this.lesson.removeCourseLessons(this, false);
		}
		this.lesson = entity;
		if (reverseAdd) {
			this.lesson.addCourseLessons(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setLesson here] off begin
		// % protected region % [Add any additional logic here after the main logic for setLesson here] end
	}

	/**
	 * Similar to {@link this#unsetLesson(boolean)} but default to true.
	 */
	public void unsetLesson() {
		this.unsetLesson(true);
	}

	/**
	 * Remove Lesson in this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetLesson(boolean reverse) {
		if (reverse && this.lesson != null) {
			this.lesson.removeCourseLessons(this, false);
		}
		this.lesson = null;
	}

	/**
	 * The CSV annotations used to generate and import CSV files require headers on the abstract entity attributes
	 * (id, created, modified, created by, modified by).  These should not be present in imports, as this information is
	 * added by the server when saving entities.  Therefore a string containing the example csv headers is required
	 *
	 * @return the headers recommended in the CSV import file in CSV format
	 */
	public static String getExampleCsvHeader() {

		// % protected region % [Modify the headers in the CSV file here] off begin
		return "ORDER,COURSE_ID,LESSON_ID,ID";
		// % protected region % [Modify the headers in the CSV file here] end
	}

	public void addRelationEntitiesToIdSet() {
		// % protected region % [Add any additional logic for entity relations here] off begin
		Optional<CourseEntity> courseRelation = Optional.ofNullable(this.course);
		courseRelation.ifPresent(entity -> this.courseId = entity.getId());

		Optional<LessonEntity> lessonRelation = Optional.ofNullable(this.lesson);
		lessonRelation.ifPresent(entity -> this.lessonId = entity.getId());

		// % protected region % [Add any additional logic for entity relations here] end
	}
	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
