/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.graphql;

import lmsspring.graphql.resolvers.*;
import lmsspring.graphql.resolvers.query.*;
import lmsspring.graphql.resolvers.mutation.*;
import com.coxautodev.graphql.tools.SchemaParser;
import lmsspring.graphql.resolvers.*;
import lmsspring.graphql.resolvers.query.*;
import lmsspring.graphql.resolvers.mutation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * Base class for every GraphQL provider. It handles parsing and merging of schemas by default.
 */
@Component
public class GraphQLCombiner {

	private final AdministratorResolver administratorResolver;
	private final AdministratorQueryResolver administratorQueryResolver;
	private final AdministratorMutationResolver administratorMutationResolver;
	private final ApplicationLocaleResolver applicationLocaleResolver;
	private final ApplicationLocaleQueryResolver applicationLocaleQueryResolver;
	private final ApplicationLocaleMutationResolver applicationLocaleMutationResolver;
	private final ArticleResolver articleResolver;
	private final ArticleQueryResolver articleQueryResolver;
	private final ArticleMutationResolver articleMutationResolver;
	private final BookResolver bookResolver;
	private final BookQueryResolver bookQueryResolver;
	private final BookMutationResolver bookMutationResolver;
	private final CoreUserResolver coreUserResolver;
	private final CoreUserQueryResolver coreUserQueryResolver;
	private final CoreUserMutationResolver coreUserMutationResolver;
	private final CourseResolver courseResolver;
	private final CourseQueryResolver courseQueryResolver;
	private final CourseMutationResolver courseMutationResolver;
	private final CourseCategoryResolver courseCategoryResolver;
	private final CourseCategoryQueryResolver courseCategoryQueryResolver;
	private final CourseCategoryMutationResolver courseCategoryMutationResolver;
	private final CourseLessonResolver courseLessonResolver;
	private final CourseLessonQueryResolver courseLessonQueryResolver;
	private final CourseLessonMutationResolver courseLessonMutationResolver;
	private final LessonResolver lessonResolver;
	private final LessonQueryResolver lessonQueryResolver;
	private final LessonMutationResolver lessonMutationResolver;
	private final TagResolver tagResolver;
	private final TagQueryResolver tagQueryResolver;
	private final TagMutationResolver tagMutationResolver;
	private final RoleResolver roleResolver;
	private final RoleQueryResolver roleQueryResolver;
	private final RoleMutationResolver roleMutationResolver;
	private final PrivilegeResolver privilegeResolver;
	private final PrivilegeQueryResolver privilegeQueryResolver;
	private final PrivilegeMutationResolver privilegeMutationResolver;
	private final WorkflowResolver workflowResolver;
	private final WorkflowQueryResolver workflowQueryResolver;
	private final WorkflowMutationResolver workflowMutationResolver;
	private final WorkflowStateResolver workflowStateResolver;
	private final WorkflowStateQueryResolver workflowStateQueryResolver;
	private final WorkflowStateMutationResolver workflowStateMutationResolver;
	private final WorkflowTransitionResolver workflowTransitionResolver;
	private final WorkflowTransitionQueryResolver workflowTransitionQueryResolver;
	private final WorkflowTransitionMutationResolver workflowTransitionMutationResolver;
	private final WorkflowVersionResolver workflowVersionResolver;
	private final WorkflowVersionQueryResolver workflowVersionQueryResolver;
	private final WorkflowVersionMutationResolver workflowVersionMutationResolver;
	private final LessonFormSubmissionResolver lessonFormSubmissionResolver;
	private final LessonFormSubmissionQueryResolver lessonFormSubmissionQueryResolver;
	private final LessonFormSubmissionMutationResolver lessonFormSubmissionMutationResolver;
	private final LessonFormVersionResolver lessonFormVersionResolver;
	private final LessonFormVersionQueryResolver lessonFormVersionQueryResolver;
	private final LessonFormVersionMutationResolver lessonFormVersionMutationResolver;
	private final LessonFormTileResolver lessonFormTileResolver;
	private final LessonFormTileQueryResolver lessonFormTileQueryResolver;
	private final LessonFormTileMutationResolver lessonFormTileMutationResolver;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	@Autowired
	public GraphQLCombiner(
			AdministratorResolver administratorResolver,
			AdministratorQueryResolver administratorQueryResolver,
			AdministratorMutationResolver administratorMutationResolver,
			ApplicationLocaleResolver applicationLocaleResolver,
			ApplicationLocaleQueryResolver applicationLocaleQueryResolver,
			ApplicationLocaleMutationResolver applicationLocaleMutationResolver,
			ArticleResolver articleResolver,
			ArticleQueryResolver articleQueryResolver,
			ArticleMutationResolver articleMutationResolver,
			BookResolver bookResolver,
			BookQueryResolver bookQueryResolver,
			BookMutationResolver bookMutationResolver,
			CoreUserResolver coreUserResolver,
			CoreUserQueryResolver coreUserQueryResolver,
			CoreUserMutationResolver coreUserMutationResolver,
			CourseResolver courseResolver,
			CourseQueryResolver courseQueryResolver,
			CourseMutationResolver courseMutationResolver,
			CourseCategoryResolver courseCategoryResolver,
			CourseCategoryQueryResolver courseCategoryQueryResolver,
			CourseCategoryMutationResolver courseCategoryMutationResolver,
			CourseLessonResolver courseLessonResolver,
			CourseLessonQueryResolver courseLessonQueryResolver,
			CourseLessonMutationResolver courseLessonMutationResolver,
			LessonResolver lessonResolver,
			LessonQueryResolver lessonQueryResolver,
			LessonMutationResolver lessonMutationResolver,
			TagResolver tagResolver,
			TagQueryResolver tagQueryResolver,
			TagMutationResolver tagMutationResolver,
			RoleResolver roleResolver,
			RoleQueryResolver roleQueryResolver,
			RoleMutationResolver roleMutationResolver,
			PrivilegeResolver privilegeResolver,
			PrivilegeQueryResolver privilegeQueryResolver,
			PrivilegeMutationResolver privilegeMutationResolver,
			WorkflowResolver workflowResolver,
			WorkflowQueryResolver workflowQueryResolver,
			WorkflowMutationResolver workflowMutationResolver,
			WorkflowStateResolver workflowStateResolver,
			WorkflowStateQueryResolver workflowStateQueryResolver,
			WorkflowStateMutationResolver workflowStateMutationResolver,
			WorkflowTransitionResolver workflowTransitionResolver,
			WorkflowTransitionQueryResolver workflowTransitionQueryResolver,
			WorkflowTransitionMutationResolver workflowTransitionMutationResolver,
			WorkflowVersionResolver workflowVersionResolver,
			WorkflowVersionQueryResolver workflowVersionQueryResolver,
			WorkflowVersionMutationResolver workflowVersionMutationResolver,
			LessonFormSubmissionResolver lessonFormSubmissionResolver,
			LessonFormSubmissionQueryResolver lessonFormSubmissionQueryResolver,
			LessonFormSubmissionMutationResolver lessonFormSubmissionMutationResolver,
			LessonFormVersionResolver lessonFormVersionResolver,
			LessonFormVersionQueryResolver lessonFormVersionQueryResolver,
			LessonFormVersionMutationResolver lessonFormVersionMutationResolver,
			LessonFormTileResolver lessonFormTileResolver,
			LessonFormTileQueryResolver lessonFormTileQueryResolver,
			LessonFormTileMutationResolver lessonFormTileMutationResolver
	) {
		this.administratorResolver = administratorResolver;
		this.administratorQueryResolver = administratorQueryResolver;
		this.administratorMutationResolver = administratorMutationResolver;
		this.applicationLocaleResolver = applicationLocaleResolver;
		this.applicationLocaleQueryResolver = applicationLocaleQueryResolver;
		this.applicationLocaleMutationResolver = applicationLocaleMutationResolver;
		this.articleResolver = articleResolver;
		this.articleQueryResolver = articleQueryResolver;
		this.articleMutationResolver = articleMutationResolver;
		this.bookResolver = bookResolver;
		this.bookQueryResolver = bookQueryResolver;
		this.bookMutationResolver = bookMutationResolver;
		this.coreUserResolver = coreUserResolver;
		this.coreUserQueryResolver = coreUserQueryResolver;
		this.coreUserMutationResolver = coreUserMutationResolver;
		this.courseResolver = courseResolver;
		this.courseQueryResolver = courseQueryResolver;
		this.courseMutationResolver = courseMutationResolver;
		this.courseCategoryResolver = courseCategoryResolver;
		this.courseCategoryQueryResolver = courseCategoryQueryResolver;
		this.courseCategoryMutationResolver = courseCategoryMutationResolver;
		this.courseLessonResolver = courseLessonResolver;
		this.courseLessonQueryResolver = courseLessonQueryResolver;
		this.courseLessonMutationResolver = courseLessonMutationResolver;
		this.lessonResolver = lessonResolver;
		this.lessonQueryResolver = lessonQueryResolver;
		this.lessonMutationResolver = lessonMutationResolver;
		this.tagResolver = tagResolver;
		this.tagQueryResolver = tagQueryResolver;
		this.tagMutationResolver = tagMutationResolver;
		this.roleResolver = roleResolver;
		this.roleQueryResolver = roleQueryResolver;
		this.roleMutationResolver = roleMutationResolver;
		this.privilegeResolver = privilegeResolver;
		this.privilegeQueryResolver = privilegeQueryResolver;
		this.privilegeMutationResolver = privilegeMutationResolver;
		this.workflowResolver = workflowResolver;
		this.workflowQueryResolver = workflowQueryResolver;
		this.workflowMutationResolver = workflowMutationResolver;
		this.workflowStateResolver = workflowStateResolver;
		this.workflowStateQueryResolver = workflowStateQueryResolver;
		this.workflowStateMutationResolver = workflowStateMutationResolver;
		this.workflowTransitionResolver = workflowTransitionResolver;
		this.workflowTransitionQueryResolver = workflowTransitionQueryResolver;
		this.workflowTransitionMutationResolver = workflowTransitionMutationResolver;
		this.workflowVersionResolver = workflowVersionResolver;
		this.workflowVersionQueryResolver = workflowVersionQueryResolver;
		this.workflowVersionMutationResolver = workflowVersionMutationResolver;
		this.lessonFormSubmissionResolver = lessonFormSubmissionResolver;
		this.lessonFormSubmissionQueryResolver = lessonFormSubmissionQueryResolver;
		this.lessonFormSubmissionMutationResolver = lessonFormSubmissionMutationResolver;
		this.lessonFormVersionResolver = lessonFormVersionResolver;
		this.lessonFormVersionQueryResolver = lessonFormVersionQueryResolver;
		this.lessonFormVersionMutationResolver = lessonFormVersionMutationResolver;
		this.lessonFormTileResolver = lessonFormTileResolver;
		this.lessonFormTileQueryResolver = lessonFormTileQueryResolver;
		this.lessonFormTileMutationResolver = lessonFormTileMutationResolver;
	}

	@Bean
	public SchemaParser schemaParser() {
		return SchemaParser.newParser()
				.file("graphql/schemas/schema.graphql")
				.file("graphql/schemas/administrator.schema.graphql")
				.file("graphql/schemas/application_locale.schema.graphql")
				.file("graphql/schemas/article.schema.graphql")
				.file("graphql/schemas/book.schema.graphql")
				.file("graphql/schemas/core_user.schema.graphql")
				.file("graphql/schemas/course.schema.graphql")
				.file("graphql/schemas/course_category.schema.graphql")
				.file("graphql/schemas/course_lesson.schema.graphql")
				.file("graphql/schemas/lesson.schema.graphql")
				.file("graphql/schemas/tag.schema.graphql")
				.file("graphql/schemas/role.schema.graphql")
				.file("graphql/schemas/privilege.schema.graphql")
				.file("graphql/schemas/workflow.schema.graphql")
				.file("graphql/schemas/workflow_state.schema.graphql")
				.file("graphql/schemas/workflow_transition.schema.graphql")
				.file("graphql/schemas/workflow_version.schema.graphql")
				.file("graphql/schemas/lesson_form_submission.schema.graphql")
				.file("graphql/schemas/lesson_form_version.schema.graphql")
				.file("graphql/schemas/lesson_form_tile.schema.graphql")
				.resolvers(administratorResolver)
				.resolvers(administratorQueryResolver)
				.resolvers(administratorMutationResolver)
				.resolvers(applicationLocaleResolver)
				.resolvers(applicationLocaleQueryResolver)
				.resolvers(applicationLocaleMutationResolver)
				.resolvers(articleResolver)
				.resolvers(articleQueryResolver)
				.resolvers(articleMutationResolver)
				.resolvers(bookResolver)
				.resolvers(bookQueryResolver)
				.resolvers(bookMutationResolver)
				.resolvers(coreUserResolver)
				.resolvers(coreUserQueryResolver)
				.resolvers(coreUserMutationResolver)
				.resolvers(courseResolver)
				.resolvers(courseQueryResolver)
				.resolvers(courseMutationResolver)
				.resolvers(courseCategoryResolver)
				.resolvers(courseCategoryQueryResolver)
				.resolvers(courseCategoryMutationResolver)
				.resolvers(courseLessonResolver)
				.resolvers(courseLessonQueryResolver)
				.resolvers(courseLessonMutationResolver)
				.resolvers(lessonResolver)
				.resolvers(lessonQueryResolver)
				.resolvers(lessonMutationResolver)
				.resolvers(tagResolver)
				.resolvers(tagQueryResolver)
				.resolvers(tagMutationResolver)
				.resolvers(roleResolver)
				.resolvers(roleQueryResolver)
				.resolvers(roleMutationResolver)
				.resolvers(privilegeResolver)
				.resolvers(privilegeQueryResolver)
				.resolvers(privilegeMutationResolver)
				.resolvers(workflowResolver)
				.resolvers(workflowQueryResolver)
				.resolvers(workflowMutationResolver)
				.resolvers(workflowStateResolver)
				.resolvers(workflowStateQueryResolver)
				.resolvers(workflowStateMutationResolver)
				.resolvers(workflowTransitionResolver)
				.resolvers(workflowTransitionQueryResolver)
				.resolvers(workflowTransitionMutationResolver)
				.resolvers(workflowVersionResolver)
				.resolvers(workflowVersionQueryResolver)
				.resolvers(workflowVersionMutationResolver)
				.resolvers(lessonFormSubmissionResolver)
				.resolvers(lessonFormSubmissionQueryResolver)
				.resolvers(lessonFormSubmissionMutationResolver)
				.resolvers(lessonFormVersionResolver)
				.resolvers(lessonFormVersionQueryResolver)
				.resolvers(lessonFormVersionMutationResolver)
				.resolvers(lessonFormTileResolver)
				.resolvers(lessonFormTileQueryResolver)
				.resolvers(lessonFormTileMutationResolver)
				.build();
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
