/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package lmsspring.cucumber.pom.pages.admin.forms;

import com.google.inject.Inject;
import lmsspring.cucumber.pom.pages.AbstractPage;
import lmsspring.cucumber.pom.pages.admin.forms.elements.FormBuilderSlideElement;
import lmsspring.cucumber.pom.pages.admin.forms.elements.SlideBuilderListElement;
import lmsspring.cucumber.utils.TypingUtils;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.*;

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

@Slf4j
@ScenarioScoped
public class FormBuilderPage extends AbstractPage {

	@Getter
	@FindBy(how = How.XPATH, using = "//h2")
	private WebElement title;

	@Getter
	@FindBy(xpath = "//section[contains(@class, 'form-builder')]")
	private WebElement formBuilderElement;

	@Getter
	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'slide-builder__list')]//section")
	private List<SlideBuilderListElement> slideList;

	@Getter
	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'slide-builder__list')]/button")
	private WebElement newSlideButton;

	@Getter
	@FindBy(how = How.XPATH, using = "//section[contains(@class, 'form-builder')]//h3")
	private List<WebElement> slideTitles;

	@Getter
	@FindBy(className = "form__slide-container")
	private List<FormBuilderSlideElement> formBuilderSlides;

	@Getter
	@FindBy(className = "forms-properties")
	private WebElement propertiesTab;

	@Getter
	@FindBy(className = "context-menu")
	private WebElement contextMenuButton;

	@Getter
	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'context-menu')]/ul//li/button")
	private List<WebElement> contextMenuOptions;

	@Getter
	@FindBy(xpath = "//*[@id='question']//input")
	private WebElement slideNameField;

	@Getter
	@FindBy(xpath = "//section[contains(@class, 'btn-group')]//button")
	private List<WebElement> actionBarButtons;

	@Getter
	@FindBy(xpath = "//div[contains(@class, 'form__question-container')]//div[contains(@class, 'btn-group--static-elements')]/button")
	private List<WebElement> questionOptionsButtons;

	@Getter
	@FindBy(xpath = "//div[contains(@class, 'form__question-container')]//div[contains(@class, 'btn-group--static-elements')]/div[contains(@class, 'context-menu')]")
	private List<WebElement> questionContextMenuButtons;

	@Getter
	@FindBy(xpath = "//div[contains(@class, 'form__question-container')]//div[contains(@class, 'btn-group--static-elements')]/div[contains(@class, 'context-menu') and contains(@class, 'active')]/ul")
	private WebElement questionContextMenuOptionsWrapper;

	@Getter
	@FindBy(xpath = "//div[contains(@class, 'form__question-container')]//div[contains(@class, 'btn-group--static-elements')]/div[contains(@class, 'context-menu') and contains(@class, 'active')]/ul//li/button")
	private List<WebElement> questionContextMenuOptions;

	@Getter
	@FindBy(xpath = "//*[@id='question-type-field']")
	private WebElement questionTypeDropdown;

	@Getter
	@FindBy(xpath = "//div[contains(@class, 'ng-dropdown-panel-items')]")
	private WebElement questionDropdownBox;

	@Getter
	@FindBy(xpath = "//div[contains(@class, 'ng-dropdown-panel-items')]//div[contains(@class, 'ng-option')]")
	private List<WebElement> questionTypeOptions;

	@Getter
	@FindBy(xpath = "//p[contains(@class, 'question__content')]")
	private List<WebElement> questionTitle;

	@Getter
	@FindBy(xpath = "//input[starts-with(@id, 'question-') and contains(@id, '-field')]")
	private WebElement questionNameInput;

	@Getter
	@FindBy(xpath = "//textarea[starts-with(@id, 'form-statement-') and contains(@id, '-field')]")
	private WebElement statementQuestionInput;

	@Getter
	@FindBy(className = "forms__add-option")
	private WebElement checkboxRadioAddOptionButton;

	@Getter
	@FindBy(xpath = "//input[starts-with(@id, 'question-checkbox-option-name')]")
	private List<WebElement> checkboxTitleInputs;

	@Getter
	@FindBy(xpath = "//input[starts-with(@id, 'question-radio-button-option-name')]")
	private List<WebElement> radioTitleInputs;

	@Getter
	@FindBy(xpath = "//ng-select[starts-with(@id, 'date-question-') and contains(@id, '-type-field')]")
	private WebElement datePickerType;

	@Getter
	@FindBy(xpath = "//ng-select[starts-with(@id, 'date-question-') and contains(@id, '-type-field')]/ng-dropdown-panel")
	private WebElement datePickerDropdownPanel;

	@Getter
	@FindBy(xpath = "//ng-select[starts-with(@id, 'date-question-') and contains(@id, '-type-field')]/ng-dropdown-panel//div[contains(@class, 'ng-option')]")
	private List<WebElement> datePickerDropdownOptions;

	@Getter
	@FindBy(tagName = "owl-date-time-container")
	private WebElement datePickerContainer;

	//In following entries for different question XPaths, only one entry is expected.
	//Variables are stored in a list so that size can be checked in order to determine
	//that the element exists in assertions without having to catch an ElementNotInteractableException
	@Getter
	@FindBy(xpath = "//*[@id='question0-field']")
	private List<WebElement> textfieldQuestionField;

	@Getter
	@FindBy(xpath = "//p[contains(@class, 'form-statement__content')]")
	private List<WebElement> statementQuestionText;

	@Getter
	@FindBy(xpath = "//input[starts-with(@id, 'form-date-question-datepicker-')]")
	private List<WebElement> datePickerInput;

	@Getter
	@FindBy(tagName = "owl-date-time-calendar")
	private List<WebElement> datePickerCalender;

	@Getter
	@FindBy(tagName = "owl-date-time-timer")
	private List<WebElement> datePickerTimer;

	//More than one entry is expected in these lists, however they are used for the same purpose as the previous lists of elements
	@Getter
	@FindBy(xpath = "//cb-checkbox/label")
	private List<WebElement> checkboxLabels;

	@Getter
	@FindBy(xpath = "//cb-radio-button/label")
	private List<WebElement> radioButtonLabels;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	@Inject
	public FormBuilderPage(
			@NonNull WebDriver webDriver,
			@NonNull Properties properties
	) {
		super(
				webDriver,
				properties,
				"admin/behaviour/forms/build/{{formName}}/{{entityId}}/build"
		);

		log.trace("Initialised {}", this.getClass().getSimpleName());
	}

	public void fillSlideNameField(String value) {
		TypingUtils.clearAndType(webDriver, slideNameField, value);
	}

	public void fillQuestionNameField(String value) {
		TypingUtils.clearAndType(webDriver, this.getQuestionNameInput(), value);
	}

	public void fillStatementTextarea(String value) {
		TypingUtils.clearAndType(webDriver, this.getStatementQuestionInput(), value);
	}

	public void fillCheckboxTitle(int index, String value) {
		TypingUtils.clearAndType(webDriver, this.getCheckboxTitleInputs().get(index), value);
	}

	public void fillRadioTitle(int index, String value) {
		TypingUtils.clearAndType(webDriver, this.getRadioTitleInputs().get(index), value);
	}

	public void clickDatePickerDropdownOption(String dropdown) {
		this.getDatePickerDropdownOptions().stream()
				.filter(option -> option.getText().matches(dropdown)).findFirst()
				.orElseThrow(() -> new NoSuchElementException(String.format("Could not find dropdown option named %s", dropdown))).click();

	}

	public void clickQuestionDropdown(String dropdownOption) {
		this.getQuestionContextMenuOptions().stream()
				.filter(option -> option.getText().matches(dropdownOption)).findFirst()
				.orElseThrow(() -> new NoSuchElementException(String.format("Could not find dropdown option named %s", dropdownOption))).click();
	}

	public boolean verifyDateTimeQuestion(String questionType) {
		int datePickerCalendarCount = this.getDatePickerCalender().size();
		int datePickerTimerCount = this.getDatePickerTimer().size();
		switch (questionType) {
			case "Date Time":
				return datePickerCalendarCount == 1 && datePickerTimerCount == 1;
			case "Date":
				return datePickerCalendarCount == 1 && datePickerTimerCount == 0;
			case "Time":
				return datePickerCalendarCount == 0 && datePickerTimerCount == 1;
			default:
				throw new RuntimeException(questionType + " is not a valid Date Picker question type");
		}
	}

	public boolean verifyPage() {
		return this.getTitle().getText().matches("Edit form");
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
