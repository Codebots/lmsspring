/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Action} from '@ngrx/store';
import {LessonFormVersionModel} from './lesson_form_version.model';
import {LessonFormVersionModelAudit} from './lesson_form_version.model.state';
import {PassableStateConfig} from '../../lib/services/http/interfaces';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * List of Lesson Form Version model actions to be dispatched by NgRx.
 */
export enum LessonFormVersionModelActionTypes {
	CREATE_LESSON_FORM_VERSION = '[ENTITY] Create LessonFormVersionModel',
	CREATE_LESSON_FORM_VERSION_OK = '[ENTITY] Create LessonFormVersionModel successfully',
	CREATE_LESSON_FORM_VERSION_FAIL = '[ENTITY] Create LessonFormVersionModel failed',

	CREATE_ALL_LESSON_FORM_VERSION = '[ENTITY] Create All LessonFormVersionModel',
	CREATE_ALL_LESSON_FORM_VERSION_OK = '[ENTITY] Create All LessonFormVersionModel successfully',
	CREATE_ALL_LESSON_FORM_VERSION_FAIL = '[ENTITY] Create All LessonFormVersionModel failed',

	DELETE_LESSON_FORM_VERSION = '[ENTITY] Delete LessonFormVersionModel',
	DELETE_LESSON_FORM_VERSION_OK = '[ENTITY] Delete LessonFormVersionModel successfully',
	DELETE_LESSON_FORM_VERSION_FAIL = '[ENTITY] Delete LessonFormVersionModel failed',


	DELETE_LESSON_FORM_VERSION_EXCLUDING_IDS = '[ENTITY] Delete LessonFormVersionModels Excluding Ids',
	DELETE_LESSON_FORM_VERSION_EXCLUDING_IDS_OK = '[ENTITY] Delete LessonFormVersionModels Excluding Ids successfully',
	DELETE_LESSON_FORM_VERSION_EXCLUDING_IDS_FAIL = '[ENTITY] Delete LessonFormVersionModels Excluding Ids failed',

	DELETE_ALL_LESSON_FORM_VERSION = '[ENTITY] Delete all LessonFormVersionModels',
	DELETE_ALL_LESSON_FORM_VERSION_OK = '[ENTITY] Delete all LessonFormVersionModels successfully',
	DELETE_ALL_LESSON_FORM_VERSION_FAIL = '[ENTITY] Delete all LessonFormVersionModels failed',

	UPDATE_LESSON_FORM_VERSION = '[ENTITY] Update LessonFormVersionModel',
	UPDATE_LESSON_FORM_VERSION_OK = '[ENTITY] Update LessonFormVersionModel successfully',
	UPDATE_LESSON_FORM_VERSION_FAIL = '[ENTITY] Update LessonFormVersionModel failed',

	UPDATE_ALL_LESSON_FORM_VERSION = '[ENTITY] Update all LessonFormVersionModel',
	UPDATE_ALL_LESSON_FORM_VERSION_OK = '[ENTITY] Update all LessonFormVersionModel successfully',
	UPDATE_ALL_LESSON_FORM_VERSION_FAIL = '[ENTITY] Update all LessonFormVersionModel failed',

	FETCH_LESSON_FORM_VERSION= '[ENTITY] Fetch LessonFormVersionModel',
	FETCH_LESSON_FORM_VERSION_OK = '[ENTITY] Fetch LessonFormVersionModel successfully',
	FETCH_LESSON_FORM_VERSION_FAIL = '[ENTITY] Fetch LessonFormVersionModel failed',

	FETCH_LESSON_FORM_VERSION_AUDIT= '[ENTITY] Fetch LessonFormVersionModel audit',
	FETCH_LESSON_FORM_VERSION_AUDIT_OK = '[ENTITY] Fetch LessonFormVersionModel audit successfully',
	FETCH_LESSON_FORM_VERSION_AUDIT_FAIL = '[ENTITY] Fetch LessonFormVersionModel audit failed',

	FETCH_LESSON_FORM_VERSION_AUDITS_BY_ENTITY_ID= '[ENTITY] Fetch LessonFormVersionModel audits by entity id',
	FETCH_LESSON_FORM_VERSION_AUDITS_BY_ENTITY_ID_OK = '[ENTITY] Fetch LessonFormVersionModel audits by entity id successfully',
	FETCH_LESSON_FORM_VERSION_AUDITS_BY_ENTITY_ID_FAIL = '[ENTITY] Fetch LessonFormVersionModel audits by entity id failed',

	FETCH_ALL_LESSON_FORM_VERSION = '[ENTITY] Fetch all LessonFormVersionModel',
	FETCH_ALL_LESSON_FORM_VERSION_OK = '[ENTITY] Fetch all LessonFormVersionModel successfully',
	FETCH_ALL_LESSON_FORM_VERSION_FAIL = '[ENTITY] Fetch all LessonFormVersionModel failed',

	FETCH_LESSON_FORM_VERSION_WITH_QUERY = '[ENTITY] Fetch LessonFormVersionModel with query',
	FETCH_LESSON_FORM_VERSION_WITH_QUERY_OK = '[ENTITY] Fetch LessonFormVersionModel with query successfully',
	FETCH_LESSON_FORM_VERSION_WITH_QUERY_FAIL = '[ENTITY] Fetch LessonFormVersionModel with query failed',

	FETCH_LAST_LESSON_FORM_VERSION_WITH_QUERY = '[ENTITY] Fetch last LessonFormVersionModel with query',
	FETCH_LAST_LESSON_FORM_VERSION_WITH_QUERY_OK = '[ENTITY] Fetch last LessonFormVersionModel with query successfully',
	FETCH_LAST_LESSON_FORM_VERSION_WITH_QUERY_FAIL = '[ENTITY] Fetch last LessonFormVersionModel with query failed',

	EXPORT_LESSON_FORM_VERSION = '[ENTITY] Export LessonFormVersionModel',
	EXPORT_LESSON_FORM_VERSION_OK = '[ENTITY] Export LessonFormVersionModel successfully',
	EXPORT_LESSON_FORM_VERSION_FAIL = '[ENTITY] Export LessonFormVersionModel failed',

	EXPORT_ALL_LESSON_FORM_VERSION = '[ENTITY] Export All LessonFormVersionModels',
	EXPORT_ALL_LESSON_FORM_VERSION_OK = '[ENTITY] Export All LessonFormVersionModels successfully',
	EXPORT_ALL_LESSON_FORM_VERSION_FAIL = '[ENTITY] Export All LessonFormVersionModels failed',

	EXPORT_LESSON_FORM_VERSION_EXCLUDING_IDS = '[ENTITY] Export LessonFormVersionModels excluding Ids',
	EXPORT_LESSON_FORM_VERSION_EXCLUDING_IDS_OK = '[ENTITY] Export LessonFormVersionModel excluding Ids successfully',
	EXPORT_LESSON_FORM_VERSION_EXCLUDING_IDS_FAIL = '[ENTITY] Export LessonFormVersionModel excluding Ids failed',

	COUNT_LESSON_FORM_VERSIONS = '[ENTITY] Fetch number of LessonFormVersionModel records',
	COUNT_LESSON_FORM_VERSIONS_OK = '[ENTITY] Fetch number of LessonFormVersionModel records successfully ',
	COUNT_LESSON_FORM_VERSIONS_FAIL = '[ENTITY] Fetch number of LessonFormVersionModel records failed',

	IMPORT_LESSON_FORM_VERSIONS = '[ENTITY] Import LessonFormVersionModels',
	IMPORT_LESSON_FORM_VERSIONS_OK = '[ENTITY] Import LessonFormVersionModels successfully',
	IMPORT_LESSON_FORM_VERSIONS_FAIL = '[ENTITY] Import LessonFormVersionModels fail',


	INITIALISE_LESSON_FORM_VERSION_COLLECTION_STATE = '[ENTITY] Initialize the CollectionState of LessonFormVersionModel',
	// % protected region % [Add any additional model actions here] off begin
	// % protected region % [Add any additional model actions here] end
}

export abstract class BaseLessonFormVersionAction implements Action {
	readonly className: string = 'LessonFormVersionModel';

	abstract readonly type: string;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	protected constructor(
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
		public readonly afterwardActions: Action[] = []
	) {
		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}
}

export class LessonFormVersionAction extends BaseLessonFormVersionAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for LessonFormVersionAction here] off begin
	// % protected region % [Add any additional class fields for LessonFormVersionAction here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<LessonFormVersionModel>,
		// % protected region % [Add any additional constructor parameters for LessonFormVersionAction here] off begin
		// % protected region % [Add any additional constructor parameters for LessonFormVersionAction here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for LessonFormVersionAction here] off begin
			// % protected region % [Add any additional constructor arguments for LessonFormVersionAction here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for LessonFormVersionAction here] off begin
		// % protected region % [Add any additional constructor logic for LessonFormVersionAction here] end
	}

	// % protected region % [Add any additional class methods for LessonFormVersionAction here] off begin
	// % protected region % [Add any additional class methods for LessonFormVersionAction here] end
}

export class LessonFormVersionActionOK extends BaseLessonFormVersionAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for LessonFormVersionActionOK here] off begin
	// % protected region % [Add any additional class fields for LessonFormVersionActionOK here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<LessonFormVersionModel>,
		// % protected region % [Add any additional constructor parameters for LessonFormVersionActionOK here] off begin
		// % protected region % [Add any additional constructor parameters for LessonFormVersionActionOK here] end
		afterwardActions: Action[] = [],
		public audits?: LessonFormVersionModelAudit[],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for LessonFormVersionActionOK here] off begin
			// % protected region % [Add any additional constructor arguments for LessonFormVersionActionOK here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for LessonFormVersionActionOK here] off begin
		// % protected region % [Add any additional constructor logic for LessonFormVersionActionOK here] end
	}

	// % protected region % [Add any additional class methods for LessonFormVersionActionOK here] off begin
	// % protected region % [Add any additional class methods for LessonFormVersionActionOK here] end
}

export class LessonFormVersionActionFail extends BaseLessonFormVersionAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for LessonFormVersionActionFail here] off begin
	// % protected region % [Add any additional class fields for LessonFormVersionActionFail here] end

	public constructor(
		public typeInput: string,
		public readonly error?: any,
		public stateConfig?: PassableStateConfig<LessonFormVersionModel>,
		// % protected region % [Add any additional constructor parameters for LessonFormVersionActionFail here] off begin
		// % protected region % [Add any additional constructor parameters for LessonFormVersionActionFail here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for LessonFormVersionActionFail here] off begin
			// % protected region % [Add any additional constructor arguments for LessonFormVersionActionFail here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for LessonFormVersionActionFail here] off begin
		// % protected region % [Add any additional constructor logic for LessonFormVersionActionFail here] end
	}

	// % protected region % [Add any additional class methods for LessonFormVersionActionFail here] off begin
	// % protected region % [Add any additional class methods for LessonFormVersionActionFail here] end
}

export function isLessonFormVersionModelAction(e: any): e is BaseLessonFormVersionAction {
	return Object.values(LessonFormVersionModelActionTypes).includes(e);
}

// % protected region % [Add any additional actions here] off begin
// % protected region % [Add any additional actions here] end
