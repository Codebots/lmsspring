/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package lmsspring.cucumber.stepdefs.behaviours.forms;

import com.google.inject.Inject;
import lmsspring.cucumber.pom.enums.VisibilityEnum;
import lmsspring.cucumber.pom.factories.AdminPageFactory;
import lmsspring.cucumber.pom.pages.admin.forms.FormBuilderPage;
import lmsspring.cucumber.pom.pages.admin.forms.FormsListPage;
import lmsspring.cucumber.stepdefs.AbstractStepDef;
import lmsspring.cucumber.utils.WaitUtils;
import io.cucumber.java.en.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import java.util.*;

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

public class FormBuilderStepDef extends AbstractStepDef {

	@Inject
	private WebDriver webDriver;

	@Inject
	private FormsListPage formsListPage;

	@Inject
	private FormBuilderPage formBuilderPage;

	@Inject
	private AdminPageFactory adminPageFactory;

	// % protected region % [Add any additional fields here] off begin
	// % protected region % [Add any additional fields here] end

	//Given Step Defs
	@Given("I create a new form tile on the {string} page")
	public void iCreateANewFormTileOnThePage(String entityName) throws Exception {
		adminPageFactory.createCrudListPage(entityName).createButton.click();

		//For Form tile page, an entity is not required to be passed, as the information is entered using dropdowns
		//Using this step def for a CrudEditPage which is not for a FormTile entity will result in an error due to passing null
		adminPageFactory.createCrudEditPage(entityName).applyEntity(null);	
	}

	// % protected region % [Add any additional Given Step Defs here] off begin
	// % protected region % [Add any additional Given Step Defs here] end

	//When Step Defs
	@When("I navigate to the forms page in the admin section")
	public void iNavigateToTheFormsPageInTheAdminSection() {
		this.formsListPage.navigate();
	}

	@When("I expand the {string} form dropdown")
	public void iExpandTheFormDropdown(String formName) {
		var dropdown = this.formsListPage.getFormDropdownByName(formName);
		dropdown.click();
	}

	@When("I click the new form button in the {string} form dropdown")
	public void iClickTheNewFormButtonInTheFormDropdown(String formName) throws Exception {
		var formDropdown = this.formsListPage.getFormDropdownByName(formName);
		this.formsListPage.waitForDropdown(formDropdown);
		formDropdown.getNewFormButton().click();
	}

	@When("I click on the valid {string} entity in the form dropdown")
	public void iClickOnTheValidEntityInTheFormDropdown(String formName) throws Exception {
		var formDropdown = this.formsListPage.getFormDropdownByName(formName);
		this.formsListPage.waitForDropdownForm(formDropdown);
		formDropdown.getListForms().get(1).click();
	}

	@When("I add a new slide to the form")
	public void iAddANewSlideToTheForm() {
		this.formBuilderPage.getNewSlideButton().click();
	}

	@When("I add a new question to a slide")
	public void iAddANewQuestionToASlide() {
		var slides = this.formBuilderPage.getFormBuilderSlides();
		slides.get(0).getAddQuestionButton().click();
	}

	@When("I click on the slide context menu and select the {string} option")
	public void iClickOnTheSlideContextMenuAndSelectTheOption(String contextOption) {
		this.formBuilderPage.getContextMenuButton().click();
		this.formBuilderPage.getContextMenuOptions().stream()
				.filter(option -> option.getText().matches(contextOption)).findFirst()
				.orElseThrow(() -> new NoSuchElementException(String.format("Could not find context menu option named %s", contextOption))).click();
	}

	@When("I edit the slide name")
	public void iEditTheSlideName() {
		this.formBuilderPage.fillSlideNameField("new-slide-name"); //Update this with generated string
	}

	@When("I click the save button")
	public void iClickTheSaveButton() {
		this.formBuilderPage.getActionBarButtons().get(1).click();
	}

	@When("I click the options button for the question")
	public void iClickTheOptionsButtonForTheQuestion() {
		this.formBuilderPage.getQuestionOptionsButtons().get(0).click();
	}

	@When("I click on the question type context menu and select the {string} option")
	public void iClickOnTheQuestionTypeContextMenuAndSelectTheOption(String questionType) throws Exception {
		this.formBuilderPage.getQuestionTypeDropdown().click();
		WaitUtils.waitForElement(webDriver, this.formBuilderPage.getQuestionDropdownBox(), VisibilityEnum.VISIBLE);
		String optionText;
		switch (questionType) {
			case "Date Time":
			case "Date":
			case "Time":
				optionText = "Date / Date Time";
				break;
			default:
				optionText = questionType;
		}
		String finalOptionText = optionText;
		this.formBuilderPage.getQuestionTypeOptions().stream()
				.filter(option -> option.getText().matches(finalOptionText)).findFirst()
				.orElseThrow(() -> new NoSuchElementException(String.format("Could not find question type option named %s", finalOptionText))).click();
	}

	@When("I edit the {string} question")
	public void iEditTheQuestion(String questionType) throws Exception {
		this.formBuilderPage.fillQuestionNameField("edited-question");
		
		switch(questionType) {
			case "Textfield":
				//No further actions required for Textfield question
				//Need to include it in switch statement to avoid throwing exception on a valid question type
				break;
			case "Statement":
				this.formBuilderPage.fillStatementTextarea("statement-question-content");
				break;
			case "Checkbox":
				this.formBuilderPage.getCheckboxRadioAddOptionButton().click();
				this.formBuilderPage.getCheckboxRadioAddOptionButton().click();
				this.formBuilderPage.fillCheckboxTitle(0, "first-checkbox-option");
				this.formBuilderPage.fillCheckboxTitle(1, "second-checkbox-option");
				break;
			case "Radio Buttons":
				this.formBuilderPage.getCheckboxRadioAddOptionButton().click();
				this.formBuilderPage.getCheckboxRadioAddOptionButton().click();
				this.formBuilderPage.fillRadioTitle(0, "first-radio-option");
				this.formBuilderPage.fillRadioTitle(1, "second-radio-option");
				break;
			case "Date Time":
			case "Date":
			case "Time":
				WaitUtils.waitForElement(webDriver, this.formBuilderPage.getDatePickerType(), VisibilityEnum.VISIBLE);
				this.formBuilderPage.getDatePickerType().click();
				WaitUtils.waitForElement(webDriver, this.formBuilderPage.getDatePickerDropdownPanel(), VisibilityEnum.VISIBLE);
				this.formBuilderPage.clickDatePickerDropdownOption(questionType);
				break;
			default:
				throw new RuntimeException(questionType + " is not a valid question type");
		}
	}

	@When("I click the question display context menu and select the {string} option")
	public void iClickTheQuestionDisplayContextMenuAndSelectTheOption(String dropdownOption) throws Exception {
		WaitUtils.waitForElement(webDriver, this.formBuilderPage.getFormBuilderElement(), VisibilityEnum.VISIBLE);
		webDriverWait.until(x -> this.formBuilderPage.getQuestionContextMenuButtons().size() > 0);
		this.formBuilderPage.getQuestionContextMenuButtons().get(0).click();
		WaitUtils.waitForElement(webDriver, this.formBuilderPage.getQuestionContextMenuOptionsWrapper(), VisibilityEnum.VISIBLE);
		this.formBuilderPage.clickQuestionDropdown(dropdownOption);
	}

	/**
	 * TAKE THIS OUT OF FORMS AND PUT IT IN ANOTHER ONE WITH ALL FRONTEND PAGES
	 */
	@When("I navigate to the {string} frontend form page")
	public void iNavigateToTheFrontendFormPage(String page) {
		switch (page) {
			default:
				throw new RuntimeException(page + " is not a page or does not contain any form components");
		}
	}

	@When("I fill in the {string} form on page {string} with id {string} with the answer {string}")
	public void iFillInTheEntityNameFormWithIdFormComponentIdWithTheAnswerFormAnswer(String form, String page, String formComponentId, String formAnswer) throws Exception {
		this.testContext.remove("form-answer");
		boolean wait = formAnswer.contains("updated-text");

		switch (page) {
			default:
				throw new RuntimeException(page + " is not a page or does not contain any form components");
		}

		this.testContext.put("form-answer", formAnswer);
	}

	@When("I edit an existing {string}")
	public void iEditAnExistingEntityNameFormSubmission(String entityName) throws Exception {
		var page = adminPageFactory.createCrudListPage(entityName);
		webDriverWait.until(x -> page.CrudListItems.size() > 0);
		page.EditButtons.get((0)).click();
	}

	// % protected region % [Add any additional When Step Defs here] off begin
	// % protected region % [Add any additional When Step Defs here] end

	//Then Step Defs
	@Then("The forms page is correct")
	public void theFormsPageIsCorrect() {
		Assert.assertTrue(this.formsListPage.verifyFormListPage());
	}

	@Then("I am on the {string} form create page")
	public void iAmOnTheFormCreatePage(String formName) {
		var newFormName = formName.replace(' ', '-').toLowerCase();
		Assert.assertTrue(webDriver.getCurrentUrl().contains("/" + newFormName +"/create"));
	}

	@Then("the new {string} form entity with id {string} is present")
	public void theNewFormEntityWithIdIsPresent(String formName, String entityId) throws Exception {
		var entity = this.testContext.get(entityId);
		var formDropdown = this.formsListPage.getFormDropdownByName(formName);
		this.formsListPage.waitForDropdown(formDropdown);
		Assert.assertTrue(formDropdown.getFormTitles().stream().anyMatch(form -> entity.toString().contains(form.getText())));
	}

	@Then("I am on the {string} form builder page")
	public void iAmOnTheFormBuilderPage(String formName) {
		var expectedUrl = "/admin/behaviour/forms/build/" + formName.toLowerCase();
		Assert.assertTrue(webDriver.getCurrentUrl().toLowerCase().contains(expectedUrl) && this.formBuilderPage.verifyPage());
	}

	@Then("I expect the slide has been added to the form builder")
	public void iExpectTheSlideHasBeenAddedToTheFormBuilder() {
		Assert.assertTrue(this.formBuilderPage.getSlideTitles().get(this.formBuilderPage.getSlideTitles().size() - 1).getText().matches("New Slide"));
	}

	@Then("I expect the question has been added to the form")
	public void iExpectTheQuestionHasBeenAddedToTheForm() {
		var slides = this.formBuilderPage.getFormBuilderSlides();
		var questions = slides.get(slides.size() - 1).getQuestions();
		Assert.assertTrue(questions.get(questions.size() - 1).getQuestionName().getText().matches("Click on options to change question details"));
	}

	@Then("I expect that the slide name is updated in the form builder")
	public void iExpectThatTheSlideNameIsUpdatedInTheFormBuilder() throws Exception {
		WaitUtils.waitForElement(webDriver, this.formBuilderPage.getFormBuilderElement(), VisibilityEnum.VISIBLE);
		webDriverWait.until(x -> this.formBuilderPage.getSlideTitles().size() > 0);
		Assert.assertTrue(this.formBuilderPage.getSlideTitles().get(0).getText().matches("new-slide-name"));
	}

	@Then("I expect that the slide has been duplicated")
	public void iExpectThatTheSlideHasBeenDuplicated() throws Exception {
		WaitUtils.waitForElement(webDriver, this.formBuilderPage.getFormBuilderElement(), VisibilityEnum.VISIBLE);
		webDriverWait.until(x -> this.formBuilderPage.getSlideTitles().size() > 0);
		String originalSlideName = this.formBuilderPage.getSlideTitles().get(0).getText();
		String duplicateName = this.formBuilderPage.getSlideTitles().get(this.formBuilderPage.getSlideTitles().size() - 1).getText();

		Assert.assertTrue(originalSlideName.matches(duplicateName));
	}

	@Then("I expect that the slide has been deleted")
	public void iExpectThatTheSlideHasBeenDeleted() throws Exception {
		WaitUtils.waitForElement(webDriver, this.formBuilderPage.getFormBuilderElement(), VisibilityEnum.VISIBLE);

		Assert.assertEquals(this.formBuilderPage.getSlideTitles().size(), 0);
	}

	@Then("I expect that the {string} question has been updated")
	public void iExpectThatTheQuestionHasBeenUpdated(String questionType) throws Exception {
		WaitUtils.waitForElement(webDriver, this.formBuilderPage.getFormBuilderElement(), VisibilityEnum.VISIBLE);

		Assert.assertTrue(this.formBuilderPage.getQuestionTitle().get(0).getText().matches("edited-question"));

		switch(questionType) {
			case "Textfield":
				Assert.assertEquals(this.formBuilderPage.getTextfieldQuestionField().size(), 1);
				break;
			case "Statement":
				Assert.assertEquals(this.formBuilderPage.getStatementQuestionText().size(), 1);
				Assert.assertTrue(this.formBuilderPage.getStatementQuestionText().get(0).getText().matches("statement-question-content"));
				break;
			case "Checkbox":
				Assert.assertEquals(this.formBuilderPage.getCheckboxLabels().size(), 2);
				Assert.assertTrue(this.formBuilderPage.getCheckboxLabels().get(0).getText().matches("first-checkbox-option"));
				Assert.assertTrue(this.formBuilderPage.getCheckboxLabels().get(1).getText().matches("second-checkbox-option"));
				break;
			case "Radio Buttons":
				Assert.assertEquals(this.formBuilderPage.getRadioButtonLabels().size(), 2);
				Assert.assertTrue(this.formBuilderPage.getRadioButtonLabels().get(0).getText().matches("first-radio-option"));
				Assert.assertTrue(this.formBuilderPage.getRadioButtonLabels().get(1).getText().matches("second-radio-option"));
				break;
			case "Date Time":
			case "Date":
			case "Time":
				Assert.assertEquals(this.formBuilderPage.getDatePickerInput().size(), 1);
				this.formBuilderPage.getDatePickerInput().get(0).click();
				WaitUtils.waitForElement(webDriver, this.formBuilderPage.getDatePickerContainer(), VisibilityEnum.VISIBLE);
				Assert.assertTrue(this.formBuilderPage.verifyDateTimeQuestion(questionType));
				break;
			default:
				throw new RuntimeException(questionType + " is not a valid question type");
		}
	}

	@Then("I expect that the question has been duplicated")
	public void iExpectThatTheQuestionHasBeenDuplicated() throws Exception {
		WaitUtils.waitForElement(webDriver, this.formBuilderPage.getFormBuilderElement(), VisibilityEnum.VISIBLE);
		Assert.assertEquals(this.formBuilderPage.getQuestionTitle().size(), 2);
		Assert.assertEquals(this.formBuilderPage.getQuestionTitle().get(0).getText(), this.formBuilderPage.getQuestionTitle().get(1).getText());
	}

	@Then("I expect that the question has been deleted")
	public void iExpectThatTheQuestionHasBeenDeleted() throws Exception {
		WaitUtils.waitForElement(webDriver, this.formBuilderPage.getFormBuilderElement(), VisibilityEnum.VISIBLE);
		Assert.assertEquals(this.formBuilderPage.getQuestionTitle().size(), 0);
	}

	@Then("I expect that the form submitted text for the {string} form on page {string} with id {string} is shown")
	public void iExpectThatTheFormSubmittedTextForTheFormIsShown(String form, String page, String formComponentId) throws Exception {
		switch (page) {
			default:
				throw new RuntimeException(page + " is not a page or does not contain any form components");
		}
	}

	@Then("I expect that the {string} form submission data is correct")
    public void iExpectThatTheFormSubmissionDataIsCorrect(String entityName) {
		String expectedAnswer = (String) this.testContext.get("form-answer");
		switch (entityName) {
			default:
				throw new RuntimeException(entityName + " is not a valid form type");
		}
    }

	@Then("The {string} form submission data has been updated")
	public void iExpectThatTheEntityNameFormSubmissionDataHasBeenUpdated(String entityName) throws Exception {
		String expectedAnswer = (String) this.testContext.get("form-answer");
		var page = adminPageFactory.createCrudListPage(entityName);
		webDriverWait.until(x -> page.CrudListItems.size() > 0);

		int submissionIndex = page.attributeIndex("Submission Data");
		Assert.assertTrue(page.CrudListItems.get(0).findElements(By.xpath("td/span")).get(submissionIndex).getText().contains(expectedAnswer));
	}

	@Then("I fill in the {string} form with the answer {string} in the form submission edit page")
	public void iFillInTheFormWithTheAnswerInTheFormSubmissionEditPage(String entityName, String formAnswer) throws Exception {
		this.testContext.remove("form-answer");
		this.testContext.put("form-answer", formAnswer);
		switch (entityName) {
			default:
				throw new RuntimeException(entityName + " is not a valid form type");
		}
	}

	// % protected region % [Add any additional Then Step Defs here] off begin
	// % protected region % [Add any additional Then Step Defs here] end
}