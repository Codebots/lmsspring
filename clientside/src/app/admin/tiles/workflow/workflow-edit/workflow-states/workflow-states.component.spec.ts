/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import Spy = jasmine.Spy;
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';
import {ToastrModule, ToastrService} from 'ngx-toastr';
import {RouterState} from '../../../../../models/model.state';
import {BehaviorSubject} from 'rxjs';
import {waitForAsync, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {Store} from '@ngrx/store';
import {WorkflowStatesComponent} from './workflow-states.component';
import {WorkflowStateModelState, initialState as initialWorkflowStateModelState} from '../../../../../models/workflowState/workflow_state.model.state';
import {ActivatedRouteStub} from '../../../../../../testing/helpers/activated-route-stub';
import {WorkflowStateModel} from '../../../../../models/workflowState/workflow_state.model';
import * as workflowStateActions from '../../../../../models/workflowState/workflow_state.model.action';
import {WorkflowStateDataFactory} from '../../../../../lib/utils/factories/workflow-state-data-factory';
import {WorkflowEditModule} from '../workflow-edit.module';
import {RouterTestingModule} from '@angular/router/testing';
import {getWorkflowStateCollectionModels} from '../../../../../models/workflowState/workflow_state.model.selector';
import {RouterLinkDirective} from '../../../../../../testing/helpers/router-link-stub';
import {ActivatedRoute} from '@angular/router';
import * as uuid from 'uuid';
import {WorkflowStateEditComponent} from './workflow-state-edit/workflow-state-edit.component';
import {AlertComponent} from '../../../../../lib/components/alert/alert.component';
import {CommonComponentModule} from '../../../../../lib/components/common.component.module';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

let store: MockStore<{ model: WorkflowStateModelState }>;
let routerStore: MockStore<{router: RouterState}>;

let testingWorkflowStates: WorkflowStateModel[];

// Workflow States to create in database
let testingNewWorkflowStates: WorkflowStateModel[];

let testingUpdateStates: WorkflowStateModel[];

let workflowStatesSubject: BehaviorSubject<WorkflowStateModel[]>;

let workflowStateModelState: WorkflowStateModelState;

let collectionId: string;

let dispatchSpy: Spy;

const workflowVersionId: string = uuid.v4();

// % protected region % [Add any additional variables here] off begin
// % protected region % [Add any additional variables here] end

/**
 * Create testing workflow states
 */
const prepareTestingData = () => {
	const workflowStateDataFactory = new WorkflowStateDataFactory();
	testingWorkflowStates = workflowStateDataFactory.createAll();
	testingWorkflowStates.forEach(workflowState => {
		workflowState.workflowVersionId = workflowVersionId;
		workflowState.isStartState = false;
	});

	testingWorkflowStates[0].isStartState = true;

	testingNewWorkflowStates = workflowStateDataFactory.createAll();
	testingNewWorkflowStates.forEach(workflowState => {
		workflowState.workflowVersionId = workflowVersionId;
		workflowState.isStartState = false;
	});

	testingUpdateStates = workflowStateDataFactory.createAll();

	collectionId = `${workflowVersionId}-workflow-states`;
};

/**
 * Spy on selector in Store
 */
const spySelectorsInStore = () =>  {
	// Setup the Mock Store and fake selector
	store = TestBed.inject(Store) as MockStore<{ model: WorkflowStateModelState }>;
	routerStore = TestBed.inject(Store) as MockStore<{ router: RouterState }>;

	workflowStateModelState = initialWorkflowStateModelState;
	store.setState({model: workflowStateModelState});

	// Create Behavior Subjects to trigger later
	workflowStatesSubject = new BehaviorSubject(testingWorkflowStates);

	// Create spy on select function to return value
	spyOn(store, 'select')
		.withArgs(getWorkflowStateCollectionModels, collectionId).and.returnValue(workflowStatesSubject);

	// Create spy on dispatch function
	dispatchSpy = spyOn(store, 'dispatch');
	dispatchSpy.and.callThrough();
};

/**
 * Click create state button to create a new workflow state
 */
const createNewState = (fixture: ComponentFixture<WorkflowStatesComponent>) => {
	const createButtonEl: DebugElement = fixture.debugElement.query(By.css('button.icon-plus'));
	// Click create button to create a new state
	createButtonEl.nativeElement.click();
	tick();
	fixture.detectChanges();
};

describe('Workflow States Component', () => {

	let fixture: ComponentFixture<WorkflowStatesComponent>;
	let workflowStatesComponent: WorkflowStatesComponent;

	let activeRouter: ActivatedRouteStub;

	beforeEach(waitForAsync (() => {
		activeRouter = new ActivatedRouteStub();

		TestBed.configureTestingModule({
			imports: [
				WorkflowEditModule,
				RouterTestingModule,
				ToastrModule.forRoot({
					iconClasses: {
						success: 'alert__success',
						info: 'alert__info',
						warning: 'alert__warning',
						error: 'alert__danger'
					},
					toastClass: '',
					positionClass: 'alert-container',
					preventDuplicates: true
				})
			],
			providers: [
				provideMockStore(),
				RouterLinkDirective,
				ToastrService,
				{
					provide: ActivatedRoute,
					useValue: activeRouter,
				}
			]
		}).compileComponents().then(() => {

			prepareTestingData();
			spySelectorsInStore();

			activeRouter.parent = new ActivatedRouteStub();
			activeRouter.parent.setParamMap({
				versionId: workflowVersionId
			});
			fixture = TestBed.createComponent(WorkflowStatesComponent);
			workflowStatesComponent = fixture.debugElement.componentInstance;

			// % protected region % [Add any additional logic before each test for 'Workflow Details Component' here] off begin
			// % protected region % [Add any additional logic before each test for 'Workflow Details Component' here] end
		});
	}));

	afterEach(() => {
		// Need to do this since for some reason the last component queried from the fixture will be rendered on the
		// browser
		if (fixture.nativeElement instanceof HTMLElement) {
			(fixture.nativeElement as HTMLElement).remove();
		}

		// % protected region % [Add any additional logic after each test for 'Workflow Details Component' here] off begin
		// % protected region % [Add any additional logic after each test for 'Workflow Details Component' here] end
	});

	it('should fetch workflow states with version id in url when component is created', () => {
		// % protected region % [Add any logic before main process of 'should fetch workflow states with version id in url when component is created' here] off begin
		// % protected region % [Add any logic before main process of 'should fetch workflow states with version id in url when component is created' here] end

		fixture.detectChanges();
		const expectedInitialAction = new workflowStateActions.WorkflowStateAction(
			workflowStateActions.WorkflowStateModelActionTypes.INITIALISE_WORKFLOW_STATE_COLLECTION_STATE,
			{
				collectionId: collectionId,
			}
		);

		const expectedFetchAction = new workflowStateActions.WorkflowStateAction(
			workflowStateActions.WorkflowStateModelActionTypes.FETCH_WORKFLOW_STATE_WITH_QUERY,
			{
				collectionId: collectionId,
				queryParams: workflowStatesComponent.queryParams
			}
		);
		expect(dispatchSpy.calls.argsFor(0)).toEqual([expectedInitialAction]);
		expect(dispatchSpy.calls.argsFor(1)).toEqual([expectedFetchAction]);

		// % protected region % [Add any logic after main process of 'should fetch workflow states with version id in url when component is created' here] off begin
		// % protected region % [Add any logic after main process of 'should fetch workflow states with version id in url when component is created' here] end
	});

	it('should create a new workflow when click create button', fakeAsync(() => {
		// % protected region % [Add any logic before main process of 'should create a new workflow when click create button' here] off begin
		// % protected region % [Add any logic before main process of 'should create a new workflow when click create button' here] end

		fixture.detectChanges();
		const createButtonEl: DebugElement = fixture.debugElement.query(By.css('button.icon-plus'));

		const clickTimes = 5;

		for (let i = 0; i < clickTimes; ++i) {
			createButtonEl.nativeElement.click();
		}

		tick();
		fixture.detectChanges();

		// Check whether new states are created
		expect(workflowStatesComponent.newStates.length).toBe(clickTimes);

		// Check number of elements
		const newWorkflowEl: DebugElement[] =
			fixture.debugElement
				.queryAll(By.directive(WorkflowStateEditComponent))
				.filter((element) => !element.attributes['data-id']);
		expect(newWorkflowEl.length).toBe(clickTimes);

		// % protected region % [Add any logic after main process of 'should create a new workflow when click create button' here] off begin
		// % protected region % [Add any logic after main process of 'should create a new workflow when click create button' here] end
	}));

	it ('should dispatch create action when click save draft function is called', fakeAsync(() =>  {
		// % protected region % [Add any logic before main process of 'should dispatch create action when click save draft function is called' here] off begin
		// % protected region % [Add any logic before main process of 'should dispatch create action when click save draft function is called' here] end

		fixture.detectChanges();

		const createButtonEl: DebugElement = fixture.debugElement.query(By.css('button.icon-plus'));

		// Creat for multiple new workflow states
		testingWorkflowStates.forEach(() => createButtonEl.nativeElement.click());

		tick();

		workflowStatesComponent.newStates.forEach((workflowState, index) => {
			workflowState.stepName = testingNewWorkflowStates[index].stepName;
			workflowState.isStartState = testingNewWorkflowStates[index].isStartState;
		});

		tick();
		fixture.detectChanges();

		workflowStatesComponent.onSaveDraftClicked();

		const expectedAction: workflowStateActions.WorkflowStateAction = new workflowStateActions.WorkflowStateAction(
			workflowStateActions.WorkflowStateModelActionTypes.CREATE_ALL_WORKFLOW_STATE,
			{
				targetModels: testingNewWorkflowStates,
				collectionId: collectionId,
			}
		);

		// Find create all action from function calls
		const actualAction: workflowStateActions.WorkflowStateAction = dispatchSpy.calls.all().find(dispatch => {
			const action = dispatch.args[0];
			return action instanceof workflowStateActions.WorkflowStateAction
					&& action.type === workflowStateActions.WorkflowStateModelActionTypes.CREATE_ALL_WORKFLOW_STATE;
		}).args[0];

		// Check whether all parameters in request
		workflowStatesComponent.newStates.forEach((workflowState, index) => {
			expect(testingNewWorkflowStates.find((model) => {
				return model.stepName === workflowState.stepName && model.isStartState === workflowState.isStartState;
			})).not.toBeNull();

			expect(workflowState.workflowVersionId).toBe(workflowVersionId);
		});
		expect(actualAction.type).toEqual(expectedAction.type);
		expect(actualAction.stateConfig.collectionId).toEqual(expectedAction.stateConfig.collectionId);

		// % protected region % [Add any logic after main process of 'should dispatch create action when click save draft function is called' here] off begin
		// % protected region % [Add any logic after main process of 'should dispatch create action when click save draft function is called' here] end
	}));

	it ('should dispatch update action when click save draft function is called', fakeAsync(() =>  {
		// % protected region % [Add any logic before main process of 'should dispatch update action when click save draft function is called' here] off begin
		// % protected region % [Add any logic before main process of 'should dispatch update action when click save draft function is called' here] end

		fixture.detectChanges();

		const workflowEditComponentEls: DebugElement[] = fixture.debugElement.queryAll(By.directive(WorkflowStateEditComponent));

		// Go through all edit components and update form value
		workflowEditComponentEls.forEach((element, index) => {
			const stateEditComponent: WorkflowStateEditComponent = element.componentInstance;
			stateEditComponent.stateForm.patchValue({
				stepName: testingUpdateStates[index].stepName,
			});
			stateEditComponent.stateForm.controls['stepName'].markAsDirty();
		});

		fixture.detectChanges();

		workflowStatesComponent.onSaveDraftClicked();

		const expectedAction: workflowStateActions.WorkflowStateAction = new workflowStateActions.WorkflowStateAction(
			workflowStateActions.WorkflowStateModelActionTypes.UPDATE_ALL_WORKFLOW_STATE,
			{
				targetModels: testingNewWorkflowStates,
				collectionId: collectionId,
			}
		);

		// Find update all action from the function calls
		const actualAction: workflowStateActions.WorkflowStateAction = dispatchSpy.calls.all().find(dispatch => {
			const action = dispatch.args[0];
			return action instanceof workflowStateActions.WorkflowStateAction
					&& action.type === workflowStateActions.WorkflowStateModelActionTypes.UPDATE_ALL_WORKFLOW_STATE;
		}).args[0];

		// Check whether all parameters in request
		expect(actualAction.stateConfig.targetModels.length).toBe(testingWorkflowStates.length);
		actualAction.stateConfig.updates.forEach((update) => {
			expect(testingUpdateStates.find((workflowState) => workflowState.stepName === update.stepName)).not.toBeNull();
		});

		expect(actualAction.type).toEqual(expectedAction.type);
		expect(actualAction.stateConfig.collectionId).toEqual(expectedAction.stateConfig.collectionId);
		expect(workflowStatesComponent.buttonDisabled).toBeFalsy('should enable save button after request finished');

		// % protected region % [Add any logic after main process of 'should dispatch update action when click save draft function is called' here] off begin
		// % protected region % [Add any logic after main process of 'should dispatch update action when click save draft function is called' here] end
	}));

	it ('should dispatch delete action when click delete button for existing states', fakeAsync(() =>  {
		// % protected region % [Add any logic before main process of 'should dispatch delete action when click delete button for existing states' here] off begin
		// % protected region % [Add any logic before main process of 'should dispatch delete action when click delete button for existing states' here] end

		fixture.detectChanges();

		const deleteButtonEls: DebugElement[] = fixture.debugElement.queryAll(By.css('button.workflow__delete-state'));

		testingWorkflowStates.forEach((workflowToDelete, index) => {
			deleteButtonEls[index].nativeElement.click();

			tick();
			fixture.detectChanges();

			const expectedAction: workflowStateActions.WorkflowStateAction = new workflowStateActions.WorkflowStateAction(
				workflowStateActions.WorkflowStateModelActionTypes.DELETE_WORKFLOW_STATE,
				{
					targetModelId: workflowToDelete.id
				}
			);

			const actualAction = dispatchSpy.calls.mostRecent().args[0];

			expect(actualAction.type).toEqual(expectedAction.type);
			expect(actualAction.stateConfig.targetModelId).toEqual(expectedAction.stateConfig.targetModelId);
		});

		// % protected region % [Add any logic after main process of 'should dispatch delete action when click delete button for existing states' here] off begin
		// % protected region % [Add any logic after main process of 'should dispatch delete action when click delete button for existing states' here] end
	}));

	it ('should delete state from array when click delete button for new created states', fakeAsync(() =>  {
		// % protected region % [Add any logic before main process of 'should delete state from array when click delete button for new created states' here] off begin
		// % protected region % [Add any logic before main process of 'should delete state from array when click delete button for new created states' here] end

		fixture.detectChanges();

		const createButtonEl: DebugElement = fixture.debugElement.query(By.css('button.icon-plus'));

		// Click create button to create a new state
		createButtonEl.nativeElement.click();
		tick();
		fixture.detectChanges();

		expect(workflowStatesComponent.newStates.length).toBe(1, 'A new Workflow State should be created');

		const deleteButtonEls: DebugElement[] = fixture.debugElement.queryAll(By.css('button.workflow__delete-state'));

		// Last one should be the new created workflow state
		const deleteButtonToClick = deleteButtonEls[testingWorkflowStates.length];

		deleteButtonToClick.nativeElement.click();
		tick();
		fixture.detectChanges();

		expect(workflowStatesComponent.newStates.length).toBe(0, 'New created workflow should be deleted');

		// % protected region % [Add any logic after main process of 'should delete state from array when click delete button for new created states' here] off begin
		// % protected region % [Add any logic after main process of 'should delete state from array when click delete button for new created states' here] end
	}));

	it ('should just have one start state when click on existing states', fakeAsync(() =>  {

		// % protected region % [Add any logic before main process of 'should just have one start state when click on existing states' here] off begin
		// % protected region % [Add any logic before main process of 'should just have one start state when click on existing states' here] end

		fixture.detectChanges();

		const indexOfNewStartState = Math.floor(Math.random() * (testingUpdateStates.length - 1));
		const startStateButtons = fixture.debugElement.queryAll(By.css('button.workflow__start-state'));

		startStateButtons[indexOfNewStartState].nativeElement.click();
		tick();
		fixture.detectChanges();

		const numOfStartState = workflowStatesComponent.stateEditComponents.filter(comp => comp.stateForm.get('isStartState').value).length;

		expect(workflowStatesComponent.validateStates()).toBeTruthy('Should be valid');
		expect(numOfStartState).toBe(1, 'Should just have one start state');

		// % protected region % [Add any logic after main process of 'should just have one start state when click on existing states' here] off begin
		// % protected region % [Add any logic after main process of 'should just have one start state when click on existing states' here] end
	}));

	it ('should just have one start state when click on new created states', fakeAsync(() =>  {

		// % protected region % [Add any logic before main process of 'should just have one start state when click on new created states' here] off begin
		// % protected region % [Add any logic before main process of 'should just have one start state when click on new created states' here] end

		fixture.detectChanges();

		createNewState(fixture);

		const indexOfNewStartState = workflowStatesComponent.stateEditComponents.length - 1;
		const startStateButtons = fixture.debugElement.queryAll(By.css('button.workflow__start-state'));

		startStateButtons[indexOfNewStartState].nativeElement.click();
		tick();
		fixture.detectChanges();

		const numOfStartState = workflowStatesComponent.stateEditComponents.filter(comp => comp.stateForm.get('isStartState').value).length;

		expect(numOfStartState).toBe(1, 'Should just have one start state');

		// % protected region % [Add any logic after main process of 'should just have one start state when click on new created states' here] off begin
		// % protected region % [Add any logic after main process of 'should just have one start state when click on new created states' here] end
	}));

	// % protected region % [Add any tests for 'Workflow States Component' here] off begin
	// % protected region % [Add any tests for 'Workflow States Component' here] end
});

// % protected region % [Add any additional describes here] off begin
// % protected region % [Add any additional describes here] end
