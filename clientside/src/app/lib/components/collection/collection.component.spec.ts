/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {DebugElement} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ComponentFixture, fakeAsync, flush, TestBed, tick, waitForAsync} from '@angular/core/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {By} from '@angular/platform-browser';
import {CollectionComponent, HeaderOption} from './collection.component';
import {AdministratorModel} from 'src/app/models/administrator/administrator.model';
import {AdministratorDataFactory} from 'src/app/lib/utils/factories/administrator-data-factory';
import {ApplicationLocaleModel} from 'src/app/models/applicationLocale/application_locale.model';
import {ApplicationLocaleDataFactory} from 'src/app/lib/utils/factories/application-locale-data-factory';
import {ArticleModel} from 'src/app/models/article/article.model';
import {ArticleDataFactory} from 'src/app/lib/utils/factories/article-data-factory';
import {BookModel} from 'src/app/models/book/book.model';
import {BookDataFactory} from 'src/app/lib/utils/factories/book-data-factory';
import {CoreUserModel} from 'src/app/models/coreUser/core_user.model';
import {CoreUserDataFactory} from 'src/app/lib/utils/factories/core-user-data-factory';
import {CourseModel} from 'src/app/models/course/course.model';
import {CourseDataFactory} from 'src/app/lib/utils/factories/course-data-factory';
import {CourseCategoryModel} from 'src/app/models/courseCategory/course_category.model';
import {CourseCategoryDataFactory} from 'src/app/lib/utils/factories/course-category-data-factory';
import {CourseLessonModel} from 'src/app/models/courseLesson/course_lesson.model';
import {CourseLessonDataFactory} from 'src/app/lib/utils/factories/course-lesson-data-factory';
import {LessonModel} from 'src/app/models/lesson/lesson.model';
import {LessonDataFactory} from 'src/app/lib/utils/factories/lesson-data-factory';
import {TagModel} from 'src/app/models/tag/tag.model';
import {TagDataFactory} from 'src/app/lib/utils/factories/tag-data-factory';
import {RoleModel} from 'src/app/models/role/role.model';
import {RoleDataFactory} from 'src/app/lib/utils/factories/role-data-factory';
import {PrivilegeModel} from 'src/app/models/privilege/privilege.model';
import {PrivilegeDataFactory} from 'src/app/lib/utils/factories/privilege-data-factory';
import {WorkflowModel} from 'src/app/models/workflow/workflow.model';
import {WorkflowDataFactory} from 'src/app/lib/utils/factories/workflow-data-factory';
import {WorkflowStateModel} from 'src/app/models/workflowState/workflow_state.model';
import {WorkflowStateDataFactory} from 'src/app/lib/utils/factories/workflow-state-data-factory';
import {WorkflowTransitionModel} from 'src/app/models/workflowTransition/workflow_transition.model';
import {WorkflowTransitionDataFactory} from 'src/app/lib/utils/factories/workflow-transition-data-factory';
import {WorkflowVersionModel} from 'src/app/models/workflowVersion/workflow_version.model';
import {WorkflowVersionDataFactory} from 'src/app/lib/utils/factories/workflow-version-data-factory';
import {LessonFormSubmissionModel} from 'src/app/models/lessonFormSubmission/lesson_form_submission.model';
import {LessonFormSubmissionDataFactory} from 'src/app/lib/utils/factories/lesson-form-submission-data-factory';
import {LessonFormVersionModel} from 'src/app/models/lessonFormVersion/lesson_form_version.model';
import {LessonFormVersionDataFactory} from 'src/app/lib/utils/factories/lesson-form-version-data-factory';
import {LessonFormTileModel} from 'src/app/models/lessonFormTile/lesson_form_tile.model';
import {LessonFormTileDataFactory} from 'src/app/lib/utils/factories/lesson-form-tile-data-factory';
import {ModelProperty} from '../../models/abstract.model';
import {CommonComponentModule} from '../common.component.module';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

describe('Collection component against Administrator', () => {

	let fixture: ComponentFixture<CollectionComponent<AdministratorModel>>;
	let collectionComponent: CollectionComponent<AdministratorModel>;

	let administratorDataFactory: AdministratorDataFactory;
	let models: AdministratorModel[];
	let modelProperties: ModelProperty[];

	let defaultHeaderOptions: HeaderOption[];

	beforeAll(() => {
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] off begin
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] end

		administratorDataFactory = new AdministratorDataFactory();

		modelProperties = AdministratorModel.getProps();

		defaultHeaderOptions =  modelProperties.map(prop => {
			return {
				...prop,
				sortable: true,
				sourceDirectFromModel: true,
				valueSource: prop.name
			} as HeaderOption;
		});

		// % protected region % [Add any additional setup in beforeAll after the main body here for Administrator] off begin
		// % protected region % [Add any additional setup in beforeAll after the main body here for Administrator] end
	});

	beforeEach(waitForAsync (() => {
		models = administratorDataFactory.createAll();

		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				CommonComponentModule
			],
			// % protected region % [Add any additional configurations here for Collection component against Administrator] off begin
			// % protected region % [Add any additional configurations here for Collection component against Administrator] end
		}).compileComponents().then(() => {
			fixture = TestBed.createComponent<CollectionComponent<AdministratorModel>>(CollectionComponent);
			collectionComponent = fixture.debugElement.componentInstance;
		});
	}));

	afterEach(() => {
		(fixture.nativeElement as HTMLElement).remove();
	});

	it('should create the collection component', () => {
		expect(collectionComponent).toBeTruthy();
	});

	it('should have no items selected when first viewed', () => {
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Administrator] off begin
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Administrator] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const selectAllInputEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllInputEl.checked).toBeFalsy(`'Select All' is checked`);

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => {
			return prev && currentValue;
		});
		expect(allChecked).toBeFalsy('Not all checkboxes are deselected');

		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Administrator] off begin
		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Administrator] end
	});

	it('should select all items when \'Select All\' is checked', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Administrator] off begin
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Administrator] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement
			.click();

		fixture.detectChanges();
		tick();

		expect(collectionComponent.selectedModels.size).toBe(models.length, 'Not all checkboxes are checked');

		fixture.detectChanges();

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('10 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Administrator] off begin
		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Administrator] end

	}));

	it('should check \'Select All\' when all checkboxes are checked',  fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Administrator] off begin
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Administrator] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkbox => checkbox.nativeElement as HTMLInputElement);

		checkboxEls.forEach(checkbox => checkbox.click());

		fixture.detectChanges();
		tick();

		fixture.detectChanges();

		expect(collectionComponent.selectedModels.size).toBe(collectionComponent.models.length, 'Not all checkboxes are checked');

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe(`${collectionComponent.models.length} selected`, 'Select number not showing correctly');

		const selectAllEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllEl.checked).toBeTruthy('\'Select All\' checkbox is not selected when all rows of the current page are selected');

		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Administrator] off begin
		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Administrator] end
	}));

	it('should select the correct row', () => {
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Administrator] off begin
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Administrator] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkboxElement => checkboxElement.nativeElement as HTMLInputElement);

		// Random row to select
		const rowIndex = 2;
		const checkbox: DebugElement = checkboxes[rowIndex];
		const checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkboxEl.click();
		fixture.detectChanges();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		const unselectedCheckboxes: boolean[] = checkboxEls
			.filter(element => !element.checked)
			.map(element => element.checked);

		expect(unselectedCheckboxes).toEqual(new Array(collectionComponent.models.length - 1).fill(false), 'Some other checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Administrator] off begin
		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Administrator] end
	});

	it('should keep selection when switch between grid and list', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Administrator] off begin
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Administrator] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		// Random row to select
		const rowIndex = Math.floor(Math.random() * 10);
		let checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		let checkbox: DebugElement = checkboxes[rowIndex];
		let checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkbox.nativeElement.click();

		fixture.detectChanges();
		tick();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		// Switch to grid view
		const gridButton = fixture.debugElement.query(By.css('.btn.icon-grid'));
		gridButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected and display correctly
		let selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');


		// Change back to list
		const listButton = fixture.debugElement.query(By.css('.btn.icon-list'));
		listButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected
		selectText = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Administrator] off begin
		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Administrator] end
	}));

	// % protected region % [Add any additional tests here for Collection component against Administrator] off begin
	// % protected region % [Add any additional tests here for Collection component against Administrator] end
});

describe('Collection component against Application Locale', () => {

	let fixture: ComponentFixture<CollectionComponent<ApplicationLocaleModel>>;
	let collectionComponent: CollectionComponent<ApplicationLocaleModel>;

	let applicationLocaleDataFactory: ApplicationLocaleDataFactory;
	let models: ApplicationLocaleModel[];
	let modelProperties: ModelProperty[];

	let defaultHeaderOptions: HeaderOption[];

	beforeAll(() => {
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] off begin
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] end

		applicationLocaleDataFactory = new ApplicationLocaleDataFactory();

		modelProperties = ApplicationLocaleModel.getProps();

		defaultHeaderOptions =  modelProperties.map(prop => {
			return {
				...prop,
				sortable: true,
				sourceDirectFromModel: true,
				valueSource: prop.name
			} as HeaderOption;
		});

		// % protected region % [Add any additional setup in beforeAll after the main body here for Application Locale] off begin
		// % protected region % [Add any additional setup in beforeAll after the main body here for Application Locale] end
	});

	beforeEach(waitForAsync (() => {
		models = applicationLocaleDataFactory.createAll();

		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				CommonComponentModule
			],
			// % protected region % [Add any additional configurations here for Collection component against Application Locale] off begin
			// % protected region % [Add any additional configurations here for Collection component against Application Locale] end
		}).compileComponents().then(() => {
			fixture = TestBed.createComponent<CollectionComponent<ApplicationLocaleModel>>(CollectionComponent);
			collectionComponent = fixture.debugElement.componentInstance;
		});
	}));

	afterEach(() => {
		(fixture.nativeElement as HTMLElement).remove();
	});

	it('should create the collection component', () => {
		expect(collectionComponent).toBeTruthy();
	});

	it('should have no items selected when first viewed', () => {
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Application Locale] off begin
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Application Locale] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const selectAllInputEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllInputEl.checked).toBeFalsy(`'Select All' is checked`);

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => {
			return prev && currentValue;
		});
		expect(allChecked).toBeFalsy('Not all checkboxes are deselected');

		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Application Locale] off begin
		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Application Locale] end
	});

	it('should select all items when \'Select All\' is checked', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Application Locale] off begin
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Application Locale] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement
			.click();

		fixture.detectChanges();
		tick();

		expect(collectionComponent.selectedModels.size).toBe(models.length, 'Not all checkboxes are checked');

		fixture.detectChanges();

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('10 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Application Locale] off begin
		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Application Locale] end

	}));

	it('should check \'Select All\' when all checkboxes are checked',  fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Application Locale] off begin
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Application Locale] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkbox => checkbox.nativeElement as HTMLInputElement);

		checkboxEls.forEach(checkbox => checkbox.click());

		fixture.detectChanges();
		tick();

		fixture.detectChanges();

		expect(collectionComponent.selectedModels.size).toBe(collectionComponent.models.length, 'Not all checkboxes are checked');

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe(`${collectionComponent.models.length} selected`, 'Select number not showing correctly');

		const selectAllEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllEl.checked).toBeTruthy('\'Select All\' checkbox is not selected when all rows of the current page are selected');

		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Application Locale] off begin
		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Application Locale] end
	}));

	it('should select the correct row', () => {
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Application Locale] off begin
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Application Locale] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkboxElement => checkboxElement.nativeElement as HTMLInputElement);

		// Random row to select
		const rowIndex = 2;
		const checkbox: DebugElement = checkboxes[rowIndex];
		const checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkboxEl.click();
		fixture.detectChanges();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		const unselectedCheckboxes: boolean[] = checkboxEls
			.filter(element => !element.checked)
			.map(element => element.checked);

		expect(unselectedCheckboxes).toEqual(new Array(collectionComponent.models.length - 1).fill(false), 'Some other checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Application Locale] off begin
		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Application Locale] end
	});

	it('should keep selection when switch between grid and list', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Application Locale] off begin
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Application Locale] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		// Random row to select
		const rowIndex = Math.floor(Math.random() * 10);
		let checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		let checkbox: DebugElement = checkboxes[rowIndex];
		let checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkbox.nativeElement.click();

		fixture.detectChanges();
		tick();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		// Switch to grid view
		const gridButton = fixture.debugElement.query(By.css('.btn.icon-grid'));
		gridButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected and display correctly
		let selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');


		// Change back to list
		const listButton = fixture.debugElement.query(By.css('.btn.icon-list'));
		listButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected
		selectText = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Application Locale] off begin
		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Application Locale] end
	}));

	// % protected region % [Add any additional tests here for Collection component against Application Locale] off begin
	// % protected region % [Add any additional tests here for Collection component against Application Locale] end
});

describe('Collection component against Article', () => {

	let fixture: ComponentFixture<CollectionComponent<ArticleModel>>;
	let collectionComponent: CollectionComponent<ArticleModel>;

	let articleDataFactory: ArticleDataFactory;
	let models: ArticleModel[];
	let modelProperties: ModelProperty[];

	let defaultHeaderOptions: HeaderOption[];

	beforeAll(() => {
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] off begin
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] end

		articleDataFactory = new ArticleDataFactory();

		modelProperties = ArticleModel.getProps();

		defaultHeaderOptions =  modelProperties.map(prop => {
			return {
				...prop,
				sortable: true,
				sourceDirectFromModel: true,
				valueSource: prop.name
			} as HeaderOption;
		});

		// % protected region % [Add any additional setup in beforeAll after the main body here for Article] off begin
		// % protected region % [Add any additional setup in beforeAll after the main body here for Article] end
	});

	beforeEach(waitForAsync (() => {
		models = articleDataFactory.createAll();

		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				CommonComponentModule
			],
			// % protected region % [Add any additional configurations here for Collection component against Article] off begin
			// % protected region % [Add any additional configurations here for Collection component against Article] end
		}).compileComponents().then(() => {
			fixture = TestBed.createComponent<CollectionComponent<ArticleModel>>(CollectionComponent);
			collectionComponent = fixture.debugElement.componentInstance;
		});
	}));

	afterEach(() => {
		(fixture.nativeElement as HTMLElement).remove();
	});

	it('should create the collection component', () => {
		expect(collectionComponent).toBeTruthy();
	});

	it('should have no items selected when first viewed', () => {
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Article] off begin
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Article] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const selectAllInputEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllInputEl.checked).toBeFalsy(`'Select All' is checked`);

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => {
			return prev && currentValue;
		});
		expect(allChecked).toBeFalsy('Not all checkboxes are deselected');

		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Article] off begin
		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Article] end
	});

	it('should select all items when \'Select All\' is checked', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Article] off begin
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Article] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement
			.click();

		fixture.detectChanges();
		tick();

		expect(collectionComponent.selectedModels.size).toBe(models.length, 'Not all checkboxes are checked');

		fixture.detectChanges();

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('10 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Article] off begin
		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Article] end

	}));

	it('should check \'Select All\' when all checkboxes are checked',  fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Article] off begin
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Article] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkbox => checkbox.nativeElement as HTMLInputElement);

		checkboxEls.forEach(checkbox => checkbox.click());

		fixture.detectChanges();
		tick();

		fixture.detectChanges();

		expect(collectionComponent.selectedModels.size).toBe(collectionComponent.models.length, 'Not all checkboxes are checked');

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe(`${collectionComponent.models.length} selected`, 'Select number not showing correctly');

		const selectAllEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllEl.checked).toBeTruthy('\'Select All\' checkbox is not selected when all rows of the current page are selected');

		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Article] off begin
		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Article] end
	}));

	it('should select the correct row', () => {
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Article] off begin
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Article] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkboxElement => checkboxElement.nativeElement as HTMLInputElement);

		// Random row to select
		const rowIndex = 2;
		const checkbox: DebugElement = checkboxes[rowIndex];
		const checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkboxEl.click();
		fixture.detectChanges();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		const unselectedCheckboxes: boolean[] = checkboxEls
			.filter(element => !element.checked)
			.map(element => element.checked);

		expect(unselectedCheckboxes).toEqual(new Array(collectionComponent.models.length - 1).fill(false), 'Some other checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Article] off begin
		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Article] end
	});

	it('should keep selection when switch between grid and list', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Article] off begin
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Article] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		// Random row to select
		const rowIndex = Math.floor(Math.random() * 10);
		let checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		let checkbox: DebugElement = checkboxes[rowIndex];
		let checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkbox.nativeElement.click();

		fixture.detectChanges();
		tick();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		// Switch to grid view
		const gridButton = fixture.debugElement.query(By.css('.btn.icon-grid'));
		gridButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected and display correctly
		let selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');


		// Change back to list
		const listButton = fixture.debugElement.query(By.css('.btn.icon-list'));
		listButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected
		selectText = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Article] off begin
		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Article] end
	}));

	// % protected region % [Add any additional tests here for Collection component against Article] off begin
	// % protected region % [Add any additional tests here for Collection component against Article] end
});

describe('Collection component against Book', () => {

	let fixture: ComponentFixture<CollectionComponent<BookModel>>;
	let collectionComponent: CollectionComponent<BookModel>;

	let bookDataFactory: BookDataFactory;
	let models: BookModel[];
	let modelProperties: ModelProperty[];

	let defaultHeaderOptions: HeaderOption[];

	beforeAll(() => {
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] off begin
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] end

		bookDataFactory = new BookDataFactory();

		modelProperties = BookModel.getProps();

		defaultHeaderOptions =  modelProperties.map(prop => {
			return {
				...prop,
				sortable: true,
				sourceDirectFromModel: true,
				valueSource: prop.name
			} as HeaderOption;
		});

		// % protected region % [Add any additional setup in beforeAll after the main body here for Book] off begin
		// % protected region % [Add any additional setup in beforeAll after the main body here for Book] end
	});

	beforeEach(waitForAsync (() => {
		models = bookDataFactory.createAll();

		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				CommonComponentModule
			],
			// % protected region % [Add any additional configurations here for Collection component against Book] off begin
			// % protected region % [Add any additional configurations here for Collection component against Book] end
		}).compileComponents().then(() => {
			fixture = TestBed.createComponent<CollectionComponent<BookModel>>(CollectionComponent);
			collectionComponent = fixture.debugElement.componentInstance;
		});
	}));

	afterEach(() => {
		(fixture.nativeElement as HTMLElement).remove();
	});

	it('should create the collection component', () => {
		expect(collectionComponent).toBeTruthy();
	});

	it('should have no items selected when first viewed', () => {
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Book] off begin
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Book] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const selectAllInputEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllInputEl.checked).toBeFalsy(`'Select All' is checked`);

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => {
			return prev && currentValue;
		});
		expect(allChecked).toBeFalsy('Not all checkboxes are deselected');

		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Book] off begin
		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Book] end
	});

	it('should select all items when \'Select All\' is checked', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Book] off begin
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Book] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement
			.click();

		fixture.detectChanges();
		tick();

		expect(collectionComponent.selectedModels.size).toBe(models.length, 'Not all checkboxes are checked');

		fixture.detectChanges();

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('10 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Book] off begin
		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Book] end

	}));

	it('should check \'Select All\' when all checkboxes are checked',  fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Book] off begin
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Book] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkbox => checkbox.nativeElement as HTMLInputElement);

		checkboxEls.forEach(checkbox => checkbox.click());

		fixture.detectChanges();
		tick();

		fixture.detectChanges();

		expect(collectionComponent.selectedModels.size).toBe(collectionComponent.models.length, 'Not all checkboxes are checked');

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe(`${collectionComponent.models.length} selected`, 'Select number not showing correctly');

		const selectAllEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllEl.checked).toBeTruthy('\'Select All\' checkbox is not selected when all rows of the current page are selected');

		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Book] off begin
		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Book] end
	}));

	it('should select the correct row', () => {
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Book] off begin
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Book] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkboxElement => checkboxElement.nativeElement as HTMLInputElement);

		// Random row to select
		const rowIndex = 2;
		const checkbox: DebugElement = checkboxes[rowIndex];
		const checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkboxEl.click();
		fixture.detectChanges();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		const unselectedCheckboxes: boolean[] = checkboxEls
			.filter(element => !element.checked)
			.map(element => element.checked);

		expect(unselectedCheckboxes).toEqual(new Array(collectionComponent.models.length - 1).fill(false), 'Some other checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Book] off begin
		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Book] end
	});

	it('should keep selection when switch between grid and list', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Book] off begin
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Book] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		// Random row to select
		const rowIndex = Math.floor(Math.random() * 10);
		let checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		let checkbox: DebugElement = checkboxes[rowIndex];
		let checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkbox.nativeElement.click();

		fixture.detectChanges();
		tick();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		// Switch to grid view
		const gridButton = fixture.debugElement.query(By.css('.btn.icon-grid'));
		gridButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected and display correctly
		let selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');


		// Change back to list
		const listButton = fixture.debugElement.query(By.css('.btn.icon-list'));
		listButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected
		selectText = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Book] off begin
		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Book] end
	}));

	// % protected region % [Add any additional tests here for Collection component against Book] off begin
	// % protected region % [Add any additional tests here for Collection component against Book] end
});

describe('Collection component against Core User', () => {

	let fixture: ComponentFixture<CollectionComponent<CoreUserModel>>;
	let collectionComponent: CollectionComponent<CoreUserModel>;

	let coreUserDataFactory: CoreUserDataFactory;
	let models: CoreUserModel[];
	let modelProperties: ModelProperty[];

	let defaultHeaderOptions: HeaderOption[];

	beforeAll(() => {
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] off begin
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] end

		coreUserDataFactory = new CoreUserDataFactory();

		modelProperties = CoreUserModel.getProps();

		defaultHeaderOptions =  modelProperties.map(prop => {
			return {
				...prop,
				sortable: true,
				sourceDirectFromModel: true,
				valueSource: prop.name
			} as HeaderOption;
		});

		// % protected region % [Add any additional setup in beforeAll after the main body here for Core User] off begin
		// % protected region % [Add any additional setup in beforeAll after the main body here for Core User] end
	});

	beforeEach(waitForAsync (() => {
		models = coreUserDataFactory.createAll();

		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				CommonComponentModule
			],
			// % protected region % [Add any additional configurations here for Collection component against Core User] off begin
			// % protected region % [Add any additional configurations here for Collection component against Core User] end
		}).compileComponents().then(() => {
			fixture = TestBed.createComponent<CollectionComponent<CoreUserModel>>(CollectionComponent);
			collectionComponent = fixture.debugElement.componentInstance;
		});
	}));

	afterEach(() => {
		(fixture.nativeElement as HTMLElement).remove();
	});

	it('should create the collection component', () => {
		expect(collectionComponent).toBeTruthy();
	});

	it('should have no items selected when first viewed', () => {
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Core User] off begin
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Core User] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const selectAllInputEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllInputEl.checked).toBeFalsy(`'Select All' is checked`);

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => {
			return prev && currentValue;
		});
		expect(allChecked).toBeFalsy('Not all checkboxes are deselected');

		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Core User] off begin
		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Core User] end
	});

	it('should select all items when \'Select All\' is checked', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Core User] off begin
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Core User] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement
			.click();

		fixture.detectChanges();
		tick();

		expect(collectionComponent.selectedModels.size).toBe(models.length, 'Not all checkboxes are checked');

		fixture.detectChanges();

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('10 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Core User] off begin
		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Core User] end

	}));

	it('should check \'Select All\' when all checkboxes are checked',  fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Core User] off begin
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Core User] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkbox => checkbox.nativeElement as HTMLInputElement);

		checkboxEls.forEach(checkbox => checkbox.click());

		fixture.detectChanges();
		tick();

		fixture.detectChanges();

		expect(collectionComponent.selectedModels.size).toBe(collectionComponent.models.length, 'Not all checkboxes are checked');

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe(`${collectionComponent.models.length} selected`, 'Select number not showing correctly');

		const selectAllEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllEl.checked).toBeTruthy('\'Select All\' checkbox is not selected when all rows of the current page are selected');

		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Core User] off begin
		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Core User] end
	}));

	it('should select the correct row', () => {
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Core User] off begin
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Core User] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkboxElement => checkboxElement.nativeElement as HTMLInputElement);

		// Random row to select
		const rowIndex = 2;
		const checkbox: DebugElement = checkboxes[rowIndex];
		const checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkboxEl.click();
		fixture.detectChanges();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		const unselectedCheckboxes: boolean[] = checkboxEls
			.filter(element => !element.checked)
			.map(element => element.checked);

		expect(unselectedCheckboxes).toEqual(new Array(collectionComponent.models.length - 1).fill(false), 'Some other checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Core User] off begin
		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Core User] end
	});

	it('should keep selection when switch between grid and list', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Core User] off begin
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Core User] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		// Random row to select
		const rowIndex = Math.floor(Math.random() * 10);
		let checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		let checkbox: DebugElement = checkboxes[rowIndex];
		let checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkbox.nativeElement.click();

		fixture.detectChanges();
		tick();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		// Switch to grid view
		const gridButton = fixture.debugElement.query(By.css('.btn.icon-grid'));
		gridButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected and display correctly
		let selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');


		// Change back to list
		const listButton = fixture.debugElement.query(By.css('.btn.icon-list'));
		listButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected
		selectText = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Core User] off begin
		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Core User] end
	}));

	// % protected region % [Add any additional tests here for Collection component against Core User] off begin
	// % protected region % [Add any additional tests here for Collection component against Core User] end
});

describe('Collection component against Course', () => {

	let fixture: ComponentFixture<CollectionComponent<CourseModel>>;
	let collectionComponent: CollectionComponent<CourseModel>;

	let courseDataFactory: CourseDataFactory;
	let models: CourseModel[];
	let modelProperties: ModelProperty[];

	let defaultHeaderOptions: HeaderOption[];

	beforeAll(() => {
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] off begin
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] end

		courseDataFactory = new CourseDataFactory();

		modelProperties = CourseModel.getProps();

		defaultHeaderOptions =  modelProperties.map(prop => {
			return {
				...prop,
				sortable: true,
				sourceDirectFromModel: true,
				valueSource: prop.name
			} as HeaderOption;
		});

		// % protected region % [Add any additional setup in beforeAll after the main body here for Course] off begin
		// % protected region % [Add any additional setup in beforeAll after the main body here for Course] end
	});

	beforeEach(waitForAsync (() => {
		models = courseDataFactory.createAll();

		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				CommonComponentModule
			],
			// % protected region % [Add any additional configurations here for Collection component against Course] off begin
			// % protected region % [Add any additional configurations here for Collection component against Course] end
		}).compileComponents().then(() => {
			fixture = TestBed.createComponent<CollectionComponent<CourseModel>>(CollectionComponent);
			collectionComponent = fixture.debugElement.componentInstance;
		});
	}));

	afterEach(() => {
		(fixture.nativeElement as HTMLElement).remove();
	});

	it('should create the collection component', () => {
		expect(collectionComponent).toBeTruthy();
	});

	it('should have no items selected when first viewed', () => {
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Course] off begin
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Course] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const selectAllInputEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllInputEl.checked).toBeFalsy(`'Select All' is checked`);

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => {
			return prev && currentValue;
		});
		expect(allChecked).toBeFalsy('Not all checkboxes are deselected');

		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Course] off begin
		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Course] end
	});

	it('should select all items when \'Select All\' is checked', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Course] off begin
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Course] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement
			.click();

		fixture.detectChanges();
		tick();

		expect(collectionComponent.selectedModels.size).toBe(models.length, 'Not all checkboxes are checked');

		fixture.detectChanges();

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('10 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Course] off begin
		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Course] end

	}));

	it('should check \'Select All\' when all checkboxes are checked',  fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Course] off begin
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Course] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkbox => checkbox.nativeElement as HTMLInputElement);

		checkboxEls.forEach(checkbox => checkbox.click());

		fixture.detectChanges();
		tick();

		fixture.detectChanges();

		expect(collectionComponent.selectedModels.size).toBe(collectionComponent.models.length, 'Not all checkboxes are checked');

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe(`${collectionComponent.models.length} selected`, 'Select number not showing correctly');

		const selectAllEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllEl.checked).toBeTruthy('\'Select All\' checkbox is not selected when all rows of the current page are selected');

		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Course] off begin
		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Course] end
	}));

	it('should select the correct row', () => {
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Course] off begin
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Course] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkboxElement => checkboxElement.nativeElement as HTMLInputElement);

		// Random row to select
		const rowIndex = 2;
		const checkbox: DebugElement = checkboxes[rowIndex];
		const checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkboxEl.click();
		fixture.detectChanges();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		const unselectedCheckboxes: boolean[] = checkboxEls
			.filter(element => !element.checked)
			.map(element => element.checked);

		expect(unselectedCheckboxes).toEqual(new Array(collectionComponent.models.length - 1).fill(false), 'Some other checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Course] off begin
		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Course] end
	});

	it('should keep selection when switch between grid and list', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Course] off begin
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Course] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		// Random row to select
		const rowIndex = Math.floor(Math.random() * 10);
		let checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		let checkbox: DebugElement = checkboxes[rowIndex];
		let checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkbox.nativeElement.click();

		fixture.detectChanges();
		tick();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		// Switch to grid view
		const gridButton = fixture.debugElement.query(By.css('.btn.icon-grid'));
		gridButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected and display correctly
		let selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');


		// Change back to list
		const listButton = fixture.debugElement.query(By.css('.btn.icon-list'));
		listButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected
		selectText = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Course] off begin
		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Course] end
	}));

	// % protected region % [Add any additional tests here for Collection component against Course] off begin
	// % protected region % [Add any additional tests here for Collection component against Course] end
});

describe('Collection component against Course Category', () => {

	let fixture: ComponentFixture<CollectionComponent<CourseCategoryModel>>;
	let collectionComponent: CollectionComponent<CourseCategoryModel>;

	let courseCategoryDataFactory: CourseCategoryDataFactory;
	let models: CourseCategoryModel[];
	let modelProperties: ModelProperty[];

	let defaultHeaderOptions: HeaderOption[];

	beforeAll(() => {
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] off begin
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] end

		courseCategoryDataFactory = new CourseCategoryDataFactory();

		modelProperties = CourseCategoryModel.getProps();

		defaultHeaderOptions =  modelProperties.map(prop => {
			return {
				...prop,
				sortable: true,
				sourceDirectFromModel: true,
				valueSource: prop.name
			} as HeaderOption;
		});

		// % protected region % [Add any additional setup in beforeAll after the main body here for Course Category] off begin
		// % protected region % [Add any additional setup in beforeAll after the main body here for Course Category] end
	});

	beforeEach(waitForAsync (() => {
		models = courseCategoryDataFactory.createAll();

		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				CommonComponentModule
			],
			// % protected region % [Add any additional configurations here for Collection component against Course Category] off begin
			// % protected region % [Add any additional configurations here for Collection component against Course Category] end
		}).compileComponents().then(() => {
			fixture = TestBed.createComponent<CollectionComponent<CourseCategoryModel>>(CollectionComponent);
			collectionComponent = fixture.debugElement.componentInstance;
		});
	}));

	afterEach(() => {
		(fixture.nativeElement as HTMLElement).remove();
	});

	it('should create the collection component', () => {
		expect(collectionComponent).toBeTruthy();
	});

	it('should have no items selected when first viewed', () => {
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Course Category] off begin
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Course Category] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const selectAllInputEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllInputEl.checked).toBeFalsy(`'Select All' is checked`);

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => {
			return prev && currentValue;
		});
		expect(allChecked).toBeFalsy('Not all checkboxes are deselected');

		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Course Category] off begin
		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Course Category] end
	});

	it('should select all items when \'Select All\' is checked', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Course Category] off begin
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Course Category] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement
			.click();

		fixture.detectChanges();
		tick();

		expect(collectionComponent.selectedModels.size).toBe(models.length, 'Not all checkboxes are checked');

		fixture.detectChanges();

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('10 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Course Category] off begin
		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Course Category] end

	}));

	it('should check \'Select All\' when all checkboxes are checked',  fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Course Category] off begin
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Course Category] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkbox => checkbox.nativeElement as HTMLInputElement);

		checkboxEls.forEach(checkbox => checkbox.click());

		fixture.detectChanges();
		tick();

		fixture.detectChanges();

		expect(collectionComponent.selectedModels.size).toBe(collectionComponent.models.length, 'Not all checkboxes are checked');

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe(`${collectionComponent.models.length} selected`, 'Select number not showing correctly');

		const selectAllEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllEl.checked).toBeTruthy('\'Select All\' checkbox is not selected when all rows of the current page are selected');

		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Course Category] off begin
		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Course Category] end
	}));

	it('should select the correct row', () => {
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Course Category] off begin
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Course Category] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkboxElement => checkboxElement.nativeElement as HTMLInputElement);

		// Random row to select
		const rowIndex = 2;
		const checkbox: DebugElement = checkboxes[rowIndex];
		const checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkboxEl.click();
		fixture.detectChanges();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		const unselectedCheckboxes: boolean[] = checkboxEls
			.filter(element => !element.checked)
			.map(element => element.checked);

		expect(unselectedCheckboxes).toEqual(new Array(collectionComponent.models.length - 1).fill(false), 'Some other checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Course Category] off begin
		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Course Category] end
	});

	it('should keep selection when switch between grid and list', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Course Category] off begin
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Course Category] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		// Random row to select
		const rowIndex = Math.floor(Math.random() * 10);
		let checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		let checkbox: DebugElement = checkboxes[rowIndex];
		let checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkbox.nativeElement.click();

		fixture.detectChanges();
		tick();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		// Switch to grid view
		const gridButton = fixture.debugElement.query(By.css('.btn.icon-grid'));
		gridButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected and display correctly
		let selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');


		// Change back to list
		const listButton = fixture.debugElement.query(By.css('.btn.icon-list'));
		listButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected
		selectText = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Course Category] off begin
		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Course Category] end
	}));

	// % protected region % [Add any additional tests here for Collection component against Course Category] off begin
	// % protected region % [Add any additional tests here for Collection component against Course Category] end
});

describe('Collection component against Course Lesson', () => {

	let fixture: ComponentFixture<CollectionComponent<CourseLessonModel>>;
	let collectionComponent: CollectionComponent<CourseLessonModel>;

	let courseLessonDataFactory: CourseLessonDataFactory;
	let models: CourseLessonModel[];
	let modelProperties: ModelProperty[];

	let defaultHeaderOptions: HeaderOption[];

	beforeAll(() => {
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] off begin
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] end

		courseLessonDataFactory = new CourseLessonDataFactory();

		modelProperties = CourseLessonModel.getProps();

		defaultHeaderOptions =  modelProperties.map(prop => {
			return {
				...prop,
				sortable: true,
				sourceDirectFromModel: true,
				valueSource: prop.name
			} as HeaderOption;
		});

		// % protected region % [Add any additional setup in beforeAll after the main body here for Course Lesson] off begin
		// % protected region % [Add any additional setup in beforeAll after the main body here for Course Lesson] end
	});

	beforeEach(waitForAsync (() => {
		models = courseLessonDataFactory.createAll();

		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				CommonComponentModule
			],
			// % protected region % [Add any additional configurations here for Collection component against Course Lesson] off begin
			// % protected region % [Add any additional configurations here for Collection component against Course Lesson] end
		}).compileComponents().then(() => {
			fixture = TestBed.createComponent<CollectionComponent<CourseLessonModel>>(CollectionComponent);
			collectionComponent = fixture.debugElement.componentInstance;
		});
	}));

	afterEach(() => {
		(fixture.nativeElement as HTMLElement).remove();
	});

	it('should create the collection component', () => {
		expect(collectionComponent).toBeTruthy();
	});

	it('should have no items selected when first viewed', () => {
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Course Lesson] off begin
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Course Lesson] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const selectAllInputEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllInputEl.checked).toBeFalsy(`'Select All' is checked`);

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => {
			return prev && currentValue;
		});
		expect(allChecked).toBeFalsy('Not all checkboxes are deselected');

		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Course Lesson] off begin
		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Course Lesson] end
	});

	it('should select all items when \'Select All\' is checked', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Course Lesson] off begin
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Course Lesson] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement
			.click();

		fixture.detectChanges();
		tick();

		expect(collectionComponent.selectedModels.size).toBe(models.length, 'Not all checkboxes are checked');

		fixture.detectChanges();

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('10 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Course Lesson] off begin
		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Course Lesson] end

	}));

	it('should check \'Select All\' when all checkboxes are checked',  fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Course Lesson] off begin
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Course Lesson] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkbox => checkbox.nativeElement as HTMLInputElement);

		checkboxEls.forEach(checkbox => checkbox.click());

		fixture.detectChanges();
		tick();

		fixture.detectChanges();

		expect(collectionComponent.selectedModels.size).toBe(collectionComponent.models.length, 'Not all checkboxes are checked');

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe(`${collectionComponent.models.length} selected`, 'Select number not showing correctly');

		const selectAllEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllEl.checked).toBeTruthy('\'Select All\' checkbox is not selected when all rows of the current page are selected');

		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Course Lesson] off begin
		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Course Lesson] end
	}));

	it('should select the correct row', () => {
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Course Lesson] off begin
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Course Lesson] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkboxElement => checkboxElement.nativeElement as HTMLInputElement);

		// Random row to select
		const rowIndex = 2;
		const checkbox: DebugElement = checkboxes[rowIndex];
		const checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkboxEl.click();
		fixture.detectChanges();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		const unselectedCheckboxes: boolean[] = checkboxEls
			.filter(element => !element.checked)
			.map(element => element.checked);

		expect(unselectedCheckboxes).toEqual(new Array(collectionComponent.models.length - 1).fill(false), 'Some other checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Course Lesson] off begin
		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Course Lesson] end
	});

	it('should keep selection when switch between grid and list', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Course Lesson] off begin
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Course Lesson] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		// Random row to select
		const rowIndex = Math.floor(Math.random() * 10);
		let checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		let checkbox: DebugElement = checkboxes[rowIndex];
		let checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkbox.nativeElement.click();

		fixture.detectChanges();
		tick();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		// Switch to grid view
		const gridButton = fixture.debugElement.query(By.css('.btn.icon-grid'));
		gridButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected and display correctly
		let selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');


		// Change back to list
		const listButton = fixture.debugElement.query(By.css('.btn.icon-list'));
		listButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected
		selectText = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Course Lesson] off begin
		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Course Lesson] end
	}));

	// % protected region % [Add any additional tests here for Collection component against Course Lesson] off begin
	// % protected region % [Add any additional tests here for Collection component against Course Lesson] end
});

describe('Collection component against Lesson', () => {

	let fixture: ComponentFixture<CollectionComponent<LessonModel>>;
	let collectionComponent: CollectionComponent<LessonModel>;

	let lessonDataFactory: LessonDataFactory;
	let models: LessonModel[];
	let modelProperties: ModelProperty[];

	let defaultHeaderOptions: HeaderOption[];

	beforeAll(() => {
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] off begin
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] end

		lessonDataFactory = new LessonDataFactory();

		modelProperties = LessonModel.getProps();

		defaultHeaderOptions =  modelProperties.map(prop => {
			return {
				...prop,
				sortable: true,
				sourceDirectFromModel: true,
				valueSource: prop.name
			} as HeaderOption;
		});

		// % protected region % [Add any additional setup in beforeAll after the main body here for Lesson] off begin
		// % protected region % [Add any additional setup in beforeAll after the main body here for Lesson] end
	});

	beforeEach(waitForAsync (() => {
		models = lessonDataFactory.createAll();

		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				CommonComponentModule
			],
			// % protected region % [Add any additional configurations here for Collection component against Lesson] off begin
			// % protected region % [Add any additional configurations here for Collection component against Lesson] end
		}).compileComponents().then(() => {
			fixture = TestBed.createComponent<CollectionComponent<LessonModel>>(CollectionComponent);
			collectionComponent = fixture.debugElement.componentInstance;
		});
	}));

	afterEach(() => {
		(fixture.nativeElement as HTMLElement).remove();
	});

	it('should create the collection component', () => {
		expect(collectionComponent).toBeTruthy();
	});

	it('should have no items selected when first viewed', () => {
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Lesson] off begin
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Lesson] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const selectAllInputEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllInputEl.checked).toBeFalsy(`'Select All' is checked`);

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => {
			return prev && currentValue;
		});
		expect(allChecked).toBeFalsy('Not all checkboxes are deselected');

		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Lesson] off begin
		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Lesson] end
	});

	it('should select all items when \'Select All\' is checked', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Lesson] off begin
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Lesson] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement
			.click();

		fixture.detectChanges();
		tick();

		expect(collectionComponent.selectedModels.size).toBe(models.length, 'Not all checkboxes are checked');

		fixture.detectChanges();

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('10 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Lesson] off begin
		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Lesson] end

	}));

	it('should check \'Select All\' when all checkboxes are checked',  fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Lesson] off begin
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Lesson] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkbox => checkbox.nativeElement as HTMLInputElement);

		checkboxEls.forEach(checkbox => checkbox.click());

		fixture.detectChanges();
		tick();

		fixture.detectChanges();

		expect(collectionComponent.selectedModels.size).toBe(collectionComponent.models.length, 'Not all checkboxes are checked');

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe(`${collectionComponent.models.length} selected`, 'Select number not showing correctly');

		const selectAllEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllEl.checked).toBeTruthy('\'Select All\' checkbox is not selected when all rows of the current page are selected');

		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Lesson] off begin
		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Lesson] end
	}));

	it('should select the correct row', () => {
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Lesson] off begin
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Lesson] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkboxElement => checkboxElement.nativeElement as HTMLInputElement);

		// Random row to select
		const rowIndex = 2;
		const checkbox: DebugElement = checkboxes[rowIndex];
		const checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkboxEl.click();
		fixture.detectChanges();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		const unselectedCheckboxes: boolean[] = checkboxEls
			.filter(element => !element.checked)
			.map(element => element.checked);

		expect(unselectedCheckboxes).toEqual(new Array(collectionComponent.models.length - 1).fill(false), 'Some other checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Lesson] off begin
		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Lesson] end
	});

	it('should keep selection when switch between grid and list', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Lesson] off begin
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Lesson] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		// Random row to select
		const rowIndex = Math.floor(Math.random() * 10);
		let checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		let checkbox: DebugElement = checkboxes[rowIndex];
		let checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkbox.nativeElement.click();

		fixture.detectChanges();
		tick();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		// Switch to grid view
		const gridButton = fixture.debugElement.query(By.css('.btn.icon-grid'));
		gridButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected and display correctly
		let selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');


		// Change back to list
		const listButton = fixture.debugElement.query(By.css('.btn.icon-list'));
		listButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected
		selectText = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Lesson] off begin
		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Lesson] end
	}));

	// % protected region % [Add any additional tests here for Collection component against Lesson] off begin
	// % protected region % [Add any additional tests here for Collection component against Lesson] end
});

describe('Collection component against Tag', () => {

	let fixture: ComponentFixture<CollectionComponent<TagModel>>;
	let collectionComponent: CollectionComponent<TagModel>;

	let tagDataFactory: TagDataFactory;
	let models: TagModel[];
	let modelProperties: ModelProperty[];

	let defaultHeaderOptions: HeaderOption[];

	beforeAll(() => {
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] off begin
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] end

		tagDataFactory = new TagDataFactory();

		modelProperties = TagModel.getProps();

		defaultHeaderOptions =  modelProperties.map(prop => {
			return {
				...prop,
				sortable: true,
				sourceDirectFromModel: true,
				valueSource: prop.name
			} as HeaderOption;
		});

		// % protected region % [Add any additional setup in beforeAll after the main body here for Tag] off begin
		// % protected region % [Add any additional setup in beforeAll after the main body here for Tag] end
	});

	beforeEach(waitForAsync (() => {
		models = tagDataFactory.createAll();

		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				CommonComponentModule
			],
			// % protected region % [Add any additional configurations here for Collection component against Tag] off begin
			// % protected region % [Add any additional configurations here for Collection component against Tag] end
		}).compileComponents().then(() => {
			fixture = TestBed.createComponent<CollectionComponent<TagModel>>(CollectionComponent);
			collectionComponent = fixture.debugElement.componentInstance;
		});
	}));

	afterEach(() => {
		(fixture.nativeElement as HTMLElement).remove();
	});

	it('should create the collection component', () => {
		expect(collectionComponent).toBeTruthy();
	});

	it('should have no items selected when first viewed', () => {
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Tag] off begin
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Tag] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const selectAllInputEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllInputEl.checked).toBeFalsy(`'Select All' is checked`);

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => {
			return prev && currentValue;
		});
		expect(allChecked).toBeFalsy('Not all checkboxes are deselected');

		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Tag] off begin
		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Tag] end
	});

	it('should select all items when \'Select All\' is checked', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Tag] off begin
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Tag] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement
			.click();

		fixture.detectChanges();
		tick();

		expect(collectionComponent.selectedModels.size).toBe(models.length, 'Not all checkboxes are checked');

		fixture.detectChanges();

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('10 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Tag] off begin
		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Tag] end

	}));

	it('should check \'Select All\' when all checkboxes are checked',  fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Tag] off begin
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Tag] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkbox => checkbox.nativeElement as HTMLInputElement);

		checkboxEls.forEach(checkbox => checkbox.click());

		fixture.detectChanges();
		tick();

		fixture.detectChanges();

		expect(collectionComponent.selectedModels.size).toBe(collectionComponent.models.length, 'Not all checkboxes are checked');

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe(`${collectionComponent.models.length} selected`, 'Select number not showing correctly');

		const selectAllEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllEl.checked).toBeTruthy('\'Select All\' checkbox is not selected when all rows of the current page are selected');

		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Tag] off begin
		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Tag] end
	}));

	it('should select the correct row', () => {
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Tag] off begin
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Tag] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkboxElement => checkboxElement.nativeElement as HTMLInputElement);

		// Random row to select
		const rowIndex = 2;
		const checkbox: DebugElement = checkboxes[rowIndex];
		const checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkboxEl.click();
		fixture.detectChanges();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		const unselectedCheckboxes: boolean[] = checkboxEls
			.filter(element => !element.checked)
			.map(element => element.checked);

		expect(unselectedCheckboxes).toEqual(new Array(collectionComponent.models.length - 1).fill(false), 'Some other checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Tag] off begin
		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Tag] end
	});

	it('should keep selection when switch between grid and list', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Tag] off begin
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Tag] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		// Random row to select
		const rowIndex = Math.floor(Math.random() * 10);
		let checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		let checkbox: DebugElement = checkboxes[rowIndex];
		let checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkbox.nativeElement.click();

		fixture.detectChanges();
		tick();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		// Switch to grid view
		const gridButton = fixture.debugElement.query(By.css('.btn.icon-grid'));
		gridButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected and display correctly
		let selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');


		// Change back to list
		const listButton = fixture.debugElement.query(By.css('.btn.icon-list'));
		listButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected
		selectText = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Tag] off begin
		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Tag] end
	}));

	// % protected region % [Add any additional tests here for Collection component against Tag] off begin
	// % protected region % [Add any additional tests here for Collection component against Tag] end
});

describe('Collection component against Role', () => {

	let fixture: ComponentFixture<CollectionComponent<RoleModel>>;
	let collectionComponent: CollectionComponent<RoleModel>;

	let roleDataFactory: RoleDataFactory;
	let models: RoleModel[];
	let modelProperties: ModelProperty[];

	let defaultHeaderOptions: HeaderOption[];

	beforeAll(() => {
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] off begin
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] end

		roleDataFactory = new RoleDataFactory();

		modelProperties = RoleModel.getProps();

		defaultHeaderOptions =  modelProperties.map(prop => {
			return {
				...prop,
				sortable: true,
				sourceDirectFromModel: true,
				valueSource: prop.name
			} as HeaderOption;
		});

		// % protected region % [Add any additional setup in beforeAll after the main body here for Role] off begin
		// % protected region % [Add any additional setup in beforeAll after the main body here for Role] end
	});

	beforeEach(waitForAsync (() => {
		models = roleDataFactory.createAll();

		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				CommonComponentModule
			],
			// % protected region % [Add any additional configurations here for Collection component against Role] off begin
			// % protected region % [Add any additional configurations here for Collection component against Role] end
		}).compileComponents().then(() => {
			fixture = TestBed.createComponent<CollectionComponent<RoleModel>>(CollectionComponent);
			collectionComponent = fixture.debugElement.componentInstance;
		});
	}));

	afterEach(() => {
		(fixture.nativeElement as HTMLElement).remove();
	});

	it('should create the collection component', () => {
		expect(collectionComponent).toBeTruthy();
	});

	it('should have no items selected when first viewed', () => {
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Role] off begin
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Role] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const selectAllInputEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllInputEl.checked).toBeFalsy(`'Select All' is checked`);

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => {
			return prev && currentValue;
		});
		expect(allChecked).toBeFalsy('Not all checkboxes are deselected');

		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Role] off begin
		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Role] end
	});

	it('should select all items when \'Select All\' is checked', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Role] off begin
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Role] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement
			.click();

		fixture.detectChanges();
		tick();

		expect(collectionComponent.selectedModels.size).toBe(models.length, 'Not all checkboxes are checked');

		fixture.detectChanges();

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('10 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Role] off begin
		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Role] end

	}));

	it('should check \'Select All\' when all checkboxes are checked',  fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Role] off begin
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Role] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkbox => checkbox.nativeElement as HTMLInputElement);

		checkboxEls.forEach(checkbox => checkbox.click());

		fixture.detectChanges();
		tick();

		fixture.detectChanges();

		expect(collectionComponent.selectedModels.size).toBe(collectionComponent.models.length, 'Not all checkboxes are checked');

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe(`${collectionComponent.models.length} selected`, 'Select number not showing correctly');

		const selectAllEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllEl.checked).toBeTruthy('\'Select All\' checkbox is not selected when all rows of the current page are selected');

		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Role] off begin
		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Role] end
	}));

	it('should select the correct row', () => {
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Role] off begin
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Role] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkboxElement => checkboxElement.nativeElement as HTMLInputElement);

		// Random row to select
		const rowIndex = 2;
		const checkbox: DebugElement = checkboxes[rowIndex];
		const checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkboxEl.click();
		fixture.detectChanges();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		const unselectedCheckboxes: boolean[] = checkboxEls
			.filter(element => !element.checked)
			.map(element => element.checked);

		expect(unselectedCheckboxes).toEqual(new Array(collectionComponent.models.length - 1).fill(false), 'Some other checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Role] off begin
		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Role] end
	});

	it('should keep selection when switch between grid and list', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Role] off begin
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Role] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		// Random row to select
		const rowIndex = Math.floor(Math.random() * 10);
		let checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		let checkbox: DebugElement = checkboxes[rowIndex];
		let checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkbox.nativeElement.click();

		fixture.detectChanges();
		tick();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		// Switch to grid view
		const gridButton = fixture.debugElement.query(By.css('.btn.icon-grid'));
		gridButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected and display correctly
		let selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');


		// Change back to list
		const listButton = fixture.debugElement.query(By.css('.btn.icon-list'));
		listButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected
		selectText = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Role] off begin
		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Role] end
	}));

	// % protected region % [Add any additional tests here for Collection component against Role] off begin
	// % protected region % [Add any additional tests here for Collection component against Role] end
});

describe('Collection component against Privilege', () => {

	let fixture: ComponentFixture<CollectionComponent<PrivilegeModel>>;
	let collectionComponent: CollectionComponent<PrivilegeModel>;

	let privilegeDataFactory: PrivilegeDataFactory;
	let models: PrivilegeModel[];
	let modelProperties: ModelProperty[];

	let defaultHeaderOptions: HeaderOption[];

	beforeAll(() => {
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] off begin
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] end

		privilegeDataFactory = new PrivilegeDataFactory();

		modelProperties = PrivilegeModel.getProps();

		defaultHeaderOptions =  modelProperties.map(prop => {
			return {
				...prop,
				sortable: true,
				sourceDirectFromModel: true,
				valueSource: prop.name
			} as HeaderOption;
		});

		// % protected region % [Add any additional setup in beforeAll after the main body here for Privilege] off begin
		// % protected region % [Add any additional setup in beforeAll after the main body here for Privilege] end
	});

	beforeEach(waitForAsync (() => {
		models = privilegeDataFactory.createAll();

		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				CommonComponentModule
			],
			// % protected region % [Add any additional configurations here for Collection component against Privilege] off begin
			// % protected region % [Add any additional configurations here for Collection component against Privilege] end
		}).compileComponents().then(() => {
			fixture = TestBed.createComponent<CollectionComponent<PrivilegeModel>>(CollectionComponent);
			collectionComponent = fixture.debugElement.componentInstance;
		});
	}));

	afterEach(() => {
		(fixture.nativeElement as HTMLElement).remove();
	});

	it('should create the collection component', () => {
		expect(collectionComponent).toBeTruthy();
	});

	it('should have no items selected when first viewed', () => {
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Privilege] off begin
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Privilege] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const selectAllInputEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllInputEl.checked).toBeFalsy(`'Select All' is checked`);

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => {
			return prev && currentValue;
		});
		expect(allChecked).toBeFalsy('Not all checkboxes are deselected');

		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Privilege] off begin
		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Privilege] end
	});

	it('should select all items when \'Select All\' is checked', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Privilege] off begin
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Privilege] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement
			.click();

		fixture.detectChanges();
		tick();

		expect(collectionComponent.selectedModels.size).toBe(models.length, 'Not all checkboxes are checked');

		fixture.detectChanges();

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('10 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Privilege] off begin
		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Privilege] end

	}));

	it('should check \'Select All\' when all checkboxes are checked',  fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Privilege] off begin
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Privilege] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkbox => checkbox.nativeElement as HTMLInputElement);

		checkboxEls.forEach(checkbox => checkbox.click());

		fixture.detectChanges();
		tick();

		fixture.detectChanges();

		expect(collectionComponent.selectedModels.size).toBe(collectionComponent.models.length, 'Not all checkboxes are checked');

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe(`${collectionComponent.models.length} selected`, 'Select number not showing correctly');

		const selectAllEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllEl.checked).toBeTruthy('\'Select All\' checkbox is not selected when all rows of the current page are selected');

		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Privilege] off begin
		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Privilege] end
	}));

	it('should select the correct row', () => {
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Privilege] off begin
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Privilege] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkboxElement => checkboxElement.nativeElement as HTMLInputElement);

		// Random row to select
		const rowIndex = 2;
		const checkbox: DebugElement = checkboxes[rowIndex];
		const checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkboxEl.click();
		fixture.detectChanges();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		const unselectedCheckboxes: boolean[] = checkboxEls
			.filter(element => !element.checked)
			.map(element => element.checked);

		expect(unselectedCheckboxes).toEqual(new Array(collectionComponent.models.length - 1).fill(false), 'Some other checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Privilege] off begin
		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Privilege] end
	});

	it('should keep selection when switch between grid and list', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Privilege] off begin
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Privilege] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		// Random row to select
		const rowIndex = Math.floor(Math.random() * 10);
		let checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		let checkbox: DebugElement = checkboxes[rowIndex];
		let checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkbox.nativeElement.click();

		fixture.detectChanges();
		tick();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		// Switch to grid view
		const gridButton = fixture.debugElement.query(By.css('.btn.icon-grid'));
		gridButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected and display correctly
		let selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');


		// Change back to list
		const listButton = fixture.debugElement.query(By.css('.btn.icon-list'));
		listButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected
		selectText = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Privilege] off begin
		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Privilege] end
	}));

	// % protected region % [Add any additional tests here for Collection component against Privilege] off begin
	// % protected region % [Add any additional tests here for Collection component against Privilege] end
});

describe('Collection component against Workflow', () => {

	let fixture: ComponentFixture<CollectionComponent<WorkflowModel>>;
	let collectionComponent: CollectionComponent<WorkflowModel>;

	let workflowDataFactory: WorkflowDataFactory;
	let models: WorkflowModel[];
	let modelProperties: ModelProperty[];

	let defaultHeaderOptions: HeaderOption[];

	beforeAll(() => {
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] off begin
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] end

		workflowDataFactory = new WorkflowDataFactory();

		modelProperties = WorkflowModel.getProps();

		defaultHeaderOptions =  modelProperties.map(prop => {
			return {
				...prop,
				sortable: true,
				sourceDirectFromModel: true,
				valueSource: prop.name
			} as HeaderOption;
		});

		// % protected region % [Add any additional setup in beforeAll after the main body here for Workflow] off begin
		// % protected region % [Add any additional setup in beforeAll after the main body here for Workflow] end
	});

	beforeEach(waitForAsync (() => {
		models = workflowDataFactory.createAll();

		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				CommonComponentModule
			],
			// % protected region % [Add any additional configurations here for Collection component against Workflow] off begin
			// % protected region % [Add any additional configurations here for Collection component against Workflow] end
		}).compileComponents().then(() => {
			fixture = TestBed.createComponent<CollectionComponent<WorkflowModel>>(CollectionComponent);
			collectionComponent = fixture.debugElement.componentInstance;
		});
	}));

	afterEach(() => {
		(fixture.nativeElement as HTMLElement).remove();
	});

	it('should create the collection component', () => {
		expect(collectionComponent).toBeTruthy();
	});

	it('should have no items selected when first viewed', () => {
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Workflow] off begin
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Workflow] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const selectAllInputEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllInputEl.checked).toBeFalsy(`'Select All' is checked`);

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => {
			return prev && currentValue;
		});
		expect(allChecked).toBeFalsy('Not all checkboxes are deselected');

		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Workflow] off begin
		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Workflow] end
	});

	it('should select all items when \'Select All\' is checked', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Workflow] off begin
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Workflow] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement
			.click();

		fixture.detectChanges();
		tick();

		expect(collectionComponent.selectedModels.size).toBe(models.length, 'Not all checkboxes are checked');

		fixture.detectChanges();

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('10 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Workflow] off begin
		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Workflow] end

	}));

	it('should check \'Select All\' when all checkboxes are checked',  fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Workflow] off begin
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Workflow] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkbox => checkbox.nativeElement as HTMLInputElement);

		checkboxEls.forEach(checkbox => checkbox.click());

		fixture.detectChanges();
		tick();

		fixture.detectChanges();

		expect(collectionComponent.selectedModels.size).toBe(collectionComponent.models.length, 'Not all checkboxes are checked');

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe(`${collectionComponent.models.length} selected`, 'Select number not showing correctly');

		const selectAllEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllEl.checked).toBeTruthy('\'Select All\' checkbox is not selected when all rows of the current page are selected');

		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Workflow] off begin
		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Workflow] end
	}));

	it('should select the correct row', () => {
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Workflow] off begin
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Workflow] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkboxElement => checkboxElement.nativeElement as HTMLInputElement);

		// Random row to select
		const rowIndex = 2;
		const checkbox: DebugElement = checkboxes[rowIndex];
		const checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkboxEl.click();
		fixture.detectChanges();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		const unselectedCheckboxes: boolean[] = checkboxEls
			.filter(element => !element.checked)
			.map(element => element.checked);

		expect(unselectedCheckboxes).toEqual(new Array(collectionComponent.models.length - 1).fill(false), 'Some other checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Workflow] off begin
		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Workflow] end
	});

	it('should keep selection when switch between grid and list', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Workflow] off begin
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Workflow] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		// Random row to select
		const rowIndex = Math.floor(Math.random() * 10);
		let checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		let checkbox: DebugElement = checkboxes[rowIndex];
		let checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkbox.nativeElement.click();

		fixture.detectChanges();
		tick();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		// Switch to grid view
		const gridButton = fixture.debugElement.query(By.css('.btn.icon-grid'));
		gridButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected and display correctly
		let selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');


		// Change back to list
		const listButton = fixture.debugElement.query(By.css('.btn.icon-list'));
		listButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected
		selectText = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Workflow] off begin
		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Workflow] end
	}));

	// % protected region % [Add any additional tests here for Collection component against Workflow] off begin
	// % protected region % [Add any additional tests here for Collection component against Workflow] end
});

describe('Collection component against Workflow State', () => {

	let fixture: ComponentFixture<CollectionComponent<WorkflowStateModel>>;
	let collectionComponent: CollectionComponent<WorkflowStateModel>;

	let workflowStateDataFactory: WorkflowStateDataFactory;
	let models: WorkflowStateModel[];
	let modelProperties: ModelProperty[];

	let defaultHeaderOptions: HeaderOption[];

	beforeAll(() => {
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] off begin
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] end

		workflowStateDataFactory = new WorkflowStateDataFactory();

		modelProperties = WorkflowStateModel.getProps();

		defaultHeaderOptions =  modelProperties.map(prop => {
			return {
				...prop,
				sortable: true,
				sourceDirectFromModel: true,
				valueSource: prop.name
			} as HeaderOption;
		});

		// % protected region % [Add any additional setup in beforeAll after the main body here for Workflow State] off begin
		// % protected region % [Add any additional setup in beforeAll after the main body here for Workflow State] end
	});

	beforeEach(waitForAsync (() => {
		models = workflowStateDataFactory.createAll();

		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				CommonComponentModule
			],
			// % protected region % [Add any additional configurations here for Collection component against Workflow State] off begin
			// % protected region % [Add any additional configurations here for Collection component against Workflow State] end
		}).compileComponents().then(() => {
			fixture = TestBed.createComponent<CollectionComponent<WorkflowStateModel>>(CollectionComponent);
			collectionComponent = fixture.debugElement.componentInstance;
		});
	}));

	afterEach(() => {
		(fixture.nativeElement as HTMLElement).remove();
	});

	it('should create the collection component', () => {
		expect(collectionComponent).toBeTruthy();
	});

	it('should have no items selected when first viewed', () => {
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Workflow State] off begin
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Workflow State] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const selectAllInputEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllInputEl.checked).toBeFalsy(`'Select All' is checked`);

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => {
			return prev && currentValue;
		});
		expect(allChecked).toBeFalsy('Not all checkboxes are deselected');

		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Workflow State] off begin
		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Workflow State] end
	});

	it('should select all items when \'Select All\' is checked', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Workflow State] off begin
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Workflow State] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement
			.click();

		fixture.detectChanges();
		tick();

		expect(collectionComponent.selectedModels.size).toBe(models.length, 'Not all checkboxes are checked');

		fixture.detectChanges();

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('10 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Workflow State] off begin
		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Workflow State] end

	}));

	it('should check \'Select All\' when all checkboxes are checked',  fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Workflow State] off begin
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Workflow State] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkbox => checkbox.nativeElement as HTMLInputElement);

		checkboxEls.forEach(checkbox => checkbox.click());

		fixture.detectChanges();
		tick();

		fixture.detectChanges();

		expect(collectionComponent.selectedModels.size).toBe(collectionComponent.models.length, 'Not all checkboxes are checked');

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe(`${collectionComponent.models.length} selected`, 'Select number not showing correctly');

		const selectAllEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllEl.checked).toBeTruthy('\'Select All\' checkbox is not selected when all rows of the current page are selected');

		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Workflow State] off begin
		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Workflow State] end
	}));

	it('should select the correct row', () => {
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Workflow State] off begin
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Workflow State] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkboxElement => checkboxElement.nativeElement as HTMLInputElement);

		// Random row to select
		const rowIndex = 2;
		const checkbox: DebugElement = checkboxes[rowIndex];
		const checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkboxEl.click();
		fixture.detectChanges();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		const unselectedCheckboxes: boolean[] = checkboxEls
			.filter(element => !element.checked)
			.map(element => element.checked);

		expect(unselectedCheckboxes).toEqual(new Array(collectionComponent.models.length - 1).fill(false), 'Some other checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Workflow State] off begin
		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Workflow State] end
	});

	it('should keep selection when switch between grid and list', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Workflow State] off begin
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Workflow State] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		// Random row to select
		const rowIndex = Math.floor(Math.random() * 10);
		let checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		let checkbox: DebugElement = checkboxes[rowIndex];
		let checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkbox.nativeElement.click();

		fixture.detectChanges();
		tick();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		// Switch to grid view
		const gridButton = fixture.debugElement.query(By.css('.btn.icon-grid'));
		gridButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected and display correctly
		let selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');


		// Change back to list
		const listButton = fixture.debugElement.query(By.css('.btn.icon-list'));
		listButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected
		selectText = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Workflow State] off begin
		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Workflow State] end
	}));

	// % protected region % [Add any additional tests here for Collection component against Workflow State] off begin
	// % protected region % [Add any additional tests here for Collection component against Workflow State] end
});

describe('Collection component against Workflow Transition', () => {

	let fixture: ComponentFixture<CollectionComponent<WorkflowTransitionModel>>;
	let collectionComponent: CollectionComponent<WorkflowTransitionModel>;

	let workflowTransitionDataFactory: WorkflowTransitionDataFactory;
	let models: WorkflowTransitionModel[];
	let modelProperties: ModelProperty[];

	let defaultHeaderOptions: HeaderOption[];

	beforeAll(() => {
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] off begin
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] end

		workflowTransitionDataFactory = new WorkflowTransitionDataFactory();

		modelProperties = WorkflowTransitionModel.getProps();

		defaultHeaderOptions =  modelProperties.map(prop => {
			return {
				...prop,
				sortable: true,
				sourceDirectFromModel: true,
				valueSource: prop.name
			} as HeaderOption;
		});

		// % protected region % [Add any additional setup in beforeAll after the main body here for Workflow Transition] off begin
		// % protected region % [Add any additional setup in beforeAll after the main body here for Workflow Transition] end
	});

	beforeEach(waitForAsync (() => {
		models = workflowTransitionDataFactory.createAll();

		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				CommonComponentModule
			],
			// % protected region % [Add any additional configurations here for Collection component against Workflow Transition] off begin
			// % protected region % [Add any additional configurations here for Collection component against Workflow Transition] end
		}).compileComponents().then(() => {
			fixture = TestBed.createComponent<CollectionComponent<WorkflowTransitionModel>>(CollectionComponent);
			collectionComponent = fixture.debugElement.componentInstance;
		});
	}));

	afterEach(() => {
		(fixture.nativeElement as HTMLElement).remove();
	});

	it('should create the collection component', () => {
		expect(collectionComponent).toBeTruthy();
	});

	it('should have no items selected when first viewed', () => {
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Workflow Transition] off begin
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Workflow Transition] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const selectAllInputEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllInputEl.checked).toBeFalsy(`'Select All' is checked`);

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => {
			return prev && currentValue;
		});
		expect(allChecked).toBeFalsy('Not all checkboxes are deselected');

		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Workflow Transition] off begin
		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Workflow Transition] end
	});

	it('should select all items when \'Select All\' is checked', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Workflow Transition] off begin
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Workflow Transition] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement
			.click();

		fixture.detectChanges();
		tick();

		expect(collectionComponent.selectedModels.size).toBe(models.length, 'Not all checkboxes are checked');

		fixture.detectChanges();

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('10 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Workflow Transition] off begin
		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Workflow Transition] end

	}));

	it('should check \'Select All\' when all checkboxes are checked',  fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Workflow Transition] off begin
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Workflow Transition] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkbox => checkbox.nativeElement as HTMLInputElement);

		checkboxEls.forEach(checkbox => checkbox.click());

		fixture.detectChanges();
		tick();

		fixture.detectChanges();

		expect(collectionComponent.selectedModels.size).toBe(collectionComponent.models.length, 'Not all checkboxes are checked');

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe(`${collectionComponent.models.length} selected`, 'Select number not showing correctly');

		const selectAllEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllEl.checked).toBeTruthy('\'Select All\' checkbox is not selected when all rows of the current page are selected');

		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Workflow Transition] off begin
		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Workflow Transition] end
	}));

	it('should select the correct row', () => {
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Workflow Transition] off begin
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Workflow Transition] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkboxElement => checkboxElement.nativeElement as HTMLInputElement);

		// Random row to select
		const rowIndex = 2;
		const checkbox: DebugElement = checkboxes[rowIndex];
		const checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkboxEl.click();
		fixture.detectChanges();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		const unselectedCheckboxes: boolean[] = checkboxEls
			.filter(element => !element.checked)
			.map(element => element.checked);

		expect(unselectedCheckboxes).toEqual(new Array(collectionComponent.models.length - 1).fill(false), 'Some other checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Workflow Transition] off begin
		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Workflow Transition] end
	});

	it('should keep selection when switch between grid and list', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Workflow Transition] off begin
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Workflow Transition] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		// Random row to select
		const rowIndex = Math.floor(Math.random() * 10);
		let checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		let checkbox: DebugElement = checkboxes[rowIndex];
		let checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkbox.nativeElement.click();

		fixture.detectChanges();
		tick();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		// Switch to grid view
		const gridButton = fixture.debugElement.query(By.css('.btn.icon-grid'));
		gridButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected and display correctly
		let selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');


		// Change back to list
		const listButton = fixture.debugElement.query(By.css('.btn.icon-list'));
		listButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected
		selectText = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Workflow Transition] off begin
		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Workflow Transition] end
	}));

	// % protected region % [Add any additional tests here for Collection component against Workflow Transition] off begin
	// % protected region % [Add any additional tests here for Collection component against Workflow Transition] end
});

describe('Collection component against Workflow Version', () => {

	let fixture: ComponentFixture<CollectionComponent<WorkflowVersionModel>>;
	let collectionComponent: CollectionComponent<WorkflowVersionModel>;

	let workflowVersionDataFactory: WorkflowVersionDataFactory;
	let models: WorkflowVersionModel[];
	let modelProperties: ModelProperty[];

	let defaultHeaderOptions: HeaderOption[];

	beforeAll(() => {
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] off begin
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] end

		workflowVersionDataFactory = new WorkflowVersionDataFactory();

		modelProperties = WorkflowVersionModel.getProps();

		defaultHeaderOptions =  modelProperties.map(prop => {
			return {
				...prop,
				sortable: true,
				sourceDirectFromModel: true,
				valueSource: prop.name
			} as HeaderOption;
		});

		// % protected region % [Add any additional setup in beforeAll after the main body here for Workflow Version] off begin
		// % protected region % [Add any additional setup in beforeAll after the main body here for Workflow Version] end
	});

	beforeEach(waitForAsync (() => {
		models = workflowVersionDataFactory.createAll();

		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				CommonComponentModule
			],
			// % protected region % [Add any additional configurations here for Collection component against Workflow Version] off begin
			// % protected region % [Add any additional configurations here for Collection component against Workflow Version] end
		}).compileComponents().then(() => {
			fixture = TestBed.createComponent<CollectionComponent<WorkflowVersionModel>>(CollectionComponent);
			collectionComponent = fixture.debugElement.componentInstance;
		});
	}));

	afterEach(() => {
		(fixture.nativeElement as HTMLElement).remove();
	});

	it('should create the collection component', () => {
		expect(collectionComponent).toBeTruthy();
	});

	it('should have no items selected when first viewed', () => {
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Workflow Version] off begin
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Workflow Version] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const selectAllInputEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllInputEl.checked).toBeFalsy(`'Select All' is checked`);

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => {
			return prev && currentValue;
		});
		expect(allChecked).toBeFalsy('Not all checkboxes are deselected');

		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Workflow Version] off begin
		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Workflow Version] end
	});

	it('should select all items when \'Select All\' is checked', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Workflow Version] off begin
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Workflow Version] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement
			.click();

		fixture.detectChanges();
		tick();

		expect(collectionComponent.selectedModels.size).toBe(models.length, 'Not all checkboxes are checked');

		fixture.detectChanges();

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('10 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Workflow Version] off begin
		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Workflow Version] end

	}));

	it('should check \'Select All\' when all checkboxes are checked',  fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Workflow Version] off begin
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Workflow Version] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkbox => checkbox.nativeElement as HTMLInputElement);

		checkboxEls.forEach(checkbox => checkbox.click());

		fixture.detectChanges();
		tick();

		fixture.detectChanges();

		expect(collectionComponent.selectedModels.size).toBe(collectionComponent.models.length, 'Not all checkboxes are checked');

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe(`${collectionComponent.models.length} selected`, 'Select number not showing correctly');

		const selectAllEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllEl.checked).toBeTruthy('\'Select All\' checkbox is not selected when all rows of the current page are selected');

		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Workflow Version] off begin
		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Workflow Version] end
	}));

	it('should select the correct row', () => {
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Workflow Version] off begin
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Workflow Version] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkboxElement => checkboxElement.nativeElement as HTMLInputElement);

		// Random row to select
		const rowIndex = 2;
		const checkbox: DebugElement = checkboxes[rowIndex];
		const checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkboxEl.click();
		fixture.detectChanges();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		const unselectedCheckboxes: boolean[] = checkboxEls
			.filter(element => !element.checked)
			.map(element => element.checked);

		expect(unselectedCheckboxes).toEqual(new Array(collectionComponent.models.length - 1).fill(false), 'Some other checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Workflow Version] off begin
		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Workflow Version] end
	});

	it('should keep selection when switch between grid and list', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Workflow Version] off begin
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Workflow Version] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		// Random row to select
		const rowIndex = Math.floor(Math.random() * 10);
		let checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		let checkbox: DebugElement = checkboxes[rowIndex];
		let checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkbox.nativeElement.click();

		fixture.detectChanges();
		tick();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		// Switch to grid view
		const gridButton = fixture.debugElement.query(By.css('.btn.icon-grid'));
		gridButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected and display correctly
		let selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');


		// Change back to list
		const listButton = fixture.debugElement.query(By.css('.btn.icon-list'));
		listButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected
		selectText = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Workflow Version] off begin
		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Workflow Version] end
	}));

	// % protected region % [Add any additional tests here for Collection component against Workflow Version] off begin
	// % protected region % [Add any additional tests here for Collection component against Workflow Version] end
});

describe('Collection component against Lesson Form Submission', () => {

	let fixture: ComponentFixture<CollectionComponent<LessonFormSubmissionModel>>;
	let collectionComponent: CollectionComponent<LessonFormSubmissionModel>;

	let lessonFormSubmissionDataFactory: LessonFormSubmissionDataFactory;
	let models: LessonFormSubmissionModel[];
	let modelProperties: ModelProperty[];

	let defaultHeaderOptions: HeaderOption[];

	beforeAll(() => {
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] off begin
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] end

		lessonFormSubmissionDataFactory = new LessonFormSubmissionDataFactory();

		modelProperties = LessonFormSubmissionModel.getProps();

		defaultHeaderOptions =  modelProperties.map(prop => {
			return {
				...prop,
				sortable: true,
				sourceDirectFromModel: true,
				valueSource: prop.name
			} as HeaderOption;
		});

		// % protected region % [Add any additional setup in beforeAll after the main body here for Lesson Form Submission] off begin
		// % protected region % [Add any additional setup in beforeAll after the main body here for Lesson Form Submission] end
	});

	beforeEach(waitForAsync (() => {
		models = lessonFormSubmissionDataFactory.createAll();

		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				CommonComponentModule
			],
			// % protected region % [Add any additional configurations here for Collection component against Lesson Form Submission] off begin
			// % protected region % [Add any additional configurations here for Collection component against Lesson Form Submission] end
		}).compileComponents().then(() => {
			fixture = TestBed.createComponent<CollectionComponent<LessonFormSubmissionModel>>(CollectionComponent);
			collectionComponent = fixture.debugElement.componentInstance;
		});
	}));

	afterEach(() => {
		(fixture.nativeElement as HTMLElement).remove();
	});

	it('should create the collection component', () => {
		expect(collectionComponent).toBeTruthy();
	});

	it('should have no items selected when first viewed', () => {
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Lesson Form Submission] off begin
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Lesson Form Submission] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const selectAllInputEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllInputEl.checked).toBeFalsy(`'Select All' is checked`);

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => {
			return prev && currentValue;
		});
		expect(allChecked).toBeFalsy('Not all checkboxes are deselected');

		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Lesson Form Submission] off begin
		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Lesson Form Submission] end
	});

	it('should select all items when \'Select All\' is checked', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Lesson Form Submission] off begin
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Lesson Form Submission] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement
			.click();

		fixture.detectChanges();
		tick();

		expect(collectionComponent.selectedModels.size).toBe(models.length, 'Not all checkboxes are checked');

		fixture.detectChanges();

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('10 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Lesson Form Submission] off begin
		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Lesson Form Submission] end

	}));

	it('should check \'Select All\' when all checkboxes are checked',  fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Lesson Form Submission] off begin
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Lesson Form Submission] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkbox => checkbox.nativeElement as HTMLInputElement);

		checkboxEls.forEach(checkbox => checkbox.click());

		fixture.detectChanges();
		tick();

		fixture.detectChanges();

		expect(collectionComponent.selectedModels.size).toBe(collectionComponent.models.length, 'Not all checkboxes are checked');

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe(`${collectionComponent.models.length} selected`, 'Select number not showing correctly');

		const selectAllEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllEl.checked).toBeTruthy('\'Select All\' checkbox is not selected when all rows of the current page are selected');

		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Lesson Form Submission] off begin
		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Lesson Form Submission] end
	}));

	it('should select the correct row', () => {
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Lesson Form Submission] off begin
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Lesson Form Submission] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkboxElement => checkboxElement.nativeElement as HTMLInputElement);

		// Random row to select
		const rowIndex = 2;
		const checkbox: DebugElement = checkboxes[rowIndex];
		const checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkboxEl.click();
		fixture.detectChanges();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		const unselectedCheckboxes: boolean[] = checkboxEls
			.filter(element => !element.checked)
			.map(element => element.checked);

		expect(unselectedCheckboxes).toEqual(new Array(collectionComponent.models.length - 1).fill(false), 'Some other checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Lesson Form Submission] off begin
		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Lesson Form Submission] end
	});

	it('should keep selection when switch between grid and list', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Lesson Form Submission] off begin
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Lesson Form Submission] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		// Random row to select
		const rowIndex = Math.floor(Math.random() * 10);
		let checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		let checkbox: DebugElement = checkboxes[rowIndex];
		let checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkbox.nativeElement.click();

		fixture.detectChanges();
		tick();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		// Switch to grid view
		const gridButton = fixture.debugElement.query(By.css('.btn.icon-grid'));
		gridButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected and display correctly
		let selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');


		// Change back to list
		const listButton = fixture.debugElement.query(By.css('.btn.icon-list'));
		listButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected
		selectText = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Lesson Form Submission] off begin
		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Lesson Form Submission] end
	}));

	// % protected region % [Add any additional tests here for Collection component against Lesson Form Submission] off begin
	// % protected region % [Add any additional tests here for Collection component against Lesson Form Submission] end
});

describe('Collection component against Lesson Form Version', () => {

	let fixture: ComponentFixture<CollectionComponent<LessonFormVersionModel>>;
	let collectionComponent: CollectionComponent<LessonFormVersionModel>;

	let lessonFormVersionDataFactory: LessonFormVersionDataFactory;
	let models: LessonFormVersionModel[];
	let modelProperties: ModelProperty[];

	let defaultHeaderOptions: HeaderOption[];

	beforeAll(() => {
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] off begin
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] end

		lessonFormVersionDataFactory = new LessonFormVersionDataFactory();

		modelProperties = LessonFormVersionModel.getProps();

		defaultHeaderOptions =  modelProperties.map(prop => {
			return {
				...prop,
				sortable: true,
				sourceDirectFromModel: true,
				valueSource: prop.name
			} as HeaderOption;
		});

		// % protected region % [Add any additional setup in beforeAll after the main body here for Lesson Form Version] off begin
		// % protected region % [Add any additional setup in beforeAll after the main body here for Lesson Form Version] end
	});

	beforeEach(waitForAsync (() => {
		models = lessonFormVersionDataFactory.createAll();

		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				CommonComponentModule
			],
			// % protected region % [Add any additional configurations here for Collection component against Lesson Form Version] off begin
			// % protected region % [Add any additional configurations here for Collection component against Lesson Form Version] end
		}).compileComponents().then(() => {
			fixture = TestBed.createComponent<CollectionComponent<LessonFormVersionModel>>(CollectionComponent);
			collectionComponent = fixture.debugElement.componentInstance;
		});
	}));

	afterEach(() => {
		(fixture.nativeElement as HTMLElement).remove();
	});

	it('should create the collection component', () => {
		expect(collectionComponent).toBeTruthy();
	});

	it('should have no items selected when first viewed', () => {
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Lesson Form Version] off begin
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Lesson Form Version] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const selectAllInputEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllInputEl.checked).toBeFalsy(`'Select All' is checked`);

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => {
			return prev && currentValue;
		});
		expect(allChecked).toBeFalsy('Not all checkboxes are deselected');

		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Lesson Form Version] off begin
		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Lesson Form Version] end
	});

	it('should select all items when \'Select All\' is checked', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Lesson Form Version] off begin
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Lesson Form Version] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement
			.click();

		fixture.detectChanges();
		tick();

		expect(collectionComponent.selectedModels.size).toBe(models.length, 'Not all checkboxes are checked');

		fixture.detectChanges();

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('10 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Lesson Form Version] off begin
		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Lesson Form Version] end

	}));

	it('should check \'Select All\' when all checkboxes are checked',  fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Lesson Form Version] off begin
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Lesson Form Version] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkbox => checkbox.nativeElement as HTMLInputElement);

		checkboxEls.forEach(checkbox => checkbox.click());

		fixture.detectChanges();
		tick();

		fixture.detectChanges();

		expect(collectionComponent.selectedModels.size).toBe(collectionComponent.models.length, 'Not all checkboxes are checked');

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe(`${collectionComponent.models.length} selected`, 'Select number not showing correctly');

		const selectAllEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllEl.checked).toBeTruthy('\'Select All\' checkbox is not selected when all rows of the current page are selected');

		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Lesson Form Version] off begin
		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Lesson Form Version] end
	}));

	it('should select the correct row', () => {
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Lesson Form Version] off begin
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Lesson Form Version] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkboxElement => checkboxElement.nativeElement as HTMLInputElement);

		// Random row to select
		const rowIndex = 2;
		const checkbox: DebugElement = checkboxes[rowIndex];
		const checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkboxEl.click();
		fixture.detectChanges();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		const unselectedCheckboxes: boolean[] = checkboxEls
			.filter(element => !element.checked)
			.map(element => element.checked);

		expect(unselectedCheckboxes).toEqual(new Array(collectionComponent.models.length - 1).fill(false), 'Some other checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Lesson Form Version] off begin
		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Lesson Form Version] end
	});

	it('should keep selection when switch between grid and list', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Lesson Form Version] off begin
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Lesson Form Version] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		// Random row to select
		const rowIndex = Math.floor(Math.random() * 10);
		let checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		let checkbox: DebugElement = checkboxes[rowIndex];
		let checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkbox.nativeElement.click();

		fixture.detectChanges();
		tick();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		// Switch to grid view
		const gridButton = fixture.debugElement.query(By.css('.btn.icon-grid'));
		gridButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected and display correctly
		let selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');


		// Change back to list
		const listButton = fixture.debugElement.query(By.css('.btn.icon-list'));
		listButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected
		selectText = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Lesson Form Version] off begin
		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Lesson Form Version] end
	}));

	// % protected region % [Add any additional tests here for Collection component against Lesson Form Version] off begin
	// % protected region % [Add any additional tests here for Collection component against Lesson Form Version] end
});

describe('Collection component against Lesson Form Tile', () => {

	let fixture: ComponentFixture<CollectionComponent<LessonFormTileModel>>;
	let collectionComponent: CollectionComponent<LessonFormTileModel>;

	let lessonFormTileDataFactory: LessonFormTileDataFactory;
	let models: LessonFormTileModel[];
	let modelProperties: ModelProperty[];

	let defaultHeaderOptions: HeaderOption[];

	beforeAll(() => {
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] off begin
		// % protected region % [Add any additional setup in beforeAll before the main body here for entity.Name] end

		lessonFormTileDataFactory = new LessonFormTileDataFactory();

		modelProperties = LessonFormTileModel.getProps();

		defaultHeaderOptions =  modelProperties.map(prop => {
			return {
				...prop,
				sortable: true,
				sourceDirectFromModel: true,
				valueSource: prop.name
			} as HeaderOption;
		});

		// % protected region % [Add any additional setup in beforeAll after the main body here for Lesson Form Tile] off begin
		// % protected region % [Add any additional setup in beforeAll after the main body here for Lesson Form Tile] end
	});

	beforeEach(waitForAsync (() => {
		models = lessonFormTileDataFactory.createAll();

		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				FormsModule,
				ReactiveFormsModule,
				CommonComponentModule
			],
			// % protected region % [Add any additional configurations here for Collection component against Lesson Form Tile] off begin
			// % protected region % [Add any additional configurations here for Collection component against Lesson Form Tile] end
		}).compileComponents().then(() => {
			fixture = TestBed.createComponent<CollectionComponent<LessonFormTileModel>>(CollectionComponent);
			collectionComponent = fixture.debugElement.componentInstance;
		});
	}));

	afterEach(() => {
		(fixture.nativeElement as HTMLElement).remove();
	});

	it('should create the collection component', () => {
		expect(collectionComponent).toBeTruthy();
	});

	it('should have no items selected when first viewed', () => {
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Lesson Form Tile] off begin
		// % protected region % [Add any additional logic before main process of 'should have no items selected when first viewed' here for Collection component against Lesson Form Tile] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const selectAllInputEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllInputEl.checked).toBeFalsy(`'Select All' is checked`);

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => {
			return prev && currentValue;
		});
		expect(allChecked).toBeFalsy('Not all checkboxes are deselected');

		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Lesson Form Tile] off begin
		// % protected region % [Add any additional logic for 'should have no items selected when first viewed' here for Collection component against Lesson Form Tile] end
	});

	it('should select all items when \'Select All\' is checked', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Lesson Form Tile] off begin
		// % protected region % [Add any additional logic before main process of 'should select all items when 'Select All' is checked' here for Collection component against Lesson Form Tile] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement
			.click();

		fixture.detectChanges();
		tick();

		expect(collectionComponent.selectedModels.size).toBe(models.length, 'Not all checkboxes are checked');

		fixture.detectChanges();

		const checkboxEls: HTMLInputElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox > input[type="checkbox"]'))
			.map(el => el.nativeElement as HTMLInputElement);

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('10 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Lesson Form Tile] off begin
		// % protected region % [Add any additional logic for 'should select all items when 'Select All' is checked' here for Collection component against Lesson Form Tile] end

	}));

	it('should check \'Select All\' when all checkboxes are checked',  fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Lesson Form Tile] off begin
		// % protected region % [Add any additional logic before main process of 'should check 'Select All' when all checkboxes are checked' here for Collection component against Lesson Form Tile] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkbox => checkbox.nativeElement as HTMLInputElement);

		checkboxEls.forEach(checkbox => checkbox.click());

		fixture.detectChanges();
		tick();

		fixture.detectChanges();

		expect(collectionComponent.selectedModels.size).toBe(collectionComponent.models.length, 'Not all checkboxes are checked');

		const allChecked = checkboxEls.map(el => el.checked).reduce((prev, currentValue) => prev && currentValue);

		expect(allChecked).toBeTruthy('Not all checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe(`${collectionComponent.models.length} selected`, 'Select number not showing correctly');

		const selectAllEl: HTMLInputElement = fixture
			.debugElement
			.query(By.css('.input-group.input-group-inline.input-group__checkbox.collection__list--select-all > input[type="checkbox"]'))
			.nativeElement;

		expect(selectAllEl.checked).toBeTruthy('\'Select All\' checkbox is not selected when all rows of the current page are selected');

		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Lesson Form Tile] off begin
		// % protected region % [Add any additional logic for 'should check 'Select All' when all checkboxes are checked' here for Collection component against Lesson Form Tile] end
	}));

	it('should select the correct row', () => {
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Lesson Form Tile] off begin
		// % protected region % [Add any additional logic before main process of 'should select the correct row' here for Collection component against Lesson Form Tile] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;
		fixture.detectChanges();

		const checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));
		const checkboxEls: HTMLInputElement[] = checkboxes.map(checkboxElement => checkboxElement.nativeElement as HTMLInputElement);

		// Random row to select
		const rowIndex = 2;
		const checkbox: DebugElement = checkboxes[rowIndex];
		const checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkboxEl.click();
		fixture.detectChanges();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		const unselectedCheckboxes: boolean[] = checkboxEls
			.filter(element => !element.checked)
			.map(element => element.checked);

		expect(unselectedCheckboxes).toEqual(new Array(collectionComponent.models.length - 1).fill(false), 'Some other checkboxes are selected');

		const multipleItemsActionBar: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options'))
			.nativeElement;

		expect(multipleItemsActionBar.hidden).toBeFalsy('Multiple items action bar not showing when select all');

		const selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;

		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Lesson Form Tile] off begin
		// % protected region % [Add any additional logic for 'should select the correct row' here for Collection component against Lesson Form Tile] end
	});

	it('should keep selection when switch between grid and list', fakeAsync(() => {
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Lesson Form Tile] off begin
		// % protected region % [Add any additional logic before main process of 'should keep selection when switch between grid and list' here for Collection component against Lesson Form Tile] end

		collectionComponent.models = models;
		collectionComponent.headerOptions = defaultHeaderOptions;

		fixture.detectChanges();
		tick();

		// Random row to select
		const rowIndex = Math.floor(Math.random() * 10);
		let checkboxes: DebugElement[] = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		let checkbox: DebugElement = checkboxes[rowIndex];
		let checkboxEl: HTMLInputElement = checkbox.nativeElement;

		checkbox.nativeElement.click();

		fixture.detectChanges();
		tick();

		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');

		// Switch to grid view
		const gridButton = fixture.debugElement.query(By.css('.btn.icon-grid'));
		gridButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected and display correctly
		let selectText: HTMLElement = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');


		// Change back to list
		const listButton = fixture.debugElement.query(By.css('.btn.icon-list'));
		listButton.nativeElement.click();

		fixture.detectChanges();
		tick();
		fixture.detectChanges();

		checkboxes = fixture
			.debugElement
			.queryAll(By.css('.input-group.input-group-block.input-group__checkbox:not(.collection__list--select-all) > input[type="checkbox"]'));

		checkbox = checkboxes[rowIndex];
		checkboxEl = checkbox.nativeElement;

		// The checkbox is still selected
		selectText = fixture
			.debugElement
			.query(By.css('.collection__select-options > p.crud__selection-count'))
			.nativeElement;
		expect(checkboxEl.checked).toBeTruthy('Checkbox is not checked when selected');
		expect(selectText.innerText).toBe('1 selected', 'Select number not showing correctly');

		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Lesson Form Tile] off begin
		// % protected region % [Add any additional logic for 'should keep selection when switch between grid and list' here for Collection component against Lesson Form Tile] end
	}));

	// % protected region % [Add any additional tests here for Collection component against Lesson Form Tile] off begin
	// % protected region % [Add any additional tests here for Collection component against Lesson Form Tile] end
});

