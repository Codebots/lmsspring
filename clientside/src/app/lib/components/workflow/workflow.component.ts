/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Component, EventEmitter, forwardRef, HostBinding,
	SimpleChanges, Input, OnInit, OnChanges, Output, AfterViewInit, Injector} from '@angular/core';
import {AbstractInputComponent, InputClassPrefix} from '../abstract.input.component';
import {ControlValueAccessor, FormControl, FormGroup, NG_VALUE_ACCESSOR, NgControl} from '@angular/forms';
import {Observable, of} from 'rxjs';
import {WorkflowVersionModel} from '../../../models/workflowVersion/workflow_version.model';
import {WorkflowStateModel} from '../../../models/workflowState/workflow_state.model';
import {WorkflowStateService} from '../../../services/workflowState/workflow_state.service';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * ControlValueAccessor for the Workflow Component
 * Use the ControlValueAccessor to make the component behave like native input element
 */
const CB_WORKFLOW_VALUE_ACCESSOR: any = {
	provide: NG_VALUE_ACCESSOR,
	useExisting: forwardRef(() => WorkflowComponent),
	multi: true
};

/**
 * Optins to display in workflow dropdowns
 */
interface WorkflowDisplayOption {
	workflowVersion: WorkflowVersionModel;
	currentState: WorkflowStateModel;
	nextStates$: Observable<WorkflowStateModel[]>;
}

/**
 * Workflow Component
 */
@Component({
	// % protected region % [Modify the existing component configurations here] off begin
	selector: 'cb-workflow, *[cb-workflow]',
	templateUrl: './workflow.component.html',
	providers: [CB_WORKFLOW_VALUE_ACCESSOR],
	// % protected region % [Modify the existing component configurations here] end
	styleUrls: [
		'./workflow.component.scss',
		// % protected region % [Add any additional css files here] off begin
		// % protected region % [Add any additional css files here] end
	],
	// % protected region % [Add any additional component configurations here] off begin
	// % protected region % [Add any additional component configurations here] end
})
export class WorkflowComponent extends AbstractInputComponent implements OnInit, OnChanges, ControlValueAccessor {
	/**
	 * Whether the component is an input or an input group
	 */
	protected classPrefix = InputClassPrefix.INPUT_GROUP;

	/**
	 * The type of the component, which would be used in the css class of the dom
	 */
	protected componentType = 'workflowGroup';

	/**
	 * Workflows to display in crud component
	 */
	workflowDisplayOptions: WorkflowDisplayOption[] = [];

	/**
	 * Form group for all workflows
	 */
	workflowForm: FormGroup;

	/**
	 * Workflows to display in the component
	 */
	@Input()
	workflows: WorkflowVersionModel[];

	currentStateIds: string[];

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	/**
	 * Default constructor for the workflow behaviour
	 */
	constructor(
		protected  injector: Injector,
		private readonly workflowStateService: WorkflowStateService,
		// % protected region % [Add any additional constructor logic before default process here] off begin
		// % protected region % [Add any additional constructor logic before default process here] end
	) {
		super(injector);

		// % protected region % [Add any additional constructor logic after default process here] off begin
		// % protected region % [Add any additional constructor logic after default process here] end
	}

	// % protected region % [Add any additional class constructors here] off begin
	// % protected region % [Add any additional class constructors here] end

	/**
	 * @inheritDoc
	 */
	ngOnInit(): void {
		// % protected region % [Add any additional ngOnInit logic before default process here] off begin
		// % protected region % [Add any additional ngOnInit logic before default process here] end

		super.ngOnInit();

		// % protected region % [Add any additional ngOnInit logic after default process here] off begin
		// % protected region % [Add any additional ngOnInit logic after default process here] end
	}

	/**
	 * Trigger when @Input change
	 */
	ngOnChanges(changes: SimpleChanges): void {
		// % protected region % [Add any additional ngOnChange logic before default process here] off begin
		// % protected region % [Add any additional ngOnChange logic before default process here] end

		super.ngOnChanges(changes);

		if (changes.hasOwnProperty('workflows') && changes.workflows.currentValue) {
			this.updateWorkflowForm();
			this.updateWorkflowDisplayOption();
		}

		// % protected region % [Add any additional ngOnChange logic after default process here] off begin
		// % protected region % [Add any additional ngOnChange logic after default process here] end
	}

	/**
	 * Sets the model value. Implemented as part of ControlValueAccessor.
	 * This would be called when the value of the ngModel / formControl
	 */
	writeValue(value: string[]) {
		// % protected region % [Add any additional ngOnChange logic before write value] off begin
		// % protected region % [Add any additional ngOnChange logic before write value] end

		this.currentStateIds = value;
		this.updateWorkflowDisplayOption();

		// % protected region % [Add any additional ngOnChange logic after write value] off begin
		// % protected region % [Add any additional ngOnChange logic after write value] end
	}

	/**
	 * Registers a callback to be triggered when the model value changes.
	 * Implemented as part of ControlValueAccessor.
	 * @param fn Callback to be registered.
	 */
	registerOnChange(fn: (value: any) => void) {
		// % protected region % [Add any additional logic before register on change] off begin
		// % protected region % [Add any additional logic before register on change] end

		this.controlValueAccessorChangeFn = fn;

		// % protected region % [Add any additional logic after register on change] off begin
		// % protected region % [Add any additional logic after register on change] end
	}

	/**
	 * Registers a callback to be triggered when the control is touched.
	 * Implemented as part of ControlValueAccessor.
	 * @param fn Callback to be registered.
	 */
	registerOnTouched(fn: any) {
		// % protected region % [Add any additional logic before register on touch] off begin
		// % protected region % [Add any additional logic before register on touch] end

		this.onTouched = fn;

		// % protected region % [Add any additional logic after register on touch] off begin
		// % protected region % [Add any additional logic after register on touch] end
	}

	/**
	 * Sets the 'disabled' property on the input element.
	 * This would be set with the form control
	 * @param isDisabled The disabled value
	 */
	setDisabledState(isDisabled: boolean): void {
		// % protected region % [Add any additional logic before set the disabled state] off begin
		// % protected region % [Add any additional logic before set the disabled state] end

		this.isDisabled = isDisabled;
		this.prepareFields();

		// % protected region % [Add any additional logic after set the disabled state] off begin
		// % protected region % [Add any additional logic after set the disabled state] end
	}

	/**
	 * Update form controls in form group
	 */
	updateWorkflowForm() {
		this.workflowForm = new FormGroup({
				...this.workflows.reduce((map, workflowVersion) => {
					map[workflowVersion.workflowName] = new FormControl();
					return map;
				}, {})
			}
		);

		// Update Current State Ids when dropdown change
		this.workflowForm.valueChanges.subscribe(changes => {
			this.currentStateIds = this.workflowDisplayOptions
				.filter(workflowDisplayOption => !!workflowDisplayOption.currentState)
				.map(workflowDisplayOption => {
				if (this.workflowForm.get(workflowDisplayOption.workflowVersion.workflowName).value) {
					return this.workflowForm.get(workflowDisplayOption.workflowVersion.workflowName).value;
				} else {
					return workflowDisplayOption.currentState.id;
				}
			});
			this.controlValueAccessorChangeFn(this.currentStateIds);
		});

		// % protected region % [Add any additional logic for updateWorkflowForm here] off begin
		// % protected region % [Add any additional logic for updateWorkflowForm here] end
	}

	/**
	 * Update display options of workflow
	 */
	updateWorkflowDisplayOption() {
		this.workflowDisplayOptions = this.workflows.map(
			workflowVersion => {
				let currentState;

				// TODO fix this logic after implement set references withoud id
				if (this.currentStateIds) {
					const currentStateId = this.currentStateIds.find(workflowStateId => workflowVersion.statesIds.includes(workflowStateId));

					if (!currentStateId) {
						currentState = workflowVersion.startState;
					} else {
						currentState = workflowVersion.states.find(workflowState => workflowState.id === currentStateId);
					}
				} else {
					currentState = workflowVersion.startState;
				}

				// Fetch next available states according to current state
				let nextStates: Observable<WorkflowStateModel[]>;
				if (currentState) {
					nextStates = this.workflowStateService.getNextStates(currentState.id);
				}

				return {
					workflowVersion: workflowVersion,
					currentState: currentState,
					nextStates$: nextStates
				};
			}
		);

		// % protected region % [Add any additional logic for updateWorkflowDisplayOption here] off begin
		// % protected region % [Add any additional logic for updateWorkflowDisplayOption here] end
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
