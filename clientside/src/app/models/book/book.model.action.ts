/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Action} from '@ngrx/store';
import {BookModel} from './book.model';
import {BookModelAudit} from './book.model.state';
import {PassableStateConfig} from '../../lib/services/http/interfaces';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * List of Book model actions to be dispatched by NgRx.
 */
export enum BookModelActionTypes {
	CREATE_BOOK = '[ENTITY] Create BookModel',
	CREATE_BOOK_OK = '[ENTITY] Create BookModel successfully',
	CREATE_BOOK_FAIL = '[ENTITY] Create BookModel failed',

	CREATE_ALL_BOOK = '[ENTITY] Create All BookModel',
	CREATE_ALL_BOOK_OK = '[ENTITY] Create All BookModel successfully',
	CREATE_ALL_BOOK_FAIL = '[ENTITY] Create All BookModel failed',

	DELETE_BOOK = '[ENTITY] Delete BookModel',
	DELETE_BOOK_OK = '[ENTITY] Delete BookModel successfully',
	DELETE_BOOK_FAIL = '[ENTITY] Delete BookModel failed',


	DELETE_BOOK_EXCLUDING_IDS = '[ENTITY] Delete BookModels Excluding Ids',
	DELETE_BOOK_EXCLUDING_IDS_OK = '[ENTITY] Delete BookModels Excluding Ids successfully',
	DELETE_BOOK_EXCLUDING_IDS_FAIL = '[ENTITY] Delete BookModels Excluding Ids failed',

	DELETE_ALL_BOOK = '[ENTITY] Delete all BookModels',
	DELETE_ALL_BOOK_OK = '[ENTITY] Delete all BookModels successfully',
	DELETE_ALL_BOOK_FAIL = '[ENTITY] Delete all BookModels failed',

	UPDATE_BOOK = '[ENTITY] Update BookModel',
	UPDATE_BOOK_OK = '[ENTITY] Update BookModel successfully',
	UPDATE_BOOK_FAIL = '[ENTITY] Update BookModel failed',

	UPDATE_ALL_BOOK = '[ENTITY] Update all BookModel',
	UPDATE_ALL_BOOK_OK = '[ENTITY] Update all BookModel successfully',
	UPDATE_ALL_BOOK_FAIL = '[ENTITY] Update all BookModel failed',

	FETCH_BOOK= '[ENTITY] Fetch BookModel',
	FETCH_BOOK_OK = '[ENTITY] Fetch BookModel successfully',
	FETCH_BOOK_FAIL = '[ENTITY] Fetch BookModel failed',

	FETCH_BOOK_AUDIT= '[ENTITY] Fetch BookModel audit',
	FETCH_BOOK_AUDIT_OK = '[ENTITY] Fetch BookModel audit successfully',
	FETCH_BOOK_AUDIT_FAIL = '[ENTITY] Fetch BookModel audit failed',

	FETCH_BOOK_AUDITS_BY_ENTITY_ID= '[ENTITY] Fetch BookModel audits by entity id',
	FETCH_BOOK_AUDITS_BY_ENTITY_ID_OK = '[ENTITY] Fetch BookModel audits by entity id successfully',
	FETCH_BOOK_AUDITS_BY_ENTITY_ID_FAIL = '[ENTITY] Fetch BookModel audits by entity id failed',

	FETCH_ALL_BOOK = '[ENTITY] Fetch all BookModel',
	FETCH_ALL_BOOK_OK = '[ENTITY] Fetch all BookModel successfully',
	FETCH_ALL_BOOK_FAIL = '[ENTITY] Fetch all BookModel failed',

	FETCH_BOOK_WITH_QUERY = '[ENTITY] Fetch BookModel with query',
	FETCH_BOOK_WITH_QUERY_OK = '[ENTITY] Fetch BookModel with query successfully',
	FETCH_BOOK_WITH_QUERY_FAIL = '[ENTITY] Fetch BookModel with query failed',

	FETCH_LAST_BOOK_WITH_QUERY = '[ENTITY] Fetch last BookModel with query',
	FETCH_LAST_BOOK_WITH_QUERY_OK = '[ENTITY] Fetch last BookModel with query successfully',
	FETCH_LAST_BOOK_WITH_QUERY_FAIL = '[ENTITY] Fetch last BookModel with query failed',

	EXPORT_BOOK = '[ENTITY] Export BookModel',
	EXPORT_BOOK_OK = '[ENTITY] Export BookModel successfully',
	EXPORT_BOOK_FAIL = '[ENTITY] Export BookModel failed',

	EXPORT_ALL_BOOK = '[ENTITY] Export All BookModels',
	EXPORT_ALL_BOOK_OK = '[ENTITY] Export All BookModels successfully',
	EXPORT_ALL_BOOK_FAIL = '[ENTITY] Export All BookModels failed',

	EXPORT_BOOK_EXCLUDING_IDS = '[ENTITY] Export BookModels excluding Ids',
	EXPORT_BOOK_EXCLUDING_IDS_OK = '[ENTITY] Export BookModel excluding Ids successfully',
	EXPORT_BOOK_EXCLUDING_IDS_FAIL = '[ENTITY] Export BookModel excluding Ids failed',

	COUNT_BOOKS = '[ENTITY] Fetch number of BookModel records',
	COUNT_BOOKS_OK = '[ENTITY] Fetch number of BookModel records successfully ',
	COUNT_BOOKS_FAIL = '[ENTITY] Fetch number of BookModel records failed',

	IMPORT_BOOKS = '[ENTITY] Import BookModels',
	IMPORT_BOOKS_OK = '[ENTITY] Import BookModels successfully',
	IMPORT_BOOKS_FAIL = '[ENTITY] Import BookModels fail',


	INITIALISE_BOOK_COLLECTION_STATE = '[ENTITY] Initialize the CollectionState of BookModel',
	// % protected region % [Add any additional model actions here] off begin
	// % protected region % [Add any additional model actions here] end
}

export abstract class BaseBookAction implements Action {
	readonly className: string = 'BookModel';

	abstract readonly type: string;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	protected constructor(
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
		public readonly afterwardActions: Action[] = []
	) {
		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}
}

export class BookAction extends BaseBookAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for BookAction here] off begin
	// % protected region % [Add any additional class fields for BookAction here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<BookModel>,
		// % protected region % [Add any additional constructor parameters for BookAction here] off begin
		// % protected region % [Add any additional constructor parameters for BookAction here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for BookAction here] off begin
			// % protected region % [Add any additional constructor arguments for BookAction here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for BookAction here] off begin
		// % protected region % [Add any additional constructor logic for BookAction here] end
	}

	// % protected region % [Add any additional class methods for BookAction here] off begin
	// % protected region % [Add any additional class methods for BookAction here] end
}

export class BookActionOK extends BaseBookAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for BookActionOK here] off begin
	// % protected region % [Add any additional class fields for BookActionOK here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<BookModel>,
		// % protected region % [Add any additional constructor parameters for BookActionOK here] off begin
		// % protected region % [Add any additional constructor parameters for BookActionOK here] end
		afterwardActions: Action[] = [],
		public audits?: BookModelAudit[],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for BookActionOK here] off begin
			// % protected region % [Add any additional constructor arguments for BookActionOK here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for BookActionOK here] off begin
		// % protected region % [Add any additional constructor logic for BookActionOK here] end
	}

	// % protected region % [Add any additional class methods for BookActionOK here] off begin
	// % protected region % [Add any additional class methods for BookActionOK here] end
}

export class BookActionFail extends BaseBookAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for BookActionFail here] off begin
	// % protected region % [Add any additional class fields for BookActionFail here] end

	public constructor(
		public typeInput: string,
		public readonly error?: any,
		public stateConfig?: PassableStateConfig<BookModel>,
		// % protected region % [Add any additional constructor parameters for BookActionFail here] off begin
		// % protected region % [Add any additional constructor parameters for BookActionFail here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for BookActionFail here] off begin
			// % protected region % [Add any additional constructor arguments for BookActionFail here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for BookActionFail here] off begin
		// % protected region % [Add any additional constructor logic for BookActionFail here] end
	}

	// % protected region % [Add any additional class methods for BookActionFail here] off begin
	// % protected region % [Add any additional class methods for BookActionFail here] end
}

export function isBookModelAction(e: any): e is BaseBookAction {
	return Object.values(BookModelActionTypes).includes(e);
}

// % protected region % [Add any additional actions here] off begin
// % protected region % [Add any additional actions here] end
