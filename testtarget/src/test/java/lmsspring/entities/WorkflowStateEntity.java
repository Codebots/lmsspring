/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.entities;

import lombok.*;
import javax.validation.constraints.NotNull;

import java.util.*;

// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
public class WorkflowStateEntity extends AbstractEntity {

	public WorkflowStateEntity() {
		initialiseReferences();
	}

	private void initialiseReferences() {


		var WorkflowVersionOneMany = new EntityReference();
			WorkflowVersionOneMany.entityName = "WorkflowVersion";
			WorkflowVersionOneMany.oppositeName = "WorkflowVersion";
			WorkflowVersionOneMany.name = "States";
			WorkflowVersionOneMany.optional = false;
			WorkflowVersionOneMany.type = "One";
			WorkflowVersionOneMany.oppositeType = "Many";

		References.add(WorkflowVersionOneMany);

		var OutgoingTransitionsOneMany = new EntityReference();
			OutgoingTransitionsOneMany.entityName = "WorkflowTransition";
			OutgoingTransitionsOneMany.oppositeName = "SourceState";
			OutgoingTransitionsOneMany.name = "OutgoingTransitions";
			OutgoingTransitionsOneMany.optional = true;
			OutgoingTransitionsOneMany.type = "One";
			OutgoingTransitionsOneMany.oppositeType = "Many";

		References.add(OutgoingTransitionsOneMany);

		var IncomingTransitionsOneMany = new EntityReference();
			IncomingTransitionsOneMany.entityName = "WorkflowTransition";
			IncomingTransitionsOneMany.oppositeName = "TargetState";
			IncomingTransitionsOneMany.name = "IncomingTransitions";
			IncomingTransitionsOneMany.optional = true;
			IncomingTransitionsOneMany.type = "One";
			IncomingTransitionsOneMany.oppositeType = "Many";

		References.add(IncomingTransitionsOneMany);

		var ArticleManyMany = new EntityReference();
			ArticleManyMany.entityName = "Article";
			ArticleManyMany.oppositeName = "Article";
			ArticleManyMany.name = "WorkflowStates";
			ArticleManyMany.optional = true;
			ArticleManyMany.type = "Many";
			ArticleManyMany.oppositeType = "Many";

		References.add(ArticleManyMany);
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Display Index here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Display Index here] end
	private Integer displayIndex;

	// % protected region % [Modify attribute annotation for Step Name here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Step Name here] end
	private String stepName;

	// % protected region % [Modify attribute annotation for State Description here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for State Description here] end
	private String stateDescription;

	// % protected region % [Modify attribute annotation for Is Start State here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Is Start State here] end
	private Boolean isStartState;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private Set<WorkflowTransitionEntity> outgoingTransitions = new HashSet<>();

	private Set<WorkflowTransitionEntity> incomingTransitions = new HashSet<>();

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private WorkflowVersionEntity workflowVersion;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private Set<ArticleEntity> article = new HashSet<>();

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
	 * Similar to {@link this#addOutgoingTransitions(WorkflowTransitionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowTransitionEntity to be added to this entity
	 */
	public void addOutgoingTransitions(@NotNull WorkflowTransitionEntity entity) {
		addOutgoingTransitions(entity, true);
	}

	/**
	 * Add a new WorkflowTransitionEntity to outgoingTransitions in this entity.
	 *
	 * @param entity the given WorkflowTransitionEntity  to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addOutgoingTransitions(@NonNull WorkflowTransitionEntity entity, boolean reverseAdd) {
		if (!this.outgoingTransitions.contains(entity)) {
			outgoingTransitions.add(entity);
			if (reverseAdd) {
				entity.setSourceState(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addOutgoingTransitions(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to be added to this entity
	 */
	public void addOutgoingTransitions(@NotNull Collection<WorkflowTransitionEntity> entities) {
		addOutgoingTransitions(entities, true);
	}

	/**
	 * Add a new collection of WorkflowTransitionEntity to Outgoing Transitions in this entity.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addOutgoingTransitions(@NonNull Collection<WorkflowTransitionEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> addOutgoingTransitions(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeOutgoingTransitions(WorkflowTransitionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowTransitionEntity to be set to this entity
	 */
	public void removeOutgoingTransitions(@NotNull WorkflowTransitionEntity entity) {
		this.removeOutgoingTransitions(entity, true);
	}

	/**
	 * Remove the given WorkflowTransitionEntity from this entity.
	 *
	 * @param entity the given WorkflowTransitionEntity to be removed from this entity
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeOutgoingTransitions(@NotNull WorkflowTransitionEntity entity, boolean reverse) {
		if (reverse) {
			entity.unsetSourceState(false);
		}
		this.outgoingTransitions.remove(entity);
	}

	/**
	 * Similar to {@link this#removeOutgoingTransitions(Collection, boolean)} but
	 * default to true for reverse remove.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to be removed to this entity
	 */
	public void removeOutgoingTransitions(@NotNull Collection<WorkflowTransitionEntity> entities) {
		this.removeOutgoingTransitions(entities, true);
	}

	/**
	 * Remove the given collection of WorkflowTransitionEntity from  to this entity.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to be removed to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void removeOutgoingTransitions(@NonNull Collection<WorkflowTransitionEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.removeOutgoingTransitions(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#setOutgoingTransitions(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to be set to this entity
	 */
	public void setOutgoingTransitions(@NotNull Collection<WorkflowTransitionEntity> entities) {
		setOutgoingTransitions(entities, true);
	}

	/**
	 * Replace the current entities in Outgoing Transitions with the given ones.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to replace the old ones
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setOutgoingTransitions(@NotNull Collection<WorkflowTransitionEntity> entities, boolean reverseAdd) {

		this.unsetOutgoingTransitions();
		this.outgoingTransitions = new HashSet<>(entities);
		if (reverseAdd) {
			this.outgoingTransitions.forEach(outgoingTransitionsEntity -> outgoingTransitionsEntity.setSourceState(this, false));
		}
	}

	/**
	 * Similar to {@link this#unsetOutgoingTransitions(boolean)} but
	 * default to true for reverse unset
	 */
	public void unsetOutgoingTransitions() {
		this.unsetOutgoingTransitions(true);
	}

	/**
	 * Remove all the entities in Outgoing Transitions from this entity.
	 * @param doReverse whether this entity should be removed from the given entities
	 */
	public void unsetOutgoingTransitions(boolean doReverse) {
		if (doReverse) {
			this.outgoingTransitions.forEach(outgoingTransitionsEntity -> outgoingTransitionsEntity.unsetSourceState(false));
		}
		this.outgoingTransitions.clear();
	}

/**
	 * Similar to {@link this#addIncomingTransitions(WorkflowTransitionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowTransitionEntity to be added to this entity
	 */
	public void addIncomingTransitions(@NotNull WorkflowTransitionEntity entity) {
		addIncomingTransitions(entity, true);
	}

	/**
	 * Add a new WorkflowTransitionEntity to incomingTransitions in this entity.
	 *
	 * @param entity the given WorkflowTransitionEntity  to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addIncomingTransitions(@NonNull WorkflowTransitionEntity entity, boolean reverseAdd) {
		if (!this.incomingTransitions.contains(entity)) {
			incomingTransitions.add(entity);
			if (reverseAdd) {
				entity.setTargetState(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addIncomingTransitions(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to be added to this entity
	 */
	public void addIncomingTransitions(@NotNull Collection<WorkflowTransitionEntity> entities) {
		addIncomingTransitions(entities, true);
	}

	/**
	 * Add a new collection of WorkflowTransitionEntity to Incoming Transitions in this entity.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addIncomingTransitions(@NonNull Collection<WorkflowTransitionEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> addIncomingTransitions(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeIncomingTransitions(WorkflowTransitionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowTransitionEntity to be set to this entity
	 */
	public void removeIncomingTransitions(@NotNull WorkflowTransitionEntity entity) {
		this.removeIncomingTransitions(entity, true);
	}

	/**
	 * Remove the given WorkflowTransitionEntity from this entity.
	 *
	 * @param entity the given WorkflowTransitionEntity to be removed from this entity
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeIncomingTransitions(@NotNull WorkflowTransitionEntity entity, boolean reverse) {
		if (reverse) {
			entity.unsetTargetState(false);
		}
		this.incomingTransitions.remove(entity);
	}

	/**
	 * Similar to {@link this#removeIncomingTransitions(Collection, boolean)} but
	 * default to true for reverse remove.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to be removed to this entity
	 */
	public void removeIncomingTransitions(@NotNull Collection<WorkflowTransitionEntity> entities) {
		this.removeIncomingTransitions(entities, true);
	}

	/**
	 * Remove the given collection of WorkflowTransitionEntity from  to this entity.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to be removed to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void removeIncomingTransitions(@NonNull Collection<WorkflowTransitionEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.removeIncomingTransitions(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#setIncomingTransitions(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to be set to this entity
	 */
	public void setIncomingTransitions(@NotNull Collection<WorkflowTransitionEntity> entities) {
		setIncomingTransitions(entities, true);
	}

	/**
	 * Replace the current entities in Incoming Transitions with the given ones.
	 *
	 * @param entities the given collection of WorkflowTransitionEntity to replace the old ones
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setIncomingTransitions(@NotNull Collection<WorkflowTransitionEntity> entities, boolean reverseAdd) {

		this.unsetIncomingTransitions();
		this.incomingTransitions = new HashSet<>(entities);
		if (reverseAdd) {
			this.incomingTransitions.forEach(incomingTransitionsEntity -> incomingTransitionsEntity.setTargetState(this, false));
		}
	}

	/**
	 * Similar to {@link this#unsetIncomingTransitions(boolean)} but
	 * default to true for reverse unset
	 */
	public void unsetIncomingTransitions() {
		this.unsetIncomingTransitions(true);
	}

	/**
	 * Remove all the entities in Incoming Transitions from this entity.
	 * @param doReverse whether this entity should be removed from the given entities
	 */
	public void unsetIncomingTransitions(boolean doReverse) {
		if (doReverse) {
			this.incomingTransitions.forEach(incomingTransitionsEntity -> incomingTransitionsEntity.unsetTargetState(false));
		}
		this.incomingTransitions.clear();
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Similar to {@link this#setWorkflowVersion(WorkflowVersionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowVersionEntity to be set to this entity
	 */
	public void setWorkflowVersion(@NotNull WorkflowVersionEntity entity) {
		setWorkflowVersion(entity, true);
	}

	/**
	 * Set or update the workflowVersion in this entity with single WorkflowVersionEntity.
	 *
	 * @param entity the given WorkflowVersionEntity to be set or updated to workflowVersion
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setWorkflowVersion(@NotNull WorkflowVersionEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setWorkflowVersion here] off begin
		// % protected region % [Add any additional logic here before the main logic for setWorkflowVersion here] end

		if (sameAsFormer(this.workflowVersion, entity)) {
			return;
		}

		if (this.workflowVersion != null) {
			this.workflowVersion.removeStates(this, false);
		}
		this.workflowVersion = entity;
		if (reverseAdd) {
			this.workflowVersion.addStates(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setWorkflowVersion here] off begin
		// % protected region % [Add any additional logic here after the main logic for setWorkflowVersion here] end
	}

	/**
	 * Similar to {@link this#unsetWorkflowVersion(boolean)} but default to true.
	 */
	public void unsetWorkflowVersion() {
		this.unsetWorkflowVersion(true);
	}

	/**
	 * Remove Workflow Version in this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetWorkflowVersion(boolean reverse) {
		if (reverse && this.workflowVersion != null) {
			this.workflowVersion.removeStates(this, false);
		}
		this.workflowVersion = null;
	}
	/**
	 * Similar to {@link this#addArticle(ArticleEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given ArticleEntity to be added to article
	 */
	public void addArticle(@NotNull ArticleEntity entity) {
		this.addArticle(entity, true);
	}

	/**
	 * Add a new ArticleEntity to article in this entity.
	 *
	 * @param entity the given ArticleEntity to be added to article
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addArticle(@NotNull ArticleEntity entity, boolean reverseAdd) {
		if (!this.article.contains(entity)) {
			this.article.add(entity);
			if (reverseAdd) {
				entity.addWorkflowStates(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addArticle(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of ArticleEntity to be added into article
	 */
	public void addArticle(@NotNull Collection<ArticleEntity> entities) {
		this.addArticle(entities, true);
	}

	/**
	 * Add new collection of ArticleEntity to article in this entity.
	 *
	 * @param entities the given collection of ArticleEntity to be added into article in this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addArticle(@NonNull Collection<ArticleEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.addArticle(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeArticle(ArticleEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given ArticleEntity to be set to article in this entity
	 */
	public void removeArticle(@NotNull ArticleEntity entity) {
		this.removeArticle(entity, true);
	}

	/**
	 * Remove the given ArticleEntity from article in this entity.
	 *
	 * @param entity the given ArticleEntity to be removed from article
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeArticle(@NotNull ArticleEntity entity, boolean reverse) {
		if (reverse) {
			entity.removeWorkflowStates(this, false);
		}
		this.article.remove(entity);
	}

	/**
	 * Similar to {@link this#removeArticle(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of ArticleEntity to be removed from article in this entity
	 */
	public void removeArticle(@NotNull Collection<ArticleEntity> entities) {
		this.removeArticle(entities, true);
	}

	/**
	 * Remove the given collection of ArticleEntity from article in this entity.
	 *
	 * @param entities the given collection of ArticleEntity to be removed from article
	 * @param reverseRemove whether this entity should be removed to the given entities
	 */
	public void removeArticle(@NonNull Collection<ArticleEntity> entities, boolean reverseRemove) {
		entities.forEach(entity -> this.removeArticle(entity, reverseRemove));
	}

	/**
	 * Similar to {@link this#setArticle(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of ArticleEntity to replace the old ones in article
	 */
	public void setArticle(@NotNull Collection<ArticleEntity> entities) {
		this.setArticle(entities, true);
	}

	/**
	 * Replace the current collection of ArticleEntity in article with the given ones.
	 *
	 * @param entities the given collection of ArticleEntity to replace the old ones in article
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setArticle(@NotNull Collection<ArticleEntity> entities, boolean reverseAdd) {
		unsetArticle();
		this.article = new HashSet<>(entities);
		if (reverseAdd) {
			this.article.forEach(articleEntity -> articleEntity.addWorkflowStates(this, false));
		}
	}

	/**
	 * Remove all entities in Article from this entity.
	 */
	public void unsetArticle() {
		this.article.forEach(entity -> entity.removeWorkflowStates(this, false));
		this.article.clear();
	}

	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
