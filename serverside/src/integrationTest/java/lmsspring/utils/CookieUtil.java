/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.utils;

import com.google.common.collect.Lists;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * Utility class used to extract cookie from various places like requests or responses.
 */
public class CookieUtil {

	//public static <T> Map<String, String> fromResponse(ResponseEntity<T> response) {
	//	HttpHeaders headers = response.getHeaders();
	//	List<String> cookies = headers.getOrDefault(HttpHeaders.SET_COOKIE, Lists.newArrayList());
//
	//	Map<String, String> cookieMap = new HashMap<>();
//
	//	cookies.forEach(cookie -> {
	//		String[] rawTokens
	//	});
//
	//	return headers.getOrDefault(key, Lists.newArrayList());
	//}
}