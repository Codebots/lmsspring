/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.inject.modules.entities;

import lmsspring.entities.*;
import lmsspring.entities.enums.*;
import lmsspring.inject.factories.*;
import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.andreinc.mockneat.MockNeat;
import net.andreinc.mockneat.types.enums.StringType;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.UUID;

// % protected region % [Apply any additional imports here] off begin
// % protected region % [Apply any additional imports here] end

/**
 * Guice module for Lesson used to register providers for dependency injection.
 */
@Slf4j
@AllArgsConstructor
public class LessonModule extends AbstractModule {
	// % protected region % [Apply any additional class fields here] off begin
	// % protected region % [Apply any additional class fields here] end

	@Override
	protected void configure() {
		log.trace("Configuring LessonModule");

		super.configure();

		// % protected region % [Apply any additional configure steps here] off begin
		// % protected region % [Apply any additional configure steps here] end

		log.trace("Configured LessonModule");
	}

	/**
	 * Return a factory for mass data generation.
	 */
	@Provides
	public LessonEntityFactory lessonEntityFactory(
			// % protected region % [Apply any additional injected arguments for lessonEntityFactory here] off begin
			// % protected region % [Apply any additional injected arguments for lessonEntityFactory here] end
			Injector injector
	) {
		log.trace("Creating LessonEntityFactory");

		// % protected region % [Apply any additional logic for lessonEntityFactory before the main body here] off begin
		// % protected region % [Apply any additional logic for lessonEntityFactory before the main body here] end

		LessonEntityFactory entityFactory = new LessonEntityFactory(
				// % protected region % [Apply any additional constructor arguments for LessonEntityFactory here] off begin
				// % protected region % [Apply any additional constructor arguments for LessonEntityFactory here] end
				injector
		);

		// % protected region % [Apply any additional logic for lessonEntityFactory after the main body here] off begin
		// % protected region % [Apply any additional logic for lessonEntityFactory after the main body here] end

		log.trace("Created LessonEntityFactory");

		return entityFactory;
	}

	/**
	 * Return an empty Lesson entity with no references set.
	 */
	@Provides
	@Named("lessonEntityWithNoRef")
	public LessonEntity lessonEntityWithNoRef(
			// % protected region % [Apply any additional constructor parameters for lessonEntityWithNoRef here] off begin
			// % protected region % [Apply any additional constructor parameters for lessonEntityWithNoRef here] end
			MockNeat mock
	) {
		log.trace("Creating entity of type LessonEntity with no reference");

		// % protected region % [Apply any additional logic for lessonWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for lessonWithNoRef before the main body here] end

		LessonEntity newEntity = new LessonEntity();
		newEntity.setId(UUID.randomUUID());
		newEntity.setCreated(OffsetDateTime.now());
		newEntity.setModified(OffsetDateTime.now());
		// % protected region % [Add customisation for Summary here] off begin
		String randomStringforSummary = mock
				.strings()
				.get();
		newEntity.setSummary(randomStringforSummary);
		// % protected region % [Add customisation for Summary here] end
		// % protected region % [Add customisation for Description here] off begin
		String randomStringforDescription = mock
				.strings()
				.get();
		newEntity.setDescription(randomStringforDescription);
		// % protected region % [Add customisation for Description here] end
		// % protected region % [Add customisation for Cover Image here] off begin
		// % protected region % [Add customisation for Cover Image here] end
		// % protected region % [Add customisation for Duration here] off begin
		newEntity.setDuration(mock.ints().get());
		// % protected region % [Add customisation for Duration here] end
		// % protected region % [Add customisation for Difficulty here] off begin
		newEntity.setDifficulty(DifficultyEnum.BEGINNER);
		// % protected region % [Add customisation for Difficulty here] end
		// % protected region % [Add customisation for Name here] off begin
		String randomStringforName = mock
				.strings()
				.get();
		newEntity.setName(randomStringforName);
		// % protected region % [Add customisation for Name here] end

		// % protected region % [Apply any additional logic for lessonWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for lessonWithNoRef after the main body here] end

		log.trace("Created entity of type LessonEntity with no reference");

		return newEntity;
	}

	/**
	 * Return a collection of Lesson entities with no reference at all.
	 */
	@Provides
	@Named("lessonEntitiesWithNoRef")
	public Collection<LessonEntity> lessonEntitiesWithNoRef(
		// % protected region % [Apply any additional constructor parameters for lessonEntitiesWithNoRef here] off begin
		// % protected region % [Apply any additional constructor parameters for lessonEntitiesWithNoRef here] end
		LessonEntityFactory lessonEntityFactory
	) {
		log.trace("Creating entities of type LessonEntity with no reference");

		// % protected region % [Apply any additional logic for lessonEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for lessonEntitiesWithNoRef before the main body here] end

		Collection<LessonEntity> newEntities = lessonEntityFactory.createMultipleWithNoRef(10);

		// % protected region % [Apply any additional logic for lessonEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for lessonEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type LessonEntity with no reference");

		return newEntities;
	}

	/**
	 * Return a Lesson entity with all references set.
	 */
	@Provides
	@Named("lessonEntityWithRefs")
	public LessonEntity lessonEntityWithRefs(
			// % protected region % [Apply any additional constructor parameters for lessonEntityWithRefs here] off begin
			// % protected region % [Apply any additional constructor parameters for lessonEntityWithRefs here] end
			Injector injector
	) {
		log.trace("Creating entity of type LessonEntity with references");

		// % protected region % [Apply any additional logic for lessonEntityWithRefs before the main body here] off begin
		// % protected region % [Apply any additional logic for lessonEntityWithRefs before the main body here] end

		LessonEntity lessonEntity = injector.getInstance(Key.get(LessonEntity.class, Names.named("lessonEntityWithNoRef")));

		// % protected region % [Apply any additional logic for lessonEntityWithRefs after the main body here] off begin
		// % protected region % [Apply any additional logic for lessonEntityWithRefs after the main body here] end

		log.trace("Created entity of type LessonEntity with references");

		return lessonEntity;
	}

	/**
	 * Return a collection of Lesson entities with all references set.
	 */
	@Provides
	@Named("lessonEntitiesWithRefs")
	public Collection<LessonEntity> lessonEntitiesWithRefs(
		// % protected region % [Apply any additional constructor parameters for lessonEntitiesWithRefs here] off begin
		// % protected region % [Apply any additional constructor parameters for lessonEntitiesWithRefs here] end
		LessonEntityFactory lessonEntityFactory
	) {
		log.trace("Creating entities of type LessonEntity with references");

		// % protected region % [Apply any additional logic for lessonEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for lessonEntitiesWithNoRef before the main body here] end

		Collection<LessonEntity> newEntities = lessonEntityFactory.createMultipleWithRefs(10);

		// % protected region % [Apply any additional logic for lessonEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for lessonEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type LessonEntity with references");

		return newEntities;
	}

	// % protected region % [Apply any additional class methods here] off begin
	// % protected region % [Apply any additional class methods here] end
}
