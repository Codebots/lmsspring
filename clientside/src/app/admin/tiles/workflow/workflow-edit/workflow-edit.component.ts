/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Component, HostBinding} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Store} from '@ngrx/store';
import {RouterState} from '../../../../models/model.state';
import {AbstractComponent} from '../../../../lib/components/abstract.component';
import {ButtonGroupAlignment} from '../../../../lib/components/buttonGroup/button.group.component';
import {ButtonAccentColour, ButtonSize, ButtonStyle} from '../../../../lib/components/button/button.component';
import {WorkflowVersionModel} from '../../../../models/workflowVersion/workflow_version.model';
import {WorkflowDetailsComponent} from './workflow-details/workflow-details.component';
import {WorkflowStatesComponent} from './workflow-states/workflow-states.component';
import {Observable, of} from 'rxjs';
import {switchMap, tap} from 'rxjs/operators';
import {getWorkflowVersionModelWithId} from '../../../../models/workflowVersion/workflow_version.model.selector';
import * as workflowVersionActions from '../../../../models/workflowVersion/workflow_version.model.action';
import * as routerActions from '../../../../lib/routing/routing.action';
// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

@Component({
	selector: 'div[cb-workflow-edit]',
	templateUrl: './workflow-edit.component.html',
	styleUrls: [
		'./workflow-edit.component.scss',
		// % protected region % [Add any additional styles here] off begin
		// % protected region % [Add any additional styles here] end
	]
})
export class WorkflowEditComponent extends AbstractComponent {
	ButtonGroupAlignment = ButtonGroupAlignment;
	routerState: RouterState;
	ButtonStyle = ButtonStyle;
	buttonSize = ButtonSize;
	buttonColor = ButtonAccentColour;

	activatedComponent: WorkflowDetailsComponent | WorkflowStatesComponent;

	versionId: string;

	version$: Observable<WorkflowVersionModel>;

	@HostBinding('class')
	className = 'workflow-behaviour';

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	constructor(
		private readonly route: ActivatedRoute,
		private readonly store: Store<{ model: WorkflowVersionModel}>,
		private readonly routerStore: Store<{ router: RouterState }>,
		// % protected region % [Add any additional cosntructor parameters here] off begin
		// % protected region % [Add any additional cosntructor parameters here] end
	) {
		super();

		this.version$ = this.route.paramMap.pipe(
			switchMap(params => {
				this.versionId = params.get('versionId');
				if (this.versionId) {
					this.store.dispatch(new workflowVersionActions.WorkflowVersionAction(
						workflowVersionActions.WorkflowVersionModelActionTypes.FETCH_WORKFLOW_VERSION,
						{
							targetModelId: this.versionId
						}
					));
					return this.store.select(getWorkflowVersionModelWithId, this.versionId);
				} else {
					return of(new WorkflowVersionModel());
				}
			}),
		);

		// % protected region % [Add any additional logic for constructor here] off begin
		// % protected region % [Add any additional logic for constructor here] end
	}

	onActivate($event) {
		this.activatedComponent = $event;
	}

	onCancelClicked() {
		if (this.versionId) {
			this.routerStore.dispatch(new routerActions.NavigateRoutingAction(['../../'], {
				relativeTo: this.route
			}));
		} else {
			this.routerStore.dispatch(new routerActions.NavigateRoutingAction(['../'], {
				relativeTo: this.route
			}));
		}

		// % protected region % [Add any additional logic for onCancelClicked here] off begin
		// % protected region % [Add any additional logic for onCancelClicked here] end
	}

	onSaveDraftClicked() {
		this.activatedComponent.onSaveDraftClicked();
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
