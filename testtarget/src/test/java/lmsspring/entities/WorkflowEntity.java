/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.entities;

import lombok.*;
import javax.validation.constraints.NotNull;

import java.util.*;

// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
public class WorkflowEntity extends AbstractEntity {

	public WorkflowEntity() {
		initialiseReferences();
	}

	private void initialiseReferences() {


		var VersionsOneMany = new EntityReference();
			VersionsOneMany.entityName = "WorkflowVersion";
			VersionsOneMany.oppositeName = "Workflow";
			VersionsOneMany.name = "Versions";
			VersionsOneMany.optional = true;
			VersionsOneMany.type = "One";
			VersionsOneMany.oppositeType = "Many";

		References.add(VersionsOneMany);

		var CurrentVersionOneOne = new EntityReference();
			CurrentVersionOneOne.entityName = "WorkflowVersion";
			CurrentVersionOneOne.oppositeName = "CurrentVersion";
			CurrentVersionOneOne.name = "CurrentWorkflow";
			CurrentVersionOneOne.optional = true;
			CurrentVersionOneOne.type = "One";
			CurrentVersionOneOne.oppositeType = "One";

		References.add(CurrentVersionOneOne);
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Name here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Name here] end
	private String name;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private WorkflowVersionEntity currentVersion;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private Set<WorkflowVersionEntity> versions = new HashSet<>();

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
	 * Similar to {@link this#addVersions(WorkflowVersionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowVersionEntity to be added to this entity
	 */
	public void addVersions(@NotNull WorkflowVersionEntity entity) {
		addVersions(entity, true);
	}

	/**
	 * Add a new WorkflowVersionEntity to versions in this entity.
	 *
	 * @param entity the given WorkflowVersionEntity  to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addVersions(@NonNull WorkflowVersionEntity entity, boolean reverseAdd) {
		if (!this.versions.contains(entity)) {
			versions.add(entity);
			if (reverseAdd) {
				entity.setWorkflow(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addVersions(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of WorkflowVersionEntity to be added to this entity
	 */
	public void addVersions(@NotNull Collection<WorkflowVersionEntity> entities) {
		addVersions(entities, true);
	}

	/**
	 * Add a new collection of WorkflowVersionEntity to Versions in this entity.
	 *
	 * @param entities the given collection of WorkflowVersionEntity to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addVersions(@NonNull Collection<WorkflowVersionEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> addVersions(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeVersions(WorkflowVersionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowVersionEntity to be set to this entity
	 */
	public void removeVersions(@NotNull WorkflowVersionEntity entity) {
		this.removeVersions(entity, true);
	}

	/**
	 * Remove the given WorkflowVersionEntity from this entity.
	 *
	 * @param entity the given WorkflowVersionEntity to be removed from this entity
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeVersions(@NotNull WorkflowVersionEntity entity, boolean reverse) {
		if (reverse) {
			entity.unsetWorkflow(false);
		}
		this.versions.remove(entity);
	}

	/**
	 * Similar to {@link this#removeVersions(Collection, boolean)} but
	 * default to true for reverse remove.
	 *
	 * @param entities the given collection of WorkflowVersionEntity to be removed to this entity
	 */
	public void removeVersions(@NotNull Collection<WorkflowVersionEntity> entities) {
		this.removeVersions(entities, true);
	}

	/**
	 * Remove the given collection of WorkflowVersionEntity from  to this entity.
	 *
	 * @param entities the given collection of WorkflowVersionEntity to be removed to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void removeVersions(@NonNull Collection<WorkflowVersionEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.removeVersions(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#setVersions(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of WorkflowVersionEntity to be set to this entity
	 */
	public void setVersions(@NotNull Collection<WorkflowVersionEntity> entities) {
		setVersions(entities, true);
	}

	/**
	 * Replace the current entities in Versions with the given ones.
	 *
	 * @param entities the given collection of WorkflowVersionEntity to replace the old ones
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setVersions(@NotNull Collection<WorkflowVersionEntity> entities, boolean reverseAdd) {

		this.unsetVersions();
		this.versions = new HashSet<>(entities);
		if (reverseAdd) {
			this.versions.forEach(versionsEntity -> versionsEntity.setWorkflow(this, false));
		}
	}

	/**
	 * Similar to {@link this#unsetVersions(boolean)} but
	 * default to true for reverse unset
	 */
	public void unsetVersions() {
		this.unsetVersions(true);
	}

	/**
	 * Remove all the entities in Versions from this entity.
	 * @param doReverse whether this entity should be removed from the given entities
	 */
	public void unsetVersions(boolean doReverse) {
		if (doReverse) {
			this.versions.forEach(versionsEntity -> versionsEntity.unsetWorkflow(false));
		}
		this.versions.clear();
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Similar to {@link this#setCurrentVersion(WorkflowVersionEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowVersionEntity to be set to this entity
	 */
	public void setCurrentVersion(WorkflowVersionEntity entity) {
		setCurrentVersion(entity, true);
	}

	/**
	 * Set or update the currentVersion in this entity with single WorkflowVersionEntity.
	 *
	 * @param entity the given WorkflowVersionEntity to be set or updated to currentVersion
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setCurrentVersion(WorkflowVersionEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setCurrentVersion here] off begin
		// % protected region % [Add any additional logic here before the main logic for setCurrentVersion here] end

		if (sameAsFormer(this.currentVersion, entity)) {
			return;
		}

		if (this.currentVersion != null) {
			this.currentVersion.unsetCurrentWorkflow(false);
		}

		this.currentVersion = entity;
		if (reverseAdd) {
			this.currentVersion.setCurrentWorkflow(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setWorkflowVersion here] off begin
		// % protected region % [Add any additional logic here after the main logic for setWorkflowVersion here] end
	}

	/**
	 * Similar to {@link this#unsetCurrentVersion(boolean)} but default to true.
	 */
	public void unsetCurrentVersion() {
		this.unsetCurrentVersion(true);
	}

	/**
	 * Remove the WorkflowVersionEntity of currentVersion from this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetCurrentVersion(boolean reverse) {
		if (reverse && this.currentVersion != null) {
			this.currentVersion.unsetCurrentWorkflow();
		}
		this.currentVersion = null;
	}

	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
