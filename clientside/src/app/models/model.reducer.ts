/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Action, ActionReducerMap} from '@ngrx/store';
import {routerReducer} from '@ngrx/router-store';
import {modelReducer as administratorReducer} from './administrator/administrator.model.reducer';
import {isAdministratorModelAction} from './administrator/administrator.model.action';
import {modelReducer as applicationLocaleReducer} from './applicationLocale/application_locale.model.reducer';
import {isApplicationLocaleModelAction} from './applicationLocale/application_locale.model.action';
import {modelReducer as articleReducer} from './article/article.model.reducer';
import {isArticleModelAction} from './article/article.model.action';
import {modelReducer as bookReducer} from './book/book.model.reducer';
import {isBookModelAction} from './book/book.model.action';
import {modelReducer as coreUserReducer} from './coreUser/core_user.model.reducer';
import {isCoreUserModelAction} from './coreUser/core_user.model.action';
import {modelReducer as courseReducer} from './course/course.model.reducer';
import {isCourseModelAction} from './course/course.model.action';
import {modelReducer as courseCategoryReducer} from './courseCategory/course_category.model.reducer';
import {isCourseCategoryModelAction} from './courseCategory/course_category.model.action';
import {modelReducer as courseLessonReducer} from './courseLesson/course_lesson.model.reducer';
import {isCourseLessonModelAction} from './courseLesson/course_lesson.model.action';
import {modelReducer as lessonReducer} from './lesson/lesson.model.reducer';
import {isLessonModelAction} from './lesson/lesson.model.action';
import {modelReducer as tagReducer} from './tag/tag.model.reducer';
import {isTagModelAction} from './tag/tag.model.action';
import {modelReducer as roleReducer} from './role/role.model.reducer';
import {isRoleModelAction} from './role/role.model.action';
import {modelReducer as privilegeReducer} from './privilege/privilege.model.reducer';
import {isPrivilegeModelAction} from './privilege/privilege.model.action';
import {modelReducer as workflowReducer} from './workflow/workflow.model.reducer';
import {isWorkflowModelAction} from './workflow/workflow.model.action';
import {modelReducer as workflowStateReducer} from './workflowState/workflow_state.model.reducer';
import {isWorkflowStateModelAction} from './workflowState/workflow_state.model.action';
import {modelReducer as workflowTransitionReducer} from './workflowTransition/workflow_transition.model.reducer';
import {isWorkflowTransitionModelAction} from './workflowTransition/workflow_transition.model.action';
import {modelReducer as workflowVersionReducer} from './workflowVersion/workflow_version.model.reducer';
import {isWorkflowVersionModelAction} from './workflowVersion/workflow_version.model.action';
import {modelReducer as lessonFormSubmissionReducer} from './lessonFormSubmission/lesson_form_submission.model.reducer';
import {isLessonFormSubmissionModelAction} from './lessonFormSubmission/lesson_form_submission.model.action';
import {modelReducer as lessonFormVersionReducer} from './lessonFormVersion/lesson_form_version.model.reducer';
import {isLessonFormVersionModelAction} from './lessonFormVersion/lesson_form_version.model.action';
import {modelReducer as lessonFormTileReducer} from './lessonFormTile/lesson_form_tile.model.reducer';
import {isLessonFormTileModelAction} from './lessonFormTile/lesson_form_tile.model.action';
import {AppState, initialModelState, initialRouterState, ModelState} from './model.state';
import {ActionTypes} from './model.action';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * Main reducer for the model state.
 */
export function modelReducer(modelState: ModelState, action: Action): ModelState {
	if (isAdministratorModelAction(action.type)) {
		return administratorReducer(modelState, action);
	}
	if (isApplicationLocaleModelAction(action.type)) {
		return applicationLocaleReducer(modelState, action);
	}
	if (isArticleModelAction(action.type)) {
		return articleReducer(modelState, action);
	}
	if (isBookModelAction(action.type)) {
		return bookReducer(modelState, action);
	}
	if (isCoreUserModelAction(action.type)) {
		return coreUserReducer(modelState, action);
	}
	if (isCourseModelAction(action.type)) {
		return courseReducer(modelState, action);
	}
	if (isCourseCategoryModelAction(action.type)) {
		return courseCategoryReducer(modelState, action);
	}
	if (isCourseLessonModelAction(action.type)) {
		return courseLessonReducer(modelState, action);
	}
	if (isLessonModelAction(action.type)) {
		return lessonReducer(modelState, action);
	}
	if (isTagModelAction(action.type)) {
		return tagReducer(modelState, action);
	}
	if (isRoleModelAction(action.type)) {
		return roleReducer(modelState, action);
	}
	if (isPrivilegeModelAction(action.type)) {
		return privilegeReducer(modelState, action);
	}
	if (isWorkflowModelAction(action.type)) {
		return workflowReducer(modelState, action);
	}
	if (isWorkflowStateModelAction(action.type)) {
		return workflowStateReducer(modelState, action);
	}
	if (isWorkflowTransitionModelAction(action.type)) {
		return workflowTransitionReducer(modelState, action);
	}
	if (isWorkflowVersionModelAction(action.type)) {
		return workflowVersionReducer(modelState, action);
	}
	if (isLessonFormSubmissionModelAction(action.type)) {
		return lessonFormSubmissionReducer(modelState, action);
	}
	if (isLessonFormVersionModelAction(action.type)) {
		return lessonFormVersionReducer(modelState, action);
	}
	if (isLessonFormTileModelAction(action.type)) {
		return lessonFormTileReducer(modelState, action);
	}

	return modelState;
}

/**
 * All the reducers available in the application.
 */
export const reducers: ActionReducerMap<AppState> = {
	router: routerReducer,
	models: modelReducer,
	// % protected region % [Add any additional reducers here] off begin
	// % protected region % [Add any additional reducers here] end
};

/**
 * Meta-reducer used to clear out store when log out.
 */
export function clearState(reducer: (AppState, Action) => AppState): (AppState, Action) => AppState {
	return (state: AppState, action: Action) => {
		// % protected region % [Add any additional logic for clearState before the main body here] off begin
		// % protected region % [Add any additional logic for clearState before the main body here] end

		if (action.type === ActionTypes.LOGOUT) {
			state = {
				router: initialRouterState,
				models: initialModelState
			};
		}

		// % protected region % [Add any additional logic for clearState after the main body here] off begin
		// % protected region % [Add any additional logic for clearState after the main body here] end

		return reducer(state, action);
	};
}

// % protected region % [Add any additional stuffs here] off begin
// % protected region % [Add any additional stuffs here] end
