/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {WorkflowVersionModelState, initialState as initialWorkflowModelVersionState} from '../../../../models/workflowVersion/workflow_version.model.state';
import {RouterState} from '../../../../models/model.state';
import {BehaviorSubject} from 'rxjs';
import {waitForAsync, ComponentFixture, TestBed} from '@angular/core/testing';
import {Store} from '@ngrx/store';
import {getRouterState} from '../../../../models/model.selector';
import {DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';
import {WorkflowVersionModel} from '../../../../models/workflowVersion/workflow_version.model';
import {WorkflowListComponent} from './workflow-list.component';
import {
	getWorkflowVersionCollectionCount, getWorkflowVersionCollectionModels,
} from '../../../../models/workflowVersion/workflow_version.model.selector';
import {WorkflowVersionDataFactory} from '../../../../lib/utils/factories/workflow-version-data-factory';
import {WorkflowModule} from '../workflow.module';
import {RouterLinkDirective} from '../../../../../testing/helpers/router-link-stub';
import {RouterTestingModule} from '@angular/router/testing';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

describe('Workflow List Component', () => {
	let fixture: ComponentFixture<WorkflowListComponent>;
	let workflowListComponent: WorkflowListComponent;

	let store: MockStore<{ model: WorkflowVersionModelState }>;
	let routerStore: MockStore<{router: RouterState}>;

	let workflowVersionModelState: WorkflowVersionModelState;

	let workflowVersionCountBehaviorSubject: BehaviorSubject<number>;
	let workflowVersionModelsBehaviorSubject: BehaviorSubject<WorkflowVersionModel[]>;
	let routerStateBehaviorSubject: BehaviorSubject<RouterState>;

	const collectionId = 'workflow-list-component-workflow-collection';

	const routerState: RouterState = {
		url: 'admin/behaviour/workflow',
		urls: ['course-crud'],
		params: {},
		queryParams: {},
		data: {},
	};

	const workflowVersionDataFactory = new WorkflowVersionDataFactory();

	const defaultDataSize = 30;
	const defaultData = workflowVersionDataFactory.createAll(defaultDataSize);

	const spySelectorsInStore = () => {
		// Setup the Mock Store and fake selector
		store = TestBed.inject(Store) as MockStore<{ model: WorkflowVersionModelState }>;
		routerStore = TestBed.inject(Store) as MockStore<{ router: RouterState }>;

		workflowVersionModelState = initialWorkflowModelVersionState;
		store.setState({model: workflowVersionModelState});

		// Create Behavior Subjects to trigger later
		workflowVersionCountBehaviorSubject = new BehaviorSubject(defaultData.length);
		workflowVersionModelsBehaviorSubject = new BehaviorSubject(defaultData);
		routerStateBehaviorSubject = new BehaviorSubject(routerState);

		// Create spy on select function to return value
		spyOn(store, 'select')
			.withArgs(getWorkflowVersionCollectionCount, collectionId).and.returnValue(workflowVersionCountBehaviorSubject)
			.withArgs(getWorkflowVersionCollectionModels, collectionId).and.returnValue(workflowVersionModelsBehaviorSubject)
			.withArgs(getRouterState).and.returnValue(routerStateBehaviorSubject);
	};

	// % protected region % [Add any additional variables here] off begin
	// % protected region % [Add any additional variables here] end

	beforeEach(waitForAsync (() => {
		TestBed.configureTestingModule({
			imports: [
				WorkflowModule,
				RouterTestingModule,
			],
			providers: [
				provideMockStore(),
				RouterLinkDirective,
			]
		}).compileComponents().then(() => {

			spySelectorsInStore();

			fixture = TestBed.createComponent(WorkflowListComponent);
			workflowListComponent = fixture.debugElement.componentInstance;

			// % protected region % [Add any additional logic before each test here] off begin
			// % protected region % [Add any additional logic before each test here] end
		});
	}));

	afterEach(() => {
		// Need to do this since for some reason the last component queried from the fixture will be rendered on the
		// browser
		if (fixture.nativeElement instanceof HTMLElement) {
			(fixture.nativeElement as HTMLElement).remove();
		}

		// % protected region % [Add any additional logic after each test here] off begin
		// % protected region % [Add any additional logic after each test here] end
	});

	it ('should display workflow list with standard dom structure', () => {
		fixture.detectChanges();
		const headerEI = fixture.debugElement.query(By.css('.behaviour-header'));

		expect(headerEI).not.toBeNull('Should find workflow behaviour header');

		const titleEI: DebugElement = headerEI.query(By.css('h2'));

		expect(titleEI).not.toBeNull('Could not find title');
		expect(titleEI.nativeElement.textContent).toBe('Workflows');
		// % protected region % [Add any additional logic for 'should display workflow list with standard dom structure' here] off begin
		// % protected region % [Add any additional logic for 'should display workflow list with standard dom structure' here] end
	});

	it ('should display workflow list page with create button', () => {
		fixture.detectChanges();

		const workflowButtons = fixture.debugElement.queryAll(By.css('section.workflow-block-items > a'));
		expect(workflowButtons[0].attributes.href).toBe('/create');

		for (let i = 1; i < workflowButtons.length; i++) {
			expect(workflowButtons[1].attributes.href).toContain('/edit');
		}
	});

	// % protected region % [Add any additional tests here] off begin
	// % protected region % [Add any additional tests here] end
});

// % protected region % [Add any additional describes here] off begin
// % protected region % [Add any additional describes here] end
