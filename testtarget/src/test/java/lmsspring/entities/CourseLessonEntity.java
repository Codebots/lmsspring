/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.entities;

import lombok.*;
import javax.validation.constraints.NotNull;


// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
public class CourseLessonEntity extends AbstractEntity {

	public CourseLessonEntity() {
		initialiseReferences();
	}

	private void initialiseReferences() {


		var CourseOneMany = new EntityReference();
			CourseOneMany.entityName = "Course";
			CourseOneMany.oppositeName = "Course";
			CourseOneMany.name = "CourseLessons";
			CourseOneMany.optional = true;
			CourseOneMany.type = "One";
			CourseOneMany.oppositeType = "Many";

		References.add(CourseOneMany);

		var LessonOneMany = new EntityReference();
			LessonOneMany.entityName = "Lesson";
			LessonOneMany.oppositeName = "Lesson";
			LessonOneMany.name = "CourseLessons";
			LessonOneMany.optional = true;
			LessonOneMany.type = "One";
			LessonOneMany.oppositeType = "Many";

		References.add(LessonOneMany);
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Order here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Order here] end
	private Integer order;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private CourseEntity course;

	private LessonEntity lesson;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Similar to {@link this#setCourse(CourseEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given CourseEntity to be set to this entity
	 */
	public void setCourse(@NotNull CourseEntity entity) {
		setCourse(entity, true);
	}

	/**
	 * Set or update the course in this entity with single CourseEntity.
	 *
	 * @param entity the given CourseEntity to be set or updated to course
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setCourse(@NotNull CourseEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setCourse here] off begin
		// % protected region % [Add any additional logic here before the main logic for setCourse here] end

		if (sameAsFormer(this.course, entity)) {
			return;
		}

		if (this.course != null) {
			this.course.removeCourseLessons(this, false);
		}
		this.course = entity;
		if (reverseAdd) {
			this.course.addCourseLessons(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setCourse here] off begin
		// % protected region % [Add any additional logic here after the main logic for setCourse here] end
	}

	/**
	 * Similar to {@link this#unsetCourse(boolean)} but default to true.
	 */
	public void unsetCourse() {
		this.unsetCourse(true);
	}

	/**
	 * Remove Course in this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetCourse(boolean reverse) {
		if (reverse && this.course != null) {
			this.course.removeCourseLessons(this, false);
		}
		this.course = null;
	}
	/**
	 * Similar to {@link this#setLesson(LessonEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonEntity to be set to this entity
	 */
	public void setLesson(@NotNull LessonEntity entity) {
		setLesson(entity, true);
	}

	/**
	 * Set or update the lesson in this entity with single LessonEntity.
	 *
	 * @param entity the given LessonEntity to be set or updated to lesson
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setLesson(@NotNull LessonEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setLesson here] off begin
		// % protected region % [Add any additional logic here before the main logic for setLesson here] end

		if (sameAsFormer(this.lesson, entity)) {
			return;
		}

		if (this.lesson != null) {
			this.lesson.removeCourseLessons(this, false);
		}
		this.lesson = entity;
		if (reverseAdd) {
			this.lesson.addCourseLessons(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setLesson here] off begin
		// % protected region % [Add any additional logic here after the main logic for setLesson here] end
	}

	/**
	 * Similar to {@link this#unsetLesson(boolean)} but default to true.
	 */
	public void unsetLesson() {
		this.unsetLesson(true);
	}

	/**
	 * Remove Lesson in this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetLesson(boolean reverse) {
		if (reverse && this.lesson != null) {
			this.lesson.removeCourseLessons(this, false);
		}
		this.lesson = null;
	}

	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
