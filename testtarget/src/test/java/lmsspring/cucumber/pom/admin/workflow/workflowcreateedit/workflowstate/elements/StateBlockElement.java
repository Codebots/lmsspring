/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package lmsspring.cucumber.pom.admin.workflow.workflowcreateedit.workflowstate.elements;

import com.github.webdriverextensions.WebComponent;
import lmsspring.cucumber.pom.elements.CbTextFieldElement;
import lmsspring.entities.WorkflowStateEntity;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;

import java.util.List;

@Slf4j
@ScenarioScoped
public class StateBlockElement extends WebComponent {
	@Getter
	@FindBy(className = "workflow__edit-state")
	private List<WebElement> editTransitionsButton;

	@Getter
	@FindBy(tagName = "cb-textfield")
	private CbTextFieldElement stateStepNameTextFieldElement;

	@Getter
	@FindBy(xpath = "//button[contains(@class, 'workflow__start-state')]")
	private WebElement startStateButton;

	@Getter
	@FindBy(className = "workflow__delete-state")
	private WebElement deleteStateButton;

	@Getter
	@FindBy(className = "workflow__outgoing--transition-wrapper")
	private WebElement outgoingTransitionNumberBoxWrapper;

	@Getter
	@FindBy(className = "workflow__incoming")
	private WebElement incomingTransitionNumberBoxWrapper;

	public boolean isStartState() {
		return this.startStateButton.getAttribute("class").contains("btn--solid");
	}

	public String getStateId() {
		return this.getWrappedWebElement().getAttribute("data-id");
	}

	public String getStateName() {
		return this.stateStepNameTextFieldElement.getValue();
	}

	public Integer getNumberOfOutgoingTransitions() {
		return Integer.valueOf(outgoingTransitionNumberBoxWrapper
				.getText()
				.replace("Edit", "")
				.strip()
		);
	}

	public Integer getNumberOfIncomingTransitions() {
		return Integer.valueOf(incomingTransitionNumberBoxWrapper
				.getText()
				.replace("Incoming transitions", "")
				.strip()
		);
	}

/**
	 * Compare the state as it appears on the page against the entity provided
	 * @param state The state to compare against the on the page state.
	 * @return True for a match, false otherwise
	 */
	public boolean validate(WorkflowStateEntity state) {
		return this.validateTransitionCount(state)
				&& this.validateName(state)
				&& this.validateId(state);
	}

	/**
	 * Validate the displayed number of incoming and outgoing transitions match the entity provided.
	 * @param state The state entity to compare the the state on the page against.
	 * @return True for a match, false otherwise.
	 */
	public boolean validateTransitionCount(WorkflowStateEntity state) {
		return state.getOutgoingTransitions().size() == this.getNumberOfOutgoingTransitions()
				&& state.getIncomingTransitions().size() == this.getNumberOfIncomingTransitions();
	}

	/**
	 * Compare the id found in the provided state with the state id on the page
	 * @param state The state to get the id to match from
	 * @return True for a match, false otherwise
	 */
	public boolean validateId(WorkflowStateEntity state) {
		return state.getId().toString().equals(this.getStateId());
	}

	/**
	 * Compare the name of the state step against entity provided.
	 * @param state The state to compare against
	 * @return True for a match, false otherwise.
	 */
	public boolean validateName(WorkflowStateEntity state) {
		return state.getStepName().equals(this.getStateName());
	}

	// % protected region % [Add custom class methods here] off begin
	// % protected region % [Add custom class methods here] end
}
