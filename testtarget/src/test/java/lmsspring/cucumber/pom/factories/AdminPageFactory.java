/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.cucumber.pom.factories;

import lmsspring.cucumber.pom.pages.admin.crud.list.*;
import lmsspring.cucumber.pom.pages.admin.crud.edit.*;

import com.google.inject.Inject;
import cucumber.runtime.java.guice.ScenarioScoped;
import java.util.Properties;
import lombok.*;
import org.openqa.selenium.WebDriver;
import lmsspring.cucumber.pom.pages.admin.crud.edit.CrudEdit;

@ScenarioScoped
public class AdminPageFactory {

	@Inject
	protected WebDriver webDriver;

	@Inject
	protected Properties properties;

	// Crud List Page in Admin Session
	@Inject
	AdminUsersAdministratorCrudListPage adminUsersAdministratorCrudListPage;
	@Inject
	AdminEntitiesApplicationLocaleCrudListPage adminEntitiesApplicationLocaleCrudListPage;
	@Inject
	AdminEntitiesArticleCrudListPage adminEntitiesArticleCrudListPage;
	@Inject
	AdminEntitiesBookCrudListPage adminEntitiesBookCrudListPage;
	@Inject
	AdminUsersCoreUserCrudListPage adminUsersCoreUserCrudListPage;
	@Inject
	AdminEntitiesCourseCrudListPage adminEntitiesCourseCrudListPage;
	@Inject
	AdminEntitiesCourseCategoryCrudListPage adminEntitiesCourseCategoryCrudListPage;
	@Inject
	AdminEntitiesCourseLessonCrudListPage adminEntitiesCourseLessonCrudListPage;
	@Inject
	AdminEntitiesLessonCrudListPage adminEntitiesLessonCrudListPage;
	@Inject
	AdminEntitiesTagCrudListPage adminEntitiesTagCrudListPage;
	@Inject
	AdminEntitiesLessonFormSubmissionCrudListPage adminEntitiesLessonFormSubmissionCrudListPage;
	@Inject
	AdminEntitiesLessonFormVersionCrudListPage adminEntitiesLessonFormVersionCrudListPage;
	@Inject
	AdminEntitiesLessonFormTileCrudListPage adminEntitiesLessonFormTileCrudListPage;

	// Crud Edit Page in Admin Session
	@Inject
	AdminUsersAdministratorCrudEditPage adminUsersAdministratorCrudEditPage;
	@Inject
	AdminEntitiesApplicationLocaleCrudEditPage adminEntitiesApplicationLocaleCrudEditPage;
	@Inject
	AdminEntitiesArticleCrudEditPage adminEntitiesArticleCrudEditPage;
	@Inject
	AdminEntitiesBookCrudEditPage adminEntitiesBookCrudEditPage;
	@Inject
	AdminUsersCoreUserCrudEditPage adminUsersCoreUserCrudEditPage;
	@Inject
	AdminEntitiesCourseCrudEditPage adminEntitiesCourseCrudEditPage;
	@Inject
	AdminEntitiesCourseCategoryCrudEditPage adminEntitiesCourseCategoryCrudEditPage;
	@Inject
	AdminEntitiesCourseLessonCrudEditPage adminEntitiesCourseLessonCrudEditPage;
	@Inject
	AdminEntitiesLessonCrudEditPage adminEntitiesLessonCrudEditPage;
	@Inject
	AdminEntitiesTagCrudEditPage adminEntitiesTagCrudEditPage;
	@Inject
	AdminEntitiesLessonFormSubmissionCrudEditPage adminEntitiesLessonFormSubmissionCrudEditPage;
	@Inject
	AdminEntitiesLessonFormVersionCrudEditPage adminEntitiesLessonFormVersionCrudEditPage;
	@Inject
	AdminEntitiesLessonFormTileCrudEditPage adminEntitiesLessonFormTileCrudEditPage;

	/**
	 * Create Crud Edit List based on the entity name
 	 * @param name Name of Entity. Should be in pascal case
 	*/
	public CrudList createCrudListPage(String name) throws Exception {
		switch (name) {

			case "Administrator":
				return adminUsersAdministratorCrudListPage;
			case "ApplicationLocale":
				return adminEntitiesApplicationLocaleCrudListPage;
			case "Article":
				return adminEntitiesArticleCrudListPage;
			case "Book":
				return adminEntitiesBookCrudListPage;
			case "CoreUser":
				return adminUsersCoreUserCrudListPage;
			case "Course":
				return adminEntitiesCourseCrudListPage;
			case "CourseCategory":
				return adminEntitiesCourseCategoryCrudListPage;
			case "CourseLesson":
				return adminEntitiesCourseLessonCrudListPage;
			case "Lesson":
				return adminEntitiesLessonCrudListPage;
			case "Tag":
				return adminEntitiesTagCrudListPage;
			case "LessonFormSubmission":
				return adminEntitiesLessonFormSubmissionCrudListPage;
			case "LessonFormVersion":
				return adminEntitiesLessonFormVersionCrudListPage;
			case "LessonFormTile":
				return adminEntitiesLessonFormTileCrudListPage;
			default :
				throw new Exception(String.format("Unexpected Crud list Page: %s", name));
		}
	}

	/**
	 * Create Crud Edit POM based on the entity name
 	 * @param name Name of Entity. Should be in pascal case
 	*/

	public CrudEdit createCrudEditPage(String name) throws Exception {
		switch (name) {

			case "Administrator":
				return adminUsersAdministratorCrudEditPage;
			case "ApplicationLocale":
				return adminEntitiesApplicationLocaleCrudEditPage;
			case "Article":
				return adminEntitiesArticleCrudEditPage;
			case "Book":
				return adminEntitiesBookCrudEditPage;
			case "CoreUser":
				return adminUsersCoreUserCrudEditPage;
			case "Course":
				return adminEntitiesCourseCrudEditPage;
			case "CourseCategory":
				return adminEntitiesCourseCategoryCrudEditPage;
			case "CourseLesson":
				return adminEntitiesCourseLessonCrudEditPage;
			case "Lesson":
				return adminEntitiesLessonCrudEditPage;
			case "Tag":
				return adminEntitiesTagCrudEditPage;
			case "LessonFormSubmission":
				return adminEntitiesLessonFormSubmissionCrudEditPage;
			case "LessonFormVersion":
				return adminEntitiesLessonFormVersionCrudEditPage;
			case "LessonFormTile":
				return adminEntitiesLessonFormTileCrudEditPage;
			default :
				throw new Exception(String.format("Unexpected Crud list Page: %s", name));
		}
	}
}
