/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Component, EventEmitter, Input, Output, HostBinding} from '@angular/core';
import {FormMode, FormQuestionData, FormSlideData} from '../../../behaviours/form/form-interfaces';
import {AbstractComponent} from '../../../components/abstract.component';
import {TextfieldType} from '../../../components/textfield/textfield.component';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

@Component({
	selector: 'section[cb-form-builder]',
	templateUrl: './form-builder.component.html',
	// % protected region % [Add any additional component configurations here] off begin
	// % protected region % [Add any additional component configurations here] end
})
export class FormBuilderComponent extends AbstractComponent {
	readonly TextfieldType = TextfieldType;

	/**
	 * Default host class.
	 */
	@HostBinding('class')
	get formBuilderCSSClass() {
		const defaultClasses = [
			this.className,
			// % protected region % [Add any additional host classes here] off begin
			// % protected region % [Add any additional host classes here] end
		];

		if (this.formMode === 'response') {
			defaultClasses.push('');
		}

		if (this.formMode !== 'build') {
			defaultClasses.push('form-submission');
		} else {
			defaultClasses.push('form-builder');
		}

		if (this.formMode === 'report') {
			defaultClasses.push('form-sbumission__preview');
		}

		return defaultClasses.join(' ');
	}

	@HostBinding('attr.aria-label')
	get formBuilderCSSAria() {
		return [
			'form-builder',
			// % protected region % [Add any additional aria label here] off begin
			// % protected region % [Add any additional aria label here] end
		].join(' ');
	}

	/**
	 * Slide data to be passed down to the slides themselves.
	 */
	@Input()
	slidesData: FormSlideData[];

	@Input()
	formMode: FormMode;

	/**
	 * Emitter for when a user wants to edit a question.
	 */
	@Output('editQuestion')
	editQuestionEmitter = new EventEmitter<FormQuestionData>();

	/**
	 * Emitter for when a user wants to duplicate a question.
	 */
	@Output('duplicateQuestion')
	duplicateQuestionEmitter = new EventEmitter<FormQuestionData>();

	// % protected region % [Add any additional class properties here] off begin
	// % protected region % [Add any additional class properties here] end

	/**
	 * Triggered on when the user wants to edit a question.
	 */
	onEditQuestion(questionData: FormQuestionData) {
		// % protected region % [Add any additional logic for onEditQuestion before the main body here] off begin
		// % protected region % [Add any additional logic for onEditQuestion before the main body here] end

		this.editQuestionEmitter.emit(questionData);

		// % protected region % [Add any additional logic for onEditQuestion after the main body here] off begin
		// % protected region % [Add any additional logic for onEditQuestion after the main body here] end
	}

	/**
	 * Triggered on when the user wants to duplicate a question.
	 */
	onDuplicateQuestion(questionData: FormQuestionData) {
		// % protected region % [Add any additional logic for onEditQuestion before the main body here] off begin
		// % protected region % [Add any additional logic for onEditQuestion before the main body here] end

		this.duplicateQuestionEmitter.emit(questionData);

		// % protected region % [Add any additional logic for onEditQuestion after the main body here] off begin
		// % protected region % [Add any additional logic for onEditQuestion after the main body here] end
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
