/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Component, OnInit} from '@angular/core';
import {
	IconPosition,
	ButtonAccentColour,
	ButtonSize,
	ButtonStyle
} from '../../../../lib/components/button/button.component';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';
import {RouterState} from '../../../../models/model.state';
import {WorkflowModelState} from '../../../../models/workflow/workflow.model.state';
import * as workflowModelVersionActions from '../../../../models/workflowVersion/workflow_version.model.action';
import {getWorkflowVersionCollectionModels} from '../../../../models/workflowVersion/workflow_version.model.selector';
import {WorkflowVersionModel} from '../../../../models/workflowVersion/workflow_version.model';
import {getAllEntitiesWithWorkflowBehaviour} from '../../../../lib/utils/workflow-utils';
import {Expand} from '../../../../lib/services/http/interfaces';
import {AbstractComponent} from '../../../../lib/components/abstract.component';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

@Component({
	selector: 'cb-workflow-list',
	templateUrl: './workflow-list.component.html',
	styleUrls: [
		'./workflow-list.component.scss',
		// % protected region % [Add any additional styles here] off begin
		// % protected region % [Add any additional styles here] end
	]
})
export class WorkflowListComponent extends AbstractComponent implements OnInit {
	IconPosition = IconPosition;

	buttonStyle = ButtonStyle;

	buttonAccentColour = ButtonAccentColour;

	buttonSize = ButtonSize;

	workflowModelVersions: Observable<WorkflowVersionModel[]>;

	workflowCollectionId: string = 'workflow-list-component-workflow-collection';

	allAssociationEntities = getAllEntitiesWithWorkflowBehaviour();

	get defaultExpands(): Expand[] {
		let expands: Expand[] = Object.entries(WorkflowVersionModel.getRelations()).map(
			([key, entry]): Expand => {
				return {
					name: key,
					fields: ['id']
				};
			}
		);
		return expands;
	}

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	constructor(
		private readonly store: Store<{ model: WorkflowModelState }>,
		private readonly routerStore: Store<{ router: RouterState }>,
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
	) {
		super();

		this.store.dispatch(new workflowModelVersionActions.WorkflowVersionAction(
			workflowModelVersionActions.WorkflowVersionModelActionTypes.INITIALISE_WORKFLOW_VERSION_COLLECTION_STATE,
			{
				collectionId: this.workflowCollectionId
			}
		));

		this.workflowModelVersions = this.store.select(getWorkflowVersionCollectionModels, this.workflowCollectionId);

		// % protected region % [Add any additional logic for constructor here] off begin
		// % protected region % [Add any additional logic for constructor here] end

	}

	/**
	 * @inheritDoc
	 */
	ngOnInit(): void {
		this.store.dispatch(new workflowModelVersionActions.WorkflowVersionAction(
			workflowModelVersionActions.WorkflowVersionModelActionTypes.FETCH_WORKFLOW_VERSION_WITH_QUERY,
			{
				collectionId: this.workflowCollectionId,
				queryParams: {
					pageIndex: 0,
					pageSize: 100,
					expands: this.defaultExpands
				}
			}
		));

		// % protected region % [Add any additional logic for ngOnInit here] off begin
		// % protected region % [Add any additional logic for ngOnInit here] end
	}

	/**
	 * Get association entities of a workflow and transfer to  a string
	 */
	getAssociatedEntities(workflowVersion: WorkflowVersionModel): string {
		return this.allAssociationEntities.filter(workflowOption => !!workflowVersion[workflowOption.fieldName])
			.map(workflowOption => workflowOption.name)
			.join(',');
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
