/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Component} from '@angular/core';
import {TextfieldType} from '../../../../components/textfield/textfield.component';
import {FormCustomQuestionComponent} from '../form-custom-question/form-custom-question.component';
import {FormControl, FormGroup} from '@angular/forms';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * Question component with checkbox as input.
 */
@Component({
	selector: 'div[cb-form-radio-buttons-question]',
	templateUrl: './form-radio-buttons-question.component.html',
	// % protected region % [Add any additional component configurations here] off begin
	// % protected region % [Add any additional component configurations here] end
})
export class FormRadioButtonsQuestionComponent extends FormCustomQuestionComponent {
	formGroup: FormGroup;

	// % protected region % [Add any additional class properties here] off begin
	// % protected region % [Add any additional class properties here] end

	parseData() {
		super.parseData();
		// % protected region % [Add any additional logic for parseData here] off begin
		// % protected region % [Add any additional logic for parseData here] end
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
