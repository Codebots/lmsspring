/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {FormGroup, Validators} from '@angular/forms';
import {Group, AbstractModel, ModelProperty, ModelPropertyType, ModelRelation, ModelRelationType} from '../../lib/models/abstract.model';
import {ArticleModel} from '../article/article.model';
import {WorkflowTransitionModel} from '../workflowTransition/workflow_transition.model';
import {WorkflowVersionModel} from '../workflowVersion/workflow_version.model';
import * as _ from 'lodash';
import {QueryOperation, Where} from '../../lib/services/http/interfaces';
import {ElementType} from '../../lib/components/abstract.input.component';
import { CustomValidators } from 'src/app/lib/utils/validators/custom-validators';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * POJO model class used to store information related to the entity.
 */
export class WorkflowStateModel extends AbstractModel {
	/**
	 * The fields which are set as searchable in the entity model
	 * The fields could be used in search in the server side
	 * The fields would be by default used as search in the crud tile.
	 * You could also use this in other tiles for searching.
	 */
	static searchFields: string[] = [
		// % protected region % [Add any additional searchable field names here] off begin
		// % protected region % [Add any additional searchable field names here] end
	];

	/**
	 * Attributes to be shown in value to display
	 */
	static displayAttributes: string[] = [
		// % protected region % [Change displayAttributes here if needed] off begin
		'displayIndex',
		// % protected region % [Change displayAttributes here if needed] end
	];

	static modelPropGroups: { [s: string]: Group } = {
		// % protected region % [Add groups for the entity here] off begin
		// % protected region % [Add groups for the entity here] end
	};

	readonly className = 'WorkflowStateModel';

	/**
	 * Default value to be displayed in dropdown etc
	 */
	get valueToDisplay(): string {
		// % protected region % [Change displayName here if needed] off begin
		return WorkflowStateModel.displayAttributes.map((attr) => this[attr]).join(' ');
		// % protected region % [Change displayName here if needed] end
	}

	/**
	 * .
	 */
	displayIndex: number;

	/**
	 * The name of the state.
	 */
	stepName: string;

	/**
	 * .
	 */
	stateDescription: string;

	/**
	 * .
	 */
	isStartState: boolean = false;

	workflowVersionId: string;

	outgoingTransitionsIds: string[] = [];

	incomingTransitionsIds: string[] = [];

	articleIds: string[] = [];

	modelPropGroups: { [s: string]: Group } = WorkflowStateModel.modelPropGroups;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	static getProps(): ModelProperty[] {
		return super.getProps().concat([
			{
				name: 'displayIndex',
				// % protected region % [Set displayName for Display Index here] off begin
				displayName: 'Display Index',
				// % protected region % [Set displayName for Display Index here] end
				type: ModelPropertyType.NUMBER,
				// % protected region % [Set display element type for Display Index here] off begin
				elementType: ElementType.NUMBER,
				// % protected region % [Set display element type for Display Index here] end
				// % protected region % [Set isSensitive for Display Index here] off begin
				isSensitive: false,
				// % protected region % [Set isSensitive for Display Index here] end
				// % protected region % [Set readonly for Display Index here] off begin
				readOnly: false,
				// % protected region % [Set readonly for Display Index here] end
				validators: [
					// % protected region % [Add other validators for Display Index here] off begin
					// % protected region % [Add other validators for Display Index here] end
				],
				// % protected region % [Add any additional model attribute properties for Display Index here] off begin
				// % protected region % [Add any additional model attribute properties for Display Index here] end
			},
			{
				name: 'stepName',
				// % protected region % [Set displayName for Step Name here] off begin
				displayName: 'Step Name',
				// % protected region % [Set displayName for Step Name here] end
				type: ModelPropertyType.STRING,
				// % protected region % [Set display element type for Step Name here] off begin
				elementType: ElementType.INPUT,
				// % protected region % [Set display element type for Step Name here] end
				// % protected region % [Set isSensitive for Step Name here] off begin
				isSensitive: false,
				// % protected region % [Set isSensitive for Step Name here] end
				// % protected region % [Set readonly for Step Name here] off begin
				readOnly: false,
				// % protected region % [Set readonly for Step Name here] end
				validators: [
					Validators.required,
					// % protected region % [Add other validators for Step Name here] off begin
					// % protected region % [Add other validators for Step Name here] end
				],
				// % protected region % [Add any additional model attribute properties for Step Name here] off begin
				// % protected region % [Add any additional model attribute properties for Step Name here] end
			},
			{
				name: 'stateDescription',
				// % protected region % [Set displayName for State Description here] off begin
				displayName: 'State Description',
				// % protected region % [Set displayName for State Description here] end
				type: ModelPropertyType.STRING,
				// % protected region % [Set display element type for State Description here] off begin
				elementType: ElementType.INPUT,
				// % protected region % [Set display element type for State Description here] end
				// % protected region % [Set isSensitive for State Description here] off begin
				isSensitive: false,
				// % protected region % [Set isSensitive for State Description here] end
				// % protected region % [Set readonly for State Description here] off begin
				readOnly: false,
				// % protected region % [Set readonly for State Description here] end
				validators: [
					// % protected region % [Add other validators for State Description here] off begin
					// % protected region % [Add other validators for State Description here] end
				],
				// % protected region % [Add any additional model attribute properties for State Description here] off begin
				// % protected region % [Add any additional model attribute properties for State Description here] end
			},
			{
				name: 'isStartState',
				// % protected region % [Set displayName for Is Start State here] off begin
				displayName: 'Is Start State',
				// % protected region % [Set displayName for Is Start State here] end
				type: ModelPropertyType.BOOLEAN,
				// % protected region % [Set display element type for Is Start State here] off begin
				elementType: ElementType.CHECKBOX,
				// % protected region % [Set display element type for Is Start State here] end
				// % protected region % [Set isSensitive for Is Start State here] off begin
				isSensitive: false,
				// % protected region % [Set isSensitive for Is Start State here] end
				// % protected region % [Set readonly for Is Start State here] off begin
				readOnly: false,
				// % protected region % [Set readonly for Is Start State here] end
				validators: [
					// % protected region % [Add other validators for Is Start State here] off begin
					// % protected region % [Add other validators for Is Start State here] end
				],
				// % protected region % [Add any additional model attribute properties for Is Start State here] off begin
				// % protected region % [Add any additional model attribute properties for Is Start State here] end
			},
			// % protected region % [Add any additional class field names here] off begin
			// % protected region % [Add any additional class field names here] end
		]);
	}

	/**
	 * The relations of the entity
	 */
	static getRelations(): { [name: string]: ModelRelation } {
		return {
			...super.getRelations(),
			outgoingTransitions: {
				type: ModelRelationType.MANY,
				name: 'outgoingTransitionsIds',
				// % protected region % [Customise your 1-1 or 1-M label for Outgoing Transitions here] off begin
				label: 'Outgoing Transitions',
				// % protected region % [Customise your 1-1 or 1-M label for Outgoing Transitions here] end
				// % protected region % [Customise your display name for Outgoing Transitions here] off begin
				displayName: 'transitionName',
				// % protected region % [Customise your display name for Outgoing Transitions here] end
				validators: [
					// % protected region % [Add other validators for Outgoing Transitions here] off begin
					// % protected region % [Add other validators for Outgoing Transitions here] end
				],
				// % protected region % [Add any additional field for relation Outgoing Transitions here] off begin
				// % protected region % [Add any additional field for relation Outgoing Transitions here] end
			},
			incomingTransitions: {
				type: ModelRelationType.MANY,
				name: 'incomingTransitionsIds',
				// % protected region % [Customise your 1-1 or 1-M label for Incoming Transitions here] off begin
				label: 'Incoming Transitions',
				// % protected region % [Customise your 1-1 or 1-M label for Incoming Transitions here] end
				// % protected region % [Customise your display name for Incoming Transitions here] off begin
				displayName: 'transitionName',
				// % protected region % [Customise your display name for Incoming Transitions here] end
				validators: [
					// % protected region % [Add other validators for Incoming Transitions here] off begin
					// % protected region % [Add other validators for Incoming Transitions here] end
				],
				// % protected region % [Add any additional field for relation Incoming Transitions here] off begin
				// % protected region % [Add any additional field for relation Incoming Transitions here] end
			},
			workflowVersion: {
				type: ModelRelationType.ONE,
				name: 'workflowVersionId',
				// % protected region % [Customise your label for Workflow Version here] off begin
				label: 'Workflow Version',
				// % protected region % [Customise your label for Workflow Version here] end
				// % protected region % [Customise your display name for Workflow Version here] off begin
				// TODO change implementation to use OrderBy or create new metamodel property DisplayBy
				displayName: 'workflowName',
				// % protected region % [Customise your display name for Workflow Version here] end
				validators: [
					Validators.required
					// % protected region % [Add other validators for Workflow Version here] off begin
					// % protected region % [Add other validators for Workflow Version here] end
				],
				// % protected region % [Add any additional field for relation Workflow Version here] off begin
				// % protected region % [Add any additional field for relation Workflow Version here] end
			},
		};
	}

	/**
	 * Convert the form group to the query conditions
	 */
	static convertFilterToCondition(formGroup: FormGroup): Where[][] {
		let conditions: Where[][] = [];

		// % protected region % [Overide the default convertFilterToCondition here] off begin
		Object.keys(formGroup.value).forEach((key) => {
			switch (key) {
				case 'created':
					const created = formGroup.value[key];
					// is the range of date
					if (created instanceof Array) {
						conditions.push([
							{
								path: key,
								operation: QueryOperation.GREATER_THAN_OR_EQUAL,
								value: created[0]
							}
						]);
						conditions.push([
							{
								path: key,
								operation: QueryOperation.LESS_THAN_OR_EQUAL,
								value: created[1]
							}
						]);
					}
			}
		});
		// % protected region % [Overide the default convertFilterToCondition here] end


		return conditions;
	}

	/**
	 * Convert a nested JSON object into an array of flatten objects.
	 */
	static deepParse(data: string | { [K in keyof WorkflowStateModel]?: WorkflowStateModel[K] }, currentModel?): AbstractModel[] {
		if (currentModel == null) {
			currentModel = new WorkflowStateModel(data);
		}

		let returned: AbstractModel[] = [currentModel];
		const json = typeof data === 'string' ? JSON.parse(data) : data;

		// Incoming one to many
		if (json.workflowVersion) {
			currentModel.workflowVersionId = json.workflowVersion.id;
			returned = _.union(returned, WorkflowVersionModel.deepParse(json.workflowVersion));
		}

		// Outgoing one to many
		if (json.outgoingTransitions) {
			currentModel.outgoingTransitionsIds = json.outgoingTransitions.map(model => model.id);
			returned = _.union(returned, _.flatten(json.outgoingTransitions.map(model => WorkflowTransitionModel.deepParse(model))));
		}
		// Outgoing one to many
		if (json.incomingTransitions) {
			currentModel.incomingTransitionsIds = json.incomingTransitions.map(model => model.id);
			returned = _.union(returned, _.flatten(json.incomingTransitions.map(model => WorkflowTransitionModel.deepParse(model))));
		}

		// % protected region % [Customise your deep parse before return here] off begin
		// % protected region % [Customise your deep parse before return here] end

		return returned;
	}

	/**
	 * @example
	 *
	 * `let workflowStateModel = new WorkflowStateModel(data);`
	 *
	 * @param data The input data to be initialised as the WorkflowStateModel,
	 *    it is expected as a JSON string or as a nullable WorkflowStateModel.
	 */
	constructor(data?: string | Partial<WorkflowStateModel>) {
		super(data);

		if (data) {
			const json = typeof data === 'string'
				? JSON.parse(data) as Partial<WorkflowStateModel>
				: data;

			this.displayIndex = json.displayIndex;
			this.stepName = json.stepName;
			this.stateDescription = json.stateDescription;
			this.isStartState = json.isStartState;
			this.workflowVersionId = json.workflowVersionId;
			this.outgoingTransitionsIds = json.outgoingTransitionsIds;
			this.incomingTransitionsIds = json.incomingTransitionsIds;
			// % protected region % [Add any additional logic here after set the data] off begin
			// % protected region % [Add any additional logic here after set the data] end
		}

		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}

	toJSON() {
		return {
			...super.toJSON(),
			displayIndex: this.displayIndex,
			stepName: this.stepName,
			stateDescription: this.stateDescription,
			isStartState: this.isStartState,
			workflowVersionId: this.workflowVersionId,
			outgoingTransitionsIds: this.outgoingTransitionsIds,
			incomingTransitionsIds: this.incomingTransitionsIds,
			// % protected region % [Add any additional logic here to json] off begin
			// % protected region % [Add any additional logic here to json] end
		};
	}

	getPropDisplayNames(): { [s: string]: ModelProperty } {
		const returned = {};
		WorkflowStateModel.getProps().map(prop => returned[prop.name] = prop);
		return returned;
	}

	applyUpdates(updates: any): WorkflowStateModel {
		let newModelJson = this.toJSON();

		if (updates.displayIndex) {
			newModelJson.displayIndex = updates.displayIndex;
		}

		if (updates.stepName) {
			newModelJson.stepName = updates.stepName;
		}

		if (updates.stateDescription) {
			newModelJson.stateDescription = updates.stateDescription;
		}

		if (updates.isStartState) {
			newModelJson.isStartState = updates.isStartState;
		}

		if (updates.workflowVersionId) {
			newModelJson.workflowVersionId = updates.workflowVersionId;
		}

		if (updates.outgoingTransitionsIds) {
			newModelJson.outgoingTransitionsIds = updates.outgoingTransitionsIds;
		}

		if (updates.incomingTransitionsIds) {
			newModelJson.incomingTransitionsIds = updates.incomingTransitionsIds;
		}

		return new WorkflowStateModel(newModelJson);
	}

	/**
	 * @inheritDoc
	 */
	difference(other: AbstractModel): any {
		if (!(other instanceof WorkflowStateModel)) {
			return {};
		}

		const diff = {};

		for (const key of _.keys(this)) {
			const thisValue = this[key];
			const otherValue = other[key];

			// Handle dates differently
			if (thisValue instanceof Date) {
				let thisDate = (thisValue) ? thisValue.getTime() : null;
				let otherDate = (otherValue) ? otherValue.getTime() : null;

				if (thisDate !== otherDate) {
					diff[key] = thisValue;
				}
			} else if (thisValue !== otherValue) {
				diff[key] = thisValue;
			}
		}

		return _.omit(diff, [
			'created',
			'modified',
			'workflowVersionIds',
			'outgoingTransitionsIds',
			'incomingTransitionsIds',
			'articleIds',
			// % protected region % [Add any other fields to omit here] off begin
			// % protected region % [Add any other fields to omit here] end
		]);
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
