/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.entities;

import lombok.*;
import javax.validation.constraints.NotNull;


// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
public class LessonFormTileEntity extends AbstractEntity {

	public LessonFormTileEntity() {
		initialiseReferences();
	}

	private void initialiseReferences() {


		var FormOneMany = new EntityReference();
			FormOneMany.entityName = "Lesson";
			FormOneMany.oppositeName = "Form";
			FormOneMany.name = "FormTiles";
			FormOneMany.optional = false;
			FormOneMany.type = "One";
			FormOneMany.oppositeType = "Many";

		References.add(FormOneMany);
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Form Tile Name here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Form Tile Name here] end
	private String formTileName;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private LessonEntity form;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Similar to {@link this#setForm(LessonEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given LessonEntity to be set to this entity
	 */
	public void setForm(@NotNull LessonEntity entity) {
		setForm(entity, true);
	}

	/**
	 * Set or update the form in this entity with single LessonEntity.
	 *
	 * @param entity the given LessonEntity to be set or updated to form
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setForm(@NotNull LessonEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setForm here] off begin
		// % protected region % [Add any additional logic here before the main logic for setForm here] end

		if (sameAsFormer(this.form, entity)) {
			return;
		}

		if (this.form != null) {
			this.form.removeFormTiles(this, false);
		}
		this.form = entity;
		if (reverseAdd) {
			this.form.addFormTiles(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setLesson here] off begin
		// % protected region % [Add any additional logic here after the main logic for setLesson here] end
	}

	/**
	 * Similar to {@link this#unsetForm(boolean)} but default to true.
	 */
	public void unsetForm() {
		this.unsetForm(true);
	}

	/**
	 * Remove Form in this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetForm(boolean reverse) {
		if (reverse && this.form != null) {
			this.form.removeFormTiles(this, false);
		}
		this.form = null;
	}

	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
