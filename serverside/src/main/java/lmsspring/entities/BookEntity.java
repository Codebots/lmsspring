/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package lmsspring.entities;

import lmsspring.dtos.BookEntityDto;
import lmsspring.entities.listeners.BookEntityListener;
import lmsspring.serializers.BookSerializer;
import lmsspring.lib.validators.ValidatorPatterns;
import lmsspring.services.utils.converters.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.opencsv.bean.CsvIgnore;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;
import org.springframework.lang.Nullable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.hibernate.envers.Audited;

import java.util.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Optional;
import java.util.UUID;

// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Entity
// % protected region % [Override the auditing annotation or add additional annotations here] off begin
@Audited
// % protected region % [Override the auditing annotation or add additional annotations here] end
@ExcludeSuperclassListeners
@EntityListeners({BookEntityListener.class, AuditingEntityListener.class})
@JsonSerialize(using = BookSerializer.class)
@Table(
	uniqueConstraints = {
		@UniqueConstraint(columnNames = {"name"}, name = "name"),
	}
)
public class BookEntity extends AbstractEntity {

	/**
	 * Takes a BookEntityDto and converts it into a BookEntity.
	 * Primarily used when endpoints are invoked, as they take DTOs as input, which need to be converted to the entity type
	 *
	 * @param bookEntityDto
	 */
	public BookEntity(BookEntityDto bookEntityDto) {
		// Need to account for potential that any field is empty.  This will be run before any server validation and
		// endpoints will most likely not be subject to clientside validation

		if (bookEntityDto.getId() != null) {
			this.setId(bookEntityDto.getId());
		}

		if (bookEntityDto.getName() != null) {
			this.setName(bookEntityDto.getName());
		}

		if (bookEntityDto.getSummary() != null) {
			this.setSummary(bookEntityDto.getSummary());
		}

		if (bookEntityDto.getArticles() != null) {
			this.setArticles(bookEntityDto.getArticles());
		}

		// % protected region % [Add any additional DTO constructor logic here] off begin
		// % protected region % [Add any additional DTO constructor logic here] end
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Name here] off begin
	@CsvBindByName(column = "NAME", required = true)
	@NotNull(message = "Name must not be empty")
	@Column(name = "name")
	@ApiModelProperty(notes = "The Name of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Name here] end
	private String name;

	// % protected region % [Modify attribute annotation for Summary here] off begin
	@CsvBindByName(column = "SUMMARY", required = false)
	@Nullable
	@Column(name = "summary")
	@ApiModelProperty(notes = "The Summary of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Summary here] end
	private String summary;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Update the annotation for Articles here] off begin
	@ApiModelProperty(notes = "The Article entities that are related to this entity.")
	@OneToMany(mappedBy = "book", cascade = {CascadeType.MERGE}, fetch = FetchType.LAZY)
	@CsvIgnore
	// % protected region % [Update the annotation for Articles here] end
	private Set<ArticleEntity> articles = new HashSet<>();

	// % protected region % [Update the annotation for articlesIds here] off begin
	@Transient
	@CsvCustomBindByName(column = "ARTICLES_IDS", converter = UUIDHashSetConverter.class)
	// % protected region % [Update the annotation for articlesIds here] end
	private Set<UUID> articlesIds = new HashSet<>();

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
	 * Similar to {@link this#addArticles(ArticleEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given ArticleEntity to be added to this entity
	 */
	public void addArticles(@NotNull ArticleEntity entity) {
		addArticles(entity, true);
	}

	/**
	 * Add a new ArticleEntity to articles in this entity.
	 *
	 * @param entity the given ArticleEntity  to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addArticles(@NonNull ArticleEntity entity, boolean reverseAdd) {
		if (!this.articles.contains(entity)) {
			articles.add(entity);
			if (reverseAdd) {
				entity.setBook(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addArticles(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of ArticleEntity to be added to this entity
	 */
	public void addArticles(@NotNull Collection<ArticleEntity> entities) {
		addArticles(entities, true);
	}

	/**
	 * Add a new collection of ArticleEntity to Articles in this entity.
	 *
	 * @param entities the given collection of ArticleEntity to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addArticles(@NonNull Collection<ArticleEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> addArticles(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeArticles(ArticleEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given ArticleEntity to be set to this entity
	 */
	public void removeArticles(@NotNull ArticleEntity entity) {
		this.removeArticles(entity, true);
	}

	/**
	 * Remove the given ArticleEntity from this entity.
	 *
	 * @param entity the given ArticleEntity to be removed from this entity
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeArticles(@NotNull ArticleEntity entity, boolean reverse) {
		if (reverse) {
			entity.unsetBook(false);
		}
		this.articles.remove(entity);
	}

	/**
	 * Similar to {@link this#removeArticles(Collection, boolean)} but
	 * default to true for reverse remove.
	 *
	 * @param entities the given collection of ArticleEntity to be removed to this entity
	 */
	public void removeArticles(@NotNull Collection<ArticleEntity> entities) {
		this.removeArticles(entities, true);
	}

	/**
	 * Remove the given collection of ArticleEntity from  to this entity.
	 *
	 * @param entities the given collection of ArticleEntity to be removed to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void removeArticles(@NonNull Collection<ArticleEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.removeArticles(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#setArticles(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of ArticleEntity to be set to this entity
	 */
	public void setArticles(@NotNull Collection<ArticleEntity> entities) {
		setArticles(entities, true);
	}

	/**
	 * Replace the current entities in Articles with the given ones.
	 *
	 * @param entities the given collection of ArticleEntity to replace the old ones
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setArticles(@NotNull Collection<ArticleEntity> entities, boolean reverseAdd) {

		this.unsetArticles();
		this.articles = new HashSet<>(entities);
		if (reverseAdd) {
			this.articles.forEach(articlesEntity -> articlesEntity.setBook(this, false));
		}
	}

	/**
	 * Similar to {@link this#unsetArticles(boolean)} but
	 * default to true for reverse unset
	 */
	public void unsetArticles() {
		this.unsetArticles(true);
	}

	/**
	 * Remove all the entities in Articles from this entity.
	 * @param doReverse whether this entity should be removed from the given entities
	 */
	public void unsetArticles(boolean doReverse) {
		if (doReverse) {
			this.articles.forEach(articlesEntity -> articlesEntity.unsetBook(false));
		}
		this.articles.clear();
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	/**
	 * The CSV annotations used to generate and import CSV files require headers on the abstract entity attributes
	 * (id, created, modified, created by, modified by).  These should not be present in imports, as this information is
	 * added by the server when saving entities.  Therefore a string containing the example csv headers is required
	 *
	 * @return the headers recommended in the CSV import file in CSV format
	 */
	public static String getExampleCsvHeader() {

		// % protected region % [Modify the headers in the CSV file here] off begin
		return "NAME,SUMMARY,ARTICLES_IDS,ID";
		// % protected region % [Modify the headers in the CSV file here] end
	}

	public void addRelationEntitiesToIdSet() {
		// % protected region % [Add any additional logic for entity relations here] off begin
		for (ArticleEntity articles: this.articles) {
			this.articlesIds.add(articles.getId());
		}

		// % protected region % [Add any additional logic for entity relations here] end
	}
	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
