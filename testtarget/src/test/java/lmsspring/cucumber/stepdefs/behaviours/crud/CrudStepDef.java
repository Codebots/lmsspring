/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.cucumber.stepdefs.behaviours.crud;

import lmsspring.cucumber.pom.enums.VisibilityEnum;
import lmsspring.cucumber.pom.pages.admin.crud.list.CrudList;
import lmsspring.entities.AbstractEntity;
import lmsspring.cucumber.utils.WaitUtils;
import lmsspring.cucumber.utils.WebElementUtils;
import lmsspring.entities.EntityReference;
import lmsspring.cucumber.stepdefs.AbstractStepDef;
import lmsspring.inject.factories.*;
import org.testng.Assert;
import com.google.inject.Inject;
import lmsspring.cucumber.pom.factories.AdminPageFactory;
import cucumber.runtime.java.guice.ScenarioScoped;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import org.openqa.selenium.TimeoutException;
import lombok.extern.slf4j.Slf4j;

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

@Slf4j
@ScenarioScoped
public class CrudStepDef extends AbstractStepDef {

	@Inject
	private AdministratorEntityFactory administratorEntityFactory;
	@Inject
	private ApplicationLocaleEntityFactory applicationLocaleEntityFactory;
	@Inject
	private ArticleEntityFactory articleEntityFactory;
	@Inject
	private BookEntityFactory bookEntityFactory;
	@Inject
	private CoreUserEntityFactory coreUserEntityFactory;
	@Inject
	private CourseEntityFactory courseEntityFactory;
	@Inject
	private CourseCategoryEntityFactory courseCategoryEntityFactory;
	@Inject
	private CourseLessonEntityFactory courseLessonEntityFactory;
	@Inject
	private LessonEntityFactory lessonEntityFactory;
	@Inject
	private TagEntityFactory tagEntityFactory;
	@Inject
	private WorkflowEntityFactory workflowEntityFactory;
	@Inject
	private WorkflowStateEntityFactory workflowStateEntityFactory;
	@Inject
	private WorkflowTransitionEntityFactory workflowTransitionEntityFactory;
	@Inject
	private WorkflowVersionEntityFactory workflowVersionEntityFactory;
	@Inject
	private LessonFormSubmissionEntityFactory lessonFormSubmissionEntityFactory;
	@Inject
	private LessonFormVersionEntityFactory lessonFormVersionEntityFactory;
	@Inject
	private LessonFormTileEntityFactory lessonFormTileEntityFactory;
	@Inject
	private AdminPageFactory adminPageFactory;

	@Given("I navigate to the {string} backend page")
	public void crud_backend_navigate(String entityName) throws Exception {
		adminPageFactory.createCrudListPage(entityName).navigate();
	}

	@Then("I am on the {string} backend page")
	public void crud_assert_on_backend_page(String entityName) throws Exception {
		var pageUrl = adminPageFactory.createCrudListPage(entityName).getPageUrl();
		webDriverWait.until(webDriver -> webDriver.getCurrentUrl().contains(pageUrl));
	}

	@Given("I click to create a {string}")
	public void crud_backend_click_create(String entityName) throws Exception {
		adminPageFactory.createCrudEditPage(entityName).createButton.click();
	}

	@Given("I {string} an existing {string}")
	public void iInteractWithAnExistingEntity(String crudAction, String entityName) throws Exception {
		var page = adminPageFactory.createCrudListPage(entityName);

		// wait for the list to be populated with values
		webDriverWait.until(x -> page.CrudListItems.size() > 0);
		// perform an action on the first entity in the list
		switch (crudAction.toLowerCase()) {
			case "update":
				page.EditButtons.get(0).click();
				var createPage = adminPageFactory.createCrudEditPage(entityName);
				var entity = getEntityFactory(entityName).createWithNoRef();
				createPage.applyEntity(entity);
				break;
			case "view":
				page.ViewButtons.get(0).click();
				break;
			case "delete":
				page.DeleteButtons.get(0).click();
				page.confirmDelete.click();
				break;
			case "select":
				page.selectionCheckboxes.get(1).click();
				break;
			default:
				throw new Exception(String.format("Unexpected crud action: %s", crudAction));
		}
	}

	@Given("I create an {string} if not exists")
	public void create_entity_if_not_exist(String entityName) throws Exception {
		var page = adminPageFactory.createCrudListPage(entityName);

		try {
			// wait for the list to be populated with values
			webDriverWait.until(x -> page.CrudListItems.size() > 0);
		} catch (TimeoutException e) {
			// Create a new entity if no one exists
			page.createButton.click();
			createValidEntity(entityName);
		}
	}

	@Given("I create a valid {string}")
	public void crud_backend_fill_in_and_save(String entityName) throws Exception {
		createValidEntity(entityName);
	}

	@Given("I create a valid {string} with id {string}")
	public void create_valid_entity_with_id_and_save(String entityName, String entityId) throws Exception {
		this.testContext.put(entityId, createValidEntity(entityName));
	}

	@Given("I delete all existing {string} entities")
	public void iDeleteAllExistingEntityNameFormTileEntities(String entityName) throws Exception {
		var page = this.adminPageFactory.createCrudListPage(entityName);
		if (page.DeleteButtons.size() > 0) {
			iIndividuallySelectAllEntitiesOnThePage(entityName);
			while (page.paginationButtons.size() > 0) {
				page.nextPage.click();
				iIndividuallySelectAllEntitiesOnThePage(entityName);
			}
			iClickTheDeleteAllButtonOnThePageAndConfirm(entityName);
		}
	}

	@Then("I {string} {string} a {string} entity")
	public void iInteractWithAnEntity(String hasAccess, String permissionType, String entityName) throws Exception {
		CrudList crudList = this.adminPageFactory.createCrudListPage(entityName);
		boolean canPerformAction = hasAccess.matches("can");
		boolean isDisabled;

		switch (permissionType) {
			case "create":
				webDriverWait.until(x -> crudList.EditButtons.size() > 0);
				isDisabled = WebElementUtils.elementHasAttribute(crudList.createButton, "disabled", "true");
				Assert.assertTrue(isDisabled != canPerformAction);
				break;
			case "read":
				isDisabled = webDriver.getCurrentUrl().endsWith("/403");
				Assert.assertTrue(isDisabled != canPerformAction);
				break;
			case "update":
				webDriverWait.until(x -> crudList.EditButtons.size() > 0);
				isDisabled = WebElementUtils.elementHasAttribute(crudList.EditButtons.get(0), "disabled", "true");
				Assert.assertTrue(isDisabled != canPerformAction);
				break;
			case "delete":
				webDriverWait.until(x -> crudList.DeleteButtons.size() > 0);
				isDisabled = WebElementUtils.elementHasAttribute(crudList.DeleteButtons.get(0), "disabled", "true");
				Assert.assertTrue(isDisabled != canPerformAction);
				break;
			default:
				throw new RuntimeException(permissionType + "is not a valid CRUD operation");
		}
	}

	@Given("I create {int} {string} entities")
	public void iCreateEntities(int entityCount, String entityName) throws Exception {
		var pageUrl = adminPageFactory.createCrudListPage(entityName).getPageUrl();
		for (int i = 0; i < entityCount; i++) {
			this.createValidEntity(entityName);
			webDriverWait.until(webDriver -> webDriver.getCurrentUrl().matches(pageUrl));
		}
	}

	@When("I click the next page button on the {string} page")
	public void iClickTheNextPageButtonOnThePage(String entityName) throws Exception {
		var page = this.adminPageFactory.createCrudListPage(entityName);
		page.nextPage.click();
	}

	@When("I click the previous page button on the {string} page")
	public void iClickThePreviousPageButtonOnThePage(String entityName) throws Exception {
		var page = this.adminPageFactory.createCrudListPage(entityName);
		page.previousPage.click();
	}

	@When("I individually select all {string} entities on the page")
	public void iIndividuallySelectAllEntitiesOnThePage(String entityName) throws Exception {
		var page = this.adminPageFactory.createCrudListPage(entityName);

		//Don't click the select all checkbox, which is at index 0
		for (int i = 1; i < page.selectionCheckboxes.size(); i++) {
			if (!page.isCheckboxActive(i)) {
				page.selectionCheckboxes.get(i).click();
			}
		}
	}

	@When("I select all {string} entities")
	public void iSelectAllEntities(String entityName) throws Exception {
		var page = this.adminPageFactory.createCrudListPage(entityName);
		webDriverWait.until(x -> page.selectAllButton.size() > 0);
		page.selectAllButton.get(0).click();
	}

	@When("I click the cancel button on the {string} page")
	public void iClickTheCancelButtonOnThePage(String entityName) throws Exception {
		var page = this.adminPageFactory.createCrudListPage(entityName);
		webDriverWait.until(x -> page.collectionOptions.size() > 0);
		page.cancelButton.click();
	}

	@When("I click the delete all button on the {string} page and confirm")
	public void iClickTheDeleteAllButtonOnThePageAndConfirm(String entityName) throws Exception {
		var page = this.adminPageFactory.createCrudListPage(entityName);
		page.deleteAllButton.click();
		WaitUtils.waitForElement(webDriver, page.confirmDelete, VisibilityEnum.CLICKABLE);
		page.confirmDelete.click();
	}

	@Then("I expect that the Select All Checkbox on the {string} page is active")
	public void iExpectThatTheSelectAllCheckboxOnThePageIsActive(String entityName) throws Exception {
		var page = this.adminPageFactory.createCrudListPage(entityName);
		Assert.assertTrue(page.isCheckboxActive(0));
	}

	@Then("I expect the {string} entity is selected")
	public void iExpectTheEntityIsSelected(String entityName) throws Exception {
		var page = this.adminPageFactory.createCrudListPage(entityName);
		webDriverWait.until(x -> this.checkboxActive(page, 1));
		Assert.assertTrue(this.checkboxActive(page, 1));
	}

	@Then("The {string} collection options bar is present")
	public void theCollectionOptionsBarIsPresent(String entityName) throws Exception {
		var page = this.adminPageFactory.createCrudListPage(entityName);
		webDriverWait.until(x -> page.collectionOptions.size() > 0);
		Assert.assertEquals(page.collectionOptions.size(), 1);
	}

	@Then("The Select All Items button is present in the {string} collection options bar")
	public void theSelectAllItemsButtonIsPresentInTheCollectionOptionsBar(String entityName) throws Exception {
		var page = this.adminPageFactory.createCrudListPage(entityName);
		webDriverWait.until(x -> page.selectAllButton.size() > 0);
		Assert.assertEquals(page.selectAllButton.size(), 1);
	}

	@Then("I expect that every {string} entity is selected")
	public void iExpectThatEveryEntityIsSelected(String entityName) throws Exception {
		var page = this.adminPageFactory.createCrudListPage(entityName);
		Assert.assertTrue(this.checkboxActive(page, 0));

		//While not on the last page
		while (!(page.nextPage.getAttribute("disabled") == null)) {
			page.nextPage.click();
			Assert.assertTrue(this.checkboxActive(page, 0));
		}
	}

	@Then("I expect that every {string} entity is deselected")
	public void iExpectThatEveryEntityIsDeselected(String entityName) throws Exception {
		var page = this.adminPageFactory.createCrudListPage(entityName);
		Assert.assertFalse(this.checkboxActive(page, 0));

		//While not on the last page
		while (!(page.nextPage.getAttribute("disabled") == null)) {
			page.nextPage.click();
			Assert.assertFalse(this.checkboxActive(page, 0));
		}
	}

	@Then("I expect that every {string} entity except {int} is deleted")
	public void iExpectThatEveryEntityIsDeleted(String entityName, int expectedRemainingEntities) throws Exception {
		var page = this.adminPageFactory.createCrudListPage(entityName);
		webDriverWait.until(x -> page.CrudListItems.size() == expectedRemainingEntities);
		Assert.assertEquals(page.CrudListItems.size(), expectedRemainingEntities);
	}

	@Then("I expect that {int} {string} entity is selected in the collection options bar")
	public void iExpectThatEntityIsSelectedInTheCollectionOptionsBar(int expectedSelected, String entityName) throws Exception {
		var page = this.adminPageFactory.createCrudListPage(entityName);
		WaitUtils.waitForElement(webDriver, page.selectedCount, VisibilityEnum.VISIBLE);
		int selectedCount = Integer.parseInt(page.selectedCount.getText());
		Assert.assertEquals(selectedCount, expectedSelected);
	}

	/**
	 * Performs a check to see whether the specified checkbox on the crud list page is active
	 *
	 * @param index The index of the checkbox to examine
	 * @return true if the checkbox is active, false otherwise
	 */
	private boolean checkboxActive(CrudList page, int index) {
		webDriverWait.until(webDriver -> webDriver.getCurrentUrl().matches(page.getPageUrl()));
		page.waitForListElementsLoaded();
		webDriverWait.until(x -> page.CrudListItems.size() > 0);
		return page.isCheckboxActive(index);
	}

	/**
	 * Given the name of entity, recursively create entities in backend
	 * @param entityName Name of the type of entity
	 * @return Created entity
	 * @throws Exception
	 */
	private AbstractEntity createValidEntity(String entityName) throws Exception{
		AbstractEntity entity = getEntityFactory(entityName).createWithNoRef();

		var createPage = adminPageFactory.createCrudEditPage(entityName);
		// for each of the required references we will create it
		for (EntityReference reference : entity.References) {
			if (!reference.optional) {
				crud_backend_fill_in_and_save(reference.entityName);
			}
		}
		createPage.navigate();

		webDriverWait.until(webDriver -> webDriver.getCurrentUrl().matches(createPage.getPageUrl()));
		createPage.applyEntity(entity);
		return entity;
	}

	private BaseFactory getEntityFactory(String entityName) throws Exception {
		BaseFactory baseFactory;
		switch (entityName)
		{
			case "Administrator":
				baseFactory = administratorEntityFactory;
				break;
			case "ApplicationLocale":
				baseFactory = applicationLocaleEntityFactory;
				break;
			case "Article":
				baseFactory = articleEntityFactory;
				break;
			case "Book":
				baseFactory = bookEntityFactory;
				break;
			case "CoreUser":
				baseFactory = coreUserEntityFactory;
				break;
			case "Course":
				baseFactory = courseEntityFactory;
				break;
			case "CourseCategory":
				baseFactory = courseCategoryEntityFactory;
				break;
			case "CourseLesson":
				baseFactory = courseLessonEntityFactory;
				break;
			case "Lesson":
				baseFactory = lessonEntityFactory;
				break;
			case "Tag":
				baseFactory = tagEntityFactory;
				break;
			case "Workflow":
				baseFactory = workflowEntityFactory;
				break;
			case "WorkflowState":
				baseFactory = workflowStateEntityFactory;
				break;
			case "WorkflowTransition":
				baseFactory = workflowTransitionEntityFactory;
				break;
			case "WorkflowVersion":
				baseFactory = workflowVersionEntityFactory;
				break;
			case "LessonFormSubmission":
				baseFactory = lessonFormSubmissionEntityFactory;
				break;
			case "LessonFormVersion":
				baseFactory = lessonFormVersionEntityFactory;
				break;
			case "LessonFormTile":
				baseFactory = lessonFormTileEntityFactory;
				break;
			default:
				throw new Exception(String.format("Unexpected entityName %s", entityName));
		}
		return baseFactory;
	}
}