/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.configs.documentation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

@Configuration
public class SwaggerConfig {
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("lmsspring"))
				.paths(PathSelectors.any())
				.build()
				.apiInfo(this.apiInfo())
				.genericModelSubstitutes(ResponseEntity.class)
				.securitySchemes(Collections.singletonList(securityScheme()));
	}

	@Bean
	public SecurityScheme securityScheme() {
		return new ApiKey("bearerToken", "Authorization", "header");
	}


	private ApiInfo apiInfo() {
		// % protected region % [Customise your API info for your swagger documentation here] off begin
		return new ApiInfo(
				"lmsspring developer REST API",
				"Developer API to allow for integration with third party systems. "
					+ "Please see the [Developer API Behaviour Overview](https://codebots.app/library-article/codebots/view/148) for a summary of the developer API.",
				"",
				"",
				null,
				"", "", Collections.emptyList());
		// % protected region % [Customise your API info for your swagger documentation here] end
	}
}
