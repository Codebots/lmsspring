/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {FormGroup, Validators} from '@angular/forms';
import {Group, AbstractModel, ModelProperty, ModelPropertyType, ModelRelation, ModelRelationType} from '../../lib/models/abstract.model';
import {BookModel} from '../book/book.model';
import {TagModel} from '../tag/tag.model';
import {WorkflowStateModel} from '../workflowState/workflow_state.model';
import * as _ from 'lodash';
import {QueryOperation, Where} from '../../lib/services/http/interfaces';
import {ElementType} from '../../lib/components/abstract.input.component';
import { CustomValidators } from 'src/app/lib/utils/validators/custom-validators';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * POJO model class used to store information related to the entity.
 */
export class ArticleModel extends AbstractModel {
	/**
	 * The fields which are set as searchable in the entity model
	 * The fields could be used in search in the server side
	 * The fields would be by default used as search in the crud tile.
	 * You could also use this in other tiles for searching.
	 */
	static searchFields: string[] = [
		'title',
		'summary',
		'content',
		// % protected region % [Add any additional searchable field names here] off begin
		// % protected region % [Add any additional searchable field names here] end
	];

	/**
	 * Attributes to be shown in value to display
	 */
	static displayAttributes: string[] = [
		// % protected region % [Change displayAttributes here if needed] off begin
		'title',
		// % protected region % [Change displayAttributes here if needed] end
	];

	static modelPropGroups: { [s: string]: Group } = {
		// % protected region % [Add groups for the entity here] off begin
		// % protected region % [Add groups for the entity here] end
	};

	readonly className = 'ArticleModel';

	/**
	 * Default value to be displayed in dropdown etc
	 */
	get valueToDisplay(): string {
		// % protected region % [Change displayName here if needed] off begin
		return ArticleModel.displayAttributes.map((attr) => this[attr]).join(' ');
		// % protected region % [Change displayName here if needed] end
	}

	/**
	 * The title of the article..
	 */
	title: string;

	/**
	 * The summary of the article..
	 */
	summary: string;

	/**
	 * The body content of the article..
	 */
	content: string;

	bookId: string;

	workflowStatesIds: string[] = [];

	tagsIds: string[] = [];

	modelPropGroups: { [s: string]: Group } = ArticleModel.modelPropGroups;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	static getProps(): ModelProperty[] {
		return super.getProps().concat([
			{
				name: 'title',
				// % protected region % [Set displayName for Title here] off begin
				displayName: 'Title',
				// % protected region % [Set displayName for Title here] end
				type: ModelPropertyType.STRING,
				// % protected region % [Set display element type for Title here] off begin
				elementType: ElementType.INPUT,
				// % protected region % [Set display element type for Title here] end
				// % protected region % [Set isSensitive for Title here] off begin
				isSensitive: false,
				// % protected region % [Set isSensitive for Title here] end
				// % protected region % [Set readonly for Title here] off begin
				readOnly: false,
				// % protected region % [Set readonly for Title here] end
				validators: [
					Validators.required,
					// % protected region % [Add other validators for Title here] off begin
					// % protected region % [Add other validators for Title here] end
				],
				// % protected region % [Add any additional model attribute properties for Title here] off begin
				// % protected region % [Add any additional model attribute properties for Title here] end
			},
			{
				name: 'summary',
				// % protected region % [Set displayName for Summary here] off begin
				displayName: 'Summary',
				// % protected region % [Set displayName for Summary here] end
				type: ModelPropertyType.STRING,
				// % protected region % [Set display element type for Summary here] off begin
				elementType: ElementType.INPUT,
				// % protected region % [Set display element type for Summary here] end
				// % protected region % [Set isSensitive for Summary here] off begin
				isSensitive: false,
				// % protected region % [Set isSensitive for Summary here] end
				// % protected region % [Set readonly for Summary here] off begin
				readOnly: false,
				// % protected region % [Set readonly for Summary here] end
				validators: [
					Validators.required,
					// % protected region % [Add other validators for Summary here] off begin
					// % protected region % [Add other validators for Summary here] end
				],
				// % protected region % [Add any additional model attribute properties for Summary here] off begin
				// % protected region % [Add any additional model attribute properties for Summary here] end
			},
			{
				name: 'content',
				// % protected region % [Set displayName for Content here] off begin
				displayName: 'Content',
				// % protected region % [Set displayName for Content here] end
				type: ModelPropertyType.STRING,
				// % protected region % [Set display element type for Content here] off begin
				elementType: ElementType.TEXTAREA,
				// % protected region % [Set display element type for Content here] end
				// % protected region % [Set isSensitive for Content here] off begin
				isSensitive: false,
				// % protected region % [Set isSensitive for Content here] end
				// % protected region % [Set readonly for Content here] off begin
				readOnly: false,
				// % protected region % [Set readonly for Content here] end
				validators: [
					// % protected region % [Add other validators for Content here] off begin
					// % protected region % [Add other validators for Content here] end
				],
				// % protected region % [Add any additional model attribute properties for Content here] off begin
				// % protected region % [Add any additional model attribute properties for Content here] end
			},
			// % protected region % [Add any additional class field names here] off begin
			// % protected region % [Add any additional class field names here] end
		]);
	}

	/**
	 * The relations of the entity
	 */
	static getRelations(): { [name: string]: ModelRelation } {
		return {
			...super.getRelations(),
			workflowStates: {
				type: ModelRelationType.MANY,
				name: 'workflowStatesIds',
				// % protected region % [Customise your 1-1 or 1-M label for Workflow States here] off begin
				label: 'Workflow States',
				// % protected region % [Customise your 1-1 or 1-M label for Workflow States here] end
				// % protected region % [Customise your display name for Workflow States here] off begin
				displayName: 'displayIndex',
				// % protected region % [Customise your display name for Workflow States here] end
				hideElement: true,
				validators: [
					// % protected region % [Add other validators for Workflow States here] off begin
					// % protected region % [Add other validators for Workflow States here] end
				],
				// % protected region % [Add any additional field for relation Workflow States here] off begin
				// % protected region % [Add any additional field for relation Workflow States here] end
			},
			book: {
				type: ModelRelationType.ONE,
				name: 'bookId',
				// % protected region % [Customise your label for Book here] off begin
				label: 'Book',
				// % protected region % [Customise your label for Book here] end
				// % protected region % [Customise your display name for Book here] off begin
				// TODO change implementation to use OrderBy or create new metamodel property DisplayBy
				displayName: 'name',
				// % protected region % [Customise your display name for Book here] end
				validators: [
					// % protected region % [Add other validators for Book here] off begin
					// % protected region % [Add other validators for Book here] end
				],
				// % protected region % [Add any additional field for relation Book here] off begin
				// % protected region % [Add any additional field for relation Book here] end
			},
			tags: {
				type: ModelRelationType.MANY,
				name: 'tagsIds',
				// % protected region % [Customise your label for Tags here] off begin
				label: 'Tags',
				// % protected region % [Customise your label for Tags here] end
				// % protected region % [Customise your display name for Tags here] off begin
				// TODO change implementation to use OrderBy or create new metamodel property DisplayBy
				displayName: 'name',
				// % protected region % [Customise your display name for Tags here] end
				validators: [
					// % protected region % [Add other validators for Tags here] off begin
					// % protected region % [Add other validators for Tags here] end
				],
				// % protected region % [Add any additional field for relation Tags here] off begin
				// % protected region % [Add any additional field for relation Tags here] end
			},
		};
	}

	/**
	 * Convert the form group to the query conditions
	 */
	static convertFilterToCondition(formGroup: FormGroup): Where[][] {
		let conditions: Where[][] = [];

		// % protected region % [Overide the default convertFilterToCondition here] off begin
		Object.keys(formGroup.value).forEach((key) => {
			switch (key) {
				case 'title':
					break;
				case 'summary':
					break;
				case 'content':
					break;
				case 'created':
					const created = formGroup.value[key];
					// is the range of date
					if (created instanceof Array) {
						conditions.push([
							{
								path: key,
								operation: QueryOperation.GREATER_THAN_OR_EQUAL,
								value: created[0]
							}
						]);
						conditions.push([
							{
								path: key,
								operation: QueryOperation.LESS_THAN_OR_EQUAL,
								value: created[1]
							}
						]);
					}
			}
		});
		// % protected region % [Overide the default convertFilterToCondition here] end


		return conditions;
	}

	/**
	 * Convert a nested JSON object into an array of flatten objects.
	 */
	static deepParse(data: string | { [K in keyof ArticleModel]?: ArticleModel[K] }, currentModel?): AbstractModel[] {
		if (currentModel == null) {
			currentModel = new ArticleModel(data);
		}

		let returned: AbstractModel[] = [currentModel];
		const json = typeof data === 'string' ? JSON.parse(data) : data;

		// Incoming one to many
		if (json.book) {
			currentModel.bookId = json.book.id;
			returned = _.union(returned, BookModel.deepParse(json.book));
		}

		// Outgoing many to many
		if (json.workflowStates) {
			currentModel.workflowStatesIds = json.workflowStates.map(model => model.id);
			returned = _.union(returned, _.flatten(json.workflowStates.map(model => WorkflowStateModel.deepParse(model))));
		}
		// Incoming many to many
		if (json.tags) {
			currentModel.tagsIds = json.tags.map(model => model.id);
			returned = _.union(returned, _.flatten(json.tags.map(model => TagModel.deepParse(model))));
		}

		// % protected region % [Customise your deep parse before return here] off begin
		// % protected region % [Customise your deep parse before return here] end

		return returned;
	}

	/**
	 * @example
	 *
	 * `let articleModel = new ArticleModel(data);`
	 *
	 * @param data The input data to be initialised as the ArticleModel,
	 *    it is expected as a JSON string or as a nullable ArticleModel.
	 */
	constructor(data?: string | Partial<ArticleModel>) {
		super(data);

		if (data) {
			const json = typeof data === 'string'
				? JSON.parse(data) as Partial<ArticleModel>
				: data;

			this.title = json.title;
			this.summary = json.summary;
			this.content = json.content;
			this.bookId = json.bookId;
			this.workflowStatesIds = json.workflowStatesIds;
			this.tagsIds = json.tagsIds;
			// % protected region % [Add any additional logic here after set the data] off begin
			// % protected region % [Add any additional logic here after set the data] end
		}

		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}

	toJSON() {
		return {
			...super.toJSON(),
			title: this.title,
			summary: this.summary,
			content: this.content,
			bookId: this.bookId,
			workflowStatesIds: this.workflowStatesIds,
			tagsIds: this.tagsIds,
			// % protected region % [Add any additional logic here to json] off begin
			// % protected region % [Add any additional logic here to json] end
		};
	}

	getPropDisplayNames(): { [s: string]: ModelProperty } {
		const returned = {};
		ArticleModel.getProps().map(prop => returned[prop.name] = prop);
		return returned;
	}

	applyUpdates(updates: any): ArticleModel {
		let newModelJson = this.toJSON();

		if (updates.title) {
			newModelJson.title = updates.title;
		}

		if (updates.summary) {
			newModelJson.summary = updates.summary;
		}

		if (updates.content) {
			newModelJson.content = updates.content;
		}

		if (updates.bookId) {
			newModelJson.bookId = updates.bookId;
		}

		if (updates.workflowStatesIds) {
			newModelJson.workflowStatesIds = updates.workflowStatesIds;
		}

		if (updates.tagsIds) {
			newModelJson.tagsIds = updates.tagsIds;
		}

		return new ArticleModel(newModelJson);
	}

	/**
	 * @inheritDoc
	 */
	difference(other: AbstractModel): any {
		if (!(other instanceof ArticleModel)) {
			return {};
		}

		const diff = {};

		for (const key of _.keys(this)) {
			const thisValue = this[key];
			const otherValue = other[key];

			// Handle dates differently
			if (thisValue instanceof Date) {
				let thisDate = (thisValue) ? thisValue.getTime() : null;
				let otherDate = (otherValue) ? otherValue.getTime() : null;

				if (thisDate !== otherDate) {
					diff[key] = thisValue;
				}
			} else if (thisValue !== otherValue) {
				diff[key] = thisValue;
			}
		}

		return _.omit(diff, [
			'created',
			'modified',
			'bookIds',
			'tagsIds',
			'workflowStatesIds',
			// % protected region % [Add any other fields to omit here] off begin
			// % protected region % [Add any other fields to omit here] end
		]);
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
