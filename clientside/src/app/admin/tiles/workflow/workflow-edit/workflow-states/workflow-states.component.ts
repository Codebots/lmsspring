/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Component, HostBinding, QueryList, ViewChildren} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {
	ButtonAccentColour,
	ButtonSize,
	ButtonStyle,
	IconPosition
} from 'src/app/lib/components/button/button.component';
import {AbstractComponent} from '../../../../../lib/components/abstract.component';
import {WorkflowStateModel} from '../../../../../models/workflowState/workflow_state.model';
import {ActivatedRoute} from '@angular/router';
import {ActionsSubject, Store} from '@ngrx/store';
import {RouterState} from '../../../../../models/model.state';
import {WorkflowStateModelState} from '../../../../../models/workflowState/workflow_state.model.state';
import * as workflowStateAction from '../../../../../models/workflowState/workflow_state.model.action';
import {Observable, of} from 'rxjs';
import {WorkflowStateEditComponent} from './workflow-state-edit/workflow-state-edit.component';
import {filter, switchMap, take} from 'rxjs/operators';
import {Expand, QueryOperation, QueryParams} from '../../../../../lib/services/http/interfaces';
import {getWorkflowStateCollectionModels} from '../../../../../models/workflowState/workflow_state.model.selector';
import * as routerActions from '../../../../../lib/routing/routing.action';
import * as uuid from 'uuid';
import * as _ from 'lodash';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

@Component({
	selector: 'section[cb-workflow-edit-states]',
	templateUrl: './workflow-states.component.html',
	styleUrls: [
		'./workflow-states.component.scss',
		// % protected region % [Add any additional styles here] off begin
		// % protected region % [Add any additional styles here] end
	]
})
export class WorkflowStatesComponent extends AbstractComponent {
	buttonStyle = ButtonStyle;
	buttonAccentColour = ButtonAccentColour;
	iconPos = IconPosition;
	buttonSize = ButtonSize;

	@HostBinding('class')
	className = 'workflow__states workflow__states__edit';

	/**
	 * Existing states fetched from database
	 */
	currentStates$: Observable<WorkflowStateModel[]>;

	/**
	 * The id of state to edit in sidebar
	 */
	workflowStateIdToEdit: string;

	/**
	 * New States to be added
	 */
	newStates: WorkflowStateModel[] = [];

	/**
	 * Expands all attributes
	 */
	get defaultExpands(): Expand[] {
		let expands: Expand[] =  Object.entries(WorkflowStateModel.getRelations()).map(
			([key, entry]): Expand => {
				return {
					name: key,
					fields: ['id']
				};
			}
		);
		return expands;
	}

	get collectionId(): string {
		return `${this.versionId}-workflow-states`;
	}

	// TODO Add queryAllWithQuery Api to remove hard code page size
	get queryParams(): QueryParams {
		return {
			pageSize: 1000,
			pageIndex: 0,
			where: [
				[
					{
						path: 'workflowVersionId',
						operation: QueryOperation.EQUAL,
						value: this.versionId
					}
				]
			],
			expands: this.defaultExpands
		};
	}

	versionId: string;

	buttonDisabled: boolean = false;

	@ViewChildren(WorkflowStateEditComponent)
	stateEditComponents: QueryList<WorkflowStateEditComponent>;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	constructor(
		private readonly activatedRoute: ActivatedRoute,
		private readonly store: Store<{ model: WorkflowStateModelState }>,
		private readonly routerStore: Store<{ router: RouterState }>,
		private readonly actionSubjects: ActionsSubject,
		private readonly toastrService: ToastrService,
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
	) {
		super();

		this.currentStates$ = this.activatedRoute.parent.paramMap.pipe(
			switchMap(
				params => {
					this.versionId = params.get('versionId');
					if (this.versionId) {
						this.store.dispatch(new workflowStateAction.WorkflowStateAction(
							workflowStateAction.WorkflowStateModelActionTypes.INITIALISE_WORKFLOW_STATE_COLLECTION_STATE,
							{
								collectionId: this.collectionId
							}
						));
						this.store.dispatch(new workflowStateAction.WorkflowStateAction(
							workflowStateAction.WorkflowStateModelActionTypes.FETCH_WORKFLOW_STATE_WITH_QUERY,
							{
								collectionId: this.collectionId,
								queryParams: this.queryParams
							}
						));
						return this.store.select(getWorkflowStateCollectionModels, this.collectionId);
					} else {
						return of([]);
					}
				}
			)
		);
		// % protected region % [Add any additional logic here for constructor] off begin
		// % protected region % [Add any additional logic here for constructor] end
	}

	/**
	 * Add a new workflow state to the collection
	 */
	onAddNewStateClicked() {
		this.newStates.push(new WorkflowStateModel({
			workflowVersionId: this.versionId
		}));
	}

	/**
	 * Validate all states
	 * @return Whether all workflow states are valid
	 */
	validateStates(): boolean {
		let allValid = this.stateEditComponents.toArray().every(comp => comp.validateFormAndUpdateValue());

		const haveOneStartState = this.stateEditComponents.filter(comp => comp.stateForm.get('isStartState').value === true).length === 1;
		allValid = allValid && haveOneStartState;

		if (!haveOneStartState) {
			this.toastrService.error('Should have 1 start state');
		}

		// % protected region % [Add any additional logic here for validateStates] off begin
		// % protected region % [Add any additional logic here for validateStates] end

		return allValid;
	}

	/**
	 * Method called when save draft is clicked
	 * Create new states and update current states
	 */
	onSaveDraftClicked() {

		if (this.validateStates()) {
			const statesToCreate = [];
			const statesToUpdate = [];
			const updates = [];

			this.stateEditComponents.forEach(workflowComponent => {
				// Update existed workflow component
				const changes = workflowComponent.getUpdateValue();
				const workflowState = workflowComponent.workflowState;
				if (workflowComponent.workflowState.id) {
					statesToUpdate.push(workflowState);
					updates.push(changes);
				} else {
					Object.assign(workflowState, changes);
					statesToCreate.push(workflowState);
				}
			});

			this.buttonDisabled = true;

			// Create variables for uuid of action and to control whether button is enabled
			const updateRequestId = uuid.v4();
			const createRequestId = uuid.v4();
			let sendingCreateRequest = false;
			let sendingUpdateRequest = false;

			if (statesToCreate.length > 0) {
				this.store.dispatch(new workflowStateAction.WorkflowStateAction(
					workflowStateAction.WorkflowStateModelActionTypes.CREATE_ALL_WORKFLOW_STATE,
					{
						targetModels: statesToCreate,
						collectionId: this.collectionId,
						requestId: createRequestId,
						queryParams: {
							expands: this.defaultExpands
						},
					}
				));
			} else {
				sendingCreateRequest = false;
			}

			if (statesToUpdate.length > 0) {
				this.store.dispatch(new workflowStateAction.WorkflowStateAction(
					workflowStateAction.WorkflowStateModelActionTypes.UPDATE_ALL_WORKFLOW_STATE,
					{
						targetModels: statesToUpdate,
						updates: updates,
						collectionId: this.collectionId,
						requestId: updateRequestId,
						queryParams: {
							expands: this.defaultExpands
						},
					}
				));
			} else {
				sendingUpdateRequest = false;
			}

			this.actionSubjects.pipe(
				filter((action: workflowStateAction.WorkflowStateActionOK ) => {
					return action.type === workflowStateAction.WorkflowStateModelActionTypes.CREATE_ALL_WORKFLOW_STATE_OK
						&& action.stateConfig.requestId === createRequestId;
				}),
				take(1)
			).subscribe(() => {
				sendingUpdateRequest = false;
				this.buttonDisabled = sendingUpdateRequest || sendingCreateRequest;
			});

			this.actionSubjects.pipe(
				filter((action: workflowStateAction.WorkflowStateAction ) => {
					return action.type === workflowStateAction.WorkflowStateModelActionTypes.UPDATE_ALL_WORKFLOW_STATE_OK
						&& action.stateConfig.requestId === updateRequestId;
				}),
				take(1)
			).subscribe(() => {
				sendingUpdateRequest = false;
				this.buttonDisabled = sendingUpdateRequest || sendingCreateRequest;
			});

			this.buttonDisabled = sendingCreateRequest || sendingUpdateRequest;

			this.newStates = [];
		}

		// % protected region % [Add any additional logic for onSaveDraftClicked here] off begin
		// % protected region % [Add any additional logic for onSaveDraftClicked here] end
	}

	/**
	 * Handle event to delete new created / existed states
	 */
	onStateDeleted(workflowState: WorkflowStateModel) {
		if (workflowState.id) {
			this.store.dispatch(new workflowStateAction.WorkflowStateAction(
				workflowStateAction.WorkflowStateModelActionTypes.DELETE_WORKFLOW_STATE,
				{
					targetModelId: workflowState.id
				}
			));
		} else {
			_.remove(this.newStates, (state) => state === workflowState);
		}

		// % protected region % [Add any additional logic for onStateDeleted here] off begin
		// % protected region % [Add any additional logic for onStateDeleted here] end
	}

	onEditButtonClicked(workflowStateId: string) {
		this.workflowStateIdToEdit = workflowStateId;
	}

	onSidebarClosed() {
		this.workflowStateIdToEdit = null;
	}

	transitionChanged() {
		this.store.dispatch(new workflowStateAction.WorkflowStateAction(
			workflowStateAction.WorkflowStateModelActionTypes.FETCH_WORKFLOW_STATE_WITH_QUERY,
			{
				collectionId: this.collectionId,
				queryParams: this.queryParams
			}
		));
	}

	/**
	 * When is start is clicked, set all other component to be not start state
	 */
	onIsStartStateClicked(clickedComponent: WorkflowStateEditComponent) {
		this.stateEditComponents.forEach(stateEditComponent => {
			if (stateEditComponent !== clickedComponent) {
				stateEditComponent.setIsStartState(false);
			}
		});
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
