/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Component, Input, HostBinding, Type, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {RouterReducerState} from '@ngrx/router-store';
import {switchMap, max, mergeAll} from 'rxjs/operators';
import {AbstractComponent} from 'src/app/lib/components/abstract.component';
import {AbstractModel} from 'src/app/lib/models/abstract.model';
import {ModelState, RouterState} from 'src/app/models/model.state';
import {getRouterState} from 'src/app/models/model.selector';
import {QueryOperation} from 'src/app/lib/services/http/interfaces';
import {LessonModel} from 'src/app/models/lesson/lesson.model';
import {LessonFormVersionModel} from 'src/app/models/lessonFormVersion/lesson_form_version.model';
import {getLessonModelWithId} from 'src/app/models/lesson/lesson.model.selector';
import * as lessonModelActions from 'src/app/models/lesson/lesson.model.action';
import * as lessonFormVersionModelActions from 'src/app/models/lessonFormVersion/lesson_form_version.model.action';
import {getLessonFormVersionCollectionModels} from 'src/app/models/lessonFormVersion/lesson_form_version.model.selector';
import {FormMode, FormSlideData} from 'src/app/lib/behaviours/form/form-interfaces';
import * as uuid from 'uuid';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

@Component({
	selector: 'section[cb-form-wrapper]',
	templateUrl: './form-wrapper.component.html',
	// % protected region % [Add any additional component configurations here] off begin
	// % protected region % [Add any additional component configurations here] end
})
export class FormWrapperComponent extends AbstractComponent implements OnInit {

	@HostBinding('class')
	get formWrapperCSSClass() {
		return [
			'forms-behaviour'
		].join(' ');
	}

	currentFormVersion: any;
	slidesData: FormSlideData[] = [];

	@Input()
	formMode: FormMode = 'build';

	@Input()
	entityType: 'lesson';

	@Input()
	entityId: string;

	@Input()
	fetchFromUrl: boolean = true;

	collectionId = uuid.v4();

	// % protected region % [Add any additional class properties here] off begin
	// % protected region % [Add any additional class properties here] end

	constructor(
		private readonly store: Store<{ model: ModelState, routerState: RouterReducerState<RouterState> }>,
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
	) {
		super(
			// % protected region % [Add any additional constructor arguments here] off begin
			// % protected region % [Add any additional constructor arguments here] end
		);

		if (this.fetchFromUrl) {
			store.select(getRouterState).subscribe(
				routerState => {
					this.entityType = routerState.params.entityType;
					this.entityId = routerState.params.id;
					if (routerState.url.endsWith('build')) {
						this.formMode = 'build';
					} else if (routerState.url.endsWith('preview')) {
						this.formMode = 'report';
					}
				}
			);
		}
	}

	ngOnInit() {

		switch (this.entityType) {
			case 'lesson':
				this.currentFormVersion = new LessonFormVersionModel({
					version: 0,
					formData: '[]'
				});

				this.store.dispatch(new lessonFormVersionModelActions.LessonFormVersionAction(
					lessonFormVersionModelActions.LessonFormVersionModelActionTypes.INITIALISE_LESSON_FORM_VERSION_COLLECTION_STATE,
					{
						collectionId: this.collectionId
					}
				));

				this.store.select(getLessonFormVersionCollectionModels, this.collectionId)
					.subscribe(models => {
						if (models.length > 0) {
							this.currentFormVersion = models[0];
							this.slidesData = JSON.parse(this.currentFormVersion.formData);
						}
				});

				this.store.dispatch(new lessonFormVersionModelActions.LessonFormVersionAction(
					lessonFormVersionModelActions.LessonFormVersionModelActionTypes.FETCH_LESSON_FORM_VERSION_WITH_QUERY,
					{
						collectionId: this.collectionId,
						queryParams: {
							pageSize: 1,
							pageIndex: 0,
							where: [
								[
									{
										path: 'formId',
										operation: QueryOperation.EQUAL,
										value: this.entityId
									}
								]
							],
							orderBy: [
								{
									path: 'version',
									descending: true
								}
							]
						}
					}
				));
				break;
		}
	}

	/**
	 * Triggered when the user wants to save a new version.
	 */
	onSaveNewVersion(doPublish: boolean = false) {
		switch (this.entityType) {
			case 'lesson':
				this.store.dispatch(new lessonFormVersionModelActions.LessonFormVersionAction(
					lessonFormVersionModelActions.LessonFormVersionModelActionTypes.CREATE_LESSON_FORM_VERSION,
					{
						targetModel: new LessonFormVersionModel({
							formData: JSON.stringify(this.slidesData),
							version: this.currentFormVersion.version + 1,
							formId: this.entityId,
							publishedFormId: this.entityId
						}),
						collectionId: this.collectionId,
					},
					[
						new lessonFormVersionModelActions.LessonFormVersionAction(
							lessonFormVersionModelActions.LessonFormVersionModelActionTypes.FETCH_LESSON_FORM_VERSION_WITH_QUERY,
							{
								collectionId: this.collectionId,
								queryParams: {
									pageSize: 1,
									pageIndex: 0,
									where: [
										[
											{
												path: 'formId',
												operation: QueryOperation.EQUAL,
												value: this.entityId
											}
										]
									],
									orderBy: [
										{
											path: 'version',
											descending: true
										}
									]
								}
							}
						)
					]
				));
				break;

		}
	}

	/**
	 * Triggered when the user wants to publish a new version.
	 */
	onPublish() {
		this.onSaveNewVersion(true);
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
