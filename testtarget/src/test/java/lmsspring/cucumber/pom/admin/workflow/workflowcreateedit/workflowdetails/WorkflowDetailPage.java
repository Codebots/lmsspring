/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.cucumber.pom.admin.workflow.workflowcreateedit.workflowdetails;

import com.google.inject.Inject;
import lmsspring.cucumber.pom.admin.workflow.workflowcreateedit.WorkflowCreateEditPage;
import lmsspring.cucumber.pom.enums.VisibilityEnum;
import lmsspring.cucumber.utils.*;
import lmsspring.entities.WorkflowVersionEntity;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * Page Object Model for Workflow Detail Page
 */
@Slf4j
@ScenarioScoped
public class WorkflowDetailPage extends WorkflowCreateEditPage {

	@Getter
	@FindBy(xpath = "//cb-textfield[./label[normalize-space()='Workflow Name']]/input")
	private WebElement workflowNameField;

	@Getter
	@FindBy(xpath = "//cb-textarea[./label[normalize-space()='Workflow Description']]/textarea")
	private WebElement workflowDescriptionField;

	@Getter
	@FindBy(xpath = "//button[normalize-space()='Save Draft']")
	private WebElement saveButton;

	@Getter
	@FindBy(xpath = "//button[normalize-space()='Cancel']")
	private WebElement cancelButton;

	@Getter
	@FindBy(xpath = "//cb-dropdown[./label[normalize-space()='Entities']]")
	private WebElement workflowEntitiesDropdown;

	// % protected region % [Add any additional fields here] off begin
	// % protected region % [Add any additional fields here] end

	@Inject
	WorkflowDetailPage(
			// % protected region % [Add any additional constructor parameters here] off begin
			// % protected region % [Add any additional constructor parameters here] end
			WebDriver webDriver,
			Properties properties
	) {
		super(webDriver, properties, "details");

		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}

	/**
	 * Fill workflow name field in workflow detail page
	 */
	public void fillWorkflowNameField(String value) {
		TypingUtils.clearAndType(webDriver, workflowNameField, value);
		// % protected region % [Add any additional logic for 'fillWorkflowNameField' here] off begin
		// % protected region % [Add any additional logic for 'fillWorkflowNameField' here] end
	}

	/**
	 * Fill workflow name field in workflow detail page
	 */
	public void fillWorkflowDescription(String value) {
		TypingUtils.clearAndType(webDriver, workflowDescriptionField, value);
		// % protected region % [Add any additional logic for 'fillWorkflowDescription' here] off begin
		// % protected region % [Add any additional logic for 'fillWorkflowDescription' here] end
	}

	/**
	 * Fill workflow form with workflow version entity in workflow detail page
	 */
	public void fillWorkflowDetailForm(WorkflowVersionEntity workflowVersionEntity) {
		fillWorkflowNameField(workflowVersionEntity.getWorkflowName());
		fillWorkflowDescription(workflowVersionEntity.getWorkflowDescription());
		// Set Workflow Entities.
		setAssignedEntityByName("Article", workflowVersionEntity.getArticleAssociation());
		// % protected region % [Add any additional logic for 'fillWorkflowDetailForm' here] off begin
		// % protected region % [Add any additional logic for 'fillWorkflowDetailForm' here] end
	}

	/**
	 * Click on save draft button
	 */
	public void clickSaveDraftButton() {
		MouseClickUtils.clickOnElement(webDriver, saveButton);
		// % protected region % [Add any additional logic for 'clickSaveDraftButton' here] off begin
		// % protected region % [Add any additional logic for 'clickSaveDraftButton' here] end
	}

	/**
	 * Click on cancel draft button
	 */
	public void clickCancelButton() {
		MouseClickUtils.clickOnElement(webDriver, cancelButton);
		// % protected region % [Add any additional logic for 'clickCancelButton' here] off begin
		// % protected region % [Add any additional logic for 'clickCancelButton' here] end
	}

	/**
	 * Fill the form and click save draft button
	 */
	public void fillWorkflowDetailFormAndSave(WorkflowVersionEntity workflowVersionEntity) {
		fillWorkflowDetailForm(workflowVersionEntity);
		clickSaveDraftButton();

		try {
			WaitUtils.waitForElement(webDriver, this.getWorkflowStatesPage().getEditWorkflowHeading(), VisibilityEnum.VISIBLE);
		} catch (Exception e) {
			throw new RuntimeException("Workflow states not present found", e);
		}

		// Update the internal workflow version entities id to match the created one
		workflowVersionEntity.setId(UUID.fromString(webDriver.getCurrentUrl().split("/")[7]));

		// % protected region % [Add any additional logic for 'fillWorkflowDetailFormAndSave' here] off begin
		// % protected region % [Add any additional logic for 'fillWorkflowDetailFormAndSave' here] end
	}

	/**
	 * Select / Unselect a entity with Workflow Behaviour
	 * @param entityName Name of entity to select / unselect
	 * @param select Select or Unselect the entity
	 */
	public void setAssignedEntityByName(String entityName, boolean select) {
		if (select) {
			DropdownUtils.selectOptionByName(webDriver, workflowEntitiesDropdown, entityName);
		} else {
			DropdownUtils.unselectOptionByName(webDriver, workflowEntitiesDropdown, entityName);
		}
	}

	public void verifyWorkflowDetail(WorkflowVersionEntity workflowVersionEntity) throws Exception {
		WaitUtils.waitForElement(webDriver, workflowNameField, VisibilityEnum.VISIBLE);
		VerifyUtils.verifyInputValue(workflowNameField, workflowVersionEntity.getWorkflowName());
		VerifyUtils.verifyInputValue(workflowDescriptionField, workflowVersionEntity.getWorkflowDescription());

		List<String> expectedSelectedEntities = new ArrayList<>();
		// Set to Expected Values
		if (workflowVersionEntity.getArticleAssociation()) {
			expectedSelectedEntities.add("Article");
		}

		DropdownUtils.verifyMultiSelect(workflowEntitiesDropdown, expectedSelectedEntities);
	}

	// % protected region % [Add any additional methods here] off begin
	// % protected region % [Add any additional methods here] end
}
