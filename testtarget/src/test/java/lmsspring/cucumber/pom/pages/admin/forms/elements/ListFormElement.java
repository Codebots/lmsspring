/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package lmsspring.cucumber.pom.pages.admin.forms.elements;

import com.github.webdriverextensions.WebComponent;
import lmsspring.cucumber.utils.WaitUtils;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

@Slf4j
@ScenarioScoped
public class ListFormElement extends WebComponent  {

	@Getter
	@FindBy(how = How.XPATH, using = "button[contains(@class, 'icon-chevron-up')]")
	private WebElement titleButton;

	@Getter
	@FindBy(how = How.XPATH, using = "div[contains(@class, 'accordion__info')]/section/a[contains(@class, 'form-item__new')]")
	private WebElement newFormButton;

	@Getter
	@FindBy(how = How.XPATH, using = "div[contains(@class, 'accordion__info')]/section/a")
	private List<WebElement> listForms;

	@Getter
	@FindBy(xpath = "div[contains(@class, 'accordion__info')]/section/a/div[contains(@class, 'form-item')]/div[contains(@class, 'form-item__heading')]/h3")
	private List<WebElement> formTitles;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	public boolean isActive() {
		return this.getAttribute("class").contains("active");
	}

	// % protected region % [Add any additional methods here] off begin
	// % protected region % [Add any additional methods here] end
}
