/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.entities;

import lombok.*;
import javax.validation.constraints.NotNull;


// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
public class WorkflowTransitionEntity extends AbstractEntity {

	public WorkflowTransitionEntity() {
		initialiseReferences();
	}

	private void initialiseReferences() {


		var SourceStateOneMany = new EntityReference();
			SourceStateOneMany.entityName = "WorkflowState";
			SourceStateOneMany.oppositeName = "SourceState";
			SourceStateOneMany.name = "OutgoingTransitions";
			SourceStateOneMany.optional = false;
			SourceStateOneMany.type = "One";
			SourceStateOneMany.oppositeType = "Many";

		References.add(SourceStateOneMany);

		var TargetStateOneMany = new EntityReference();
			TargetStateOneMany.entityName = "WorkflowState";
			TargetStateOneMany.oppositeName = "TargetState";
			TargetStateOneMany.name = "IncomingTransitions";
			TargetStateOneMany.optional = false;
			TargetStateOneMany.type = "One";
			TargetStateOneMany.oppositeType = "Many";

		References.add(TargetStateOneMany);
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Transition Name here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Transition Name here] end
	private String transitionName;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private WorkflowStateEntity sourceState;

	private WorkflowStateEntity targetState;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Similar to {@link this#setSourceState(WorkflowStateEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowStateEntity to be set to this entity
	 */
	public void setSourceState(@NotNull WorkflowStateEntity entity) {
		setSourceState(entity, true);
	}

	/**
	 * Set or update the sourceState in this entity with single WorkflowStateEntity.
	 *
	 * @param entity the given WorkflowStateEntity to be set or updated to sourceState
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setSourceState(@NotNull WorkflowStateEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setSourceState here] off begin
		// % protected region % [Add any additional logic here before the main logic for setSourceState here] end

		if (sameAsFormer(this.sourceState, entity)) {
			return;
		}

		if (this.sourceState != null) {
			this.sourceState.removeOutgoingTransitions(this, false);
		}
		this.sourceState = entity;
		if (reverseAdd) {
			this.sourceState.addOutgoingTransitions(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setWorkflowState here] off begin
		// % protected region % [Add any additional logic here after the main logic for setWorkflowState here] end
	}

	/**
	 * Similar to {@link this#unsetSourceState(boolean)} but default to true.
	 */
	public void unsetSourceState() {
		this.unsetSourceState(true);
	}

	/**
	 * Remove Source State in this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetSourceState(boolean reverse) {
		if (reverse && this.sourceState != null) {
			this.sourceState.removeOutgoingTransitions(this, false);
		}
		this.sourceState = null;
	}
	/**
	 * Similar to {@link this#setTargetState(WorkflowStateEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowStateEntity to be set to this entity
	 */
	public void setTargetState(@NotNull WorkflowStateEntity entity) {
		setTargetState(entity, true);
	}

	/**
	 * Set or update the targetState in this entity with single WorkflowStateEntity.
	 *
	 * @param entity the given WorkflowStateEntity to be set or updated to targetState
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setTargetState(@NotNull WorkflowStateEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setTargetState here] off begin
		// % protected region % [Add any additional logic here before the main logic for setTargetState here] end

		if (sameAsFormer(this.targetState, entity)) {
			return;
		}

		if (this.targetState != null) {
			this.targetState.removeIncomingTransitions(this, false);
		}
		this.targetState = entity;
		if (reverseAdd) {
			this.targetState.addIncomingTransitions(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setWorkflowState here] off begin
		// % protected region % [Add any additional logic here after the main logic for setWorkflowState here] end
	}

	/**
	 * Similar to {@link this#unsetTargetState(boolean)} but default to true.
	 */
	public void unsetTargetState() {
		this.unsetTargetState(true);
	}

	/**
	 * Remove Target State in this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetTargetState(boolean reverse) {
		if (reverse && this.targetState != null) {
			this.targetState.removeIncomingTransitions(this, false);
		}
		this.targetState = null;
	}

	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
