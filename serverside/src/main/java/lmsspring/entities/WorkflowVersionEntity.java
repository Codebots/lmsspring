/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package lmsspring.entities;

import lmsspring.dtos.WorkflowVersionEntityDto;
import lmsspring.entities.listeners.WorkflowVersionEntityListener;
import lmsspring.serializers.WorkflowVersionSerializer;
import lmsspring.lib.validators.ValidatorPatterns;
import lmsspring.services.utils.converters.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.opencsv.bean.CsvIgnore;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;
import org.springframework.lang.Nullable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.hibernate.envers.Audited;

import java.util.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Optional;
import java.util.UUID;

// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Entity
// % protected region % [Override the auditing annotation or add additional annotations here] off begin
@Audited
// % protected region % [Override the auditing annotation or add additional annotations here] end
@ExcludeSuperclassListeners
@EntityListeners({WorkflowVersionEntityListener.class, AuditingEntityListener.class})
@JsonSerialize(using = WorkflowVersionSerializer.class)
@Table(
	uniqueConstraints = {
	}
)
public class WorkflowVersionEntity extends AbstractEntity {

	/**
	 * Takes a WorkflowVersionEntityDto and converts it into a WorkflowVersionEntity.
	 * Primarily used when endpoints are invoked, as they take DTOs as input, which need to be converted to the entity type
	 *
	 * @param workflowVersionEntityDto
	 */
	public WorkflowVersionEntity(WorkflowVersionEntityDto workflowVersionEntityDto) {
		// Need to account for potential that any field is empty.  This will be run before any server validation and
		// endpoints will most likely not be subject to clientside validation

		if (workflowVersionEntityDto.getId() != null) {
			this.setId(workflowVersionEntityDto.getId());
		}

		if (workflowVersionEntityDto.getWorkflowName() != null) {
			this.setWorkflowName(workflowVersionEntityDto.getWorkflowName());
		}

		if (workflowVersionEntityDto.getWorkflowDescription() != null) {
			this.setWorkflowDescription(workflowVersionEntityDto.getWorkflowDescription());
		}

		if (workflowVersionEntityDto.getVersionNumber() != null) {
			this.setVersionNumber(workflowVersionEntityDto.getVersionNumber());
		}

		if (workflowVersionEntityDto.getArticleAssociation() != null) {
			this.setArticleAssociation(workflowVersionEntityDto.getArticleAssociation());
		}

		if (workflowVersionEntityDto.getStates() != null) {
			this.setStates(workflowVersionEntityDto.getStates());
		}

		if (workflowVersionEntityDto.getCurrentWorkflow() != null) {
			this.setCurrentWorkflow(workflowVersionEntityDto.getCurrentWorkflow());
		}

		if (workflowVersionEntityDto.getWorkflow() != null) {
			this.setWorkflow(workflowVersionEntityDto.getWorkflow());
		}

		// % protected region % [Add any additional DTO constructor logic here] off begin
		// % protected region % [Add any additional DTO constructor logic here] end
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Workflow Name here] off begin
	@CsvBindByName(column = "WORKFLOW_NAME", required = true)
	@NotNull(message = "Workflow Name must not be empty")
	@Column(name = "workflow_name")
	@ApiModelProperty(notes = "The Workflow Name of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Workflow Name here] end
	private String workflowName;

	// % protected region % [Modify attribute annotation for Workflow Description here] off begin
	@CsvBindByName(column = "WORKFLOW_DESCRIPTION", required = false)
	@Nullable
	@Column(name = "workflow_description")
	@ApiModelProperty(notes = "The Workflow Description of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Workflow Description here] end
	private String workflowDescription;

	// % protected region % [Modify attribute annotation for Version Number here] off begin
	@CsvBindByName(column = "VERSION_NUMBER", required = false)
	@Nullable
	@Column(name = "version_number")
	@ApiModelProperty(notes = "The Version Number of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Version Number here] end
	private Integer versionNumber;

	// % protected region % [Modify attribute annotation for Article Association here] off begin
	@CsvBindByName(column = "ARTICLE_ASSOCIATION", required = false)
	@Nullable
	@Column(name = "article_association")
	@ApiModelProperty(notes = "The Article Association of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Article Association here] end
	private Boolean articleAssociation;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Update the annotation for States here] off begin
	@ApiModelProperty(notes = "The Workflow State entities that are related to this entity.")
	@OneToMany(mappedBy = "workflowVersion", cascade = {CascadeType.MERGE, CascadeType.REMOVE}, fetch = FetchType.LAZY)
	@CsvIgnore
	// % protected region % [Update the annotation for States here] end
	private Set<WorkflowStateEntity> states = new HashSet<>();

	// % protected region % [Update the annotation for statesIds here] off begin
	@Transient
	@CsvCustomBindByName(column = "STATES_IDS", converter = UUIDHashSetConverter.class)
	// % protected region % [Update the annotation for statesIds here] end
	private Set<UUID> statesIds = new HashSet<>();

	// % protected region % [Update the annotation for Current Workflow here] off begin
	@ApiModelProperty(notes = "The Workflow entities that are related to this entity.")
	@OneToOne(mappedBy = "currentVersion", cascade = {CascadeType.MERGE}, fetch = FetchType.LAZY)
	@CsvIgnore
	// % protected region % [Update the annotation for Current Workflow here] end
	private WorkflowEntity currentWorkflow;

	// % protected region % [Update the annotation for currentWorkflowId here] off begin
	@Transient
	@CsvCustomBindByName(column = "CURRENT_WORKFLOW_ID", converter = UUIDConverter.class)
	// % protected region % [Update the annotation for currentWorkflowId here] end
	private UUID currentWorkflowId;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Update the annotation for Workflow here] off begin
	@ApiModelProperty(notes = "The Workflow entities that are related to this entity.")
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@NotNull(message = "Reference Workflow is required")
	@CsvIgnore
	// % protected region % [Update the annotation for Workflow here] end
	private WorkflowEntity workflow;

	// % protected region % [Update the annotation for workflowId here] off begin
	@Transient
	@CsvCustomBindByName(column = "WORKFLOW_ID", converter = UUIDConverter.class)
	// % protected region % [Update the annotation for workflowId here] end
	private UUID workflowId;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
	 * Similar to {@link this#addStates(WorkflowStateEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowStateEntity to be added to this entity
	 */
	public void addStates(@NotNull WorkflowStateEntity entity) {
		addStates(entity, true);
	}

	/**
	 * Add a new WorkflowStateEntity to states in this entity.
	 *
	 * @param entity the given WorkflowStateEntity  to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addStates(@NonNull WorkflowStateEntity entity, boolean reverseAdd) {
		if (!this.states.contains(entity)) {
			states.add(entity);
			if (reverseAdd) {
				entity.setWorkflowVersion(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addStates(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of WorkflowStateEntity to be added to this entity
	 */
	public void addStates(@NotNull Collection<WorkflowStateEntity> entities) {
		addStates(entities, true);
	}

	/**
	 * Add a new collection of WorkflowStateEntity to States in this entity.
	 *
	 * @param entities the given collection of WorkflowStateEntity to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addStates(@NonNull Collection<WorkflowStateEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> addStates(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeStates(WorkflowStateEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowStateEntity to be set to this entity
	 */
	public void removeStates(@NotNull WorkflowStateEntity entity) {
		this.removeStates(entity, true);
	}

	/**
	 * Remove the given WorkflowStateEntity from this entity.
	 *
	 * @param entity the given WorkflowStateEntity to be removed from this entity
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeStates(@NotNull WorkflowStateEntity entity, boolean reverse) {
		if (reverse) {
			entity.unsetWorkflowVersion(false);
		}
		this.states.remove(entity);
	}

	/**
	 * Similar to {@link this#removeStates(Collection, boolean)} but
	 * default to true for reverse remove.
	 *
	 * @param entities the given collection of WorkflowStateEntity to be removed to this entity
	 */
	public void removeStates(@NotNull Collection<WorkflowStateEntity> entities) {
		this.removeStates(entities, true);
	}

	/**
	 * Remove the given collection of WorkflowStateEntity from  to this entity.
	 *
	 * @param entities the given collection of WorkflowStateEntity to be removed to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void removeStates(@NonNull Collection<WorkflowStateEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.removeStates(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#setStates(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of WorkflowStateEntity to be set to this entity
	 */
	public void setStates(@NotNull Collection<WorkflowStateEntity> entities) {
		setStates(entities, true);
	}

	/**
	 * Replace the current entities in States with the given ones.
	 *
	 * @param entities the given collection of WorkflowStateEntity to replace the old ones
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setStates(@NotNull Collection<WorkflowStateEntity> entities, boolean reverseAdd) {

		this.unsetStates();
		this.states = new HashSet<>(entities);
		if (reverseAdd) {
			this.states.forEach(statesEntity -> statesEntity.setWorkflowVersion(this, false));
		}
	}

	/**
	 * Similar to {@link this#unsetStates(boolean)} but
	 * default to true for reverse unset
	 */
	public void unsetStates() {
		this.unsetStates(true);
	}

	/**
	 * Remove all the entities in States from this entity.
	 * @param doReverse whether this entity should be removed from the given entities
	 */
	public void unsetStates(boolean doReverse) {
		if (doReverse) {
			this.states.forEach(statesEntity -> statesEntity.unsetWorkflowVersion(false));
		}
		this.states.clear();
	}

	/**
	 * Similar to {@link this#setCurrentWorkflow(WorkflowEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowVersionEntity to be set to this entity
	 */
	public void setCurrentWorkflow(@NotNull WorkflowEntity entity) {
		setCurrentWorkflow(entity, true);
	}

	/**
	 * Set or update currentWorkflow with the given WorkflowEntity.
	 *
	 * @param entity the WorkflowVersionEntity to be set or updated
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setCurrentWorkflow(@NotNull WorkflowEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setCurrentWorkflow here] off begin
		// % protected region % [Add any additional logic here before the main logic for setCurrentWorkflow here] end

		if (sameAsFormer(this.currentWorkflow, entity)) {
			return;
		}

		if (this.currentWorkflow != null) {
			this.currentWorkflow.unsetCurrentVersion();
		}

		this.currentWorkflow = entity;

		if (reverseAdd) {
			this.currentWorkflow.setCurrentVersion(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setCurrentWorkflow here] off begin
		// % protected region % [Add any additional logic here after the main logic for setCurrentWorkflow here] end
	}

	/**
	 * Similar to {@link this#unsetCurrentWorkflow(boolean)} but default to true.
	 */
	public void unsetCurrentWorkflow() {
		this.unsetCurrentWorkflow(true);
	}

	/**
	 * Remove the WorkflowEntity in Current Workflow from this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetCurrentWorkflow(boolean reverse) {
		if (reverse && this.currentWorkflow != null) {
			this.currentWorkflow.unsetCurrentVersion(false);
		}
		this.currentWorkflow = null;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Similar to {@link this#setWorkflow(WorkflowEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given WorkflowEntity to be set to this entity
	 */
	public void setWorkflow(@NotNull WorkflowEntity entity) {
		setWorkflow(entity, true);
	}

	/**
	 * Set or update the workflow in this entity with single WorkflowEntity.
	 *
	 * @param entity the given WorkflowEntity to be set or updated to workflow
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setWorkflow(@NotNull WorkflowEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setWorkflow here] off begin
		// % protected region % [Add any additional logic here before the main logic for setWorkflow here] end

		if (sameAsFormer(this.workflow, entity)) {
			return;
		}

		if (this.workflow != null) {
			this.workflow.removeVersions(this, false);
		}
		this.workflow = entity;
		if (reverseAdd) {
			this.workflow.addVersions(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setWorkflow here] off begin
		// % protected region % [Add any additional logic here after the main logic for setWorkflow here] end
	}

	/**
	 * Similar to {@link this#unsetWorkflow(boolean)} but default to true.
	 */
	public void unsetWorkflow() {
		this.unsetWorkflow(true);
	}

	/**
	 * Remove Workflow in this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetWorkflow(boolean reverse) {
		if (reverse && this.workflow != null) {
			this.workflow.removeVersions(this, false);
		}
		this.workflow = null;
	}

	/**
	 * The CSV annotations used to generate and import CSV files require headers on the abstract entity attributes
	 * (id, created, modified, created by, modified by).  These should not be present in imports, as this information is
	 * added by the server when saving entities.  Therefore a string containing the example csv headers is required
	 *
	 * @return the headers recommended in the CSV import file in CSV format
	 */
	public static String getExampleCsvHeader() {

		// % protected region % [Modify the headers in the CSV file here] off begin
		return "WORKFLOW_NAME,WORKFLOW_DESCRIPTION,VERSION_NUMBER,ARTICLE_ASSOCIATION,WORKFLOW_ID,STATES_IDS,CURRENT_WORKFLOW_ID,ID";
		// % protected region % [Modify the headers in the CSV file here] end
	}

	public void addRelationEntitiesToIdSet() {
		// % protected region % [Add any additional logic for entity relations here] off begin
		Optional<WorkflowEntity> workflowRelation = Optional.ofNullable(this.workflow);
		workflowRelation.ifPresent(entity -> this.workflowId = entity.getId());

		for (WorkflowStateEntity states: this.states) {
			this.statesIds.add(states.getId());
		}

		Optional<WorkflowEntity> currentWorkflowRelation = Optional.ofNullable(this.currentWorkflow);
		currentWorkflowRelation.ifPresent(entity -> this.currentWorkflowId = entity.getId());

		// % protected region % [Add any additional logic for entity relations here] end
	}
	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
