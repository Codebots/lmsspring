/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {FormGroup, Validators} from '@angular/forms';
import {Group, AbstractModel, ModelProperty, ModelPropertyType, ModelRelation, ModelRelationType} from '../../lib/models/abstract.model';
import {WorkflowModel} from '../workflow/workflow.model';
import {WorkflowStateModel} from '../workflowState/workflow_state.model';
import * as _ from 'lodash';
import {QueryOperation, Where} from '../../lib/services/http/interfaces';
import {ElementType} from '../../lib/components/abstract.input.component';
import { CustomValidators } from 'src/app/lib/utils/validators/custom-validators';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * POJO model class used to store information related to the entity.
 */
export class WorkflowVersionModel extends AbstractModel {
	/**
	 * The fields which are set as searchable in the entity model
	 * The fields could be used in search in the server side
	 * The fields would be by default used as search in the crud tile.
	 * You could also use this in other tiles for searching.
	 */
	static searchFields: string[] = [
		// % protected region % [Add any additional searchable field names here] off begin
		// % protected region % [Add any additional searchable field names here] end
	];

	/**
	 * Attributes to be shown in value to display
	 */
	static displayAttributes: string[] = [
		// % protected region % [Change displayAttributes here if needed] off begin
		'workflowName',
		// % protected region % [Change displayAttributes here if needed] end
	];

	static modelPropGroups: { [s: string]: Group } = {
		// % protected region % [Add groups for the entity here] off begin
		// % protected region % [Add groups for the entity here] end
	};

	readonly className = 'WorkflowVersionModel';

	/**
	 * Default value to be displayed in dropdown etc
	 */
	get valueToDisplay(): string {
		// % protected region % [Change displayName here if needed] off begin
		return WorkflowVersionModel.displayAttributes.map((attr) => this[attr]).join(' ');
		// % protected region % [Change displayName here if needed] end
	}

	/**
	 * Workflow Name.
	 */
	workflowName: string;

	/**
	 * Description of Workflow.
	 */
	workflowDescription: string;

	/**
	 * Version Number of Workflow Version.
	 */
	versionNumber: number;

	/**
	 * If Article's are associated with this workflow version.
	 */
	articleAssociation: boolean = false;

	startState: WorkflowStateModel;

	states: WorkflowStateModel[];

	currentWorkflowId: string;

	workflowId: string;

	statesIds: string[] = [];

	modelPropGroups: { [s: string]: Group } = WorkflowVersionModel.modelPropGroups;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	static getProps(): ModelProperty[] {
		return super.getProps().concat([
			{
				name: 'workflowName',
				// % protected region % [Set displayName for Workflow Name here] off begin
				displayName: 'Workflow Name',
				// % protected region % [Set displayName for Workflow Name here] end
				type: ModelPropertyType.STRING,
				// % protected region % [Set display element type for Workflow Name here] off begin
				elementType: ElementType.INPUT,
				// % protected region % [Set display element type for Workflow Name here] end
				// % protected region % [Set isSensitive for Workflow Name here] off begin
				isSensitive: false,
				// % protected region % [Set isSensitive for Workflow Name here] end
				// % protected region % [Set readonly for Workflow Name here] off begin
				readOnly: false,
				// % protected region % [Set readonly for Workflow Name here] end
				validators: [
					Validators.required,
					// % protected region % [Add other validators for Workflow Name here] off begin
					// % protected region % [Add other validators for Workflow Name here] end
				],
				// % protected region % [Add any additional model attribute properties for Workflow Name here] off begin
				// % protected region % [Add any additional model attribute properties for Workflow Name here] end
			},
			{
				name: 'workflowDescription',
				// % protected region % [Set displayName for Workflow Description here] off begin
				displayName: 'Workflow Description',
				// % protected region % [Set displayName for Workflow Description here] end
				type: ModelPropertyType.STRING,
				// % protected region % [Set display element type for Workflow Description here] off begin
				elementType: ElementType.INPUT,
				// % protected region % [Set display element type for Workflow Description here] end
				// % protected region % [Set isSensitive for Workflow Description here] off begin
				isSensitive: false,
				// % protected region % [Set isSensitive for Workflow Description here] end
				// % protected region % [Set readonly for Workflow Description here] off begin
				readOnly: false,
				// % protected region % [Set readonly for Workflow Description here] end
				validators: [
					// % protected region % [Add other validators for Workflow Description here] off begin
					// % protected region % [Add other validators for Workflow Description here] end
				],
				// % protected region % [Add any additional model attribute properties for Workflow Description here] off begin
				// % protected region % [Add any additional model attribute properties for Workflow Description here] end
			},
			{
				name: 'versionNumber',
				// % protected region % [Set displayName for Version Number here] off begin
				displayName: 'Version Number',
				// % protected region % [Set displayName for Version Number here] end
				type: ModelPropertyType.NUMBER,
				// % protected region % [Set display element type for Version Number here] off begin
				elementType: ElementType.NUMBER,
				// % protected region % [Set display element type for Version Number here] end
				// % protected region % [Set isSensitive for Version Number here] off begin
				isSensitive: false,
				// % protected region % [Set isSensitive for Version Number here] end
				// % protected region % [Set readonly for Version Number here] off begin
				readOnly: false,
				// % protected region % [Set readonly for Version Number here] end
				validators: [
					// % protected region % [Add other validators for Version Number here] off begin
					// % protected region % [Add other validators for Version Number here] end
				],
				// % protected region % [Add any additional model attribute properties for Version Number here] off begin
				// % protected region % [Add any additional model attribute properties for Version Number here] end
			},
			{
				name: 'articleAssociation',
				// % protected region % [Set displayName for Article Association here] off begin
				displayName: 'Article Association',
				// % protected region % [Set displayName for Article Association here] end
				type: ModelPropertyType.BOOLEAN,
				// % protected region % [Set display element type for Article Association here] off begin
				elementType: ElementType.CHECKBOX,
				// % protected region % [Set display element type for Article Association here] end
				// % protected region % [Set isSensitive for Article Association here] off begin
				isSensitive: false,
				// % protected region % [Set isSensitive for Article Association here] end
				// % protected region % [Set readonly for Article Association here] off begin
				readOnly: false,
				// % protected region % [Set readonly for Article Association here] end
				validators: [
					// % protected region % [Add other validators for Article Association here] off begin
					// % protected region % [Add other validators for Article Association here] end
				],
				// % protected region % [Add any additional model attribute properties for Article Association here] off begin
				// % protected region % [Add any additional model attribute properties for Article Association here] end
			},
			// % protected region % [Add any additional class field names here] off begin
			// % protected region % [Add any additional class field names here] end
		]);
	}

	/**
	 * The relations of the entity
	 */
	static getRelations(): { [name: string]: ModelRelation } {
		return {
			...super.getRelations(),
			states: {
				type: ModelRelationType.MANY,
				name: 'statesIds',
				// % protected region % [Customise your 1-1 or 1-M label for States here] off begin
				label: 'States',
				// % protected region % [Customise your 1-1 or 1-M label for States here] end
				// % protected region % [Customise your display name for States here] off begin
				displayName: 'displayIndex',
				// % protected region % [Customise your display name for States here] end
				validators: [
					// % protected region % [Add other validators for States here] off begin
					// % protected region % [Add other validators for States here] end
				],
				// % protected region % [Add any additional field for relation States here] off begin
				// % protected region % [Add any additional field for relation States here] end
			},
			currentWorkflow: {
				type: ModelRelationType.ONE,
				name: 'currentWorkflowId',
				// % protected region % [Customise your 1-1 or 1-M label for Current Workflow here] off begin
				label: 'Current Workflow',
				// % protected region % [Customise your 1-1 or 1-M label for Current Workflow here] end
				// % protected region % [Customise your display name for Current Workflow here] off begin
				displayName: 'name',
				// % protected region % [Customise your display name for Current Workflow here] end
				validators: [
					// % protected region % [Add other validators for Current Workflow here] off begin
					// % protected region % [Add other validators for Current Workflow here] end
				],
				// % protected region % [Add any additional field for relation Current Workflow here] off begin
				// % protected region % [Add any additional field for relation Current Workflow here] end
			},
			workflow: {
				type: ModelRelationType.ONE,
				name: 'workflowId',
				// % protected region % [Customise your label for Workflow here] off begin
				label: 'Workflow',
				// % protected region % [Customise your label for Workflow here] end
				// % protected region % [Customise your display name for Workflow here] off begin
				// TODO change implementation to use OrderBy or create new metamodel property DisplayBy
				displayName: 'name',
				// % protected region % [Customise your display name for Workflow here] end
				validators: [
					Validators.required
					// % protected region % [Add other validators for Workflow here] off begin
					// % protected region % [Add other validators for Workflow here] end
				],
				// % protected region % [Add any additional field for relation Workflow here] off begin
				// % protected region % [Add any additional field for relation Workflow here] end
			},
		};
	}

	/**
	 * Convert the form group to the query conditions
	 */
	static convertFilterToCondition(formGroup: FormGroup): Where[][] {
		let conditions: Where[][] = [];

		// % protected region % [Overide the default convertFilterToCondition here] off begin
		Object.keys(formGroup.value).forEach((key) => {
			switch (key) {
				case 'created':
					const created = formGroup.value[key];
					// is the range of date
					if (created instanceof Array) {
						conditions.push([
							{
								path: key,
								operation: QueryOperation.GREATER_THAN_OR_EQUAL,
								value: created[0]
							}
						]);
						conditions.push([
							{
								path: key,
								operation: QueryOperation.LESS_THAN_OR_EQUAL,
								value: created[1]
							}
						]);
					}
			}
		});
		// % protected region % [Overide the default convertFilterToCondition here] end


		return conditions;
	}

	/**
	 * Convert a nested JSON object into an array of flatten objects.
	 */
	static deepParse(data: string | { [K in keyof WorkflowVersionModel]?: WorkflowVersionModel[K] }, currentModel?): AbstractModel[] {
		if (currentModel == null) {
			currentModel = new WorkflowVersionModel(data);
		}

		let returned: AbstractModel[] = [currentModel];
		const json = typeof data === 'string' ? JSON.parse(data) : data;

		// Outgoing one to one
		if (json.currentWorkflow) {
			currentModel.currentWorkflowId = json.currentWorkflow.id;
			returned = _.union(returned, WorkflowModel.deepParse(json.currentWorkflow));
		}

		// Incoming one to many
		if (json.workflow) {
			currentModel.workflowId = json.workflow.id;
			returned = _.union(returned, WorkflowModel.deepParse(json.workflow));
		}

		// Outgoing one to many
		if (json.states) {
			currentModel.statesIds = json.states.map(model => model.id);
			returned = _.union(returned, _.flatten(json.states.map(model => WorkflowStateModel.deepParse(model))));
		}

		// % protected region % [Customise your deep parse before return here] off begin
		// % protected region % [Customise your deep parse before return here] end

		return returned;
	}

	/**
	 * @example
	 *
	 * `let workflowVersionModel = new WorkflowVersionModel(data);`
	 *
	 * @param data The input data to be initialised as the WorkflowVersionModel,
	 *    it is expected as a JSON string or as a nullable WorkflowVersionModel.
	 */
	constructor(data?: string | Partial<WorkflowVersionModel>) {
		super(data);

		if (data) {
			const json = typeof data === 'string'
				? JSON.parse(data) as Partial<WorkflowVersionModel>
				: data;

			this.workflowName = json.workflowName;
			this.workflowDescription = json.workflowDescription;
			this.versionNumber = json.versionNumber;
			this.articleAssociation = json.articleAssociation;
			this.currentWorkflowId = json.currentWorkflowId;
			this.workflowId = json.workflowId;
			this.statesIds = json.statesIds;
			this.startState = json.startState;
			this.states = json.states;
			// % protected region % [Add any additional logic here after set the data] off begin
			// % protected region % [Add any additional logic here after set the data] end
		}

		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}

	toJSON() {
		return {
			...super.toJSON(),
			workflowName: this.workflowName,
			workflowDescription: this.workflowDescription,
			versionNumber: this.versionNumber,
			articleAssociation: this.articleAssociation,
			currentWorkflowId: this.currentWorkflowId,
			workflowId: this.workflowId,
			statesIds: this.statesIds,
			// % protected region % [Add any additional logic here to json] off begin
			// % protected region % [Add any additional logic here to json] end
		};
	}

	getPropDisplayNames(): { [s: string]: ModelProperty } {
		const returned = {};
		WorkflowVersionModel.getProps().map(prop => returned[prop.name] = prop);
		return returned;
	}

	applyUpdates(updates: any): WorkflowVersionModel {
		let newModelJson = this.toJSON();

		if (updates.workflowName) {
			newModelJson.workflowName = updates.workflowName;
		}

		if (updates.workflowDescription) {
			newModelJson.workflowDescription = updates.workflowDescription;
		}

		if (updates.versionNumber) {
			newModelJson.versionNumber = updates.versionNumber;
		}

		if (updates.articleAssociation) {
			newModelJson.articleAssociation = updates.articleAssociation;
		}

		if (updates.currentWorkflowId) {
			newModelJson.currentWorkflowId = updates.currentWorkflowId;
		}

		if (updates.workflowId) {
			newModelJson.workflowId = updates.workflowId;
		}

		if (updates.statesIds) {
			newModelJson.statesIds = updates.statesIds;
		}

		return new WorkflowVersionModel(newModelJson);
	}

	/**
	 * @inheritDoc
	 */
	difference(other: AbstractModel): any {
		if (!(other instanceof WorkflowVersionModel)) {
			return {};
		}

		const diff = {};

		for (const key of _.keys(this)) {
			const thisValue = this[key];
			const otherValue = other[key];

			// Handle dates differently
			if (thisValue instanceof Date) {
				let thisDate = (thisValue) ? thisValue.getTime() : null;
				let otherDate = (otherValue) ? otherValue.getTime() : null;

				if (thisDate !== otherDate) {
					diff[key] = thisValue;
				}
			} else if (thisValue !== otherValue) {
				diff[key] = thisValue;
			}
		}

		return _.omit(diff, [
			'created',
			'modified',
			'currentWorkflowId',
			'workflowIds',
			'statesIds',
			// % protected region % [Add any other fields to omit here] off begin
			// % protected region % [Add any other fields to omit here] end
		]);
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
