###
# @bot-written
#
# WARNING AND NOTICE
# Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
# Full Software Licence as accepted by you before being granted access to this source code and other materials,
# the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
# commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
# licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
# including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
# access, download, storage, and/or use of this source code.
#
# BOT WARNING
# This file is bot-written.
# Any changes out side of "protected regions" will be lost next time the bot makes any changes.
###

@botwritten
@behaviour @behaviour-forms @forms @create
Feature: Create Forms

	Background:
		Given I navigate to the login page
			And I login with username "administrator@example.com" with password "password"
			And I see the homepage
			And I navigate to the forms page in the admin section

	Scenario: Display Forms Page
		Then The forms page is correct

	Scenario Outline: Access <entityName> Form Builder
		Given I expand the <entityName> form dropdown
			And I click the new form button in the <entityName> form dropdown
			And I am on the <entityName> backend page
		When I create a valid <entityName>
			And I navigate to the forms page in the admin section
			And I expand the <entityName> form dropdown
			And I click on the valid <entityName> entity in the form dropdown
		Then I am on the <entityName> form builder page

	Examples:
		| entityName |
		| "Lesson" |

	# % protected region % [Add additional scenarios for Forms Create feature] off begin
	# % protected region % [Add additional scenarios for Forms Create feature] end
