/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.inject.modules.entities;

import lmsspring.entities.*;
import lmsspring.entities.enums.*;
import lmsspring.inject.factories.*;
import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.andreinc.mockneat.MockNeat;
import net.andreinc.mockneat.types.enums.StringType;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.UUID;

// % protected region % [Apply any additional imports here] off begin
// % protected region % [Apply any additional imports here] end

/**
 * Guice module for Lesson Form Tile used to register providers for dependency injection.
 */
@Slf4j
@AllArgsConstructor
public class LessonFormTileModule extends AbstractModule {
	// % protected region % [Apply any additional class fields here] off begin
	// % protected region % [Apply any additional class fields here] end

	@Override
	protected void configure() {
		log.trace("Configuring LessonFormTileModule");

		super.configure();

		// % protected region % [Apply any additional configure steps here] off begin
		// % protected region % [Apply any additional configure steps here] end

		log.trace("Configured LessonFormTileModule");
	}

	/**
	 * Return a factory for mass data generation.
	 */
	@Provides
	public LessonFormTileEntityFactory lessonFormTileEntityFactory(
			// % protected region % [Apply any additional injected arguments for lessonFormTileEntityFactory here] off begin
			// % protected region % [Apply any additional injected arguments for lessonFormTileEntityFactory here] end
			Injector injector
	) {
		log.trace("Creating LessonFormTileEntityFactory");

		// % protected region % [Apply any additional logic for lessonFormTileEntityFactory before the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormTileEntityFactory before the main body here] end

		LessonFormTileEntityFactory entityFactory = new LessonFormTileEntityFactory(
				// % protected region % [Apply any additional constructor arguments for LessonFormTileEntityFactory here] off begin
				// % protected region % [Apply any additional constructor arguments for LessonFormTileEntityFactory here] end
				injector
		);

		// % protected region % [Apply any additional logic for lessonFormTileEntityFactory after the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormTileEntityFactory after the main body here] end

		log.trace("Created LessonFormTileEntityFactory");

		return entityFactory;
	}

	/**
	 * Return an empty Lesson Form Tile entity with no references set.
	 */
	@Provides
	@Named("lessonFormTileEntityWithNoRef")
	public LessonFormTileEntity lessonFormTileEntityWithNoRef(
			// % protected region % [Apply any additional constructor parameters for lessonFormTileEntityWithNoRef here] off begin
			// % protected region % [Apply any additional constructor parameters for lessonFormTileEntityWithNoRef here] end
			MockNeat mock
	) {
		log.trace("Creating entity of type LessonFormTileEntity with no reference");

		// % protected region % [Apply any additional logic for lessonFormTileWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormTileWithNoRef before the main body here] end

		LessonFormTileEntity newEntity = new LessonFormTileEntity();
		newEntity.setId(UUID.randomUUID());
		newEntity.setCreated(OffsetDateTime.now());
		newEntity.setModified(OffsetDateTime.now());
		// % protected region % [Add customisation for Form Tile Name here] off begin
		String randomStringforFormTileName = mock
				.strings()
				.get();
		newEntity.setFormTileName(randomStringforFormTileName);
		// % protected region % [Add customisation for Form Tile Name here] end

		// % protected region % [Apply any additional logic for lessonFormTileWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormTileWithNoRef after the main body here] end

		log.trace("Created entity of type LessonFormTileEntity with no reference");

		return newEntity;
	}

	/**
	 * Return a collection of Lesson Form Tile entities with no reference at all.
	 */
	@Provides
	@Named("lessonFormTileEntitiesWithNoRef")
	public Collection<LessonFormTileEntity> lessonFormTileEntitiesWithNoRef(
		// % protected region % [Apply any additional constructor parameters for lessonFormTileEntitiesWithNoRef here] off begin
		// % protected region % [Apply any additional constructor parameters for lessonFormTileEntitiesWithNoRef here] end
		LessonFormTileEntityFactory lessonFormTileEntityFactory
	) {
		log.trace("Creating entities of type LessonFormTileEntity with no reference");

		// % protected region % [Apply any additional logic for lessonFormTileEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormTileEntitiesWithNoRef before the main body here] end

		Collection<LessonFormTileEntity> newEntities = lessonFormTileEntityFactory.createMultipleWithNoRef(10);

		// % protected region % [Apply any additional logic for lessonFormTileEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormTileEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type LessonFormTileEntity with no reference");

		return newEntities;
	}

	/**
	 * Return a Lesson Form Tile entity with all references set.
	 */
	@Provides
	@Named("lessonFormTileEntityWithRefs")
	public LessonFormTileEntity lessonFormTileEntityWithRefs(
			// % protected region % [Apply any additional constructor parameters for lessonFormTileEntityWithRefs here] off begin
			// % protected region % [Apply any additional constructor parameters for lessonFormTileEntityWithRefs here] end
			@Named("lessonEntityWithRefs") LessonEntity form,
			Injector injector
	) {
		log.trace("Creating entity of type LessonFormTileEntity with references");

		// % protected region % [Apply any additional logic for lessonFormTileEntityWithRefs before the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormTileEntityWithRefs before the main body here] end

		LessonFormTileEntity lessonFormTileEntity = injector.getInstance(Key.get(LessonFormTileEntity.class, Names.named("lessonFormTileEntityWithNoRef")));
		lessonFormTileEntity.setForm(form, true);

		// % protected region % [Apply any additional logic for lessonFormTileEntityWithRefs after the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormTileEntityWithRefs after the main body here] end

		log.trace("Created entity of type LessonFormTileEntity with references");

		return lessonFormTileEntity;
	}

	/**
	 * Return a collection of Lesson Form Tile entities with all references set.
	 */
	@Provides
	@Named("lessonFormTileEntitiesWithRefs")
	public Collection<LessonFormTileEntity> lessonFormTileEntitiesWithRefs(
		// % protected region % [Apply any additional constructor parameters for lessonFormTileEntitiesWithRefs here] off begin
		// % protected region % [Apply any additional constructor parameters for lessonFormTileEntitiesWithRefs here] end
		LessonFormTileEntityFactory lessonFormTileEntityFactory
	) {
		log.trace("Creating entities of type LessonFormTileEntity with references");

		// % protected region % [Apply any additional logic for lessonFormTileEntitiesWithNoRef before the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormTileEntitiesWithNoRef before the main body here] end

		Collection<LessonFormTileEntity> newEntities = lessonFormTileEntityFactory.createMultipleWithRefs(10);

		// % protected region % [Apply any additional logic for lessonFormTileEntitiesWithNoRef after the main body here] off begin
		// % protected region % [Apply any additional logic for lessonFormTileEntitiesWithNoRef after the main body here] end

		log.trace("Created entities of type LessonFormTileEntity with references");

		return newEntities;
	}

	// % protected region % [Apply any additional class methods here] off begin
	// % protected region % [Apply any additional class methods here] end
}
