/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminComponent} from './admin.component';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

const appRoutes: Routes = [
	{
		path: '',
		component: AdminComponent,
		children: [
			{
				path: '',
				pathMatch: 'full',
				loadChildren: () => import('./pages/adminHome/home.admin.page.module').then(m => m.AdminHomePageModule),
			},
			// Admin Crud tile for user and entities
			{
				path: 'users/administrator',
				loadChildren: () => import('./tiles/crud/administrator/administrator-admin-crud.module').then(m => m.AdminAdministratorCrudModule),
			},
			{
				path: 'entities/application-locale',
				loadChildren: () => import('./tiles/crud/applicationLocale/application-locale-admin-crud.module').then(m => m.AdminApplicationLocaleCrudModule),
			},
			{
				path: 'entities/article',
				loadChildren: () => import('./tiles/crud/article/article-admin-crud.module').then(m => m.AdminArticleCrudModule),
			},
			{
				path: 'entities/book',
				loadChildren: () => import('./tiles/crud/book/book-admin-crud.module').then(m => m.AdminBookCrudModule),
			},
			{
				path: 'users/core-user',
				loadChildren: () => import('./tiles/crud/coreUser/core-user-admin-crud.module').then(m => m.AdminCoreUserCrudModule),
			},
			{
				path: 'entities/course',
				loadChildren: () => import('./tiles/crud/course/course-admin-crud.module').then(m => m.AdminCourseCrudModule),
			},
			{
				path: 'entities/course-category',
				loadChildren: () => import('./tiles/crud/courseCategory/course-category-admin-crud.module').then(m => m.AdminCourseCategoryCrudModule),
			},
			{
				path: 'entities/course-lesson',
				loadChildren: () => import('./tiles/crud/courseLesson/course-lesson-admin-crud.module').then(m => m.AdminCourseLessonCrudModule),
			},
			{
				path: 'entities/lesson',
				loadChildren: () => import('./tiles/crud/lesson/lesson-admin-crud.module').then(m => m.AdminLessonCrudModule),
			},
			{
				path: 'entities/tag',
				loadChildren: () => import('./tiles/crud/tag/tag-admin-crud.module').then(m => m.AdminTagCrudModule),
			},
			{
				path: 'entities/lesson-form-submission',
				loadChildren: () => import('./tiles/crud/lessonFormSubmission/lesson-form-submission-admin-crud.module').then(m => m.AdminLessonFormSubmissionCrudModule),
			},
			{
				path: 'entities/lesson-form-version',
				loadChildren: () => import('./tiles/crud/lessonFormVersion/lesson-form-version-admin-crud.module').then(m => m.AdminLessonFormVersionCrudModule),
			},
			{
				path: 'entities/lesson-form-tile',
				loadChildren: () => import('./tiles/crud/lessonFormTile/lesson-form-tile-admin-crud.module').then(m => m.AdminLessonFormTileCrudModule),
			},

			// behviours
			{
				path: 'behaviour/workflow',
				loadChildren: () => import('./pages/workflow/workflow-admin.page.module').then(m => m.WorkflowAdminPageModule),
			},
			{
				path: 'behaviour/forms',
				loadChildren: () => import('./pages/forms/forms-admin.page.module').then(m => m.FormsAdminPageModule),
			},
		]
	}
];

@NgModule({
	declarations: [
		// % protected region % [Add any additional declarations here] off begin
		// % protected region % [Add any additional declarations here] end
	],
	imports: [
		RouterModule.forChild(appRoutes),
		// % protected region % [Add any additional imports] off begin
		// % protected region % [Add any additional imports] end
	],
	exports: [
		RouterModule,
		// % protected region % [Add any additional exports] off begin
		// % protected region % [Add any additional exports] end
	],
	// % protected region % [Add any additional module data] off begin
	// % protected region % [Add any additional module data] end
})
export class AdminRoutingModule { }
