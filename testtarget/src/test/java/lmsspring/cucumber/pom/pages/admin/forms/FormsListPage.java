/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package lmsspring.cucumber.pom.pages.admin.forms;

import com.google.inject.Inject;
import lmsspring.cucumber.pom.enums.VisibilityEnum;
import lmsspring.cucumber.pom.pages.AbstractPage;
import lmsspring.cucumber.pom.pages.admin.forms.elements.ListFormElement;
import lmsspring.cucumber.utils.WaitUtils;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.*;

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * FormListPage is a Page POM that is associated with the admin/behaviour/forms url.
 *
 */
@Slf4j
@ScenarioScoped
public class FormsListPage extends AbstractPage {

	@Getter
	@FindBy(how = How.XPATH, using = "//h2")
	private WebElement title;

	@Getter
	@FindBy(className = "accordion")
	private List<ListFormElement> listFormElements;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	@Inject
	public FormsListPage(
			@NonNull WebDriver webDriver,
			@NonNull Properties properties
	) {
		super(
				webDriver,
				properties,
				"admin/behaviour/forms"
		);

		log.trace("Initialised {}", this.getClass().getSimpleName());
	}

	public ListFormElement getFormDropdownByName(String formName) {
		return listFormElements.stream()
				.filter(form -> form.getTitleButton().getText().replaceAll(" ", "").matches(formName))
				.findFirst().orElseThrow(() -> new NoSuchElementException(String.format("Could not find form named %s", formName)));
	}

	public void waitForDropdown(ListFormElement formDropdown) throws Exception {
		WaitUtils.waitForElement(webDriver, formDropdown.getNewFormButton(), VisibilityEnum.VISIBLE);
	}
	public void waitForDropdownForm(ListFormElement formDropdown) throws Exception {
		WaitUtils.waitForElement(webDriver, formDropdown.getListForms().get(formDropdown.getListForms().size() - 1), VisibilityEnum.VISIBLE);
	}

	public boolean verifyFormListPage() {
		return this.getTitle().getText().matches("Forms") && verifyFormListElements();
	}

	private boolean verifyFormListElements() {
		return this.doesFormExistInPage("Lesson")
		;
	}

	private boolean doesFormExistInPage(String formName) {
		return listFormElements.stream()
				.anyMatch(form -> form.getTitleButton().getText().matches(formName));
	}

	// % protected region % [Add any additional methods here] off begin
	// % protected region % [Add any additional methods here] end
}
