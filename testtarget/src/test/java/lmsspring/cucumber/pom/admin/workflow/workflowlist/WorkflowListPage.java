/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.cucumber.pom.admin.workflow.workflowlist;

import com.google.inject.Inject;
import lombok.Getter;
import lmsspring.cucumber.pom.pages.AbstractPage;
import lmsspring.cucumber.pom.enums.VisibilityEnum;
import lmsspring.cucumber.utils.WaitUtils;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.*;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import java.util.*;

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * Page Object Model for Workflow List Page
 */
@Slf4j
@ScenarioScoped
public class WorkflowListPage extends AbstractPage {

	/**
	 * Xpath of the Block for edit workflow item
	 */
	private final String workflowBlockXpath = "//a[./div[contains(concat(' ', normalize-space(@class), ' '), ' workflow-item ')]]";

	@FindBy(xpath = "//h2[normalize-space()='Workflows']")
	private WebElement title;

	@FindBy(xpath = "//button[contains(@class, 'icon-help') and contains(@class, 'icon-right') and normalize-space()='Help Documentation']")
	private WebElement helpDocButton;

	@FindBy(xpath = "//section[contains(@class, 'workflow-block-items')]")
	private WebElement workflowBlocksContainer;

	@FindBy(xpath = "//a[./div[contains(@class, 'workflow-item__new')]]")
	private WebElement createButton;

	@Getter
	@FindBys(value = {@FindBy(xpath = workflowBlockXpath)})
	private List<WebElement> workflowBlocks;

	// % protected region % [Add any additional fields here] off begin
	// % protected region % [Add any additional fields here] end

	@Inject
	WorkflowListPage(
			// % protected region % [Add any additional constructor parameters here] off begin
			// % protected region % [Add any additional constructor parameters here] end
			WebDriver webDriver,
			Properties properties) {
		super(webDriver, properties, "/admin/behaviour/workflow");
		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}

	/**
	 * Navigate to the workflow list page.
	 */
	public void navigate() {
		log.debug("Navigating to the workflow list page in admin session");

		webDriver.get(this.pageUrl);

		// % protected region % [Add any additional logic for 'navigate' here] off begin
		// % protected region % [Add any additional logic for 'navigate' here] end

		log.debug("Navigated to the workflow list page in admin session");
	}

	/**
	 * Verify Dom Structure in Workflow List Page
	 */
	public void verifyWorkflowListPage() {

		log.debug("Verifying the workflow list page");

		Assert.assertTrue(this.title.isDisplayed());

		Assert.assertTrue(this.helpDocButton.isDisplayed());

		Assert.assertEquals(this.createButton.findElement(By.xpath("//h3")).getText(), "New Workflow");

		Assert.assertTrue(this.createButton.isDisplayed());

		// % protected region % [Add any additional logic for 'verifyWorkflowListPage' here] off begin
		// % protected region % [Add any additional logic for 'verifyWorkflowListPage' here] end

		log.debug("Verified the workflow list page");
	}

	/**
	 * Click Create Button in Workflow List Page
	 */
	public void clickCreateButton() {
		this.createButton.click();
		// % protected region % [Add any additional logic for 'clickCreateButton' here] off begin
		// % protected region % [Add any additional logic for 'clickCreateButton' here] end
	}

	/**
	 * Check whether any workflow item exists in workflow list page
	 */
	public boolean hasWorkflowItem() {
		waitForWorkflowItemsLoaded();
		return !workflowBlocks.isEmpty();
		// % protected region % [Add any additional logic for 'hasWorkflowItem' here] off begin
		// % protected region % [Add any additional logic for 'hasWorkflowItem' here] end
	}

	/**
	 * Get the first workflow item in the list
	 * @return Id of the found workflow
	 */
	public WebElement findWorkflowItemByName(String workflowName)  {
		waitForWorkflowItemsLoaded();

		// % protected region % [Add any additional logic for 'findWorkflowItemByName' here] off begin
		// % protected region % [Add any additional logic for 'findWorkflowItemByName' here] end

		try {
			WebElement workflowItem = workflowBlocks
					.stream()
					.filter((workflowBlock) -> {
						WebElement titleElement = workflowBlock.findElement(By.xpath(".//div['.workflow-item__heading']/h3"));
						return titleElement.getText().trim().equals(workflowName);
					})
					.findFirst()
					.orElseThrow(() -> {
						return new NoSuchElementException("Could not found workflow item");
					});
			return workflowItem;
		} catch (Exception except) {
			Assert.fail(String.format("Could not find element with name: %s in workflow list", workflowName), except);
		}

		return null;
	}

	/**
	 * Wait for page loading all workflow items
	 */
	public void waitForWorkflowItemsLoaded() {
		try {
			WaitUtils.waitForElement(webDriver, By.xpath(workflowBlockXpath), VisibilityEnum.VISIBLE);

			// % protected region % [Add any additional logic for 'waitForWorkflowItemsLoaded' here] off begin
			// % protected region % [Add any additional logic for 'waitForWorkflowItemsLoaded' here] end
		} catch (Exception except) {
			log.debug("No workflow item exists in the list");
		}
	}

	/**
	 * Find workflow item block from workflow list page by the id
	 */
	public WebElement findWorkflowItemById(String workflowId, WebDriverWait webDriverWait) {
		waitForWorkflowItemsLoaded();
		webDriverWait.until(x -> workflowBlocks.stream().anyMatch(element -> element.getAttribute("data-id").equals(workflowId)));
		WebElement workflowBlock =  workflowBlocks.stream()
				.filter(element -> element.getAttribute("data-id").equals(workflowId))
				.findFirst()
				.orElseThrow(() -> new NotFoundException(String.format("Could not find workflow item with expected id %s", workflowId)));
		// % protected region % [Add any additional logic for 'findWorkflowItemById' here] off begin
		// % protected region % [Add any additional logic for 'findWorkflowItemById' here] end

		return workflowBlock;
	}

	/**
	 * Get the first workflow item in the list
	 * @return Id of the found workflow
	 */
	public WebElement getFirstWorkflowItem() {
		waitForWorkflowItemsLoaded();
		WebElement workflowItem = workflowBlocks
				.stream()
				.findFirst()
				.orElseThrow(() -> {
					return new NotFoundException("No any workflow items exists");
				});

		// % protected region % [Add any additional logic for 'getFirstWorkflowItem' here] off begin
		// % protected region % [Add any additional logic for 'getFirstWorkflowItem' here] end

		return workflowItem;
	}

	/**
	 * Find workflow item by name and click
	 */
	public void clickWorkflowItemByName(String workflowName) {
		this.findWorkflowItemByName(workflowName).click();

		// % protected region % [Add any additional logic for 'clickWorkflowItemByName' here] off begin
		// % protected region % [Add any additional logic for 'clickWorkflowItemByName' here] end
	}

	/**
	 * Find find workflow item by id and click
	 */
	public void clickWorkflowItemById(String workflowVersionId, WebDriverWait webDriverWait) {
		this.findWorkflowItemById(workflowVersionId, webDriverWait).click();

		// % protected region % [Add any additional logic for 'clickWorkflowItemById' here] off begin
		// % protected region % [Add any additional logic for 'clickWorkflowItemById' here] end
	}

	/**
	 * Get the id of workflow from Workflow item web element
	 */
	public String getWorkflowIdFromWorkflowItemBlock(WebElement workflowItem) {
		return workflowItem.getAttribute("data-id");
	}
}
