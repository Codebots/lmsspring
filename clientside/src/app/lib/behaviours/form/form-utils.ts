/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Type} from '@angular/core';
import {FormCustomQuestionComponent} from './form-questions/form-custom-question/form-custom-question.component';
import {FormCustomQuestionConfigComponent} from './form-questions/form-custom-question/config/form-custom-question-config.component';
import {FormStatementComponent} from './form-questions/form-statement/form-statement.component';
import {FormStatementConfigComponent} from './form-questions/form-statement/config/form-statement-config.component';
import {FormTextfieldQuestionComponent} from './form-questions/form-textfield-question/form-textfield-question.component';
import {FormTextfieldQuestionConfigComponent} from './form-questions/form-textfield-question/config/form-textfield-question-config.component';
import {FormCheckboxQuestionComponent} from './form-questions/form-checkbox-question/form-checkbox-question.component';
import {FormCheckboxQuestionConfigComponent} from './form-questions/form-checkbox-question/config/form-checkbox-question-config.component';
import {FormRadioButtonsQuestionComponent} from './form-questions/form-radio-buttons-question/form-radio-buttons-question.component';
import {FormRadioButtonsQuestionConfigComponent} from './form-questions/form-radio-buttons-question/config/form-radio-buttons-question-config.component';
import {FormDateQuestionConfigComponent} from './form-questions/form-date-question/config/form-date-question-config.component';
import {FormDateQuestionComponent} from './form-questions/form-date-question/form-date-question.component';

import {FormSlideData} from './form-interfaces';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * All the question types
 */
export const allQuestionTypes: any[] = [
	{
		key: 'Statement',
		value: 'statement',
	},
	{
		key: 'Textfield',
		value: 'textfield',
	},
	{
		key: 'Checkbox',
		value: 'checkbox',
	},
	{
		key: 'Radio Buttons',
		value: 'radioButtons',
	},
	{
		key: 'Date / Date Time',
		value: 'datepicker',
	},
	// % protected region % [Add any additional questions types here] off begin
	// % protected region % [Add any additional questions types here] end
];


/**
 * Const map question type to the question component
 */
export const questionComponentMap = {
	textfield: FormTextfieldQuestionComponent,
	custom: FormCustomQuestionComponent,
	checkbox: FormCheckboxQuestionComponent,
	radioButtons: FormRadioButtonsQuestionComponent,
	datepicker: FormDateQuestionComponent,
	statement: FormStatementComponent,
	// % protected region % [Add any additional question component here] off begin
	// % protected region % [Add any additional question component here] end
};

/**
 * Const map question type to the question config component
 */
export const questionConfigComponentMap = {
	textfield: FormTextfieldQuestionConfigComponent,
	checkbox: FormCheckboxQuestionConfigComponent,
	custom: FormCustomQuestionConfigComponent,
	radioButtons: FormRadioButtonsQuestionConfigComponent,
	datepicker: FormDateQuestionConfigComponent,
	statement: FormStatementConfigComponent,
	// % protected region % [Add any additional question config component here] off begin
	// % protected region % [Add any additional question config component here] end
};

/**
 * Get Question Component from the type of question
 */
export const getQuestionType = (rawType: string): Type<any> => {
	return questionComponentMap[rawType];
};

/**
 * Get the question config component from the question type
 */
export const getQuestionConfig = (rawType: string): Type<any> => {
	return questionConfigComponentMap[rawType];
};

/**
 * Extract Submission Data json from the slide data
 * return The key-value pair json of the answer. The key is the id of question
 */
export const extractSubmissionData = (slidesData: FormSlideData[]) => {
	// % protected region % [Add any additional logic before main body of parseSubmissionData here] off begin
	// % protected region % [Add any additional logic before main body of parseSubmissionData here] end

	const submissionData = {};
	if (slidesData !== undefined) {
		slidesData.forEach(slide => {
			slide.questionsData.forEach(
				question => {
					if (question.answer) {
						submissionData[question.id] = question.answer;
					}
				}
			);
		});
	}

	// % protected region % [Add any additional logic after main body of parseSubmissionData here] off begin
	// % protected region % [Add any additional logic after main body of parseSubmissionData here] end

	return submissionData;
};

/**
 * Apply the sumission data to slides data answer
 */
export const parseSubmissionData = (submissionData: {} | string, slidesData: FormSlideData[] | string): FormSlideData[] => {

	// % protected region % [Add any additional logic before main body of parseSubmissionData here] off begin
	// % protected region % [Add any additional logic before main body of parseSubmissionData here] end

	if (typeof submissionData === 'string') {
		submissionData = JSON.parse(submissionData);
	}

	if (typeof slidesData === 'string') {
		slidesData = (JSON.parse(slidesData)) as FormSlideData[];
	}

	if (slidesData) {
		slidesData.forEach(slide => {
			slide.questionsData.forEach(
				question => {
					if (submissionData.hasOwnProperty(question.id)) {
						question.answer = submissionData[question.id];
					}
				}
			);
		});

		return slidesData;
	}

	// % protected region % [Add any additional logic after main body of parseSubmissionData here] off begin
	// % protected region % [Add any additional logic after main body of parseSubmissionData here] end
};

// % protected region % [Add any additional methods here] off begin
// % protected region % [Add any additional methods here] end
