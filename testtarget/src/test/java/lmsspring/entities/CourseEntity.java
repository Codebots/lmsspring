/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package lmsspring.entities;

import lmsspring.entities.enums.*;
import lmsspring.lib.file.models.FileEntity;
import lombok.*;
import javax.validation.constraints.NotNull;

import java.util.*;

// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
public class CourseEntity extends AbstractEntity {

	public CourseEntity() {
		initialiseReferences();
	}

	private void initialiseReferences() {


		var CourseLessonsOneMany = new EntityReference();
			CourseLessonsOneMany.entityName = "CourseLesson";
			CourseLessonsOneMany.oppositeName = "Course";
			CourseLessonsOneMany.name = "CourseLessons";
			CourseLessonsOneMany.optional = true;
			CourseLessonsOneMany.type = "One";
			CourseLessonsOneMany.oppositeType = "Many";

		References.add(CourseLessonsOneMany);

		var CourseCategoryOneMany = new EntityReference();
			CourseCategoryOneMany.entityName = "CourseCategory";
			CourseCategoryOneMany.oppositeName = "CourseCategory";
			CourseCategoryOneMany.name = "Courses";
			CourseCategoryOneMany.optional = true;
			CourseCategoryOneMany.type = "One";
			CourseCategoryOneMany.oppositeType = "Many";

		References.add(CourseCategoryOneMany);
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Name here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Name here] end
	private String name;

	// % protected region % [Modify attribute annotation for Summary here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Summary here] end
	private String summary;

	// % protected region % [Modify attribute annotation for Difficulty here] off begin
	@ToString.Include
	@Setter
	// % protected region % [Modify attribute annotation for Difficulty here] end
	private DifficultyEnum difficulty;

	private Set<FileEntity> coverImage = new HashSet<>();

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-one
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private Set<CourseLessonEntity> courseLessons = new HashSet<>();

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming one-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private CourseCategoryEntity courseCategory;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming many-to-many
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
	 * Similar to {@link this#addCourseLessons(CourseLessonEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given CourseLessonEntity to be added to this entity
	 */
	public void addCourseLessons(@NotNull CourseLessonEntity entity) {
		addCourseLessons(entity, true);
	}

	/**
	 * Add a new CourseLessonEntity to courseLessons in this entity.
	 *
	 * @param entity the given CourseLessonEntity  to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addCourseLessons(@NonNull CourseLessonEntity entity, boolean reverseAdd) {
		if (!this.courseLessons.contains(entity)) {
			courseLessons.add(entity);
			if (reverseAdd) {
				entity.setCourse(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addCourseLessons(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of CourseLessonEntity to be added to this entity
	 */
	public void addCourseLessons(@NotNull Collection<CourseLessonEntity> entities) {
		addCourseLessons(entities, true);
	}

	/**
	 * Add a new collection of CourseLessonEntity to Course Lessons in this entity.
	 *
	 * @param entities the given collection of CourseLessonEntity to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addCourseLessons(@NonNull Collection<CourseLessonEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> addCourseLessons(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeCourseLessons(CourseLessonEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given CourseLessonEntity to be set to this entity
	 */
	public void removeCourseLessons(@NotNull CourseLessonEntity entity) {
		this.removeCourseLessons(entity, true);
	}

	/**
	 * Remove the given CourseLessonEntity from this entity.
	 *
	 * @param entity the given CourseLessonEntity to be removed from this entity
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeCourseLessons(@NotNull CourseLessonEntity entity, boolean reverse) {
		if (reverse) {
			entity.unsetCourse(false);
		}
		this.courseLessons.remove(entity);
	}

	/**
	 * Similar to {@link this#removeCourseLessons(Collection, boolean)} but
	 * default to true for reverse remove.
	 *
	 * @param entities the given collection of CourseLessonEntity to be removed to this entity
	 */
	public void removeCourseLessons(@NotNull Collection<CourseLessonEntity> entities) {
		this.removeCourseLessons(entities, true);
	}

	/**
	 * Remove the given collection of CourseLessonEntity from  to this entity.
	 *
	 * @param entities the given collection of CourseLessonEntity to be removed to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void removeCourseLessons(@NonNull Collection<CourseLessonEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.removeCourseLessons(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#setCourseLessons(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of CourseLessonEntity to be set to this entity
	 */
	public void setCourseLessons(@NotNull Collection<CourseLessonEntity> entities) {
		setCourseLessons(entities, true);
	}

	/**
	 * Replace the current entities in Course Lessons with the given ones.
	 *
	 * @param entities the given collection of CourseLessonEntity to replace the old ones
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setCourseLessons(@NotNull Collection<CourseLessonEntity> entities, boolean reverseAdd) {

		this.unsetCourseLessons();
		this.courseLessons = new HashSet<>(entities);
		if (reverseAdd) {
			this.courseLessons.forEach(courseLessonsEntity -> courseLessonsEntity.setCourse(this, false));
		}
	}

	/**
	 * Similar to {@link this#unsetCourseLessons(boolean)} but
	 * default to true for reverse unset
	 */
	public void unsetCourseLessons() {
		this.unsetCourseLessons(true);
	}

	/**
	 * Remove all the entities in Course Lessons from this entity.
	 * @param doReverse whether this entity should be removed from the given entities
	 */
	public void unsetCourseLessons(boolean doReverse) {
		if (doReverse) {
			this.courseLessons.forEach(courseLessonsEntity -> courseLessonsEntity.unsetCourse(false));
		}
		this.courseLessons.clear();
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Similar to {@link this#setCourseCategory(CourseCategoryEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given CourseCategoryEntity to be set to this entity
	 */
	public void setCourseCategory(@NotNull CourseCategoryEntity entity) {
		setCourseCategory(entity, true);
	}

	/**
	 * Set or update the courseCategory in this entity with single CourseCategoryEntity.
	 *
	 * @param entity the given CourseCategoryEntity to be set or updated to courseCategory
	 * @param reverseAdd whether this entity should be set or updated to the given entity
	 */
	public void setCourseCategory(@NotNull CourseCategoryEntity entity, boolean reverseAdd) {
		// % protected region % [Add any additional logic here before the main logic for setCourseCategory here] off begin
		// % protected region % [Add any additional logic here before the main logic for setCourseCategory here] end

		if (sameAsFormer(this.courseCategory, entity)) {
			return;
		}

		if (this.courseCategory != null) {
			this.courseCategory.removeCourses(this, false);
		}
		this.courseCategory = entity;
		if (reverseAdd) {
			this.courseCategory.addCourses(this, false);
		}

		// % protected region % [Add any additional logic here after the main logic for setCourseCategory here] off begin
		// % protected region % [Add any additional logic here after the main logic for setCourseCategory here] end
	}

	/**
	 * Similar to {@link this#unsetCourseCategory(boolean)} but default to true.
	 */
	public void unsetCourseCategory() {
		this.unsetCourseCategory(true);
	}

	/**
	 * Remove Course Category in this entity.
	 *
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void unsetCourseCategory(boolean reverse) {
		if (reverse && this.courseCategory != null) {
			this.courseCategory.removeCourses(this, false);
		}
		this.courseCategory = null;
	}

	public void addCoverImage(FileEntity newFile) {
		coverImage.add(newFile);
	}

	public void addAllCoverImage(Collection<FileEntity> newFiles) {
		coverImage.addAll(newFiles);
	}

	public void removeCoverImage(FileEntity newFile) {
		coverImage.remove(newFile);
	}

	public boolean containsCoverImage(FileEntity newFile) {
		return coverImage.contains(newFile);
	}

	public void clearAllCoverImage() {
		coverImage.clear();
	}

	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
