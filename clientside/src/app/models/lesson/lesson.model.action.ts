/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Action} from '@ngrx/store';
import {LessonModel} from './lesson.model';
import {LessonModelAudit} from './lesson.model.state';
import {PassableStateConfig} from '../../lib/services/http/interfaces';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * List of Lesson model actions to be dispatched by NgRx.
 */
export enum LessonModelActionTypes {
	CREATE_LESSON = '[ENTITY] Create LessonModel',
	CREATE_LESSON_OK = '[ENTITY] Create LessonModel successfully',
	CREATE_LESSON_FAIL = '[ENTITY] Create LessonModel failed',

	CREATE_ALL_LESSON = '[ENTITY] Create All LessonModel',
	CREATE_ALL_LESSON_OK = '[ENTITY] Create All LessonModel successfully',
	CREATE_ALL_LESSON_FAIL = '[ENTITY] Create All LessonModel failed',

	DELETE_LESSON = '[ENTITY] Delete LessonModel',
	DELETE_LESSON_OK = '[ENTITY] Delete LessonModel successfully',
	DELETE_LESSON_FAIL = '[ENTITY] Delete LessonModel failed',


	DELETE_LESSON_EXCLUDING_IDS = '[ENTITY] Delete LessonModels Excluding Ids',
	DELETE_LESSON_EXCLUDING_IDS_OK = '[ENTITY] Delete LessonModels Excluding Ids successfully',
	DELETE_LESSON_EXCLUDING_IDS_FAIL = '[ENTITY] Delete LessonModels Excluding Ids failed',

	DELETE_ALL_LESSON = '[ENTITY] Delete all LessonModels',
	DELETE_ALL_LESSON_OK = '[ENTITY] Delete all LessonModels successfully',
	DELETE_ALL_LESSON_FAIL = '[ENTITY] Delete all LessonModels failed',

	UPDATE_LESSON = '[ENTITY] Update LessonModel',
	UPDATE_LESSON_OK = '[ENTITY] Update LessonModel successfully',
	UPDATE_LESSON_FAIL = '[ENTITY] Update LessonModel failed',

	UPDATE_ALL_LESSON = '[ENTITY] Update all LessonModel',
	UPDATE_ALL_LESSON_OK = '[ENTITY] Update all LessonModel successfully',
	UPDATE_ALL_LESSON_FAIL = '[ENTITY] Update all LessonModel failed',

	FETCH_LESSON= '[ENTITY] Fetch LessonModel',
	FETCH_LESSON_OK = '[ENTITY] Fetch LessonModel successfully',
	FETCH_LESSON_FAIL = '[ENTITY] Fetch LessonModel failed',

	FETCH_LESSON_AUDIT= '[ENTITY] Fetch LessonModel audit',
	FETCH_LESSON_AUDIT_OK = '[ENTITY] Fetch LessonModel audit successfully',
	FETCH_LESSON_AUDIT_FAIL = '[ENTITY] Fetch LessonModel audit failed',

	FETCH_LESSON_AUDITS_BY_ENTITY_ID= '[ENTITY] Fetch LessonModel audits by entity id',
	FETCH_LESSON_AUDITS_BY_ENTITY_ID_OK = '[ENTITY] Fetch LessonModel audits by entity id successfully',
	FETCH_LESSON_AUDITS_BY_ENTITY_ID_FAIL = '[ENTITY] Fetch LessonModel audits by entity id failed',

	FETCH_ALL_LESSON = '[ENTITY] Fetch all LessonModel',
	FETCH_ALL_LESSON_OK = '[ENTITY] Fetch all LessonModel successfully',
	FETCH_ALL_LESSON_FAIL = '[ENTITY] Fetch all LessonModel failed',

	FETCH_LESSON_WITH_QUERY = '[ENTITY] Fetch LessonModel with query',
	FETCH_LESSON_WITH_QUERY_OK = '[ENTITY] Fetch LessonModel with query successfully',
	FETCH_LESSON_WITH_QUERY_FAIL = '[ENTITY] Fetch LessonModel with query failed',

	FETCH_LAST_LESSON_WITH_QUERY = '[ENTITY] Fetch last LessonModel with query',
	FETCH_LAST_LESSON_WITH_QUERY_OK = '[ENTITY] Fetch last LessonModel with query successfully',
	FETCH_LAST_LESSON_WITH_QUERY_FAIL = '[ENTITY] Fetch last LessonModel with query failed',

	EXPORT_LESSON = '[ENTITY] Export LessonModel',
	EXPORT_LESSON_OK = '[ENTITY] Export LessonModel successfully',
	EXPORT_LESSON_FAIL = '[ENTITY] Export LessonModel failed',

	EXPORT_ALL_LESSON = '[ENTITY] Export All LessonModels',
	EXPORT_ALL_LESSON_OK = '[ENTITY] Export All LessonModels successfully',
	EXPORT_ALL_LESSON_FAIL = '[ENTITY] Export All LessonModels failed',

	EXPORT_LESSON_EXCLUDING_IDS = '[ENTITY] Export LessonModels excluding Ids',
	EXPORT_LESSON_EXCLUDING_IDS_OK = '[ENTITY] Export LessonModel excluding Ids successfully',
	EXPORT_LESSON_EXCLUDING_IDS_FAIL = '[ENTITY] Export LessonModel excluding Ids failed',

	COUNT_LESSONS = '[ENTITY] Fetch number of LessonModel records',
	COUNT_LESSONS_OK = '[ENTITY] Fetch number of LessonModel records successfully ',
	COUNT_LESSONS_FAIL = '[ENTITY] Fetch number of LessonModel records failed',

	IMPORT_LESSONS = '[ENTITY] Import LessonModels',
	IMPORT_LESSONS_OK = '[ENTITY] Import LessonModels successfully',
	IMPORT_LESSONS_FAIL = '[ENTITY] Import LessonModels fail',


	INITIALISE_LESSON_COLLECTION_STATE = '[ENTITY] Initialize the CollectionState of LessonModel',
	// % protected region % [Add any additional model actions here] off begin
	// % protected region % [Add any additional model actions here] end
}

export abstract class BaseLessonAction implements Action {
	readonly className: string = 'LessonModel';

	abstract readonly type: string;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	protected constructor(
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
		public readonly afterwardActions: Action[] = []
	) {
		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}
}

export class LessonAction extends BaseLessonAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for LessonAction here] off begin
	// % protected region % [Add any additional class fields for LessonAction here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<LessonModel>,
		// % protected region % [Add any additional constructor parameters for LessonAction here] off begin
		// % protected region % [Add any additional constructor parameters for LessonAction here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for LessonAction here] off begin
			// % protected region % [Add any additional constructor arguments for LessonAction here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for LessonAction here] off begin
		// % protected region % [Add any additional constructor logic for LessonAction here] end
	}

	// % protected region % [Add any additional class methods for LessonAction here] off begin
	// % protected region % [Add any additional class methods for LessonAction here] end
}

export class LessonActionOK extends BaseLessonAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for LessonActionOK here] off begin
	// % protected region % [Add any additional class fields for LessonActionOK here] end

	public constructor(
		public typeInput: string,
		public readonly stateConfig: PassableStateConfig<LessonModel>,
		// % protected region % [Add any additional constructor parameters for LessonActionOK here] off begin
		// % protected region % [Add any additional constructor parameters for LessonActionOK here] end
		afterwardActions: Action[] = [],
		public audits?: LessonModelAudit[],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for LessonActionOK here] off begin
			// % protected region % [Add any additional constructor arguments for LessonActionOK here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for LessonActionOK here] off begin
		// % protected region % [Add any additional constructor logic for LessonActionOK here] end
	}

	// % protected region % [Add any additional class methods for LessonActionOK here] off begin
	// % protected region % [Add any additional class methods for LessonActionOK here] end
}

export class LessonActionFail extends BaseLessonAction {

	readonly type: string;

	// % protected region % [Add any additional class fields for LessonActionFail here] off begin
	// % protected region % [Add any additional class fields for LessonActionFail here] end

	public constructor(
		public typeInput: string,
		public readonly error?: any,
		public stateConfig?: PassableStateConfig<LessonModel>,
		// % protected region % [Add any additional constructor parameters for LessonActionFail here] off begin
		// % protected region % [Add any additional constructor parameters for LessonActionFail here] end
		afterwardActions: Action[] = [],
	) {
		super(
			// % protected region % [Add any additional constructor arguments for LessonActionFail here] off begin
			// % protected region % [Add any additional constructor arguments for LessonActionFail here] end
			afterwardActions
		);
		this.type = typeInput;
		// % protected region % [Add any additional constructor logic for LessonActionFail here] off begin
		// % protected region % [Add any additional constructor logic for LessonActionFail here] end
	}

	// % protected region % [Add any additional class methods for LessonActionFail here] off begin
	// % protected region % [Add any additional class methods for LessonActionFail here] end
}

export function isLessonModelAction(e: any): e is BaseLessonAction {
	return Object.values(LessonModelActionTypes).includes(e);
}

// % protected region % [Add any additional actions here] off begin
// % protected region % [Add any additional actions here] end
