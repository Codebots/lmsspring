/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormQuestionData} from '../../form-interfaces';
import {ButtonStyle, ButtonSize, IconPosition} from '../../../../components/button/button.component';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * Fragment component to be displayed after the main content of the question component. Mainly used to centralise all
 * post-question logic into one place.
 */
@Component({
	selector: '*[cb-form-post-question]',
	template: `
	<div cb-button-group>
		<button cb-button
						aria-label="New Text"
						[iconName]="'edit'"
						[iconPos]="IconPosition.LEFT"
						[buttonStyle]="ButtonStyle.OUTLINE"
						[buttonSize]="ButtonSize.NONE"
						(click)="onEditQuestion()">Options
		</button>
		<div cb-context-menu>
			<ul>
				<li>
					<button cb-button (click)="onDuplicateQuestion()" [buttonStyle]="ButtonStyle.TEXT">Duplicate</button>
				</li>
				<li>
					<button cb-button (click)="onDeleteQuestion()" [buttonStyle]="ButtonStyle.TEXT">Delete</button>
				</li>
				<li>
					<button cb-button (click)="onMoveUpQuestion()" [buttonStyle]="ButtonStyle.TEXT">Move Question Up</button>
				</li>
				<li>
					<button cb-button (click)="onMoveDownQuestion()" [buttonStyle]="ButtonStyle.TEXT">Move Question Down</button>
				</li>
			</ul>
		</div>
	</div>
	`
})
export class FormPostCustomQuestionComponent {
	readonly ButtonStyle = ButtonStyle;
	readonly ButtonSize = ButtonSize;
	readonly IconPosition = IconPosition;

	@Input()
	data: FormQuestionData;

	/**
	 * Event emitter for question edit requests.
	 */
	@Output('edit')
	editEmitter = new EventEmitter();

	/**
	 * Event emitter for question duplicate requests.
	 */
	@Output('duplicate')
	duplicateEmitter = new EventEmitter();

	/**
	 * Event emitter for question duplicate requests.
	 */
	@Output('delete')
	deleteEmitter = new EventEmitter();

	/**
	 * Event emitter to change order of question
	 */
	@Output('changeOrder')
	changeOrderEmitter = new EventEmitter<number>();

	// % protected region % [Add any additional class properties here] off begin
	// % protected region % [Add any additional class properties here] end

	/**
	 * Triggered when the user wants to edit the current question.
	 */
	onEditQuestion() {
		// % protected region % [Add any additional logic for onEditQuestion before the main body here] off begin
		// % protected region % [Add any additional logic for onEditQuestion before the main body here] end

		this.editEmitter.emit(null);

		// % protected region % [Add any additional logic for onEditQuestion after the main body here] off begin
		// % protected region % [Add any additional logic for onEditQuestion after the main body here] end
	}

	/**
	 * Triggered when the user wants to duplicate the current question.
	 */
	onDuplicateQuestion() {
		// % protected region % [Add any additional logic for onDuplicateQuestion before the main body here] off begin
		// % protected region % [Add any additional logic for onDuplicateQuestion before the main body here] end

		this.duplicateEmitter.emit(null);

		// % protected region % [Add any additional logic for onDuplicateQuestion after the main body here] off begin
		// % protected region % [Add any additional logic for onDuplicateQuestion after the main body here] end
	}

	/**
	 * Triggered when user wants to delete the current question
	 */
	onDeleteQuestion() {
		this.deleteEmitter.emit(null);
	}

	/**
	 * Triggered when user move the question up
	 */
	onMoveUpQuestion() {
		this.changeOrderEmitter.emit(-1);
	}

	/**
	 * Triggered when user move the question down
	 */
	onMoveDownQuestion() {
		this.changeOrderEmitter.emit(1);
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
