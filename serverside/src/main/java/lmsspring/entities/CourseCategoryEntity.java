/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package lmsspring.entities;

import lmsspring.dtos.CourseCategoryEntityDto;
import lmsspring.entities.listeners.CourseCategoryEntityListener;
import lmsspring.serializers.CourseCategorySerializer;
import lmsspring.lib.validators.ValidatorPatterns;
import lmsspring.services.utils.converters.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.opencsv.bean.CsvIgnore;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;
import org.springframework.lang.Nullable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.hibernate.envers.Audited;

import java.util.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Optional;
import java.util.UUID;

// % protected region % [Import any additional imports here] off begin
// % protected region % [Import any additional imports here] end

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Entity
// % protected region % [Override the auditing annotation or add additional annotations here] off begin
@Audited
// % protected region % [Override the auditing annotation or add additional annotations here] end
@ExcludeSuperclassListeners
@EntityListeners({CourseCategoryEntityListener.class, AuditingEntityListener.class})
@JsonSerialize(using = CourseCategorySerializer.class)
@Table(
	uniqueConstraints = {
		@UniqueConstraint(columnNames = {"name"}, name = "name"),
	}
)
public class CourseCategoryEntity extends AbstractEntity {

	/**
	 * Takes a CourseCategoryEntityDto and converts it into a CourseCategoryEntity.
	 * Primarily used when endpoints are invoked, as they take DTOs as input, which need to be converted to the entity type
	 *
	 * @param courseCategoryEntityDto
	 */
	public CourseCategoryEntity(CourseCategoryEntityDto courseCategoryEntityDto) {
		// Need to account for potential that any field is empty.  This will be run before any server validation and
		// endpoints will most likely not be subject to clientside validation

		if (courseCategoryEntityDto.getId() != null) {
			this.setId(courseCategoryEntityDto.getId());
		}

		if (courseCategoryEntityDto.getName() != null) {
			this.setName(courseCategoryEntityDto.getName());
		}

		if (courseCategoryEntityDto.getColour() != null) {
			this.setColour(courseCategoryEntityDto.getColour());
		}

		if (courseCategoryEntityDto.getSummary() != null) {
			this.setSummary(courseCategoryEntityDto.getSummary());
		}

		if (courseCategoryEntityDto.getCourses() != null) {
			this.setCourses(courseCategoryEntityDto.getCourses());
		}

		// % protected region % [Add any additional DTO constructor logic here] off begin
		// % protected region % [Add any additional DTO constructor logic here] end
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Attributes
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Modify attribute annotation for Name here] off begin
	@CsvBindByName(column = "NAME", required = true)
	@NotNull(message = "Name must not be empty")
	@Column(name = "name")
	@ApiModelProperty(notes = "The Name of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Name here] end
	private String name;

	// % protected region % [Modify attribute annotation for Colour here] off begin
	@CsvBindByName(column = "COLOUR", required = false)
	@Nullable
	@Column(name = "colour")
	@ApiModelProperty(notes = "The Colour of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Colour here] end
	private String colour;

	// % protected region % [Modify attribute annotation for Summary here] off begin
	@CsvBindByName(column = "SUMMARY", required = false)
	@Nullable
	@Lob
	@Column(name = "summary")
	@ApiModelProperty(notes = "The Summary of this entity.")
	@ToString.Include
	// % protected region % [Modify attribute annotation for Summary here] end
	private String summary;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Update the annotation for Courses here] off begin
	@ApiModelProperty(notes = "The Course entities that are related to this entity.")
	@OneToMany(mappedBy = "courseCategory", cascade = {CascadeType.MERGE}, fetch = FetchType.LAZY)
	@CsvIgnore
	// % protected region % [Update the annotation for Courses here] end
	private Set<CourseEntity> courses = new HashSet<>();

	// % protected region % [Update the annotation for coursesIds here] off begin
	@Transient
	@CsvCustomBindByName(column = "COURSES_IDS", converter = UUIDHashSetConverter.class)
	// % protected region % [Update the annotation for coursesIds here] end
	private Set<UUID> coursesIds = new HashSet<>();

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Outgoing reference methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
	 * Similar to {@link this#addCourses(CourseEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given CourseEntity to be added to this entity
	 */
	public void addCourses(@NotNull CourseEntity entity) {
		addCourses(entity, true);
	}

	/**
	 * Add a new CourseEntity to courses in this entity.
	 *
	 * @param entity the given CourseEntity  to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entity
	 */
	public void addCourses(@NonNull CourseEntity entity, boolean reverseAdd) {
		if (!this.courses.contains(entity)) {
			courses.add(entity);
			if (reverseAdd) {
				entity.setCourseCategory(this, false);
			}
		}
	}

	/**
	 * Similar to {@link this#addCourses(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of CourseEntity to be added to this entity
	 */
	public void addCourses(@NotNull Collection<CourseEntity> entities) {
		addCourses(entities, true);
	}

	/**
	 * Add a new collection of CourseEntity to Courses in this entity.
	 *
	 * @param entities the given collection of CourseEntity to be added to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void addCourses(@NonNull Collection<CourseEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> addCourses(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#removeCourses(CourseEntity, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entity the given CourseEntity to be set to this entity
	 */
	public void removeCourses(@NotNull CourseEntity entity) {
		this.removeCourses(entity, true);
	}

	/**
	 * Remove the given CourseEntity from this entity.
	 *
	 * @param entity the given CourseEntity to be removed from this entity
	 * @param reverse whether this entity should be removed from the given entity
	 */
	public void removeCourses(@NotNull CourseEntity entity, boolean reverse) {
		if (reverse) {
			entity.unsetCourseCategory(false);
		}
		this.courses.remove(entity);
	}

	/**
	 * Similar to {@link this#removeCourses(Collection, boolean)} but
	 * default to true for reverse remove.
	 *
	 * @param entities the given collection of CourseEntity to be removed to this entity
	 */
	public void removeCourses(@NotNull Collection<CourseEntity> entities) {
		this.removeCourses(entities, true);
	}

	/**
	 * Remove the given collection of CourseEntity from  to this entity.
	 *
	 * @param entities the given collection of CourseEntity to be removed to this entity
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void removeCourses(@NonNull Collection<CourseEntity> entities, boolean reverseAdd) {
		entities.forEach(entity -> this.removeCourses(entity, reverseAdd));
	}

	/**
	 * Similar to {@link this#setCourses(Collection, boolean)} but
	 * default to true for reverse add.
	 *
	 * @param entities the given collection of CourseEntity to be set to this entity
	 */
	public void setCourses(@NotNull Collection<CourseEntity> entities) {
		setCourses(entities, true);
	}

	/**
	 * Replace the current entities in Courses with the given ones.
	 *
	 * @param entities the given collection of CourseEntity to replace the old ones
	 * @param reverseAdd whether this entity should be added to the given entities
	 */
	public void setCourses(@NotNull Collection<CourseEntity> entities, boolean reverseAdd) {

		this.unsetCourses();
		this.courses = new HashSet<>(entities);
		if (reverseAdd) {
			this.courses.forEach(coursesEntity -> coursesEntity.setCourseCategory(this, false));
		}
	}

	/**
	 * Similar to {@link this#unsetCourses(boolean)} but
	 * default to true for reverse unset
	 */
	public void unsetCourses() {
		this.unsetCourses(true);
	}

	/**
	 * Remove all the entities in Courses from this entity.
	 * @param doReverse whether this entity should be removed from the given entities
	 */
	public void unsetCourses(boolean doReverse) {
		if (doReverse) {
			this.courses.forEach(coursesEntity -> coursesEntity.unsetCourseCategory(false));
		}
		this.courses.clear();
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Incoming references methods
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	/**
	 * The CSV annotations used to generate and import CSV files require headers on the abstract entity attributes
	 * (id, created, modified, created by, modified by).  These should not be present in imports, as this information is
	 * added by the server when saving entities.  Therefore a string containing the example csv headers is required
	 *
	 * @return the headers recommended in the CSV import file in CSV format
	 */
	public static String getExampleCsvHeader() {

		// % protected region % [Modify the headers in the CSV file here] off begin
		return "NAME,COLOUR,SUMMARY,COURSES_IDS,ID";
		// % protected region % [Modify the headers in the CSV file here] end
	}

	public void addRelationEntitiesToIdSet() {
		// % protected region % [Add any additional logic for entity relations here] off begin
		for (CourseEntity courses: this.courses) {
			this.coursesIds.add(courses.getId());
		}

		// % protected region % [Add any additional logic for entity relations here] end
	}
	// % protected region % [Add any additional class methods  here] off begin
	// % protected region % [Add any additional class methods  here] end
}
